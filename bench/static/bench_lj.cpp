#define __STDC_FORMAT_MACROS
#include<inttypes.h>
#include<random>
#include "simulator/session.hpp"
#include "simulation_config/simulation_config.hpp"
#include "simulation_config/view.hpp"
#include "simulator/mc_moves.hpp"
#include "simulator/mc_nvt_sampler.hpp"
#include "simulator/forces.hpp"
#include "simulator/pair_calculation.hpp"

int main(int argc, char **argv) {
  using namespace mdst;
  std::mt19937 gen(42);

  // Define parameters, taken from 
  // Rahman, A. (1964). Correlations in the motion of atoms in liquid argon. Physical Review, 136(2A). 
  Attribute sigma = 3.4; // Angstrom
  Attribute epsilon = 120; // Kelvin
  Attribute temperature = 94.4;
  Attribute beta = epsilon / temperature;
  Attribute mass = 39.95 * 1.6747e-24;

  shared_ptr<simulator::Session> session(new simulator::Session());
  session->set_num_threads(8);
  shared_ptr<SimulationConfiguration::SimulationConfig> config = session->config();
  Attribute w = 10.229 * sigma;
  config->set_box(Tuple3(w, w, w));
  Tag N = 864;
  config->set_num_atoms(N);
  for (Tag i = 0; i < N; i++) {
    SimulationConfiguration::ParticleView p = config->particle(i);
    p.set_type("A");
    Attribute x = (std::generate_canonical<double, 10>(gen) - 0.5) * w;
    Attribute y = (std::generate_canonical<double, 10>(gen) - 0.5) * w;
    Attribute z = (std::generate_canonical<double, 10>(gen) - 0.5) * w;
    p.set_position(Tuple3(x, y, z));
  }
  shared_ptr<simulator::Simulator> sim(new simulator::MonteCarloNVTSampler(beta));
  session->set_simulator(sim);
  session->random_seed(int(std::generate_canonical<double, 10>(gen) * 100));
  shared_ptr<ReducedUnitsSet> ru = session->reduced_units_set();
  shared_ptr<simulator::Force> force(new simulator::LennardJonesPairforce(
    SortedTypePair("A", "A"), ru->expand_energy(1.0) /*epsilon*/, ru->expand_length(sigma), 
    1.0 /*alpha*/, ru->expand_length(2.25 * sigma) /*r_cut*/, 0 /*exclude_bonded*/
  ));
  session->potential_add(force);
  shared_ptr<simulator::MonteCarloMove> move(new simulator::RandomDisplacementMove());
  session->moves_add(move);
  session->analyzer_add("pe");
  session->analyzer_add("osmotic_pressure");
  session->analyzer_add("acceptance_ratio");
  session->run(1000);
//  printf(">>>\n%20" PRIu64 "\n%20" PRIu64 "\n", simulator::timer_equal, simulator::timer_all);
}

