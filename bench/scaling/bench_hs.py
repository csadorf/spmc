#!/usr/bin/env python
import math

def hs_benchmark(n_threads, n_particles, n_steps, density_factor, beta_factor):
    import random
    random.seed(42) # Comment this line for true randomness!
    from math import pi
    N = n_particles # 100
    packing_fraction = 0.5
    sphere_radius = 1.0
    sphere_volume = 4.0 / 3 * pi * pow(sphere_radius, 3)
    cubic_box_length = pow(N * density_factor * sphere_volume / packing_fraction, 1.0 / 3)

    import spmc
    session = spmc.Session()
    session.num_threads = n_threads
    session.config.box = spmc.Tuple3(cubic_box_length)
    import spmc.init
    spmc.init.create_random(config = session.config, N = N, p_type = 'A')
    import spmc.potentials
    potentials = spmc.potentials.Potentials(session.reduced_units)
    hs = potentials.hard_sphere_potential(
        'A', 'A', r_cut = sphere_radius)
    session.add_potential(hs)
    session.add_move(spmc.RandomDisplacementMove(1))
    session.add_analyzer('pe')
    session.add_analyzer('acceptance_ratio')
    session.simulator = spmc.MonteCarloNVTSampler(beta_factor * 1.0)
    session.random_seed(int(random.random() * 100))
    import time
    start = time.time()
    session.run(n_steps)
    return time.time() - start

output = ""

for n_particles in [100, 200, 400, 800]:
    for n_steps in [2000, 4000]:
        for n_threads in [1, 2, 4, 8, 16]:
            for density_factor in [0.5, 0.9, 1.0, 1.1, 1.5, 2.0]:
                for beta_factor in [0.1, 0.5, 0.9, 1.0, 1.1, 1.5, 2.0]:
                    t = hs_benchmark(n_threads, n_particles, n_steps, density_factor, beta_factor)
                    print "RESULT %d %d %d %f %f %f\n" % (n_particles, n_steps, n_threads, density_factor, beta_factor, t) 

