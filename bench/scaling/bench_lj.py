#!/usr/bin/env python
import math

def lj_benchmark(n_threads, n_particles, n_steps, density_factor, beta_factor):
    import random
    random.seed(42) # Comment this line for true randomness!
    
    sigma = 3.4 # Angstrom
    epsilon = 120 # Kelvin
    temperature = 94.4
    beta = epsilon / temperature * beta_factor
    mass = 39.95 * 1.6747e-24
    width = math.pow(math.pow(10.229, 3.0) / 864 * density_factor * n_particles, 1.0 / 3.0)
    
    import spmc
    session = spmc.Session()
    session.num_threads = n_threads
    session.config.box = spmc.Tuple3(width * sigma)
    import spmc.init
    spmc.init.create_random(config = session.config, N = n_particles, p_type = 'A')
    import spmc.potentials
    potentials = spmc.potentials.Potentials(session.reduced_units)
    lj = potentials.lennard_jones_potential(
        'A', 'A',
        epsilon = 1.0,
        sigma = sigma,
        r_cut = 2.25 * sigma,)
    session.add_potential(lj)
    session.add_move(spmc.RandomDisplacementMove(1))
    session.add_analyzer('pe')
    session.add_analyzer('acceptance_ratio')
    session.simulator = spmc.MonteCarloNVTSampler(beta)
    session.random_seed(int(random.random() * 100))
    import time
    start = time.time()
    session.run(n_steps)
    return time.time() - start

output = ""

for n_particles in [200, 400, 800]:
    for n_steps in [400, 800]:
        for n_threads in [1, 4, 16]:
            for density_factor in [0.5, 0.9, 1.0, 1.1, 1.5, 2.0]:
                for beta_factor in [0.1, 0.5, 0.9, 1.0, 1.1, 1.5, 2.0]:
                    t = lj_benchmark(n_threads, n_particles, n_steps, density_factor, beta_factor)
                    print "RESULT %d %d %d %f %f %f\n" % (n_particles, n_steps, n_threads, density_factor, beta_factor, t) 
