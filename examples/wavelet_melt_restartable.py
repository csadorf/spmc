#!/usr/bin/env python

# Define parameters
n_molecules = 20
N_p = pow(2, 12)
density = 0.05
cubic_box_length = pow(n_molecules * N_p / density, 1.0 / 3)
coarse_graining_levels = 8
random_seed = 42
run_id = '_example'
num_steps = 20000
dump_freq = int(max(1, num_steps/1000))
fn_init = run_id + '_init.xml'
fn_restart = run_id + '_restart.xml'

# import the SPMC package
import spmc

# create a simulation session
from wavelet import melt

melt_session = melt.WaveletMeltSession(             # This is a generator
    beta = 1.0,                                     # class, used to generate
    box = spmc.Tuple3(cubic_box_length),            # spmc sessions.
    id_ = run_id,
    )

def read_config(filename):
    "Wrapper for parser for better error handling."
    tmp = spmc.Session()
    try:
        tmp.read(filename)
    except RuntimeError as error:
        if str(error) == 'File was not found':
            raise IOError(filename)
    else:
        return tmp.config

try:
    melt_session.config = read_config(fn_restart)
    session = melt_session.session()
except IOError:
    melt_session.add_molecules(
        R = N_p,
        n = n_molecules,
        )
    # Coarse-grain system
    for i in xrange(coarse_graining_levels):
        melt_session.coarse_grain()
    session = melt_session.session()
    session.write(fn_init, 'hoomd_blue_xml')

## Add analysis
session.add_analyzer('radius_of_gyration')
session.add_analyzer('osmotic_pressure')
session.add_analyzer('acceptance_ratio')
session.add_dumper(run_id + '_dump.xyz', 'xyz_trajectory', dump_freq)
session.add_dumper(fn_restart, 'xml', dump_freq)

# run 20,000 MC cycles in NVT ensemble
session.random_seed(random_seed)
session.run_upto(num_steps)
session.write(run_id + '_coarse_grained_sampled.xml', 'hoomd_blue_xml')

# Reconstruct system
for i in xrange(coarse_graining_levels):            # Without explicit ReFit,
    melt_session.reconstruct()                      # default parameters are used.

# Map result into first image box
import spmc.utility
session = melt_session.session()
session.write(run_id + '_reconstructed.xml', 'hoomd_blue_xml')
session.config = spmc.utility.map_into_box(melt_session.config)
session.write(run_id + '_reconstructed_mapped.xml', 'hoomd_blue_xml')
