#!/usr/bin/env python

# Implemented as described by
# Flyvbjerg, H., & Petersen, H. (1989), J. Chem. Phys.

def block(x):
    assert len(x) > 1
    ret = [type(x[0])()] * (len(x) / 2)
    for i in xrange(len(ret)):
       ret[i] = 0.5 * (x[2*i + 1] + x[2*i])
    return ret

def estimate_std(x):
    from math import log, sqrt
    import numpy as np
    n_0 = len(x)
    S = []
    S_error = []
    assert len(x) > 0
    for i in xrange(int(log(len(x), 2) - 1)):
        s = np.var(x) / (len(x) - 1)
        s_error = abs(s * sqrt(2 / (len(x) - 1)))
        S.append(s)
        S_error.append(s_error)
        x = block(x)
    S = np.sqrt(n_0 * np.array(S))
    S_error = np.sqrt(n_0 * np.array(S_error))
    return S, S_error

# The following functions serve as verification of the method

def correlation(x, t):
    import numpy as np
    if t == 0:
        return np.var(x)
    else:
        n = len(x)
        s = np.cov(x[:n-t], x[t:n])
        return s[0][1]

def alt_estimate_std(x, T):
    num = 0.0
    n = len(x)
    assert T < n
    for t in xrange(int(T)):
        num += (1 - float(t) / n) * correlation(x, t)
    print num
    num = correlation(x, 0) + 2 * num
    den = n - 2 * T - 1 + T * (T + 1) / n
    from math import sqrt
    return sqrt(abs(n * num / den))

# The following functions serve as a demonstration of the method.

def sample(n, f = lambda x: 1, sigma = 0.0):
    import numpy as np
    x = np.random.normal(scale = sigma, size = n)
    for i in xrange(n):
        x[i] += f(i)
    return x

def verify(x, ax, color = 'black', sigma = None, T_set = None, alternative_method = False):
    import numpy as np
    from math import log

    std = estimate_std(x)

    n = len(x)
    tr = lambda n: 0.5 * (2**n - 1)
    nr = lambda t: log(2 * t + 1, 2)

    ax[0].plot(range(len(x)), x, color = color)
    #ax[0].plot(range(n), [np.average(x)] * n)
    ax[0].set_xlabel("t")
    ax[0].set_ylabel("m")
    ax[0].grid()

    ax[1].set_xlabel("T + 1")
    ax[1].set_ylabel(r'$\sigma$')
    ax[1].set_xscale('log')
    ax[1].set_yscale('log')
    T = [tr(i) for i in xrange(len(std[0]))]
    ax[1].errorbar(T, std[0], std[1], label = 'blocking method', color = color)
    if alternative_method:
        d_alt = [alt_estimate_std(x, t) for t in T]
        ax[1].plot(T, d_alt, label = 'alternative calculation', color = color)
    if sigma is not None:
        ax[1].plot(T, [sigma] * len(T), color = 'black', label = 'true value', ls = 'dashed')
    ax[1].grid()
    if T_set is not None:
        ax[1].axvline(T_set, color = 'red')

def main(n):

    from math import sin, exp
    sigma = 1.0
    T_set = 50
    A = 100
    B = 10
    sinus = lambda x: B * sin(float(x) / T_set)
    decay = lambda x: A * exp(- float(x) / T_set)

    from matplotlib import pyplot as plt
    fig, ax = plt.subplots(2)
    fig.suptitle("Blocking Method Verification - T = {}".format(T_set))

    from itertools import cycle
    colors = cycle(['c','m','y','k'])
    X = []
    X.append(sample(n, lambda x: 0, sigma = sigma))
    X.append(sample(n, lambda x: sinus(x), sigma = sigma))
    X.append(sample(n, lambda x: sinus(x) + decay(x), sigma = sigma))
    for x in X:
        verify(x, ax, colors.next(), sigma = sigma)

    #plt.show()
    fig.tight_layout()
    plt.savefig('_blocking_plot.pdf')

if __name__ == '__main__':
    main(5000)
