#!/usr/bin/env python

# generate random numbers for initialization
import random
random.seed(42) # Comment this line for true randomness!

# Define parameters
n_molecules = 5
N_p = pow(2, 10)
density = 0.05
cubic_box_length = pow(n_molecules * N_p / density, 1.0 / 3)
coarse_graining_levels = 6

# import the SPMC package
import spmc

# create a simulation session
from wavelet import melt
melt_session = melt.WaveletMeltSession(   # This is generator
    beta = 1.0,                           # class, used to generate
    box = spmc.Tuple3(cubic_box_length),  # spmc sessions
    id_ = 'example',
    )
melt_session.add_molecules(
    R = N_p,
    n = n_molecules,
    )

# Coarse-grain system
for i in xrange(coarse_graining_levels):
    melt_session.coarse_grain()

config_test =melt_session.config()
session = melt_session.session()

## Add analysis
session.add_analyzer('radius_of_gyration')
session.add_analyzer('osmotic_pressure')
session.add_analyzer('acceptance_ratio')
#session.add_dumper('_dump.xyz', 'xyz_trajectory', 10)
#session.add_dumper('_restart_#.xml', 'xml', 100)

# run 20,000 MC cycles in NVT ensemble
session.random_seed(int(random.random() * 100))
session.write('_example_init.xml', 'hoomd_blue_xml')
session.run(20000)
session.write('_example_coarse_grained_sampled.xml', 'hoomd_blue_xml')

# Reconstruct system
for i in xrange(coarse_graining_levels):            # Without explicit ReFit,
    melt_session.reconstruct()                      # default parameters are used.

# Map result into first image box
import spmc.utility
session = melt_session.session()
session.write('_example_reconstructed.xml', 'hoomd_blue_xml')
session.config = spmc.utility.map_into_box(melt_session.config())
session.write('_example_reconstructed_mapped.xml', 'hoomd_blue_xml')
