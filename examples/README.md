# Examples

## Introduction

These examples are designed to demonstrate basic concepts on how to set up simulations and analysis generated data using SPMC. They might also serve as template scripts for other projects.
For execution, simply copy a scripts into your local working directory and execute it.

## Descriptions

### `lennard_jones.py`

This example is a replication of Rahman et al.'s work from 1964 published in Physical Review, 136(2A). 
It is a simulation of a lennard jones particle system in a periodic boundary box with simple displacement moves at a specific density.

### `lj_minimal.py`

This example is similar to `lennard_jones.py` but reduced to the absolute basics to serve as a minimal example.
    
### `hard_spheres.py`

This example is a simulation of spheres within a periodic boundary box and simple displacement moves at a specific density.
The hard sphere potential has a very high constant energy for `r<=r_c` and 0 for `r>r_c`.

### `wavelet_melt.py`

This is an example of a wavelet-accelerated monte-carlo simulation and the Reconstruction Fitting (ReFit) algorithm to simulate systems at multiple resolutions.
Linked hard spheres
A configuration of bonded hard spheres representing a polymer at the atomistic level is effectively equilibrated by coarse-graining and subsequent reconstruction.
The potential is normalized and can flexibely used at arbitrary resolutions.

### `advanced_analysis.py`

This example demonstrates how to process generated data while the simulation runs using a callback function.
Direct processing of data allows to effectively store data in a preferrable format without the need to write and parse log files at a preferrable location.

See also: `mongodb.py`

### `mongodb.py`

This example demonstrates how to store generated data directly within a MongoDB database.

See also: `advanced_analysis.py`

### `lj_mongodb_job.py`

Requires: `sim_tools`

This example demonstrates how to store thermal analysis data and sampling snapshots in a MongoDB database, using the SPMCJob class from the spmc.job module.

The simulation is carried out, while analysis data and snapshots are stored in a MongoDB database.
After sampling the data is retrieved from the database for analysis.

See also: `mongodb.py`

### `pyplot_analysis.py`

This example demonstrates how to analyze data using the matplotlib module.
In this particular case the data is plotted to the screen while the simulation is running.

See also: `advanced_analysis.py`
