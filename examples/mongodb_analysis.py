#!/usr/bin/env python

import pymongo
from pymongo import MongoClient
client = MongoClient()
db = client['spmc']
mc = db['mongodb_example']

pipe = [
        {'$group': {
            '_id'           : '$step',
            'pe'            : {'$push': '$potential_energy'},
            },
        },
        {'$sort': {'_id': 1}},
    ]

cursor = mc.aggregate(pipeline = pipe)
try:
    import numpy as np
    print 'step  V/eps sigma '
    for c in cursor['result']:
        step = c['_id']
        pe = c['pe']
        print "{:5.0f} {:1.5g} {:1.5g}".format(
            step,
            np.average(pe),
            np.std(pe),
            )
except ImportError:
    print 'step  potential_energy'
    for c in cursor['result']:
        step = c['_id']
        pe = c['pe']
        print "{:5.0f} {:1.5g}".format(
            step,
            sum[pe]/len(pe)
            )

from blocking_method import estimate_std
steps = [c['_id'] for c in cursor['result']]
pe = [np.average(c['pe']) for c in cursor['result']]
pe_error = [np.std(c['pe']) for c in cursor['result']]
n = len(pe)
n_split = n / 2
steps = steps[n_split:]
pe = pe[n_split:]
pe_error = pe_error[n_split:]
#steps = steps[int(0.75 * n):]
#pe = pe[int(0.75 * n):]

std = estimate_std(pe)
tr = lambda n: 0.5 * (2**n - 1)
T = [tr(i) for i in xrange(len(std[0]))]

print std
from matplotlib import pyplot as plt
#plt.plot(steps, pe, scaley = 'log')
plt.errorbar(steps, pe, pe_error)
plt.show()
plt.errorbar(T, std[0], std[1])
plt.show()
