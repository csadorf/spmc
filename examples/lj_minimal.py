import spmc, spmc.init, spmc.potentials
session = spmc.Session()
session.config.box = spmc.Tuple3(100, 100, 100)
spmc.init.create_random(config = session.config, N = 100, p_type = 'A')
potentials = spmc.potentials.Potentials(session.reduced_units)
session.add_potential(potentials.lennard_jones_potential(
    a = 'A', b = 'A',
    epsilon = 1.0,
    sigma = 1.0, 
    r_cut = 3.0))
session.add_move(spmc.RandomDisplacementMove(1))
session.simulator = spmc.MonteCarloNVTSampler(1.0) # beta = 1.0
session.random_seed(42)
session.run(10000)
