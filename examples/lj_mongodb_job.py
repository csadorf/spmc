#!/usr/bin/env python

import logging
logger = logging.getLogger('lj_mongodb_job')
logging.basicConfig(level = logging.DEBUG)

from spmc.job import SPMCJob
from sim_tools.job_management import MongoDBJob

PERIOD_SNAPSHOTS = 10
PERIOD_ANALYSIS = 10

class MyJob(MongoDBJob, SPMCJob):

    def session(self):
        import spmc, spmc.init, spmc.potentials
        session = spmc.Session()
        session.config.box = spmc.Tuple3(self.parameters()['box_L'])
        spmc.init.create_random(config = session.config, N = self.parameters()['N'], p_type = 'A')
        potentials = spmc.potentials.Potentials(session.reduced_units)
        session.add_potential(potentials.lennard_jones_potential(
            a = 'A', b = 'A',
            epsilon = self.parameters()['epsilon'],
            sigma = self.parameters()['sigma'],
            r_cut = self.parameters()['r_cut']))
        session.add_move(spmc.RandomDisplacementMove(1))
        session.simulator = spmc.MonteCarloNVTSampler(1.0) # beta = 1.0
        session.add_analyzer('pe')
        session.add_analyzer('osmotic_pressure')
        session.add_analyzer('acceptance_ratio')
        session.analyzer_frequency = PERIOD_ANALYSIS
        return session

    def run(self):
        import random
        session = self.session()
        callback = self.get_thermo_callback('thermo')
        session.set_analyzer_callback(callback)
        snapshot_callback = self.get_snapshot_callback('thermo')
        session.set_callback(PERIOD_SNAPSHOTS, snapshot_callback)
        session.random_seed(int(random.random() * 100000))
        session.run(self.parameters()['steps'])

def convert_attribute_map(attribute_map):
    for entry in attribute_map:
        yield [float(s.strip()) for s in str(entry)[1:-1].split(',')]

def analyze(collection, collection_secondary):
    import spmc.analysis
    query = {}
    documents = list(collection.find(query))
    print 'num of docs: {}'.format(len(documents))
    for document in documents:
        _id = document['_id']
        with MyJob(parameters = {}, collection = collection, _id = _id) as job:
            job.set_secondary_collection(collection_secondary)

            num_steps = job.parameters()['steps']
            steps = range(0, num_steps, PERIOD_SNAPSHOTS)
            snapshots = job.get_snapshots('thermo', steps)
            for snapshot in snapshots:
                rdf = spmc.Analysis.radial_distribution_function(snapshot)
                import numpy as np
                rdf_a = np.array(list(convert_attribute_map(rdf)))
                print max(rdf_a.transpose()[1])

            # We can retrieve a session object for analysis in this way
            #session = job.session() 

def main():
    sigma = 3.4
    rho = 864 / pow(10.229 * sigma, 3)
    N = 1000
    N = 100
    parameters = {
        'box_L' : pow(N / rho, 1.0 / 3),
        'N' : N,
        'epsilon' : 120,
        'sigma' : sigma,
        'r_cut' : 2.25 * sigma,
        'beta' : 120 / 94.4,
        'steps' : 1000,
    }

    from pymongo import MongoClient
    client = MongoClient()
    db = client['testing']
    mc = db['lj_minimal']
    # Because of the MongoDB size limiation on sigle documents,
    # we need to use a secondary collection for large production runs,
    # to store large amounts of data.
    mc_secondary = db['lj_minimal_secondary']
    # Comment the next two lines to prevent removal of previous data.
    mc.remove()
    mc_secondary.remove()

    num_jobs = 1
    for i in xrange(num_jobs):

        with MyJob(mc, parameters) as job:
            # We use a secondary collection to store large amounts of data (see above).
            job.set_secondary_collection(mc_secondary)
            # This will execute our job.
            job.start()
            # After running the job we can retrieve stored snaphots.
            for i in xrange(0, job.parameters()['steps'], PERIOD_SNAPSHOTS):
                job.get_snapshot('thermo', i)

        parameters['box_L'] *= 1.1

    analyze(mc, mc_secondary)

if __name__ == '__main__':
    main()
