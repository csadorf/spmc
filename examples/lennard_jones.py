#!/usr/bin/env python

# generate random numbers for initialization
import random
random.seed(42) # Comment this line for true randomness!

# Define parameters, taken from 
# Rahman, A. (1964). Correlations in the motion of atoms in liquid argon. Physical Review, 136(2A). 

sigma = 3.4 # Angstrom
epsilon = 120 # Kelvin
temperature = 94.4
beta = epsilon / temperature
mass = 39.95 * 1.6747e-24

# import the SPMC package
import spmc

# create a simulation session
session = spmc.Session()

# Set box size to 100x100x100
session.config.box = spmc.Tuple3(10.229 * sigma)

# create 864 random particles of name A
import spmc.init
spmc.init.create_random(config = session.config, N = 864, p_type = 'A')

# Set up Lennard-Jones potential and add to session
import spmc.potentials
potentials = spmc.potentials.Potentials(session.reduced_units)
lj = potentials.lennard_jones_potential(
    'A', 'A',
    epsilon = 1.0,
    sigma = sigma,
    r_cut = 2.25 * sigma,)
session.add_potential(lj)

# Add MC moves
session.add_move(spmc.RandomDisplacementMove(1))

# Add analysis
session.add_analyzer('pe')
session.add_analyzer('osmotic_pressure')
session.add_analyzer('acceptance_ratio')
session.add_dumper('dump.xyz', 'xyz_trajectory', 10)
session.add_dumper('restart_#.xml', 'xml', 100)

# run 1,000 MC cycles in NVT ensemble
session.simulator = spmc.MonteCarloNVTSampler(beta)
session.random_seed(int(random.random() * 100))
session.write('init.xml', 'hoomd_blue_xml')
session.run(1000)
session.write('sampled.xml', 'hoomd_blue_xml')

# Map into first image box
import spmc.utility
session.config = spmc.utility.map_into_box(session.config)
session.write('mapped.xml', 'hoomd_blue_xml')
