#!/usr/bin/env python

# generate random numbers for initialization
import random
#random.seed(42) # Comment this line for true randomness!

# Define parameters, taken from 
# Rahman, A. (1964). Correlations in the motion of atoms in liquid argon. Physical Review, 136(2A). 

sigma = 3.4 # Angstrom
epsilon = 120 # Kelvin
temperature = 94.4
beta = epsilon / temperature
mass = 39.95 * 1.6747e-24
num_steps = 1000
init_cutoff = 10

# import the SPMC package
import spmc

# create a simulation session
session = spmc.Session()

# Set box size to 100x100x100
session.config.box = spmc.Tuple3(10.229 * sigma)

# create 864 random particles of name A
import spmc.init
spmc.init.create_random(config = session.config, N = 864, p_type = 'A')

# Set up Lennard-Jones potential and add to session
import spmc.potentials
potentials = spmc.potentials.Potentials(session.reduced_units)
lj = potentials.lennard_jones_potential(
    'A', 'A',
    epsilon = 1.0,
    sigma = sigma,
    r_cut = 2.25 * sigma,)
session.add_potential(lj)

# Add MC moves
session.add_move(spmc.RandomDisplacementMove(1))

# Add analysis
pe = session.add_analyzer('pe')
session.analyzer_frequency = 5
session.add_analyzer('osmotic_pressure')
session.add_analyzer('acceptance_ratio')
#session.add_dumper('dump.xyz', 'xyz_trajectory', 10)
#session.add_dumper('restart_#.xml', 'xml', 100)

# Analysis data may be processed directly through a callback
# function. We will define an example callback function to
# save all analyzer data within a dictionary:
import collections
analysis_data = collections.defaultdict(list)

# We use the matplotlib module to plot the simulation analysis
# data, while it is generated. For this we setup a figure with
# line objects, which will be updated in a callback function.
import matplotlib.pyplot as plt
fig = plt.figure()
plt.xlim(0, num_steps)
plt.ylim(-5000, 5000)
plt.grid()
plt.xlabel('steps')
plt.ylabel('$V / \epsilon$')
plt.title('Potential Energy Trajectory')
line = plt.plot([],[],'b-', linewidth=2)[0]
plt.show(block = False)


# The callback function requires 3 arguments:
# step: The step, where analyzer data was evaluated.
# freq: The number of steps since last evaluation, as determined
#       by 'analyzer_frequency'.
# data: A dictionary of the analyzer data, where the key is
#       the analyzer id and the value is the value as computed
#       by the corresponding analyzer.

def update_analysis(step, freq, data):
    if step <= init_cutoff:
        return
    data['step'] = step
    for analyzer_id, value in data.iteritems():
        analysis_data[analyzer_id].append(value)
    steps = analysis_data['step']
    pe = analysis_data['potential_energy']
    #plt.ylim(min(pe), max(pe)) # Uncomment for dynamic update of limits
    line.set_data(steps, pe)
    plt.draw()

# We register this function to be executed with 
# each analyzer computation.
session.set_analyzer_callback(update_analysis)

# run 1,000 MC cycles in NVT ensemble
session.simulator = spmc.MonteCarloNVTSampler(beta)
session.random_seed(int(random.random() * 100))
session.write('_init.xml', 'hoomd_blue_xml')
session.run(num_steps)


session.write('_sampled.xml', 'hoomd_blue_xml')

# Map into first image box
import spmc.utility
session.config = spmc.utility.map_into_box(session.config)
session.write('_mapped.xml', 'hoomd_blue_xml')

