#file( 
#  COPY python-module 
#  DESTINATION ${CMAKE_BINARY_DIR}
#  PATTERN *.pyc EXCLUDE
#  PATTERN *.in EXCLUDE
#  )

configure_file(
  ${CMAKE_CURRENT_SOURCE_DIR}/python-module/spmc/__init__.py.in
  ${CMAKE_CURRENT_BINARY_DIR}/python-module/spmc/__init__.py)

find_program(PYTHON "python")

if (PYTHON)
  set(SETUP_PY_IN "${CMAKE_CURRENT_SOURCE_DIR}/setup.py.in")
  set(SETUP_PY "${CMAKE_CURRENT_BINARY_DIR}/setup.py")
  set(DEPS "${CMAKE_CURRENT_SOURCE_DIR}/python-module/spmc/__init__.py")
  set(OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/build")

  configure_file(${SETUP_PY_IN} ${SETUP_PY})

  add_custom_command(
    OUTPUT ${OUTPUT}/timestamp
    COMMAND ${PYTHON} setup.py build
    COMMAND ${CMAKE_COMMAND} -E touch ${OUTPUT}/timestamp)

  add_custom_target(target ALL DEPENDS ${OUTPUT}/timestamp)

  if(INSTALL_LOCAL)
    install(CODE "execute_process(COMMAND ${PYTHON} ${SETUP_PY} install --user)")
  else()
    install(CODE "execute_process(COMMAND ${PYTHON} ${SETUP_PY} install)")
  endif()

endif()

add_subdirectory(python-module)
