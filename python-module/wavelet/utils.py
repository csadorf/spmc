import sys, logging, random
import spmc
from spmc import utility, analysis, moves
import wavelet.potential as potential
import libreduced_units as r_units
import numpy as np

logger = logging.getLogger('wavelet_reconstruction')

DEFAULT_I_MAX = 32000 / 5
DEFAULT_BETA  = 1

DISTRIBUTIONS_DIR = 'distributions/'

def distributions_dir():
  from wavelet import PACKAGE_DIR
  import os
  return os.path.join(PACKAGE_DIR, DISTRIBUTIONS_DIR)

FILENAME_DUMP_XYZ = "_{}_dump_reconstruction.xyz"
FILENAME_DUMP_DCD = "_{}_dump_reconstruction.dcd"

def generate_atomistic_linear_molecule(box, N_beads, origin, direction, p_type):
    assert(not N_beads % 2)
    result = spmc.SimulationConfig()
    result.box = box
    result.num_atoms = N_beads
    direction_normalized = direction / abs(direction)
    for i in range(N_beads):
        p = result.particle(i)
        p.type = p_type
        p.position = origin + i * direction_normalized
    result.bond_all_particles()
    return result

def generate_atomistic_molecule(box, N_beads, z, p_type):
    assert(not N_beads % 2)
    Lx = box.x
    Lc = 0.8 * Lx
    N_line = min(int(Lc), N_beads)
    N_lines = int(N_beads / N_line)
    N_complete = N_lines * N_line
    extra = N_beads - N_complete
    shift = 0.5 * (Lx - N_line)
    result = spmc.SimulationConfig()
    result.box = box
    result.num_atoms = N_beads
    for line in range(N_lines):
        back = (line % 2) == 0
        for x in range(N_line):
            p = result.particle(line * N_line + x)
            p.type = p_type
            if back:
                p.position = spmc.Tuple3(N_line + shift - x - 1, line, z)
            else:
                p.position = spmc.Tuple3(shift + x, line, z)
    for x in range(extra):
        back = N_lines % 2 == 0
        p = result.particle(N_complete + x)
        p.type = p_type
        if back:
            p.position = spmc.Tuple3(N_line + shift - x - 1, N_lines, z)
        else:
            p.position = spmc.Tuple3(shift + x, N_lines, z)

    result.bond_all_particles()
    return result

def generate_atomistic_config(box, N_molecules, N_beads = 512, p_type = 'A'):
    result = spmc.SimulationConfig()
    result.box = box
    Lz = box.z
    Lc = 0.8 * Lz
    Ln = min(Lc, N_molecules)
    Lm = Lc / N_molecules
    shift = 0.5 * (Lz - Ln)
    msg = "Building molecule {}/{}..."
    for z in range(N_molecules):
        logger.debug(msg.format(z + 1, N_molecules))
        result.append(generate_atomistic_molecule(box, N_beads, z * Lm + shift, p_type))
    return result

def normalize_atomistic_molecule(config, molecule):
    import random
    id_ = random.random()
    s = spmc.Session()
    def write(name):
        return
        s.config = config
        s.write("__normalize_{}_{}.xml".format(id_, name), 'hoomd_blue_xml')

    write('before')
    com_before = analysis.center_of_mass(config, molecule)
    n = len(molecule)
    assert(not (n > 1 and n % 2))
    for i in range(n - 1):
        p_i = config.particle(i)
        p_j = config.particle(i + 1)
        p_i_pos = p_i.remapped_position()
        p_j_pos = p_j.remapped_position()
        delta = p_j_pos - p_i_pos
        p_j.position = p_i_pos + delta / abs(delta)
    write('normalized')
    com_after = analysis.center_of_mass(config, molecule)
    com_delta = com_after - com_before
    for i in range(n):
        p_i = config.particle(i)
        p_i_pos = p_i.remapped_position()
        p_i.position = p_i_pos + delta
    write('shifted')
    com_after_2 = analysis.center_of_mass(config, molecule)
    if abs(com_after_2 - com_before) > 1e-8:
        print(com_before)
        print(com_after)
        print(com_after_2)

def normalize_atomistic_config(config):
    msg = "Atomistic normalization not properly implemented!"
    logger.warning(msg)
    #raise NotImplementedError(msg)
    logger.debug("Normalizing atomistic configuration...")
    molecules = config.property.molecules()
    for molecule in molecules:
        normalize_atomistic_molecule(config, molecule)

def coarse_grain(config, particle_type = 'A', mapping = dict()):
    result = spmc.SimulationConfig()
    result.box = config.box
    wavelets = set(config.group_by_type(particle_type))
    for molecule in config.property.molecules():
        local_mapping = dict()
        if wavelets.isdisjoint(set(molecule)):
            msg = "Coarse graining of mixed type molecules not validated."
            logger.warning(msg)
            raise NotImplementedWarning(msg)
            result.append(spmc.SimulationConfig(molecule))
            for p in molecule:
                local_mapping[p] = p
            continue
        assert((len(molecule) % 2) == 0)
        mol_list = list(molecule)
        template = config.particle(mol_list[0])

        molecule_config = spmc.SimulationConfig()
        molecule_config.box = config.box
        molecule_config.num_atoms = int(len(molecule) / 2)

        for p in range(int(len(mol_list) / 2)):
            subset = spmc.TagSet()
            subset.insert(mol_list[2 * p])
            subset.insert(mol_list[2 * p + 1])
            center = analysis.centroid(config, subset)
            molecule_config.particle(p).clone(template)
            molecule_config.particle(p).position = center
            local_mapping[2 * p + mol_list[0]] = p
            local_mapping[2 * p + 1 + mol_list[0]] = p

        molecule_config.bond_all_particles()
        mapping.update(((k, v + result.num_atoms) for k, v in local_mapping.items()))
        result.append(molecule_config)
    return result

def guess_mapping(config_cg, config_at):
    result = dict()
    N_p = int(config_cg.num_atoms)
    assert config_at.num_atoms == 2 * N_p
    for i in range(N_p):
        at_beads = spmc.TagSet()
        at_beads.insert(i * 2)
        at_beads.insert(i * 2 + 1)
        result[i] = at_beads
    return result

def check_transformation(config_cg, config_at, mapping = None, tolerance = 1e-12):
    import uuid
    logger.debug("Checking wavelet transformation...")
    if mapping is None:
        mapping = guess_mapping(config_cg, config_at)
    for cg_bead, at_beads in mapping.items():
        pos_cg_bead = config_cg.particle(cg_bead).remapped_position()
        c = analysis.centroid(config_at, at_beads)
        delta = abs(c - pos_cg_bead)
        logger.debug("Delta = {}".format(delta))
        if delta > tolerance:
            id = str(uuid.uuid4())
            msg = "Invalid wavelet transformation! Position of '{cg_bead}' '{pos_cg_bead}' " +\
                "deviates from centroid of atomistic particles '{at_beads}' '{centroid}' by " +\
                "'{delta}'. Tolerance: {tolerance}. ID = {id}."
            #raise RuntimeError(msg.format(
            logger.warning(msg.format(
                cg_bead = cg_bead,
                pos_cg_bead = pos_cg_bead,
                at_beads = at_beads,
                centroid = c,
                delta = delta,
                tolerance = tolerance,
                id = id,
                ))
            filename = '_invalid_wavelet_transformation_{id}_{delta}_{tolerance}_{type}.xml'
            s = spmc.Session()
            s.config = config_cg
            s.write(
                filename.format(id = id, delta = delta,
                  tolerance = tolerance, type = 'cg'), 'hoomd_blue_xml')
            s.config = config_at
            s.write(
                filename.format(id = id, delta = delta,
                  tolerance = tolerance, type = 'at'), 'hoomd_blue_xml')
            return id
        else:
            return None

def generate_beads(box, bond_length, center_of_mass, p_type = 'A'):
    cog = center_of_mass
    beads = spmc.SimulationConfig()
    beads.box = box
    beads.num_atoms = 2
    beads.particle(0).type = p_type
    beads.particle(1).type = p_type
    beads.particle(0).position = spmc.Tuple3(
      cog.x - bond_length / 2, cog.y, cog.z)
    beads.particle(1).position = spmc.Tuple3(
      cog.x + bond_length / 2, cog.y, cog.z)
    return beads

def translate_jiggle_move(N_e, freq, units, adj = 1):
    if N_e > 1:
        r_g = potential.rg_approximation(N_e)
        params = moves.parse_tjb_parameters(distributions_dir(), N_e, units, r_g)
        tjbm = moves.TranslateJiggleBiasedMove(freq, adj, params)
        tjbm.section = True
        return tjbm
        #return moves.TranslateJiggleBiasedMove(freq, adj, params)
    else:
        tjm = spmc.TranslateJiggleMove(freq, adj)
        tjm.section = True
        return tjm
        #return spmc.TranslateJiggleMove(freq, adj)

def change_bond_length_move(N_e, freq, units, adj = 1):
    r_g = potential.rg_approximation(N_e)
    params = moves.parse_tjb_parameters(distributions_dir(), N_e, units, r_g)
    logger.debug("ChangeBondLengthMove: N_e={}".format(N_e))
    logger.debug("average bond length: {}".format(potential.average_bond_length(N_e)))
    return moves.ChangeBondLengthBiasedMove(freq, params[0], adj)

def moves_equilibration(N_e, reduced_units):
    moves = []
    moves.append(translate_jiggle_move(N_e, 1, reduced_units))
    moves.append(spmc.TranslateMoleculeMove(13, 20.0))
    return moves

def moves_reconstruction(N_e, reduced_units):
    moves = []
    moves.append(spmc.RotateBeadsMove(1, 1))
    moves.append(change_bond_length_move(N_e, 3, reduced_units))
    return moves

def wavelet_potential(N_e, reduced_units, cut_off_radius = None):
    if N_e == 1:
        return [potential.WaveletPotential(reduced_units).atomistic()]
    else:
        return potential.WaveletPotential(reduced_units).all(N_e, cut_off_radius, 'A')

def wavelet_potential_soft(N_e, reduced_units):
    if N_e == 1:
        return [potential.WaveletPotential(reduced_units).atomistic(epsilon = 1)]
    else:
        return potential.WaveletPotential(reduced_units).neighbor(N_e)[:2]
    #return [potential.WaveletPotential(reduced_units).harmonic(N_e)]

def redirect_logs(session):
    try:
      id_ = session.id()
    except RuntimeError:
      id_ = ""
    filename_general    = "_{}_general.log".format(id_)
    filename_simulator  = "_{}_simulator.log".format(id_)
    filename_thermal    = "_{}_thermal.log".format(id_)
    filename_compute    = "_{}_compute.log".format(id_)
    filename_misc       = "_{}_misc.log".format(id_)
    session.log_redirect_to_std_cerr(0)
    session.log_redirect_to_file(0, filename_general)
    session.log_redirect_to_file(1, filename_simulator)
    session.log_redirect_to_file(2, filename_thermal)
    session.log_redirect_to_file(3, filename_compute)
    session.log_redirect_to_file(4, filename_misc)

def session_basic(beta, id_ = None):
    if id_ is None:
        id_ = ''
    session = spmc.Session(id_)
    session.reduced_units = r_units.argon
    session.simulator = spmc.MonteCarloNVTSampler(beta)
    session.use_neighbor_lists = False;
    session.frequency = 1
    #redirect_logs(session)
    return session

def session_equilibration(N_e, beta, cut_off_radius = None, id_ = None):
    session = session_with_potential(N_e, beta, cut_off_radius, id_)
    for move in moves_equilibration(N_e, session.reduced_units):
        session.add_move(move)
    return session

def session_reconstruction(N_e, beta, cut_off_radius = None, id_ = 'reconstruction'):
    session = session_basic(beta = beta, id_ = id_)
    session.add_potential(
        potential.WaveletPotential(session.reduced_units).harmonic(N_e = N_e))
    for move in moves_reconstruction(N_e, session.reduced_units):
        session.add_move(move)
    return session

def session_atomistic(beta, id_, epsilon = 1000):
    session = spmc.Session(id_)
    session.reduced_units = r_units.argon
    session.simulator = spmc.MonteCarloNVTSampler(beta)
    session.use_neighbor_lists = False
    session.frequency = 1
    session.add_potential(potential.WaveletPotential(session.reduced_units).atomistic(epsilon = epsilon))
    #redirect_logs(session)
    session.add_move(spmc.TranslateJiggleMove(1))
    return session

def session_soft(N_e, beta, id_ = None):
    session = session_basic(beta = beta, id_ = id_)
    for ff in wavelet_potential_soft(N_e, session.reduced_units):
        session.add_potential(ff)
    for move in moves_reconstruction(N_e, session.reduced_units):
        session.add_move(move)
    session.init()
    return session

def session_lj(beta, id_ = None):
    session = session_basic(beta = beta, id_ = id_)
    session.add_potential(wavelet_potential_lj(session.reduced_units))
    session.init()
    return session

def session_with_potential(N_e, beta, cut_off_radius = None, id_ = None):
    session = session_basic(beta = beta, id_ = id_)
    for ff in wavelet_potential(N_e, session.reduced_units, cut_off_radius):
        session.add_potential(ff)
    session.init()
    return session

def add_basic_analysis(session):
    analyzers_ids = [   
        'beta', 'pe', 'osmotic_pressure', 
        'radius_of_gyration', 'acceptance_ratio']
    analyzers = dict()
    for analyzer_id in analyzers_ids:
        analyzers[analyzer_id] = session.add_analyzer(analyzer_id)
    return analyzers

def add_xyz_dumper(session, callback_period):
    pass
    #session.add_dumper(FILENAME_DUMP_XYZ.format(session.id()), "xyz_trajectory", callback_period)

def reverse_map(N_e, random_seed):
    msg = "N_e = {} is invalid. Must be positive and power of 2!"
    if N_e <= 0 or (N_e > 1 and N_e % 2):
        raise ValueError(msg.format(N_e))
    random.seed(random_seed)
    bld = potential.bond_length_distribution()
    R_g_e = potential.rg_approximation(N_e)
    def reverse_map(config, mapping = None, mapping_dict = None):
        if mapping is None:
            mapping = spmc.TagSetMap()
        result = spmc.SimulationConfig()
        result.box = config.box
        molecules = config.property.molecules()
        offset = 0
        msg = "Reverse-mapping molecule {}/{}..."
        for i, molecule in enumerate(molecules):
            logger.debug(msg.format(i + 1, len(molecules)))
            tmp = spmc.SimulationConfig()
            tmp_mapping = dict()
            for p in molecule:
                com = config.particle(p).remapped_position()
                if N_e > 1:
                    bond_length = \
                      potential.chose_from_distribution(bld, random.random()) * R_g_e
                else:
                    bond_length = 1.0
                tmp_set = \
                  tmp.append(generate_beads(config.box, bond_length, com))
                shifted_tmp_set = spmc.TagSet()
                for t in tmp_set:
                    shifted_tmp_set.add(t + offset)
                mapping[p] = shifted_tmp_set
                if mapping_dict is not None:
                    mapping_dict[p] = list(shifted_tmp_set)
            tmp.bond_all_particles()
            offset += tmp.num_atoms
            result.append(tmp)
        if N_e == 1:
            pass #deactivated due to problems
            #normalize_atomistic_config(result)
        check_transformation(config, result)
        return result
    return reverse_map

def target_session(N_e, beta, random_seed, callback_period, callback, id_ = None):
    msg = "Creating target session for N_e={}."
    logger.debug(msg.format(N_e))
    target_session = session_reconstruction(N_e, beta, id_ = id_)
    target_session.set_callback(callback_period, callback)
    target_session.random_seed(random_seed)
    return target_session

def target_sessions(box, N_e, random_seed, callback_period, callback, id_ = None):
    msg = "Creating target session for N_e={}."
    logger.debug(msg.format(N_e))
    beta = DEFAULT_BETA
    target_session = session_reconstruction(N_e, beta, id_ = id_)
    add_basic_analysis(target_session)
    target_session.set_callback(callback_period, callback)
    target_session.random_seed(random_seed)

    id_soft = "{}_soft".format(id_)
    target_session_soft = session_soft(N_e, beta, id_ = id_soft)
    add_basic_analysis(target_session_soft)
    target_session.set_callback(callback_period, callback)
    target_session_soft.random_seed(random_seed)
    return target_session, target_session_soft


def reconstruct(
        cg_config, 
        N_e,
        parameters,
        random_seed,
        compare_config = None,
        config_library = None,
        cycle_factor = 1,
        pre_equilibration = True,
        id_ = 'reconstruction',
        simulate = False,
        callback_period = 0,
        callback = None,
        ):
    msg = 'This function should not be used anzmore.'
    raise NotImplementedError(msg)
    target_session = target_sessions(cg_config.box, N_e, random_seed, callback_period = callback_period, callback = callback, id_ = id_)
    i_max = DEFAULT_I_MAX

    # This is the primary function for reconstruction procedures.
    evaluation_record = ReFit.evaluation_record()
    reconstruction.reconstruct(
      cg_config           = cg_config,                        # coarse-grained configuration
      target_session      = target_session[0],                # target session (environment)
      rm_fct              = reverse_map(N_e, random_seed),    # naive-reverse-mapping function
      evaluation_record   = evaluation_record,                # evaluation_record stores evaluation data
      QEsF                = ReFit.QEsF_family(parameters),    # QEsFunction
      cycle_factor        = cycle_factor,                     # scaling of cycles
      pre_equilibration   = pre_equilibration,                # equilibrate prior convergence attempt
      target_session_soft = target_session[1],                # soft potential session (optional)
      config_library      = config_library,                   # config dictionary (optional)
      callback_period     = callback_period,                  # Number of steps between callback calls
      callback            = callback,                         # Callback to be called every 'callback_period' steps
      simulate            = simulate,                         # Simulate process
      id_                 = id_,                              # session id
      )

    analysis_data = None
    return target_session[0].config, evaluation_record, analysis_data

def ReFit_default_parameters():
    return ReFit.default_parameters()

def wavelet_ReFit(
        atomistic_configurations,
        N_e,
        QEsF_parameters,
        random_seed,
        config_library,
        id_,
        simulate = False,
        ):
    if len(atomistic_configurations) > 1:
        raise NotImplementedError("ReFit only implemented for one atomistic configuration.")
    atomistic_config = atomistic_configurations[0]
    coarse_grained_config = coarse_grain(atomistic_config)
    target_session = target_sessions(coarse_grained_config.box, N_e, random_seed, callback_period = 100, callback = None, id_ = id_)
    parameters = ReFit.determine_parameters(
        configurations            = (atomistic_config, coarse_grained_config),
        target_session            = target_session[0],
        reverse_mapping_function  = reverse_map(N_e, random_seed),
        evaluation_record         = ReFit.evaluation_record(),
        QEvF_family               = ReFit.QEvF_family,
        QEsF_family               = ReFit.QEsF_family,
        QEsF_parameters           = QEsF_parameters,
        cycle_factor              = 1,
        target_session_soft       = target_session[1],
        config_library            = config_library,
        id_                       = id_,
        simulate                  = simulate,
        )
    return parameters, target_session[0].config

def analyze_data(data, parameters, session, atomistic_config, N_e, id_):
    ReFit.analyze_data(
        data,
        ReFit.QEvF_family(session, atomistic_config),
        ReFit.QEsF_family(parameters),
        id_)
