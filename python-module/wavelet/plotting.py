import logging
logger = logging.getLogger('wavelet_plotting')

import handle_data as hd
from . import plotting

def marker(plot, r, e):
    return plot.markers[r - 3]

def marker_e(plot, r, e):
    return plot.markers[e]

def marker_r(plot, r, e):
    return plot.markers[r - 3]

def color(plot, r, e):
    return plot.colors[r - 8]

def linestyle(plot, r, e):
    return plot.linestyles[e]

def linewidth(plot, r, e):
    return max(0.1, 1.0 / (e + 1))

def style(plot, r, e):
    return {
        'color'         : color(plot, r, e),
        'marker'        : marker_e(plot, r, e),
        #'linestyle'     : linestyle(plot, r, e),
        'linewidth'     : linewidth(plot, r, e),
    }

def legend_e(plot, e_s):
    import collections
    ret = collections.OrderedDict()
    for e in e_s:
        s = style(plot, 0, e)
        s['color'] = 'black'
        ret[e] = plotting.artist_from_style(
            ** s
        )
    return ret

def legend_r(plot, r_s):
    import collections
    ret = collections.OrderedDict()
    for r in r_s:
        ret[r] = plotting.artist_from_style(
            ** style(plot, r, 0))
    return ret

def get_rs(data):
    handler = hd.DataHandler(data)
    return  sorted(set(handler.get('key/r')))

def get_es(data, r):
    e_max = get_value(data, 'key/e_max', r = r)
    return list(range(e_max))
