from spmc import reconstruction
import spmc
import wavelet.utils as utils
import logging
from libreconstruction import WaveletReverseMappingLibrary as WaveletLibrary

DEFAULT_BETA = 1.0
DEFAULT_DUMPING_FREQ = 100

logger = logging.getLogger('wavelet_reconstruction')

def random_number():
    import random
    return int(random.random() * 100)

class WaveletReconstruction(reconstruction.Reconstruction):

    def __init__(self, 
            target_N_e = pow(2, 1),
            parameters = None,
            random_seed = random_number(),
            library = None,
            rm_function = utils.reverse_map,
            id_ = 'no_id',
            ):
        super(WaveletReconstruction, self).__init__(id_)
        self._logger = logging.getLogger("WaveletReconstruction_{}".format(id_))
        self._id = id_;

        self._beta = DEFAULT_BETA
        self.callback_period = 0
        self.callback = None

        self._random_seed = random_seed
        self.reverse_mapping_function = rm_function

        self._analyzers = None
        self.N_e = target_N_e

        if target_N_e == 1:
            self._QEsF = reconstruction.AthermalQEsF(parameters)
        else:
            self._QEsF = reconstruction.BasicQEsF(parameters)
        
        if library is None:
            self._library = WaveletLibrary()
        else:
            self._library = library

    @property
    def N_e(self):
        return self._target_N_e

    @N_e.setter
    def N_e(self, value):
        msg = "N_e = {} is invalid. Must be power of 2!"
        if (value <= 0) or (value > 1 and value % 2):
            raise ValueError(msg.format(value))
        else:
            self._target_N_e = value

    def _init(self):
        self._target_session = utils.target_session(
            N_e = self._target_N_e,
            beta = self._beta,
            random_seed = self._random_seed,
            callback_period = self.callback_period,
            callback = self.callback,
            id_ = self._id,
            )
        self._analyzers = utils.add_basic_analysis(self._target_session)
        self._reverse_map = self.reverse_mapping_function(self._target_N_e, self._random_seed)

    def analyzers(self):
        return self._analyzers

    def _check_transformation(self, config_cg, config_at, mapping):
        error_id = utils.check_transformation(config_cg, config_at, mapping)
        #if error_id is not None:
        reconstruction.check_transformation(
          config_cg, config_at, utils.coarse_grain, id = error_id)

    def _post_process(self, config_cg, config_at, mapping):
        if self.N_e == 1:
            pass # not correctly implemented
            #utils.normalize_atomistic_config(config)
        self._check_transformation(config_cg, config_at, mapping)

    def _library_post_process(self, config_cg, config_at, mapping):
        self._check_transformation(config_cg, config_at, mapping)

class WaveletReFit(reconstruction.ReFit):

    def __init__(
      self, 
      N_e = pow(2, 0),
      coarse_grain = utils.coarse_grain,
      id_ = 'WaveletReFit'):
        super(WaveletReFit, self).__init__()
        self._reconstruction = WaveletReconstruction(
            target_N_e = N_e,
            id_ = id_)
        #self._reconstruction.fast = True
        self._coarse_grain = coarse_grain
        if N_e == 1:
            self._QEvF = reconstruction.AthermalQEvF
        else:
            self._QEvF = reconstruction.BasicQEvF
