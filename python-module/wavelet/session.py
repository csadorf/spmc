import spmc
from libreconstruction import WaveletReverseMappingLibrary as WaveletLibrary
import collections

import logging
logger = logging.getLogger('wavelet_session')

def random_vector():
    import random
    return spmc.Tuple3(
        random.random(),
        random.random(),
        random.random())

def random_position(config):
    import random, spmc.utility
    return spmc.utility.element_product(
        random_vector(), config.box)

class WaveletSession(object):

    def __init__( self, beta, box, cut_off_radius = None,
                  particle_type = 'A', id_ = None):
        self._id = id_
        self._e = 0
        self.beta = beta
        self._config = spmc.SimulationConfig()
        self._config.box = box
        self._cut_off_radius = cut_off_radius
        self._particle_type = particle_type

        self._parameters = dict()
        self._reduced_units = spmc.Session().reduced_units

        self.default_library = WaveletLibrary
        self._library = collections.defaultdict(self.default_library)
        self.default_rod_partition = pow(2, 6)
        self.callback_period = 0
        self.callback = None
        self.use_library = True

    def __deepcopy__(self, memo):
        a_copy = WaveletSession(
            beta = self.beta,
            box = self._config.box,
            cut_off_radius = self._cut_off_radius,
            particle_type = self._particle_type,
            id_ = self._id)
        a_copy._config = spmc.SimulationConfig(self._config)
        from copy import deepcopy as dc
        a_copy._parameters = dc(self._parameters, memo)
        a_copy._reduced_units = self._reduced_units
        a_copy.default_library = dc(self.default_library, memo)
        
        #a_copy._library = dc(self._library, memo)
        a_copy._library = collections.defaultdict(a_copy.default_library)
        logger.debug("The library will not be copied.")

        a_copy.default_rod_partition = dc(self.default_rod_partition, memo)
        a_copy.callback_period = dc(self.callback_period)
        a_copy.callback = dc(self.callback)
        a_copy.use_library = dc(self.use_library, memo)
        #a_copy.x = dc(self.x, memo)
        return a_copy

    def __copy__(self):
        a_copy = WaveletSession(
            beta = self.beta,
            box = self._config.box,
            cut_off_radius = self._cut_off_radius,
            particle_type = self._particle_type,
            id_ = self._id)
        a_copy._config = self._config
        from copy import copy
        from copy import deepcopy
        a_copy._parameters = copy(self._parameters)
        a_copy._reduced_units = self._reduced_units
        a_copy.default_library = deepcopy(self.default_library)
        a_copy._library = copy(self._library)
        a_copy.default_rod_partition = copy(self.default_rod_partition)
        a_copy.callback_period = copy(self.callback_period)
        a_copy.callback = copy(self.callback)
        a_copy.use_library = copy(self.use_library)
        #a_copy.x = copy(self.x)
        return a_copy

    def id():
        return self._id

    def e(self):
        return self._e

    @property
    def config(self):
        return spmc.SimulationConfig(self._config)

    @config.setter
    def config(self, config):
        assert self._config.num_atoms == config.num_atoms
        assert self._config.num_bonds == config.num_bonds
        assert list(self._config.bonds()) == list(config.bonds())
        self._config = config

    def session(self):
        import wavelet.utils as utils
        session = utils.session_equilibration(
          N_e = pow(2, self._e),
          beta = self.beta,
          cut_off_radius = self._cut_off_radius,
          id_ = self._id,
          )
        session.reduced_units = self._reduced_units
        session.config = self._config

        if self.callback is not None:
            session.set_callback(self.callback_period, self.callback)
        return session

    def add_particles(self, n = 1, particle_type = None):
        if particle_type is None:
            particle_type = self._particle_type
        config = spmc.SimulationConfig()
        config.box = self._config.box
        config.num_atoms = n
        for i in range(n):
            p_i = config.particle(i)
            p_i.type = particle_type
            p_i.position = random_position(config)
        self._config.append(config)

    def add_molecule(self, R, origin = None, direction = None):
        import wavelet.utils as utils
        assert self._e == 0
        if origin is None:
            origin = random_position(self._config)
        if direction is None:
            direction = random_vector() - spmc.Tuple3(0.5)
        config_molecule = utils.generate_atomistic_linear_molecule(
            box = self._config.box,
            N_beads = R,
            origin = origin - R * direction,
            direction = direction,
            p_type = self._particle_type
            )
        self._config.append(config_molecule)

    def add_molecules(self, R, n):
        for i in range(n):
            self.add_molecule(R)

    def coarse_grain(self):
        import wavelet.utils as utils
        target_e = self._e + 1
        mapping = dict()
        config_cg = utils.coarse_grain(
            self._config, particle_type = self._particle_type, mapping = mapping)
        self._config = config_cg
        self._e = target_e
        return mapping

    def reconstruct(    self,
                        parameters = None, 
                        library = None,
                        save_config = None,
                        mapping = None,
                        ):
        from wavelet.reconstruction import WaveletReconstruction
        from spmc import utility
        if self._e < 1:
            msg = "Cannot reconstruct to N_e < 1!"
            raise ValueError(msg)
        target_e = self._e - 1
        if parameters is None and target_e in self._parameters:
            parameters = self._parameters[target_e]
        if library is None:
            library = self._library[target_e]
        if mapping is None:
            mapping = spmc.TagSetMap()
        ids = []
        if self._id is not None:
            ids += [self._id]
        ids += ['reconstruct', str(target_e)]
        wv_re = WaveletReconstruction(
            target_N_e = pow(2, target_e),
            parameters = parameters,
            library = library,
            id_ = '.'.join(ids),
            )
        wv_re.detect_equilibration = False
        wv_re.use_library = self.use_library
        wv_re.fast = True
        wv_re.callback_period = self.callback_period
        wv_re.callback = self.callback
        n = self._config.num_atoms
        rod_size = max(int(n / self.default_rod_partition), pow(2, 2))
        wv_re.library.default_rod_size = rod_size
        analysis = dict()
        analysis['rod_size_default'] = wv_re.library.default_rod_size
        config_re, evaluation_data = wv_re.reconstruct(
            self._config,
            mapping,
            save_config,
            analysis)
        QEsF = wv_re._QEsF
        initial_quality = QEsF.evaluate(evaluation_data[0])[0]
        #analysis['initial_quality'] = float(initial_quality)
        analysis['initial_quality'] = initial_quality
        msg = "Initial quality (e={e}, lib={lib}): {q}"
        logger.info(msg.format(
            e = target_e,
            lib = self.use_library,
            q = initial_quality))
        analysis['analysis'] = utility.convert_analyzer_data(wv_re.analyzers())
        self._config = config_re
        self._e = target_e
        return analysis

    def refit(self):
        from wavelet.reconstruction import WaveletReFit
        ids = []
        if self._id is not None:
            ids += [self._id]
        ids += ['refit', str(self._e)]
        refit = WaveletReFit(
            N_e = pow(2, self._e),
            id_ = '.'.join(ids),
            )
        parameters = None
        library = None
        try:
            parameters, library = refit.determine_parameters(self._config)
        except:
            #parameters = self.refit_default_parameters()
            raise
        else:
            if self._e not in self._library:
                self._library[self._e] = library
            self._parameters[self._e] = parameters
            logger.info("ReFit parameters (e={e}): {p}".format(e = self._e, p = parameters))
        return parameters, library

    def library(self):
        return self._library[self._e]

    def clear_library(self):
        del self._library[self._e]

    def clear_all_libraries(self):
        self._library.clear()
