from spmc import potentials, utility
import spmc.units as u
import libinterface as spmc
import logging, numpy, random

logger = logging.getLogger('potential')

POTENTIAL_DIR = 'potential'

def potential_filename(filename):
    from wavelet import PACKAGE_DIR
    import os
    return os.path.join(PACKAGE_DIR, POTENTIAL_DIR, filename)

class WaveletPotential(potentials.Potentials):

    def non_bonded(self, N_e, r_c = None, p_type = 'A'):
        R_g = rg_approximation(N_e)
        x_scale         = R_g
        y_scale_pp      = pow(N_e, - 0.19)
        pp_scale        = [x_scale, y_scale_pp]
        table_nb  = utility.parse_table_file(potential_filename('non_bonded.ff'))

        r_max = float(table_nb[-1][0]) * R_g
        if r_c is None:
          r_c = r_max
        else:
          r_c = min(r_c, r_max)
        skin = 0.1 * r_c

        tpf = self.tabulated_pairforce
        pp = tpf(p_type, p_type, r_c, utility.scaled_table(table_nb, pp_scale), 3, skin)
        return pp

    def neighbor(self, N_e, p_type = 'A'):
        R_g = rg_approximation(N_e)
        x_scale         = R_g
        y_scale_n       = 1.0
        n_scale         = [x_scale, y_scale_n]
        table_b12 = utility.parse_table_file(potential_filename('potential_1_2.ff'))
        table_b13 = utility.parse_table_file(potential_filename('potential_1_3.ff'))
        table_b14 = utility.parse_table_file(potential_filename('potential_1_4.ff'))

        tnf = self.tabulated_neighbor_force
        nb_12 = tnf(p_type, 0, 1, utility.scaled_table(table_b12, n_scale))
        nb_13 = tnf(p_type, 1, 1, utility.scaled_table(table_b13, n_scale))
        nb_14 = tnf(p_type, 2, 1, utility.scaled_table(table_b14, n_scale))
        return [nb_12, nb_13, nb_14]

    def harmonic(self, N_e, p_type = 'A', epsilon = 10, iterations = 1000):
        if N_e > 1:
            r_0 = average_bond_length(N_e)
            c_0 = 2000 * float(utility.parse_table_file(
                potential_filename('potential_1_2.ff'))[0][1])
        else:
            r_0 = 1.0
            c_0 = epsilon
        msg = "wavelet_harmonic: r_0 = {}, c_0={}".format(r_0, c_0)
        logger.debug(msg)
        return self.harmonic_neighbor_force(
            particle_type = p_type,
            skip = 0,
            steps = 1,
            r0 = r_0,
            c0 = c_0)
           # r0 = r_0 * u.D_u,
           # c0 = c_0 * (u.E_u / (u.D_u * u.D_u)))

    def lj(self, r_c = 2.5, p_type = 'A'):
        return self.lennard_jones_force(
            p_type, p_type,
            -1 * u.E_u,     # epsilon
            1 * u.D_u,      # sigma
            1,              # alpha
            r_c * u.D_u,    # r_cut
            3)              # exclude neighbors

    def atomistic(self, r_c = 1.0, p_type = 'A', epsilon = 1000):
        return self.hard_sphere_potential(
                p_type, p_type,
                r_c,      # r_cut
                epsilon,  # epsilon
                1)                # exclude bonded

    def all(self, N_e, r_c = None, p_type = 'A'):
        logger.debug("Return ff w/ N_e={}".format(N_e))
        if N_e < 1:
            msg = "Requested wavelet potential with N_e={}, but N_e must be greater or equal than 1."
            raise ValueError(msg.format(N_e))
        if N_e == 1:
            ff = [self.atomistic(r_c = 1.0, p_type = p_type)]
        else:
            ff = self.neighbor(N_e, p_type)
            ff.append(self.non_bonded(N_e, r_c, p_type))
        return ff


def rg_approximation(N_e):
    return 0.39 * pow(N_e, 0.601)

def bond_length_distribution():
    bld = numpy.array(utility.parse_table_file(potential_filename('bond_normalized.txt')))
    return bld[:, [1, 0]].astype(float).tolist()

def average_bond_length(N_e, iterations = 1000):
    if N_e > 1:
        bls = [0] * iterations
        bld = bond_length_distribution()
        for i in range(iterations):
            bls[i] = potentials.chose_from_distribution(bld, random.random())
        return numpy.average(bls) * rg_approximation(N_e)
    else:
        return 1

def chose_from_distribution(distribution, number):
    for row in distribution:
        if number < row[0]:
            return row[1]
