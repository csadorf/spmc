import vmd_style

def vdw_radius(e):
    return e + 1

def cpk_radius(e):
    return pow(2,(4.0/7)*e + 1)

class WaveletStyle(vmd_style.VMDStyle):
    
    def __init__(self, preview = False, e = 0):
        super(WaveletStyle, self).__init__(preview)
        self._e = e

    def _defaults(self, vmd):
        vmd.set_bg_color(8)
        vmd.show_axes(False)
        if not self._preview:
            vmd.shadows()
            vmd.ambientocclusion()

    def _mol_color(self):
        return 23

class WaveletDenseStyle(WaveletStyle):
    
    def __init__(self, preview = False, e = 0):
        super(WaveletDenseStyle, self).__init__(preview, e)
  
    def _mol_style(self):
        r_vdw = vdw_radius(self._e)
        resolution = 10 if self._preview else 50
        return 'VDW', [r_vdw, resolution]

class WaveletDiluteStyle(WaveletStyle):
    
    def __init__(self, preview = False, e = 0):
        super(WaveletDiluteStyle, self).__init__(preview, e)
  
    def _mol_style(self):
        r_cpk = cpk_radius(self._e)
        resolution = 10 if self._preview else 50
        return 'CPK', [r_cpk, r_cpk / 2, resolution, resolution]
