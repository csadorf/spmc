import libinterface as mdst
import logging
from wavelet.wavelet_session import WaveletSession

logger = logging.getLogger('wavelet_ligands')

def cross_product(a, b):
  return mdst.Tuple3(
      a.y * b.z - a.z * b.y,
      a.z * b.x - a.x * b.z,
      a.x * b.y - a.y * b.x)

def hesse_form(origin, a, b):
  n = cross_product(a, b) + origin
  n = n / abs(n)
  d = abs(origin)
  return n, d

def tether_surface_positions(origin, a, b, density):
  import math
  area = abs(cross_product(a, b))
  N = int(area * density)
  assert(N > 0)
  N_a = int(math.sqrt(N * abs(a) / abs(b)))
  N_b = int(N / N_a)
  #assert(N_a * N_b == N)
  positions = [mdst.Tuple3()] * N_a * N_b
  for i in range(N_a):
      for j in range(N_b):
          p = i * N_b + j
          f_a = float(i) / N_a
          f_b = float(j) / N_b
          positions[p] = f_a * a + f_b * b + origin
  return positions

def tether_surface(origin, a, b, density, box, ligand_type = 'T'):
    positions = tether_surface_positions(origin, a, b, density)
    config_surface = mdst.SimulationConfig()
    config_surface.box = box
    config_surface.num_atoms = N
    for i in range(N_a):
        for j in range(N_b):
            p = i * N_b + j
            particle = config_surface.particle(p)
            particle.type = ligand_type
            particle.position = positions[p]
            config_surface.lock_particle(p)
    return config_surface

def session_ligands(
        N_e = 1,
        beta = 1.0,
        cut_off_radius = None,
        id_ = ''):
    import wavelet.utils as utils
    session = utils.session_with_ff(N_e, beta, cut_off_radius, id_)
    translate_jiggle_move = utils.translate_jiggle_move(N_e, 1, session.reduced_units)
    session.add_move(translate_jiggle_move)
    return session

def session_ligands_equilibration(
        N_e = 1,
        beta = 1.0,
        cut_off_radius = None,
        id_ = ''):
    session = utils.session_basic(beta = beta, id_ = id_)
    ff = []
    moves = [utils.translate_jiggle_move(N_e, 1, session.reduced_units)]
    if N_e > 1:
        ff.extend(utils.wavelet_ff(N_e, session.reduced_units, cut_off_radius))
    else:
        wavelet_ff = wavelet_forcefield.WaveletForces(session.reduced_units)
        ff.extend([
          wavelet_ff.harmonic(N_e = 1),
          wavelet_ff.atomistic_lj()])
        moves.extend(utils.moves_reconstruction(N_e, session.reduced_units))
    for f in ff:
        session.add_forcefield(f)
    for move in moves:
        session.add_move(move)
    return session

class WaveletLigandsSession(WaveletSession):

    def __init__( self, beta, box, cut_off_radius = None, 
                  particle_type = 'A', id_ = ''):
        super(WaveletLigandsSession, self).__init__(
            beta, box, cut_off_radius, particle_type, id_)

        self._adhesion_spots = dict()
        self._excluded_volumes = set()

    def session(self, equilibration = False):
        import forcefield
        from mds_units import u
        from wavelet import wavelet_forcefield
        s_generator = session_ligands_equilibration if equilibration else session_ligands
        session = s_generator(
          N_e = pow(2, self._e),
          beta = self.beta,
          cut_off_radius = self._cut_off_radius,
          id_ = self._id)
        session.reduced_units = self._reduced_units
        session.config = self._config
        if self._e > 0:
            abl = wavelet_forcefield.average_bond_length(pow(2, self._e))
        else:
            abl = 0.0
        box = session.config.box
        hpbp = forcefield.Forces(session.reduced_units).harmonic_pseudo_bond_potential(
          particle_type = self._particle_type,
          r_0 = abl / 2 * u.D_u,
          cut_off = max((box.x, box.y, box.z)),
          epsilon = 10000 * (u.E_u / (u.D_u * u.D_u)))
        #logger.debug("ADH: session(): {}".format(self._adhesion_spots))
        for tag, spot in self._adhesion_spots.items():
            hpbp.add_pseudo_bond(tag, spot)
        session.add_forcefield(hpbp)

        for excluded_volume in self._excluded_volumes:
            session.add_forcefield(excluded_volume)
        #self._check_adh()

        if self.dumping:
            session.add_dumper("_{}_restart_e_{}_#.xml".format(self._id, self._e), 'xml', 1000)
            session.add_dumper("_{}_dump_e_{}.xyz".format(self._id, self._e), 'xyz_trajectory', 100)
            
            
        return session

    def _check_adh(self, threshold = 100):
        for tag, spot in self._adhesion_spots.items():
            pos = self._config.particle(tag).remapped_position()
            d = abs(pos - spot)
            if d > threshold:
                s = mdst.Session()
                s.config = self._config
                s.write("_{}_DEVIATION!.xml".format(self._id), 'hoomd_blue_xml')
                msg = "Position of particle {tag} deviates from adhesion spot!" +\
                    " Particle's position: {pos}, adhesion spot: {spot}. Distance: {d}."
                raise RuntimeWarning(msg.format(tag = tag, spot = spot, pos = pos, d = d))


    def add_planar_tether_surface(self, N_b, origin, a, b, density, v_z = None, ligand_type = 'A'):
        import wavelet.utils as utils
        import forcefield
        from mds_units import u
        assert self._e == 0
        if v_z is None:
            v_z = N_b
        else:
            if v_z < N_b:
                raise ValueError("v_z value {} too small!".format(v_z))
        tether_positions = tether_surface_positions(
          origin = origin, a = a, b = b, density = density)
        grow_direction = cross_product(a, b)
        grow_direction /= abs(grow_direction)
        logger.debug("origin = {}".format(origin))
        logger.debug("grow direction = {}".format(grow_direction))
        logger.debug("box = {}".format(self._config.box))
        excl_vol_dfo = origin * grow_direction - v_z
        logger.debug("distance from origin = {}".format(excl_vol_dfo))
        logger.debug("cut off = {}".format(v_z - 0.01))
        for tether_position in tether_positions:
            config_ligand = utils.generate_atomistic_linear_molecule(
                box = self._config.box,
                origin = tether_position,
                direction = grow_direction,
                N_beads = N_b,
                p_type = ligand_type,
                )
            logger.debug("tether position: {}".format(config_ligand.particle(0).remapped_position()))
            next_tag = self._config.num_atoms
            self._adhesion_spots[next_tag] = tether_position
            new_ligand = self._config.append(config_ligand)
            if abs(self._config.particle(next_tag).position - self._adhesion_spots[next_tag]) > 0.01:
                msg = "Bad positioning: {} != {}"
                logger.error("Box: {}".format(self._config.box))
                raise RuntimeError(msg.format(
                    self._config.particle(next_tag).position, self._adhesion_spots[next_tag]))
            #logger.debug("ADH: add_planar_tether_surface(): {}".format(self._adhesion_spots))

        self._excluded_volumes.add(forcefield.Forces(self._reduced_units).hard_field_potential(
            particle_type = ligand_type,
            distance_from_origin = origin * grow_direction - v_z,
            normal_vector = grow_direction,
            cut_off = v_z - 0.01,
            epsilon = 10000 * u.E_u,
            ))

    def coarse_grain(self):
        mapping = super(WaveletLigandsSession, self).coarse_grain()
        new_adh_spots = dict()
        for tag, spot in self._adhesion_spots.items():
            new_adh_spots[mapping[tag]] = spot
        self._adhesion_spots = new_adh_spots
        #logger.debug("ADH: coarse_grain(): {}".format(self._adhesion_spots))

    def reconstruct(self, parameters = None, library = None, configs = None, mapping = None):
        if mapping is None:
            mapping = mdst.TagSetMap()
        analysis = super(WaveletLigandsSession, self).reconstruct(
            parameters, library, configs, mapping)

        new_adh_spots = dict()
        for tag, spot in self._adhesion_spots.items():
            new_tag = list(mapping[tag])[0]
            new_adh_spots[new_tag] = spot
        self._adhesion_spots = new_adh_spots
        return analysis
