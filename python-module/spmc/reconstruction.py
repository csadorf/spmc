import libinterface as spmc
from libanalysis import Analysis
#import evaluation_record as EvaluationRecord
from .evaluation_record import EvaluationRecord
from . import utility
import numpy as np
from scipy.optimize import leastsq

DEFAULT_STORAGE_SIZE              = 5
DEFAULT_NUM_SAMPLES               = 5

DEFAULT_CYCLES_MAX                = 1000
DEFAULT_CYCLES_EQUILIBRATION      = 1000
DEFAULT_CYCLES_SOFT_EQUILIBRATION = 100

FILENAME_CONFIG_BEFORE_IMPROVEMENT  = "_{}_before_improvement.xml"
FILENAME_CONFIG_AFTER_IMPROVEMENT   = "_{}_after_improvement.xml"
FILENAME_CONFIG_NO_IMPROVEMENT      = "_{}_no_improvement.xml"

import logging
logger = logging.getLogger('reconstruction')

def check_transformation(config_cg, config_at, cg_method, tolerance = 1e-8, id = None):
    import uuid
    config_cg_compare = cg_method(config_at)
    rms = Analysis.mean_squared_distance(config_cg, config_cg_compare)
    if rms > tolerance:
        if id is None:
            id = str(uuid.uuid4())
        msg = "Invalid transformation! States deviate by {rms} (toleranec: {tolerance}. "
        msg += "Configs written to hdd, id = {id}."
        filename = "_invalid_transformation_{id}_{rms}_{tolerance}_{{type}}.xml".format(
          id = id,
          rms = rms,
          tolerance = tolerance,
          )
        s = spmc.Session()
        s.config = config_cg
        s.write(filename.format(type = 'k+1_original'), 'hoomd_blue_xml')
        s.config = config_cg_compare
        s.write(filename.format(type = 'k+1_from_reverse_mapped'), 'hoomd_blue_xml')
        s.config = config_at
        s.write(filename.format(type = 'k'), 'hoomd_blue_xml')
        logger.warning(msg.format(
          rms = rms,
          tolerance = tolerance,
          id = id))
        return id
    else:
        return None

def pe_evaluation(session, molecule):
    pe_analyzer = session.add_analyzer('potential_energy')
    data = pe_analyzer.data()
    if len(data):
        return data[-1]
    else:
        return Analysis.potential_energy(session, molecule)

def pe_molecule_evaluation(session, molecule):
    return Analysis.potential_energy(session, molecule)

def pe_inter_evaluation(session, molecule):
    pe_inter_analyzer = session.add_analyzer('pe_inter')
    data = list(pe_inter_analyzer.data())
    if len(data):
        return np.average(data)
    else:
        return Analysis.inter_molecular_potential_energy(session, molecule)

def pe_intra_evaluation(session, molecule):
    pe_intra_analyzer = session.add_analyzer('pe_intra')
    data = list(pe_intra_analyzer.data())
    if len(data):
        return np.average(data)
    else:
        return Analysis.intra_molecular_potential_energy(session, molecule)

def num_atoms_evaluation(session, molecule):
    return session.config.num_atoms

def average_bond_lengths_evaluation(session, molecule):
    bond_lengths = utility.bond_lengths(session.config, molecule)
    return sum(bond_lengths) / len(bond_lengths)

def osmotic_pressure_evaluation(session, molecule):
    return Analysis.osmotic_pressure(session)

def evaluation_functions():
    return [    
        pe_evaluation, 
        pe_molecule_evaluation,
        #pe_intra_evaluation,
        average_bond_lengths_evaluation, 
        ]

def mapped_molecule(molecule, mapping):
    mapped = []
    try:
        for p in molecule:
            mapped.extend(mapping[p])
    except KeyError as e:
        print(mapping)
        raise
    result = spmc.TagSet()
    for p in mapped:
        result.add(p)
    return result

def block_sample(session,
                 molecule,
                 evaluation_record,
                 cycles, 
                 num_samples = DEFAULT_NUM_SAMPLES):
    if session.status() != 1:
        try:
            session.run(cycles, molecule)
            evaluation_record.evaluate(session, molecule)
        except RuntimeError as e:
            msg = "Error while executing convergence. Step: {}. {}"
            logger.fatal(msg.format(session.current_step(), e))
            raise
    else:
        raise RuntimeError

class QualityFunct(object):
    def evaluation_record(self):
        raise NotImplementedError("Abstract base class.")

    def evaluate(self, record):
        raise NotImplementedError("Abstract base class.")

class BasicQualityFunct(QualityFunct):

    def evaluation_record(self):
        return EvaluationRecord(evaluation_functions())

class BasicQEsF(BasicQualityFunct):

    def __init__(self, parameters = None):
        if parameters is None:
            self._parameters = self.default_parameters()
        else:
            self._parameters = parameters

    def evaluate(self, record):
        x = record.data
        p = self._parameters
        try:
            alpha = np.divide(x[:,1], x[:,0])
            delta = [a - p[1] for a in alpha]
            ret = np.array(p[0] * np.power(delta, 2))
            return ret

        except TypeError as e:
            print(x)
            print(p)
            logger.error(e)
            raise

    def parameters(self):
        return self._parameters

    def number_of_parameters(self):
        return 2

    def default_parameters(self):
        return [1] * self.number_of_parameters()

class AthermalQEsF(BasicQualityFunct):
    
    def __init__(self, parameters = None):
        if parameters is None:
            self._parameters = self.default_parameters()
        else:
            self._parameters = parameters

    def evaluate(self, record):
        x = record.data
        p = self._parameters
        abl = x[:,2]
        phi_0 = np.power(abl - 1, 2)
        ret = np.divide(phi_0, p[0] - 1)
        return ret

    def parameters(self):
        return self._parameters

    def number_of_parameters(self):
        return 1

    def default_parameters(self):
        return [2]

class BasicQEvF(BasicQualityFunct):

    def __init__(self, session, config_at):
        self.evaluate = self._make_evaluator(session, config_at)

    def _make_evaluator(self, session, config_at):
        s_at = session
        s_at.config = config_at
        pe_at = Analysis.potential_energy(s_at)
        def QEvF(record):
            x = record.data
            molecule_indeces = [list(molecule)[0] for molecule in record.molecules]
            pe_at_sm = [
                Analysis.potential_energy(s_at, s_at.config.property.molecule(i)) \
                    for i in molecule_indeces]
            beta = np.divide(x[:,1], x[:,0])
            alpha = np.divide(pe_at_sm, pe_at)
            delta = beta - alpha
            phi_0 = np.power(delta, 2)
            phi_1 = np.divide(phi_0, np.average(phi_0))
            return phi_1
        return QEvF

class AthermalQEvF(BasicQualityFunct):
    
    def __init__(self, session, config_at):
        self.evaluate = self._make_evaluator(session, config_at)

    def _make_evaluator(self, session, config_at):
        s_at = session
        s_at.config = config_at
        def QEvF(record):
            x = record.data
            abl = x[:,2]
            phi_0 = np.power(abl - 1, 2)
            ret = np.divide(phi_0, np.average(phi_0))
            return ret
        return QEvF

class Reconstruction(object):

    def __init__(self, id_):
        self._logger = logging.getLogger('Reconstruction')
        self._id = id_
        self._target_session = None
        self._target_session_soft = None
        self._reverse_map = None
        self._QEsF = None
        self._library = None

        self._cycles_max = DEFAULT_CYCLES_MAX
        self._cycles = DEFAULT_CYCLES_EQUILIBRATION
        self._cycles_soft = DEFAULT_CYCLES_SOFT_EQUILIBRATION
        self._storage_size = DEFAULT_STORAGE_SIZE
        self._pre_equilibration = False
        self._use_library = True
        self.fast = False
        self.detect_equilibration = False
        self.write_all_configs = False
        self.write_format = 'hoomd_blue_xml'

    def _id(self, custom = None):
        if custom is None:
            return self._id
        else:
            return "{}_{}".format(self._id, custom)

    def session(self):
        return self._target_session

    def QEsF(self):
        return self._QEsF

    @property
    def library(self):
        return self._library

    @library.setter
    def library(self, value):
        self._library = value

    @property
    def use_library(self):
        return self._use_library

    @use_library.setter
    def use_library(self, value):
        self._use_library = bool(value)

    def _add_equilibration_fix(self, session):
        if self.detect_equilibration:
            session.add_fix(
                spmc.DetectEquilibrationFix(
                    threshold = 0.01,
                    num_blocks = 4))

    def _converge_molecule(
        self,
        session,
        molecule,
        evaluation_record,
        cycles_max):
        from .config_storage import ConfigStorage, ConfigContainerMemory, ConfigContainerHDD
        #logger.warning("Ignoring by molecule approach!")
        session.clear_status()
        interval = max(1, int(cycles_max / self._storage_size / 2))
        evaluation_record.evaluate(session, molecule)
        quality = float(self._QEsF.evaluate(evaluation_record[-1]))
        try:
            with ConfigStorage(ConfigContainerHDD) as candidate_storage:
            #with ConfigStorage(ConfigContainerMemory) as candidate_storage:
                logger.debug(candidate_storage)
                candidate_storage[quality] = session.config
                step_null = session.current_step()
                while (len(candidate_storage) < self._storage_size):
                    if session.current_step() >= self._cycles_max:
                        break
                    if session.current_step() - step_null >= cycles_max:
                        break
                    if quality <= 1:
                        if self.fast:
                            break
                    block_sample(session, molecule, evaluation_record, interval)
                    #quality = float(np.average(self._QEsF.evaluate(last_record)))
                    quality = float(np.average(self._QEsF.evaluate(evaluation_record[-1])))
                    candidate_storage[quality] = session.config
                quality_min = sorted(candidate_storage)[0]
                if quality_min <= 1:
                    msg = "Found good candidate: quality = {}"
                else:
                    msg = "Found candidate: quality = {}"
                logger.debug(msg.format(quality_min))
                session.config = candidate_storage[quality_min]
                return quality_min
        except IOError as error:
            msg = "IOError during convergence process. {error}"
            logger.error(msg.format(error = error))
        finally:
            return quality

    def _converge(
        self,
        session,
        mapping,
        evaluation_record,
        config_cg):
        molecules = config_cg.property.molecules()
        quality = dict()


        msg_mol = "Converge molecule {}/{}"
        msg_it = "Converge iteration {}/{}"
        c_iterations = 5
        for c_i in range(c_iterations):
            logger.debug(msg_it.format(c_i + 1, c_iterations))
            logger.info("Equilibration for {} cycles...".format(self._cycles))
            session.run(self._cycles)
            logger.info("Converging molecules...")
            for i, cg_molecule in enumerate(molecules):
                #if quality.has_key(i):
                #    if quality[i] <= 1:
                #        continue
                self._logger.debug(msg_mol.format(i + 1, len(molecules)))
                quality[i] = self._converge_molecule(
                    session,
                    mapped_molecule(cg_molecule, mapping),
                    evaluation_record,
                    int(self._cycles_max / len(molecules)),
                    )
            if all(q <= 1 for q in quality.values()):
                break
        if self._library is not None:
            self._library.learn_from(config_cg, session.config, mapping)
            msg = "New library size: {}"
            logger.debug(msg.format(self._library.size()))
        return quality

    def _init(self):
        pass
        # Implement this function to perform necessary 
        # preprocessing prior reconstruction.

    def _library_post_process(self, config_cg, config_at, mapping):
        pass
        # Implement this function to perform post
        # processing after library improvement attempt!

    def _post_process(self, config_cg, config_at, mapping):
        pass
        # Implement this function to perform post
        # processing after reconstruction attempt!

    def reconstruct(    self,
                        config_cg, 
                        mapping = spmc.TagSetMap(), 
                        save_config = None, # callable of the form save_config(key_str, config)
                        analysis = None,
                        ):
        self._logger.info("Reconstructing...")
        self._init()

        from .config_storage import ConfigStorage, ConfigContainerMemory, ConfigContainerHDD
        with ConfigStorage(ConfigContainerHDD) as config_storage:
            def save(name, config = None):
                if config is None:
                    config = target_session.config
                config_storage[name] = config
                if self.write_all_configs:
                    filename = "_{}_config_{}.xml".format(self._id, name)
                    utility.write(config, filename)
                if save_config is not None:
                    save_config('{}_{}'.format(self._id, name), config)

            save('config_cg', config_cg)

            target_session = spmc.Session(
                self._target_session, self._id)
            mapping_dict = dict()
            target_session.config = self._reverse_map(config_cg, mapping, mapping_dict)
            save('reverse_mapped')

            evaluation_record = self._QEsF.evaluation_record()
            self._add_equilibration_fix(target_session)
          
            if self._use_library and self._library is not None:
                logger.debug("Improving with library '{}'...".format(self._library))
                try:
                    self._library.improve(config_cg, target_session.config, mapping)
                except RuntimeWarning as warning:
                    logger.warning(warning)
                save('improved')
                logger.debug("execute lib_post_processing...")
                self._library_post_process(config_cg, target_session.config, mapping_dict)
                save('lib_post_processed')

            self._logger.debug("Initializing target session...")
            target_session.init()

            if self._pre_equilibration:
                if self._target_session_soft is not None:
                    target_session_soft = spmc.Session(
                        self._target_session_soft, 
                        id_ = self._id("soft_equilibration"))
                    target_session_soft.config = target_session.config
                    self._add_equilibration_fix(target_session_soft)
                    target_session_soft.run(self._cycles_soft)
                    target_session.config = target_session_soft.config
                    save('soft_equilibrated')
                else:
                    logger.debug("Pre equilibration...")
                    target_session.run(self._cycles)

            quality = self._converge(target_session, mapping, evaluation_record, config_cg)
            if analysis is not None:
                analysis['quality'] = quality
                analysis['steps'] = target_session.current_step()
            save('reconstructed')
            logger.debug("execute post processing...")
            self._post_process(config_cg, target_session.config, mapping_dict)
            save('post_processed')
            if analysis is not None:
                rmsd = Analysis.mean_squared_distance
                cs = config_storage
                config_re = target_session.config
                rmsd_d = dict()
                rmsd_d['naive'] = rmsd(cs['reverse_mapped'], config_re)
                try:
                    rmsd_d['improved'] = rmsd(cs['improved'], config_re)
                    rmsd_d['pp'] = rmsd(cs['lib_post_processed'], config_re)
                except KeyError as error:
                    pass
                analysis['rmsd'] = rmsd_d

        logger.info("Reconstruction complete.")
        return target_session.config, evaluation_record

class InsufficientDataError(ValueError):
    pass

class BadFitWarning(RuntimeWarning):
    pass

def plot_refit(y, y_estimated, filename = '_refit.pdf'):
    return
    from matplotlib import pyplot as plt
    from . import utility
    plt.plot(
        y,
        y_estimated,
        label = "{}".format(utility.utc_timestamp()),
        )
    plt.savefig(filename)

def fit_QEsF_to_QEvF(QEvF, QEsF, parameters, record):
    if len(record) < len(parameters):
        msg = "Number of datapoints ({}) is not sufficient for number of parameters ({})."
        raise InsufficientDataError(msg.format(len(record), len(parameters)))
    x = QEsF(parameters).evaluate(record)
    y = QEvF.evaluate(record)
    p, cov, infodict, mesg, ier = leastsq(
        lambda p, y, x: y - QEsF(p).evaluate(x),
        parameters,
        args=(QEvF.evaluate(record), record),
        full_output = True)
    parameters = p.tolist()

    y_ = QEsF(parameters).evaluate(record)

    # Check convergence status
    if not ier in (1,2,3,4):
        plot_refit(y, y_)
        raise BadFitWarning("No convergence.")

    # Goodness of fit evaluation:
    # http://en.wikipedia.org/wiki/Goodness_of_fit
    ss_err = (infodict['fvec']**2).sum()
    ss_tot = ((y - y.mean())**2).sum()
    r_squared = 1 - (ss_err / ss_tot)
    #if (r_squared != 1.0):
    if (abs(r_squared) < 0.1):
        msg = "Bad fit! R^2 = {} (!= 1.0) parameters = {}"
        plot_refit(y, y_)
        raise BadFitWarning(msg.format(r_squared, parameters))

    return parameters

class ReFitException(Exception):
    pass

class ReFit(object):

    class BadFitWarning(RuntimeWarning, ReFitException):
        pass

    class InsufficientDataError(ValueError, ReFitException):
        pass

    def __init__(self):
        self._reconstruction = None
        self._QEvF = None
        self._coarse_grain = None

    def determine_parameters(self, config_at, config_cg = None, parameters = None):
        if parameters is None:
            parameters = self._reconstruction.QEsF().default_parameters()
        if config_cg is None:
            config_cg = self._coarse_grain(config_at)
        config, evaluation_record = self._reconstruction.reconstruct(config_cg)
        QEvF = self._QEvF(self._reconstruction.session(), config_at)
        try:
            new_parameters = fit_QEsF_to_QEvF(
                self._QEvF(self._reconstruction.session(), config_at),
                type(self._reconstruction.QEsF()),
                parameters,
                evaluation_record)
        except InsufficientDataError as e:
            raise
            #raise ReFit.InsufficientDataError(e)
        except BadFitWarning as e:
            raise
            #raise ReFit.BadFitWarning(e)
        else:
            return new_parameters, self._reconstruction.library

    def determine_QEsF(self, config_at, config_cg, parameters = None):
        return type(self._reconstruction.QEsF())(
            parameters = self.determine_parameters(config_at, config_cg, parameters))
