import libinterface as spmc

class DeveloperSimulationConfig(object):

    def __init__(self):
        self._config = spmc.SimulationConfig()

    def __enter__(self):
        self._config.developer_mode = True
        return self._config

    def __exit__(self, exception_type, exception_value, traceback):
        self._config.developer_mode = False
