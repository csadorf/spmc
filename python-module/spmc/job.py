import logging
logger = logging.getLogger('spmc_job')

from sim_tools.job_management import Job

def get_config_xml(config, target):
    import spmc
    from tempfile import NamedTemporaryFile
    with NamedTemporaryFile() as file:
        s = spmc.Session()
        s.config = config
        s.write(file.name, 'hoomd_blue_xml')
        file.seek(0)
        target.write(file.read())
    return target

def get_config_from_xml(xml):
    import spmc
    from tempfile import NamedTemporaryFile
    with NamedTemporaryFile(suffix = '.xml') as file:
        s = spmc.Session()
        file.write(xml)
        file.seek(0)
        s.read(file.name)
        return s.config

class SPMCJob(Job):
    
    def __init__(self, parameters, target = None, _id = None, compression = True):
        super(SPMCJob, self).__init__(
            parameters = parameters,
            target = target,
            _id = _id)
        self._compression = compression

    def _config_key(self, key):
        return "config.{}.simulation_config".format(key)

    def save_config(self, key, config):
        from sim_tools.simulation_config import SimulationConfig
        sc = SimulationConfig.from_spmc_config(config)
        scd = sc.to_dict()
        self._save_secondary(self._config_key(key), scd)

    def get_config(self, key):
        from sim_tools.simulation_config import SimulationConfig
        sc_dict = self._get_secondary(self._config_key(key))
        sc = SimulationConfig.from_dict(sc_dict)
        return sc.to_spmc_config()

    def _snapshot_key(self, key, step):
        return "sampling.{}.{}.snapshot".format(key, str(step))

    def save_snapshot(self, key, step, snapshot_binary):
        self._save_binary(
            self._snapshot_key(key, step),
            snapshot_binary)

    def get_snapshot(self, key, step):
        from .config_storage import BinarySnapshot
        config = self.get_config(key)
        binary = BinarySnapshot(config, compression = True)
        snapshot_binary = self._get_binary(self._snapshot_key(key, step))
        binary.read(snapshot_binary)
        return config

    def get_snapshots(self, key, steps, ignore_missing = False):
        from .config_storage import BinarySnapshot
        config = self.get_config(key)
        binary = BinarySnapshot(config, compression = True)
        for step in steps:
            try:
                snapshot_binary = self._get_binary(self._snapshot_key(key, step))
            except KeyError as error:
                if not ignore_missing:
                    msg = "No snapshot at step: {}"
                    raise KeyError(msg.format(step))
            else:
                binary.read(snapshot_binary)
                yield config

    def get_thermo_callback(self, key):
        def callback(step, freq, data):
            for k, v in data.items():
                self._set('{}.{}.{}'.format(key, k, str(step)), v)
            #self._set('{}.{}'.format(key, str(step)), data)
        return callback

    def get_snapshot_callback(self, key):
        from .config_storage import BinarySnapshot
        def callback(step, period, session):
            if callback.first_call:
                self.save_config(key, session.config)
                callback.first_call = False
            binary = BinarySnapshot(session.config, compression = True)
            self.save_snapshot(key, step, binary.dump())
        callback.first_call = True
        return callback

def main():
    from sim_tools.job_management import MongoDBJob
    logging.basicConfig(level = logging.DEBUG)
    parameters = {
# These parameters are close to the break-even point, where the retrieval of all snapshots
# costs just as much as the sampling itself. 
# I tested storing and retrieving for up to N=10e6.
        'N':        1000,
        'steps':    1000,
    }
    
    from pymongo import MongoClient
    client = MongoClient()
    db = client['testing']
    mc = db['spmc_job']
    mc_snapshots = db['spmc_job_snapshots']
    mc.remove()
    mc_snapshots.remove()

    class TestJob(MongoDBJob, SPMCJob):
        
        def run(self):
            from sim_tools.simulation_config import generate_test_config
            import spmc, spmc.potentials
            import random

            session = spmc.Session()
            c1 = generate_test_config(self.parameters()['N'])
            session.config = c1
            session.add_move(spmc.RandomDisplacementMove(1))
            session.add_move(spmc.TranslateJiggleMove(3))
            session.add_move(spmc.TranslateMoleculeMove(7))
            potentials = spmc.potentials.Potentials(session.reduced_units)
            session.add_potential(potentials.harmonic_neighbor_force(
                'A', 0, 1, 0.01, 10))
            session.simulator = spmc.MonteCarloNVTSampler(1.0)
            session.add_analyzer('potential_energy')
            callback = self.get_thermo_callback('thermo')
            session.set_analyzer_callback(callback)
            session.analyzer_frequency = 10
            snapshot_callback = self.get_snapshot_callback('thermo')
            session.set_callback(1, snapshot_callback)
            session.random_seed(int(random.random() * 100000))
            session.run(self.parameters()['steps'])

        @staticmethod
        def analyze(collection, collection_secondary):
            print('analyzing...')
            query = {}
            documents = list(collection.find(query))
            print('num of docs: {}'.format(len(documents)))
            for document in documents:
                _id = document['_id']
                with TestJob(parameters = {}, collection = collection, _id = _id) as job:
                    job.set_secondary_collection(collection_secondary)

                    import time
                    start = time.time()
                    num_steps = job.parameters()['steps']
                    steps = list(range(0, num_steps, 1))
                    snapshots = job.get_snapshots('thermo', steps)
                    for snapshot in snapshots:
                        n = snapshot.num_atoms
                    t_delta = time.time() - start
                    logger.debug("Time per sample: {} s".format(t_delta / len(steps)))
                    logger.debug("Time per steps: {} s".format(t_delta / num_steps))

    with TestJob(mc, parameters) as job:
        job.set_secondary_collection(mc_snapshots)
        import time
        start = time.time()
        job.start()
        t_delta = time.time() - start
        logger.debug("Time per sample: {} s".format(t_delta / parameters['steps']))

    TestJob.analyze(mc, mc_snapshots)

if __name__ == '__main__':
    main()
