#!/usr/bin/env python
import logging
import libinterface as spmc
from libanalysis import Analysis
from math import sqrt

logger = logging.getLogger('analysis')

def bond_lengths(config):
    result = []
    molecules = config.property.molecules()
    for molecule in molecules:
        result.extend(bond_lengths(config, molecule))
    return result

def bond_lengths(config, molecule = None):
    if molecule is None:
        molecules = config.property.molecules()
    else:
        molecules = [molecule]
    result = []
    for molecule in molecules:
        mol_list = list(molecule)
        for p in mol_list[1:]:
            p1 = config.particle(p - 1)
            p2 = config.particle(p)
            result.append(abs(p2.remapped_position() - p1.remapped_position()))
    return result

def bond_lengths_average(config, molecule = None):
    bl = bond_lengths(config, molecule)
    return sum(bl) / len(bl)

def center_of_mass(config, subset):
    M = 0.0
    R = spmc.Tuple3(0)
    for s in subset:
        p = config.particle(s)
        M += p.mass
        R += p.mass * p.remapped_position()
    return R / M

def centroid(config, subset):
    R = spmc.Tuple3(0)
    for s in subset:
        R += config.particle(s).remapped_position()
    return R / len(subset)

def set_mass(config, mass):
    for p in config.all():
        config.particle(p).mass = mass

def set_particle_type(config, particle_type):
    for p in config.all():
        config.particle(p).type = particle_type;

def radius_of_gyration_molecule(config, molecule):
    rg = 0
    r_sum = spmc.Tuple3(0)
    for p in molecule:
        r_sum += config.particle(p).remapped_position()
    r_mean = r_sum / len(molecule)
    for p in molecule:
        delta = abs(config.particle(p).remapped_position() - r_mean)
        rg += pow(delta, 2)
    return sqrt(rg / len(molecule))

def radius_of_gyration(config):
    molecules = config.property.molecules()
    rg = 0.0
    for molecule in molecules:
        rg += radius_of_gyration_molecule(config, molecule)
    return rg / len(molecules)

def mean_squared_distance(config_a, config_b):
    return Analysis.mean_squared_distance(config_a, config_b)

def potential_energy(session, molecule = None):
    if molecule is None:
        molecule = session.config.all()
    pe = Analysis.potential_energy(session, molecule)
#    print "pe_overall={}".format(Analysis.potential_energy(session))
#    print "pe={}".format(pe)
    return pe

def osmotic_pressure(session, steps = 100):
  try:
    osm = Analysis.osmotic_pressure(session)
  except RuntimeError:
    session.run(steps)
    osm = Analysis.osmotic_pressure(session)
  return osm

def linear_auto_correlation(x):
    import numpy as np
    split = int(0.5 * len(x))
    x_a = x[:split]
    x_b = x[len(x_a):2 * len(x_a)]
    y = np.array((x_a, x_b))
    cov = np.cov(y)[0, 1]
    return cov / (np.sqrt(np.var(x_a)) * np.sqrt(np.var(x_b)))

