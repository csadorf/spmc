import spmc
import collections

import logging
logger = logging.getLogger('config_storage')

def unique_filename_(ext = 'tmp'):
    import uuid, os.path
    fn = "_{id}.{ext}".format(
      id = str(uuid.uuid4()),
      ext = ext)
    if os.path.isfile(fn):
        msg = "Unable to generate unique filename. "
        msg += "File '{fn}' already exists."
        raise RuntimeError(msg.format(fn = fn))
    else:
        return fn

def unique_filename(ext = 'tmp', max_trials = 3):
    for i in range(max_trials):
        try:
            return unique_filename_(ext)
        except RuntimeError as error:
            raise
            continue
    raise error

class Fileformat(object):
    def __init__(self, format, ext):
        if not format in spmc.Session.dump_available_formats():
            raise ValueError(format)
        self.format = format
        self.ext = ext

class ConfigContainer(object):
    
    def __init__(self, config):
        self._released = True
        self.store(config)

    def _write(self, config):
        raise NotImplementedError('Pure virtual class')

    def store(self, config):
        self._write(config)
        self._released = False

    def _read(self):
        raise NotImplementedError('Pure virtual class.')

    def _release(self):
        raise NotImplementedError('Pure virtual class.')

    def __del__(self):
        self.release()

    def __call__(self):
        if self._released:
            return None
        else:
            return self._read()

    def release(self):
        if not self._released:
            self._release()
        self._released = True

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_val, trace):
        self.release()
        return False

    @staticmethod
    def supported_formats():
        return spmc.Session.dump_available_formats()

class ConfigContainerMemory(ConfigContainer):

    def __init__(self, config):
        self._config = None
        super(type(self), self).__init__(config)

    def _write(self, config):
        self._config = spmc.SimulationConfig(config)

    def _read(self):
        copy = spmc.SimulationConfig(self._config)
        return copy

    def _release(self):
        self._config = None

def dump_reduced_positions(config, filename):
    import struct
    format = '3d'
    with open(filename, 'wb') as file:
        file.write(struct.pack(
          format, config.box.x, config.box.y, config.box.z))
        for p in config.all():
            pos = config.particle(p).reduced_position()
            file.write(
              struct.pack(format, pos.x, pos.y, pos.z))

def read_reduced_positions(config, filename):
    import struct
    format = '3d'
    chunk_size = len(struct.pack(format, 0.0, 0.0, 0.0))
    with open(filename, 'rb') as file:
        config.box = spmc.Tuple3(* struct.unpack(format, file.read(chunk_size)))
        for p in config.all():
            v = struct.unpack(format, file.read(chunk_size))
            config.particle(p).set_reduced_position(spmc.Tuple3(
              * v))

class BinarySnapshot(object):

    def __init__(self, config, compression = False):
        self._config = config
        self._compression = compression

    def dump(self):
        config = self._config
        import struct
        format = '3d'
        binary = bytes()
        binary += struct.pack(
            format, config.box.x, config.box.y, config.box.z)
        for p in config.all():
            pos = config.particle(p).reduced_position()
            im = config.particle(p).image
            binary += \
                struct.pack(format, pos.x, pos.y, pos.z)
            binary += \
                struct.pack(format, im.x, im.y, im.z)
        if self._compression:
            import zlib
            #logger.debug("Compressing ({})...".format(len(binary)))
            return zlib.compress(binary)
        else:
            return binary

    def read(self, binary):
        if self._compression:
            import zlib
            #logger.debug("Decompressing ({})...".format(len(binary)))
            binary = zlib.decompress(binary)
            #logger.debug("Finished decompressing.")
        config = self._config
        import struct
        format = '3d'
        s_chunk = len(struct.pack(format, 0.0, 0.0, 0.0))
        config.box = spmc.Tuple3(* struct.unpack(format, binary[:s_chunk]))
        for i, p in enumerate(config.all()):
            v_pos = struct.unpack(
                format, binary[(2*i +1) * s_chunk:(2*i + 2) * s_chunk])
            v_im = struct.unpack(
                format, binary[(2*i +2) * s_chunk:(2*i + 3) * s_chunk])
            config.particle(p).set_reduced_position(spmc.Tuple3(* v_pos))
            config.particle(p).image = spmc.Tuple3(*v_im)

class ConfigContainerBinary(ConfigContainer):
    
    def __init__(self, config):
        self._config = None
        super(ConfigContainerBinary, self).__init__(config)

    def _write(self, config):
        self._config = config
        self._binary = BinarySnapshot(config).dump()

    def _read(self):
        copy = spmc.SimulationConfig(self._config)
        BinarySnapshot(copy).read(self._binary)
        return copy

    def _release(self):
        self._binary = None

class ConfigContainerCompressedBinary(ConfigContainerBinary):
    
    def __init__(self, config):
        self._config = None
        super(ConfigContainerBinary, self).__init__(config)

    def _write(self, config):
        self._config = config
        self._binary = BinarySnapshot(config, compression = True).dump()

    def _read(self):
        copy = spmc.SimulationConfig(self._config)
        BinarySnapshot(copy, compression = True).read(self._binary)
        return copy

    def _release(self):
        self._binary = None



class ConfigContainerHDD(ConfigContainer):

    def __init__(self, config):
        self._fileformat = Fileformat('hoomd_blue_xml', 'xml')
        self._filename = unique_filename(ext = self._fileformat.ext)
        self._filename_rp = "{}.rp".format(self._filename)
        super(type(self), self).__init__(config)

    def _write(self, config):
        s = spmc.Session()
        s.config = config
        s.write(self._filename, self._fileformat.format)
        dump_reduced_positions(s.config, self._filename_rp)
        self._released = False

    def _read(self):
        try:
            s = spmc.Session()
            s.read(self._filename)
            read_reduced_positions(s.config, self._filename_rp)
        except RuntimeError as error:
            raise IOError(error)
        else:
            return s.config

    def _release(self):
        import os
        os.remove(self._filename)
        os.remove(self._filename_rp)

    def filename(self):
        return self._filename

class ConfigStorage(collections.MutableMapping):
  
    def __init__(self, config_container_type = ConfigContainerMemory):
        self._map = dict()
        self.config_container_type = config_container_type
        logger.debug("Container type used: {}".format(self.config_container_type))

    def __len__(self):
        return len(self._map)

    def __getitem__(self, key):
        ret = self._map[key]()
        msg = "Retrieving config '{config}' with key = '{key}'."
        logger.debug(msg.format(config = ret, key = key))
        return ret

    def __setitem__(self, key, config):
        msg = "Storing config '{config}' with key = '{key}'."
        logger.debug(msg.format(config = config, key = key))
        container = self.config_container_type(config)
        self._map[key] = container

    def __delitem__(self, key):
        self._map[key].release()
        del self._map[key]

    def __contains__(self, key):
        return key in self._map

    def __iter__(self):
        return self._map.__iter__()

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_val, trace):
        for value in self._map.values():
            value.release()
        self._map.clear()
        return False

def selfcheck():
    import random
    def random_vector():
        return 100 * spmc.Tuple3(
          random.random(),
          random.random(),
          random.random())

    def read_remapped_positions(config):
        ret = []
        for p in config.all():
            rp = config.particle(p).remapped_position()
            ret.extend([rp.x, rp.y, rp.z])
        return ret

    num_atoms = pow(10, 5)

    c1 = spmc.SimulationConfig()
    c1.num_atoms = num_atoms
    c1.box = spmc.Tuple3(random.random() * 10)
    for i, p in enumerate(c1.all()):
        c1.particle(p).position = random_vector()
    c2 = spmc.SimulationConfig(c1)
    positions_c1 = read_remapped_positions(c1)
    positions_c2 = read_remapped_positions(c2)

    logger.info("Self-check")
    try:
        for container_type in (ConfigContainerBinary, ConfigContainerCompressedBinary, ConfigContainerMemory, ConfigContainerHDD):
            with container_type(c1) as container:
                logger.info("Use container '{}' as resource...".format(container))
            logger.info("Use storage as resource...")
            with ConfigStorage(container_type) as storage:
                print(storage)
                storage[0] = c1
                storage[1] = c2
                assert read_remapped_positions(storage[0]) == positions_c1
                assert read_remapped_positions(storage[1]) == positions_c2
    except:
        logger.error("Failed.")
        raise
    else:
        logger.info("OK")

def main():
    logging.basicConfig(level = logging.DEBUG)
    selfcheck()

if __name__ == '__main__':
    main()
