from sympy.physics import units
from sympy import sqrt

u = units

u.angstrom = 10 * u.nanometer
u.kilocalorie = 4184 * u.joule

#u.defunit(u.meter*u.ten**-10, 'angstrom','angstroms','A')
#u.defunit(4184*u.joule, 'kilocalorie','kilocalories','kcal')

# Values for Argon as default
T = 119.8*u.K
u.D_u		= 3.405*u.angstrom
u.E_u		= T*u.boltzmann
u.M_u		= 0.039994*u.kilogram/u.mole/u.avogadro
u.Q_u		= u.eV / u.V

u.t_u		= u.D_u*sqrt(u.M_u / u.E_u)
u.V_u		= u.D_u**3
u.v_u		= u.D_u / u.t_u
u.mom_u		= u.M_u * u.D_u / u.t_u**2
u.acc_u		= u.D_u * u.t_u**-2
u.f_u		= u.E_u / u.D_u
u.p_u		= u.E_u / u.D_u**3

u.k_B       = u.E_u / T

def calc_temperature_from_dump(temp_dump):
    return temp_dump / u.k_B

