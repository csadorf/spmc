import logging
import libinterface as spmc
from . import utility
import numpy as np

logger = logging.getLogger('moves')

def ChangeBondLengthMove(freq, adj = 1):
    return spmc.ChangeBondLengthMove(freq, adj)

def TranslateJiggleBiasedMove(freq, adj = 1, params = None):
    tjb_move = spmc.TranslateJiggleBiasedMove(freq, adj)
    if params is not None:
        tjb_move.set_parameters(* params)
    return tjb_move

def ChangeBondLengthBiasedMove(freq, params = None, adj = 1):
    cblb_move = spmc.ChangeBondLengthBiasedMove(freq, adj)
    if params is not None:
        cblb_move.set_parameters(params)
    return cblb_move

def RandomDisplacementMove(freq, adj = 1.0):
    return spmc.RandomDisplacementMove(freq, adj)

def parse_tjb_parameters(filename_prefix, N_e, units, scale = 1):
    FILENAME_LENGTH_CD  = filename_prefix + "bond_normalized.txt"
    FILENAME_ANGLE_CD   = filename_prefix + "angle_atomistic.out"
    FILENAME_TORSION_CD = filename_prefix + "torsion_atomistic.out"

    length_cd_table = np.array(utility.parse_table_file(FILENAME_LENGTH_CD), dtype=float)
    length_cd = spmc.Lengths()
    for row in length_cd_table:
        length_cd[row[1]] = units.length(row[0] * scale)
    angle_cd_table = np.array(utility.parse_table_file(FILENAME_ANGLE_CD), dtype=float)
    angle_small_cd = spmc.Angles()
    angle_big_cd = spmc.Angles()
    for row in angle_cd_table:
        angle = units.plane_angle(row[0])
        angle_small_cd[row[1]] = angle
        angle_big_cd[row[2]] = angle
    torsion_cd_table = np.array(utility.parse_table_file(FILENAME_TORSION_CD), dtype = float)
    torsion_cd = spmc.Angles()
    for row in torsion_cd_table:
        torsion_cd[row[1]] = units.plane_angle(row[0])
    bead_length = units.area(N_e**2)
    return length_cd, angle_small_cd, angle_big_cd, torsion_cd, bead_length
