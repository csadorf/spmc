#!/usr/bin/env python

import logging
import libinterface as spmc
from libanalysis import Analysis
from math import sqrt
import time, os, random

logger = logging.getLogger('utility')

def random_number():
    return int(random.random() * 1000)

def timestamp():
    return time.strftime("%Y%m%d%H%M%S")

def utc_timestamp():
    return time.strftime("%Y-%m-%dT%H:%M:%S%z")

def reduce_config(config, upper_bound):
    if upper_bound == 0:
        upper_bound = config.num_atoms
    subset = config.group_by_tag(0, upper_bound)
    return spmc.SimulationConfig(config, subset)

def adapt_box(config, subset = None):
    adapted = spmc.SimulationConfig(config)
    if subset is None:
      subset = adapted.all()
    max_pos = Analysis.maximal_position(adapted, subset)
    min_pos = Analysis.minimal_position(adapted, subset)
    new_box = spmc.Tuple3(
      max(abs(max_pos.x), abs(min_pos.x)),
      max(abs(max_pos.y), abs(min_pos.y)),
      max(abs(max_pos.z), abs(min_pos.z)))
    adapted.box = new_box
    return adapted

def trim_config(config, subset = None):
    trimmed = spmc.SimulationConfig(config)
    if subset is None:
      subset = trimmed.all()
    delta = (  Analysis.maximal_position(trimmed, subset)
             + Analysis.minimal_position(trimmed, subset)) / (-2)
    for p in subset:
      trimmed.particle(p).position = trimmed.particle(p).remapped_position() + delta
    return trimmed

def converge_to_density(session, target_density, number_of_steps = 1000, intervals = 10):
    config = session.config
    subset = config.all()

    density = Analysis.number_density(config)
    delta_density = target_density - density
    interval = int(number_of_steps / intervals)
    density_gradient = delta_density / number_of_steps
    n = len(subset)
    box_0 = spmc.Tuple3(config.box)
    v_0 = Analysis.box_volume(config)
    
    for i in range(intervals):
      x = (1 + density_gradient * (i+1) * interval * v_0 / n)
      session.config.box = (1.0/float(x))**(1.0/3) * box_0
      session.run(interval)

def shrink_session(session, upper_bound):
    original_density = Analysis.number_density(session.config)
    session.config = reduce_config(session.config, upper_bound)
    converge_to_density(session, original_density)

def read(filename):
    s = spmc.Session()
    s.read(filename)
    return s.config

def dump(config, format = 'xyz'):
    s = spmc.Session()
    s.config = config
    s.dump(format)

def write(config, filename, format = 'hoomd_blue_xml'):
    msg = "Write configuration to file '{}', format '{}'."
    logger.debug(msg.format(filename, format))
    s = spmc.Session()
    s.config = config
    s.write(filename, format)

def write_with_timestamp(config, filename, format = 'hoomd_blue_xml'):
    root, ext = os.path.splitext(filename)
    filename = root + "_" + timestamp() + ext
    write(config, filename, format)

def bond_lengths(config):
    result = []
    molecules = config.property.molecules()
    for molecule in molecules:
        result.extend(bond_lengths(config, molecule))
    return result

def bond_lengths(config, molecule = None):
    if molecule is None:
        molecules = config.property.molecules()
    else:
        molecules = [molecule]
    result = []
    for molecule in molecules:
        mol_list = list(molecule)
        for p in mol_list[1:]:
            p1 = config.particle(p - 1)
            p2 = config.particle(p)
            result.append(abs(p2.remapped_position() - p1.remapped_position()))
    return result

def bond_lengths_average(config, molecule = None):
    bl = bond_lengths(config, molecule)
    return sum(bl) / len(bl)

def center_of_mass(config, subset):
    M = 0.0
    R = spmc.Tuple3(0)
    for s in subset:
        p = config.particle(s)
        M += p.mass
        R += p.mass * p.remapped_position()
    return R / M

def set_mass(config, mass):
    for p in config.all():
        config.particle(p).mass = mass

def set_particle_type(config, particle_type):
    for p in config.all():
        config.particle(p).type = particle_type;

def radius_of_gyration_molecule(config, molecule):
    rg = 0
    r_sum = spmc.Tuple3(0)
    for p in molecule:
        r_sum += config.particle(p).remapped_position()
    r_mean = r_sum / len(molecule)
    for p in molecule:
        delta = abs(config.particle(p).remapped_position() - r_mean)
        rg += pow(delta, 2)
    return sqrt(rg / len(molecule))

def radius_of_gyration(config):
    molecules = config.property.molecules()
    rg = 0.0
    for molecule in molecules:
        rg += radius_of_gyration_molecule(config, molecule)
    return rg / len(molecules)

def mean_squared_distance(config_a, config_b):
    return Analysis.mean_squared_distance(config_a, config_b)

def parse_table_file(filename):
    result = []
    with open(filename, 'r') as f:
        for line in f.readlines():
            result.append(line.split())
    return result

def scaled_table(table, scaling):
    result = []
    for row in table:
          result.append([])
          for i, column in enumerate(row):
              result[-1].append(float(column) * float(scaling[i]))
#    print scaling
#    for row in result:
#        print row
    return result

def convert_xyz_trajectory(filename_xyz, filename_converted):
    from MDAnalysis.coordinates import XYZ
    xyz = XYZ.XYZReader(filename_xyz)
    other = xyz.OtherWriter(filename_converted)
    for t in xyz:
        other.write(t)
    other.close()

def convert_analyzer_data(analyzers):
    data = dict()
    for analyzer_id, analyzer in analyzers.items():
        data[analyzer_id] = list(analyzer.data())
    return data

def element_product(a, b):
    return spmc.Tuple3(
        a.x * b.x,
        a.y * b.y,
        a.z * b.z)

def reset_images(config):
    result = spmc.SimulationConfig(config)
    null = spmc.Tuple3(0)
    for p in result.all():
        result.particle(p).image = null
    return result

def map_into_box(config):
    result = spmc.SimulationConfig(config)
    molecules = config.property.molecules()
    for molecule in molecules:
        d_min = -1.0
        delta = None
        for p in molecule:
            delta_remapped = config.particle(p).remapped_position() - config.particle(p).position
            d = delta_remapped.abs_squared()
            if delta is None or d < d_min:
                delta = delta_remapped
                d_min = d
        for p in molecule:
            pos_remapped = config.particle(p).remapped_position()
            result.particle(p).position = pos_remapped - delta
    return result

def convert_xyz_to_dcd(fn_xyz, fn_dcd):
    import MDAnalysis

    trajectory_xyz = MDAnalysis.coordinates.XYZ.XYZReader(fn_xyz)
    with MDAnalysis.coordinates.DCD.DCDWriter(fn_dcd, trajectory_xyz.numatoms) as writer:
        for ts in trajectory_xyz:
            writer.write_next_timestep(ts)
