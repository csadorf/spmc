import libinterface as spmc
import logging
logger = logging.getLogger('potentials')

class Potentials(object):

    def __init__(self, reduced_unitsset):
        self._units = reduced_unitsset

    def hard_sphere_potential(self, a, b, r_cut = 1.0, epsilon = 1000, exclude_bonded = 0):
      u_ = self._units
      return spmc.HardSpherePotential(
              spmc.SortedTypePair(a, b),
              u_.energy(epsilon),
              exclude_bonded,
              u_.length(r_cut)
              )

    def hard_field_potential(
        self,
        particle_type, 
        normal_vector,
        distance_from_origin,
        cut_off,
        epsilon):
        return spmc.HardFieldPotential(
            particle_type, 
            normal_vector,
            distance_from_origin,
            cut_off,
            self._units.energy(epsilon)
            )
    
    def polynomial_point_potential(
        self,
        particle_type,
        position,
        degree,
        cut_off,
        epsilon):
        return spmc.PolynomialPointPotential(
            particle_type,
            position,
            degree,
            cut_off,
            self._units.energy(epsilon)
            )

    def polynomial_pseudo_bond_potential(
        self,
        particle_type,
        r_0,
        degree,
        cut_off,
        epsilon):
        return spmc.PolynomialPseudoBondPotential(
          particle_type,
          self._units.length(r_0),
          degree,
          cut_off,
          self._units.energy(epsilon)
          )

    def harmonic_pseudo_bond_potential(
        self,
        particle_type,
        r_0,
        cut_off,
        epsilon):
        return self.polynomial_pseudo_bond_potential(particle_type, r_0, 2, cut_off, epsilon)

    def lennard_jones_potential(
        self,
        a = 'A',
        b = 'A',
        epsilon = 1.0, 
        sigma = 1.0,
        alpha = 1.0, 
        r_cut = 3.0,
        exclude_bonded = 0):
      u_ = self._units
      return spmc.LennardJonesPairforce(
              spmc.SortedTypePair(a, b),
              u_.energy(epsilon),
              u_.length(sigma),
              alpha,
              u_.length(r_cut),
              exclude_bonded)

    def tabulated_pairforce(self, a, b, r_c, reduced_table, 
                            exclude_bonded = 0, skin = None):
        r_cut = min(r_c, reduced_table[-1][0])
        if skin is None:
          skin = 0.2 * r_c
        return spmc.TabulatedPairforce(
                spmc.SortedTypePair(a, b),
                expand_distance_table(reduced_table, self._units),
                exclude_bonded,
                self._units.length(r_cut),
                self._units.length(skin))

    def tabulated_neighbor_force(self, particle_type, skip, steps, reduced_table):
        return spmc.TabulatedNeighborForce(
          particle_type, skip, steps,
          expand_distance_table(reduced_table, self._units))

    def harmonic_neighbor_force(self, particle_type, skip, steps, r0, c0):
        return spmc.HarmonicNeighborForce(
          particle_type, skip, steps,
          self._units.length(r0),
          self._units.bond_stretching_force(c0)
          )

    def harmonic_bond(self, bond_type, l0, k):
        return spmc.HarmonicBondForce(
            bond_type,
            self._units.length(l0),
            self._units.bond_stretching_force(k)
            )

    def angle_harmonic(self, angle_type, phi0, k):
        return spmc.HarmonicAngleForce(
            angle_type,
            self._units.plane_angle(phi0),
            self._units.bond_bending_force(k)
            )

def expand_distance_table(reduced_table, units, _exponent = 2):
    u_ = units
    table = spmc.TabulatedEnergy()
    for row in reduced_table:
        area = u_.area(float(row[0])**_exponent)
        energy = u_.energy(float(row[1]))
        table[area] = energy
    return table

def expand_area_energy_table(reduced_table, units):
    return expand_distance_table(reduced_table, units, _exponent = 1)

def bond_length_distribution():
    bld = numpy.array(utility.parse_table_file('bond_normalized.txt'))
    return bld[:, [1, 0]].astype(float).tolist()

def chose_from_distribution(distribution, number):
    for row in distribution:
        if number < row[0]:
            return row[1]
