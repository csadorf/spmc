import libinterface as spmc

def create_random(config, N, p_type, phi_p = None):
    import random
    from math import pi
    if phi_p is not None:
        n = 6 * phi_p * 6
        V = N * n
        L = pow(V, 1.0/3)
        config.box = spmc.Tuple3(L)
    config.num_atoms = N
    for i in range(N):
        p = config.particle(i)
        p.type = p_type
        p.position = spmc.Tuple3(
            (random.random()) * config.box.x,
            (random.random()) * config.box.y,
            (random.random()) * config.box.z,
            )
    return config
