import numpy as np

class EvaluationRecord(object):
    def __init__(self, evaluation_functions = []):
        self._evaluation_functions = evaluation_functions
        self._molecules = []
        self._data = []

    def extend(self, rhs):
        self._molecules.extend(rhs.molecules)
        self._data.extend(rhs.molecules)

    def evaluate(self, session, molecule):
        new_data = []
        for ef in self._evaluation_functions:
           new_data.append(ef(session, molecule))
        self._data.append(new_data)
        self._molecules.append(molecule)
    
    def __getitem__(self, val):
        sliced_er = EvaluationRecord()
        sliced_er._evaluation_functions = self._evaluation_functions
        if type(val) == type(int()):
            sliced_er._molecules = [self._molecules[val]]
        else:
            sliced_er._molecules = self._molecules[val]
        sliced_er._data = self._data[val]
        return sliced_er

    def __len__(self):
        return len(self._data)

    @property
    def molecules(self):
        return self._molecules

    @property
    def data(self):
        return np.array(self._data, ndmin = 2)

    def last_indeces(self):
        indeces = dict()
        for i, molecule in enumerate(self._molecules):
            indeces[molecule] = i
        return indeces

    def data_by_molecule(self):
        import collections
        ordered = collections.defaultdict(list)
        for molecule, data in zip(self._molecules, self._data):
            ordered[molecule].append(data)
        result = dict()
        for molecule, data in ordered.items():
            #result[molecule]  = np.array(data)
            result[int(list(molecule)[0])] = np.array(data)
        return result
