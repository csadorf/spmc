class UnitTest(object):
    
    def __init__(self, name, session, analyzers, steps, sigma_factor = 1.5, equilibration = 0.5):
        self._name = name
        self._analyzers = []
        for analyzer in analyzers:
            self._analyzers.append(session.add_analyzer(analyzer))
        self._session = session
        self._steps = steps
        self._sigma_factor = sigma_factor
        self._equilibration = equilibration

    def obtain_results(self):
        import numpy as np
        ret = []
        self._session.init()
        self._session.run(self._steps)
        for a in self._analyzers:
            data = list(a.data())
            n = len(data)
            n_equil = int(n * self._equilibration)
            data_equil = data[n_equil:]
            av = np.average(data_equil)
            std = np.std(data_equil)
            ret.append((av, std))
        print(ret)
        return ret

    def write_results(self, results):
        with open("{name}.results".format(name = self._name), 'w') as f:
            for result in results:
                f.write("{} {}\n".format(* result))

    def read_expected_results(self):
        ret = []
        with open("{name}.results".format(name = self._name), 'r') as f:
            for line in f.readlines():
                ret.append([float(x) for x in line.split(' ')])
        return ret

    def compare(self, results, results_expected):
        for r, e in zip(results, results_expected):
            if abs(r[0] - e[0]) > self._sigma_factor * e[1]:
                msg = "Deviation. Expected: {} ({}). Obtained: {}"
                raise RuntimeError(msg.format(
                    e[0], e[1], r[0]))

    def calibrate(self):
        self.write_results(self.obtain_results())

    def run_test(self):
        expected_results = self.read_expected_results()
        results = self.obtain_results()
        self.compare(results, expected_results)
