# Rapid Reconstruction Framework

from . import reconstruction
import numpy as np
from scipy.optimize import leastsq
import libinterface as spmc
from libanalysis import Analysis
from . import analysis
from . import utility
import matplotlib.pyplot as plt
import copy
import time
import logging
from .evaluation_record import EvaluationRecord

logger = logging.getLogger("ReFit")

SHOW_PLOTS = 0
FILENAME_QESF_OVER_QEVF_AFTER_FITTING         = "_{}_qesf_over_qevf_after_fitting.pdf"
FILENAME_QESF_OVER_QEVF_AFTER_RECONSTRUCTION  = "_{}_qesf_over_qevf_after_reconstruction.pdf"
FILENAME_PLOT_ANALYSIS                        = "_{}_analysis.pdf"

def pe_evaluation(session, molecule):
    return Analysis.potential_energy(session, molecule)
    return Analysis.intra_molecular_potential_energy(session, molecule)

def pe_inter_evaluation(session, molecule):
    return Analysis.inter_molecular_potential_energy(session, molecule)

def pe_intra_evaluation(session, molecule):
    return Analysis.intra_molecular_potential_energy(session, molecule)

def num_atoms_evaluation(session, molecule):
    return session.config.num_atoms

def average_bond_lengths_evaluation(session, molecule):
    bond_lengths = utility.bond_lengths(session.config, molecule)
    return sum(bond_lengths) / len(bond_lengths)

def osmotic_pressure_evaluation(session, molecule):
    return Analysis.osmotic_pressure(session)

def evaluation_functions():
    return [    
        pe_evaluation, 
        pe_intra_evaluation,
        average_bond_lengths_evaluation, 
    #    osmotic_pressure_evaluation,
        ]

def evaluation_record():
    return EvaluationRecord(evaluation_functions())

def QEvF_family(
        target_session,
        atomistic_config):
    s_at = spmc.Session(target_session)
    s_at.config = atomistic_config
    pe_at = Analysis.potential_energy(s_at)
    bl_at = utility.bond_lengths(atomistic_config)
    abl_at = sum(bl_at) / len(bl_at)
    #osm_at = analysis.osmotic_pressure(s_at, 1000)
    epsilon = 1.0
    epsilon = 0.5
    if pe_at != 0:
        def QEvF(record):
            x = record.data
#        pe_mols = []
#        for mol in record.molecules:
#            pe_mols.append(analysis.potential_energy(s_at, mol))
#        pe_at = np.array(pe_mols)
#        print "pe_at={}".format(pe_at)
            #return np.abs(1 - (x[:,0] / pe_at)) + np.abs(1 - (x[:,3] / osm_at))
            pe_at_sm = Analysis.potential_energy(s_at, s_at.config.property.molecule(0))
            c = pe_at / pe_at_sm
            term = c * x[:,0] / pe_at - 1
            return np.abs(term) / epsilon
    else:
        def QEvF(record):
            x = record.data
            term = x[:,0]
            return np.abs(term) / epsilon
    return QEvF

def QEsF_family(p):
    def QEsF(record):
        x = record.data
        try:
            return np.abs(x[:,0] / p[0] - p[1])
        except TypeError as e:
            print(x)
            print(p)
            logger.error(e)
            raise

    return QEsF

def number_of_parameters():
    return 2

def default_parameters():
    return [1] * number_of_parameters()

def fit_QEsF_to_QEvF(
        QEvF,
        QEsF_family,
        QEsF_parameters,
        record,
        id_):
    if len(record) < len(QEsF_parameters):
        msg = "Number of datapoints ({}) is not sufficient for number of parameters ({})."
        raise ValueError(msg.format(len(record), len(QEsF_parameters)))
    p, cov, infodict, mesg, ier = leastsq(
        lambda p, y, x: y - QEsF_family(p)(x),
        QEsF_parameters,
        args=(QEvF(record), record),
        full_output = True)
    parameters = p.tolist()
    if not ier in (1,2,3,4):
        logger.debug("r_squared = {}".format(r_squared))
        logger.debug("ier = {}".format(ier))
        raise RuntimeError(mesg)
    ss_err=(infodict['fvec']**2).sum()
    y = QEvF(record)
    y_ = QEsF_family(parameters)(record)
    ss_tot=((y-y.mean())**2).sum()
    r_squared=1-(ss_err/ss_tot)
    plt.clf()
    plt.plot(y, y_)
    plt.title('QEsF over QEvF after fitting')
    plt.savefig(FILENAME_QESF_OVER_QEVF_AFTER_FITTING.format(id_))
    return parameters

def check_nfp_criteria(
        configurations,
        QEvF, QEsF):
        return True

def determine_parameters(
        configurations,                # a tuple of one atomistic and once cg'ed configuration
#        configurations,                # a list of atomistic, coarse-grained configuration tuples
        target_session,                # target session (environment)
        reverse_mapping_function,      # naive-reverse-mapping function
        evaluation_record,             # evaluation_record
        QEvF_family,                   # Quality Evaluation Function
        QEsF_family,
        QEsF_parameters,               # Family of Quality Estimator Functions and resp. parameters
        cycle_factor,                  # cycle factor
        target_session_soft = None,    # soft potential session (optional)
        config_library = None,         # config dictionary (optional)
        id_ = '',
        simulate = False,
        ):
#    data = []
#    for config_atomistic, config_coarse_grained in configurations:
#        data.append((config_atomistic, reconstruct(
    if simulate:
        logger.warning("Simulating ReFit algorithm!")
    logger.debug("Collect data for parameter determination...")
    data = reconstruction.reconstruct(
                cg_config             = configurations[1],
                target_session        = target_session,
                rm_fct                = reverse_mapping_function,
                evaluation_record     = evaluation_record,
                QEsF                  = QEvF_family(target_session, configurations[0]),
                cycle_factor          = cycle_factor,
                pre_equilibration     = True,
                target_session_soft   = target_session_soft,
                config_library        = config_library,
                fast                  = False,
                id_                   = id_,
                simulate              = simulate,
                )
    logger.debug("Fitting parameters to data...")
    if simulate:
        return default_parameters()
    else:
        parameters = fit_QEsF_to_QEvF(
            QEvF_family(target_session, configurations[0]),
            QEsF_family,
            QEsF_parameters,
            evaluation_record,
            id_)
#    check_nfp_criteria(
#        QEvF,
#        QEsF_family(QEsF_parameters),
#        configurations)
        return parameters

def analyze_data(data, QEvF, QEsF, id_):
    plt.clf()
    plt.title("QEsF over QEvF after reconstruction")
    real = QEvF(data)
    estimated = QEsF(data)
    plt.plot(real, real, real, estimated)
    plt.savefig(FILENAME_QESF_OVER_QEVF_AFTER_RECONSTRUCTION.format(id_))
    if SHOW_PLOTS:
        plt.show()

def analyze(configs, session):
    def pe(config):
        session.config = config
        return Analysis.potential_energy(session)

    evf_single = [
        pe,
        utility.bond_lengths_average,
        utility.radius_of_gyration,
        ]

    evf_combined = [
        utility.mean_squared_distance,
        ]

    rows = []
    for config_original, config_reconstructed in configs:
        column = []
        for efs in evf_single:
            column.append(efs(config_original) / efs(config_reconstructed))
        for efc in evf_combined:
            column.append(efc(config_original, config_reconstructed))
        rows.append(column)

    return np.asarray(rows)

def plot_legend():
    return [
        "Potential energy",
        "Bond lengths average",
        "Radius of Gyration",
        "Mean squared distance",
        ]

def plot_analysis(data, id_):
    legend = plot_legened()
    fig, ax1 = plt.subplots()
    for i, d in enumerate(data[:-1]):
        ax1.plot(d, 'x', label = legend[i])
#    plt.setp(plt.gca().axes.get_xticklabels())
    ax1.set_xlabel("x100 Monte Carlo cycles")
    ax1.set_ylabel("original / reconstructed")
    ax1.legend()
    ax2 = ax1.twinx()
    ax2.plot(data[-1], 'o', label = legend[-1])
    ax2.legend()
    plt.tight_layout()
    fig.savefig(FILENAME_PLOT_ANALYSIS.format(id_))

def analyze_and_plot(config_tuples, session, id_):
    plot_analysis(analyze(config_tuples, session), id_)
