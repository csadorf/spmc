# About

# Installation and update

## Linux

1. Clone the spmc-package from [SPMC](https://bitbucket.org/csadorf/spmc).
```
git clone https://bitbucket.org/csadorf/spmc
```
2. build it
```
mkdir spmc_build
cd spmc_build
cmake ../spmc -DCMAKE_BUILD_TYPE=Release
make
```
3. and install it
```
sudo make install
```

## Home directory installation
For a local install you need to tell cmake the local directory for installation.
The `INSTALL_LOCAL` flag invokes a local installation of the python package.
```
cmake ../spmc -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=~/.local -DINSTALL_LOCAL=ON
```

## Usage

To run a simulation set up a python script.
This is a minimal example for a Lennard-Jones particle system:

```python
import spmc, spmc.init, spmc.potentials
session = spmc.Session()
session.config.box = spmc.Tuple3(100, 100, 100)
spmc.init.create_random(config = session.config, N = 100, p_type = 'A')
potentials = spmc.potentials.Potentials(session.reduced_units)
session.add_forcefield(potentials.lennard_jones_potential(
    a = 'A', b = 'A',
    epsilon = 1.0,
    sigma = 1.0, 
    r_cut = 3.0))
session.add_move(spmc.RandomDisplacementMove(1))
session.simulator = spmc.MonteCarloNVTSampler(1.0) # beta = 1.0
session.random_seed(42)
session.run(10000)
```

Then execute 
```
python run.py
```

## Examples

A number of other illustrative example scripts can be found in `examples`.
These examples are designed to demonstrate basic concepts on how to set up simulations and analysis generated data using SPMC. They might also serve as template scripts for other projects.
For execution, simply copy a scripts into your local working directory and execute it.

## Testing

To check whether the package was built and installed correctly, execute `make test` within your build directory.

## Debugging
You can build the tool locally without installing it for local testing and debugging. Extend your $PYTHONPATH variable to include the local build.

```
git clone https://bitbucket.org/csadorf/spmc
mkdir spmc_build
cd spmc_build
cmake ../spmc -DCMAKE_BUILD_TYPE=Release
make
export PYTHONPATH=$PYTHONPATH:`pwd`/lib_spmc/src/simulator/interface:`pwd`/python-module
```
