#!/usr/bin/env python

import logging
logger = logging.getLogger('convert_config')

class Scalar3(object):

    def __init__(self, x = 0.0, y = 0.0, z = 0.0):
        self._v = [x, y, z]

    @property
    def x(self):
        return self._v[0]

    @property
    def y(self):
        return self._v[1]

    @property
    def z(self):
        return self._v[2]

    @x.setter
    def x(self, v):
        self._v[0] = v

    @y.setter
    def y(self, v):
        self._v[1] = v

    @z.setter
    def z(self, v):
        self._v[2] = v

    def __eq__(self, rhs):
        return self._v == rhs._v

    def __iter__(self):
        return iter(self._v)

    def __str__(self):
        return "[{} {} {}]".format(self._v[0], self._v[1], self._v[2])

    def to_dict(self):
        return {
            'x':    self._v[0],
            'y':    self._v[1],
            'z':    self._v[2]
        }

    #def __repr__(self):
    #    return self.__str__()

    @staticmethod
    def from_spmc_tuple3(rhs):
        return Scalar3(x = rhs.x, y = rhs.y, z = rhs.z)

    def to_spmc_tuple3(self):
        import spmc
        return spmc.Tuple3(self._v[0], self._v[1], self._v[2])

def encode_scalar3(scalar3):
    return {
        'x':    scalar3.x,
        'y':    scalar3.y,
        'z':    scalar3.z}

def decode_scalar3(encoded):
    return Scalar3(
        x = encoded['x'],
        y = encoded['y'],
        z = encoded['z'])
    return {
        'x':    scalar3.x,
        'y':    scalar3.y,
        'z':    scalar3.z}

def encode_iterable_of_scalar3(iterable):
    result = {
        'x':    [],
        'y':    [],
        'z':    []}
    for i in iterable:
        result['x'].append(i.x)
        result['y'].append(i.y)
        result['z'].append(i.z)
    return result

def decode_iterable_of_scalar3(encoded):
    result = []
    for xyz in zip(encoded['x'], encoded['y'], encoded['z']):
        result.append(Scalar3(* xyz))
    return result

from bson.binary import Binary
def to_binary(scalar3):
    return Binary(str(scalar3), 128)

def from_binary(binary):
    return Scalar3(int(binary))

from pymongo.son_manipulator import SONManipulator
class TransformSimulationConfig(SONManipulator):
    
    def transform_incoming(self, son, collection):
        for key, value in son.items():
            if isinstance(value, SimulationConfig):
                print 'incoming simulationconfig'
                son[key] = value.to_dict()
            elif isinstance(value, dict):
                son[key] = self.transform_incoming(value, collection)
        return son

    def transform_outgoing(self, son, collection):
        for key, value in son.items():
            if isinstance(value, dict):
                if '_type' in value and value['_type'] == SimulationConfig:
                    son[key] = SimulationConfig.from_dict(value)
                else:
                    son[key] = self.transform_outgoing(value, collection)
            return son

class SimulationConfig(object):
    
    def __init__(self, N = 0):
        self._default_type = 'A'
        self._box = Scalar3(0.0)
        self._set_size(N)

    def _set_size(self, N):
        self._types = [self._default_type] * N
        self._positions = [Scalar3(0.0,0.0,0.0)] * N
        self._images = [Scalar3(0,0,0)] * N
        self._bond_types = []
        self._bonds = []
        self._angle_types = []
        self._angles = []
        self._dihedral_types = []
        self._dihedrals = []

    def __eq__(self, rhs):
        return \
            self._box == rhs._box and \
            self._types == rhs._types and \
            self._positions == rhs._positions and \
            self._images == rhs._images and \
            self._bond_types == rhs._bond_types and \
            self._bonds == rhs._bonds and \
            self._angle_types == rhs._angle_types and \
            self._angles == rhs._angles and \
            self._dihedral_types == rhs._dihedral_types and \
            self._dihedrals == rhs._dihedrals

    def __str__(self):
        return \
            str(self._box) +\
            str(self._types) +\
            str(self._positions) +\
            str(self._images) +\
            str(self._bond_types) +\
            str(self._bonds) +\
            str(self._angle_types) +\
            str(self._angles) +\
            str(self._dihedral_types) +\
            str(self._dihedrals)

    @property
    def num_atoms(self):
        return len(self._types)

    @num_atoms.setter
    def num_atoms(self, value):
        msg = "Changing of initial config size not implemented."
        raise NotImplementedError(msg)

    @staticmethod
    def from_dict(config_dict):
        result = SimulationConfig()
        config_dict['_box'] = decode_scalar3(config_dict['_box'])
        for s3 in ['_positions', '_images']:
            config_dict[s3] = decode_iterable_of_scalar3(config_dict[s3])
        result.__dict__.update(config_dict)
        return result

    def to_dict(self):
        import copy
        result = copy.copy(self.__dict__)
        result['_type'] = 'SimulationConfig'
        result['_version'] = 0.1
        result['_box'] = encode_scalar3(result['_box'])
        for s3 in ['_positions', '_images']:
            result[s3] = encode_iterable_of_scalar3(result[s3])
        return result

    @staticmethod
    def from_spmc_config(spmc_config):
        result = SimulationConfig(N = spmc_config.num_atoms)
        result._box = Scalar3.from_spmc_tuple3(spmc_config.box)
        for i, p_i in enumerate(spmc_config.all()):
            p = spmc_config.particle(p_i)
            result._types[i] = p.type
            result._positions[i] = Scalar3.from_spmc_tuple3(p.reduced_position())
            result._images[i] = Scalar3.from_spmc_tuple3(p.image)
        result._bonds = []
        result._bond_types = []
        for b in spmc_config.bonds():
            bond = spmc_config.bond(b)
            result._bonds.append([bond.a, bond.b])
            result._bond_types.append(bond.type)
        result._angles = []
        result._angle_types = []
        for a in spmc_config.angles():
            angle = spmc_config.angle(a)
            result._angles.append([angle.a, angle.b, angle.c])
            result._angle_types.append(angle.type)
        result._dihedrals = []
        result._dihedral_types = []
        for d in spmc_config.dihedrals():
            dihedral = spmc_config.dihedral(d)
            result._dihedrals.append([dihedral.a, dihedral.b, dihedral.c, dihedral.d])
            result._dihedral_types.append(dihedral.type)
        return result

    def to_spmc_config(self):
        from spmc.developer import DeveloperSimulationConfig
        with DeveloperSimulationConfig() as result:
            result.num_atoms = self.num_atoms
            result.box = self._box.to_spmc_tuple3()
            for i, (t, pos, img) in enumerate(zip(self._types, self._positions, self._images)):
                particle = result.particle(i)
                particle.type = str(t)
                particle.set_reduced_position(pos.to_spmc_tuple3())
                particle.image = img.to_spmc_tuple3()
            for i, (t, bond) in enumerate(zip(self._bond_types, self._bonds)):
                result.add_bond(bond[0], bond[1], str(t))
            if len(self._angle_types) or len(self._dihedral_types):
                msg = "Conversion of angles or dihedrals not yet supported."
                raise NotImplementedError(msg)
            return result

def check_mongo_db(N):
    from pymongo import MongoClient
    client = MongoClient()
    client.drop_database('test_simulation_config')
    db = client['test_simulation_config']
    db.add_son_manipulator(TransformSimulationConfig())
    mc = db['test']
    c1 = SimulationConfig.from_spmc_config(generate_test_config(N))
    _id = mc.insert({'config': c1.to_dict()})
    retrieved = mc.find_one({'_id': _id})['config']
    c2 = SimulationConfig.from_dict(retrieved)
    assert c1 == c2

def generate_test_config(N, num_mols = 2):
    import spmc
    import random
    def random_vector():
        return 100 * spmc.Tuple3(
          random.random(),
          random.random(),
          random.random())
    config = spmc.SimulationConfig()
    config.box = spmc.Tuple3(random.random() * 100000)
    for i in xrange(num_mols):
        config_molecule = spmc.SimulationConfig()
        config_molecule.num_atoms = N / num_mols
        config_molecule.box = config.box
        for i, p in enumerate(config_molecule.all()):
            config_molecule.particle(p).type = 'A'
            config_molecule.particle(p).position = random_vector()
        config_molecule.bond_all_particles()
        config.append(config_molecule)
    return config

def selfcheck(N):
    import spmc

    def read_remapped_positions(config):
        ret = []
        for p in config.all():
            rp = config.particle(p).remapped_position()
            ret.extend([rp.x, rp.y, rp.z])
        return ret

    c1 = generate_test_config(N)
    positions_c1 = read_remapped_positions(c1)

    logger.info("Self-check")
    try:
        s1 = SimulationConfig.from_spmc_config(c1)
        d1 = s1.to_dict()
        s2 = SimulationConfig.from_dict(d1)
        c2 = s2.to_spmc_config()
        assert s1 == s2
        assert positions_c1 == read_remapped_positions(c2)
    except:
        logger.error("Failed.")
        raise
    else:
        logger.info("OK")

def main():
    logging.basicConfig(level = logging.DEBUG)
    N = pow(10, 1)
    selfcheck(N)
    check_mongo_db(N)

if __name__ == '__main__':
    main()
