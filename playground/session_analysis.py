class SessionAnalysis(object):

    def __init__(self):
        pass

    def read(self, session):
        analyzers = session.analyzers()
        new_data = dict()
        num_blocks = 0
        for analyzer in analyzers:
            analyzer_data = list(analyzer.data())
            num_blocks = max(len(analyzer_data), num_blocks)
            new_data[analyzer.id()] = list(analyzer.data())
        for entry in new_data.itervalues():
            assert num_blocks == len(entry)
        freq = session.analyzer_frequency
        current_step = session.current_step()
        restart = freq * num_blocks <= current_step
        if restart:
            first_step = current_step - (freq * (num_blocks - 1))
        else:
            first_step = 0
        steps = range(first_step, first_step + freq * num_blocks, freq)
        new_data['steps'] = steps
        return new_data
