#!/usr/bin/env python

import logging
logger = logging.getLogger('dump_trajectory')
logging.basicConfig(level = logging.DEBUG)

def open_collection(args):
    from pymongo import MongoClient
    client = MongoClient()
    db = client[args.database]
    collection = db[args.collection]
    collection_secondary = db[args.secondary_collection]
    return collection, collection_secondary

def valid_filename(a_string):
    import string
    valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    return ''.join(c for c in a_string if c in valid_chars)

def fn_job_parameters(job):
    import json
    parameters = job.parameters()
    fn = str(json.dumps(parameters))
    return valid_filename(fn).replace(' ', '_')

def fn_job_id(job):
    fn = str(job.id())
    return valid_filename(fn)

def dump_config(collection, collection_secondary, args):
    from sim_tools.job_management import MongoDBJob
    from spmc.job import SPMCJob
    from spmc import Session
    class GenericJob(MongoDBJob, SPMCJob):
        pass

    docs = collection.find()
    for doc in docs:
        try:
            with GenericJob(collection, parameters = {}, _id = doc['_id']) as job:
                assert job.id() == doc['_id']
                job.set_secondary_collection(collection_secondary)
                config = job.get_config(args.config)
                session = Session()
                session.config = config
                if args.explicit_parameters:
                    fn_job = fn_job_parameters(job)
                else:
                    fn_job = fn_job_id(job)
                fn = '_' + '.'.join([fn_job, args.config, 'xml'])
                msg = "Dumping '{}'..."
                logger.info(msg.format(fn))
                session.write(fn, 'hoomd_blue_xml')
        except KeyError as error:
            logger.warning(error)

def main(path):
    import argparse
    parser = argparse.ArgumentParser(
        description = "Execute jobs from database.",
        )
    parser.add_argument(
        'database',
        help = "The database, which will be used for analysis.",
        type = str,
        )
    parser.add_argument(
        'collection',
        help = "The collection, which will be used for analysis.",
        type = str,
        )
    parser.add_argument(
        'config',
        help = "The key, which identifies the config, to be fetched.",
        type = str,
        )
    parser.add_argument(
        '--secondary-collection', '-s',
        help = "The secondary collection for large data.",
        type = str,
        default = None)
    parser.add_argument(
        '-p', '--explicit-parameters',
        help = "Use parameters for filename dumps, instead of job id.",
        action = 'store_true',
        )
    parser.add_argument(
        '--path',
        help = "Specify path for input files.",
        type = str,
        default = path,
        )
    args = parser.parse_args()
    if args.secondary_collection is None:
        args.secondary_collection = args.collection + '_secondary'
    
    collection, collection_secondary = open_collection(args)
    dump_config(collection, collection_secondary, args)
    return 0
    

if __name__ == '__main__':
    import sys, os
    path = os.path.abspath(os.path.dirname(__file__))
    if not path in sys.path:
        sys.path.insert(1, path)
    sys.exit(main(path))
