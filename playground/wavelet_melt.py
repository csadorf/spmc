#!/usr/bin/env python

import logging
logger = logging.getLogger('wavelet_mongodb_job')

import spmc
from spmc.job import SPMCJob
from sim_tools.job_management import MongoDBJob, convert_keys_to_string
from sim_tools.job_management import GitRevisionControlledJob as RevisionControlledJob
#from sim_tools.job_management import GitStrictRevisionControlledJob as RevisionControlledJob

PERIOD_SNAPSHOTS = 100
PERIOD_ANALYSIS = 100
DATABASE = 'testing'
COLLECTION_PRIMARY = 'wavelet_melt'
COLLECTION_SECONDARY = 'wavelet_melt_secondary'

class WaveletMeltJob(MongoDBJob, SPMCJob, RevisionControlledJob):

    def melt_session(self, id_ = None, parameters = None):
        if parameters is None:
            parameters = self.parameters()
        from wavelet import melt
        melt_session = melt.WaveletMeltSession(  
            beta = 1.0,                          
            box = spmc.Tuple3(parameters['cubic_box_length']), 
            id_ = id_,
            )
        melt_session.add_molecules(
            R = parameters['N_p'],
            n = parameters['n_molecules'],
            )
        melt_session.dumping_callback = self.get_snapshot_callback('dumping')
        return melt_session
    
#    def save_config(self, key, config):
#        super(WaveletMeltJob, self).save_config('config.{}'.format(key), config)
#
#    def get_config(self, key):
#        return super(WaveletMeltJob, self).get_config('config.{}'.format(key))
#
class WaveletMeltEquilibrateCGJob(WaveletMeltJob):

    def run(self):
        melt_session = self.melt_session('')
        # generate random numbers for initialization
        import random
        random.seed(42) # Comment this line for true randomness!

        coarse_graining_levels = self.parameters()['coarse_graining_levels']
        
        # Coarse-grain system
        for i in xrange(coarse_graining_levels):
            melt_session.coarse_grain()
        
        session = melt_session.session()
        
        ## Add analysis
        session.add_analyzer('radius_of_gyration')
        session.add_analyzer('osmotic_pressure')
        session.add_analyzer('acceptance_ratio')
        snapshot_callback = self.get_snapshot_callback('cg_sampling')
        session.set_callback(PERIOD_SNAPSHOTS, snapshot_callback)
        
        # run 20,000 MC cycles in NVT ensemble
        session.random_seed(int(random.random() * 100))
        self.save_config('init', session.config)
        #session.run(20000)
        session.run(2000)
        self.save_config('cg_sampled', session.config)

class WaveletMeltReFitJob(WaveletMeltJob):
    
    def run(self):
        from spmc import reconstruction
        coarse_graining_levels = self.parameters()['coarse_graining_levels']
        melt_session = self.melt_session()
        melt_session.config = self.get_config('reconstructed')

        msg = "Refit for e = {e}..."
        for e in xrange(coarse_graining_levels):
            try:
                logger.info(msg.format(e = e))
                parameters, lib = melt_session.refit()
                self._set(
                    'refit.{}.parameters'.format(e), 
                    parameters)
            except reconstruction.BadFitWarning as refit_error:
                self._set(
                    'refit.{}.status'.format(e),
                    1)
                logger.warning(refit_error)
                #logger.warning("Using default parameters for e = {e}.".format(e = e))
            else:
                self._set(
                    'refit.{}.status'.format(e),
                    0)
                logger.info("Success.")
            melt_session.coarse_grain()

class WaveletMeltReconstructJob(WaveletMeltJob):
    
    def run(self):
        coarse_graining_levels = self.parameters()['coarse_graining_levels']
        melt_session = self.melt_session()
        for i in xrange(coarse_graining_levels):
            melt_session.coarse_grain()
        cg_sampled = self.get_config('cg_sampled')
        melt_session.config = cg_sampled
        
        # Reconstruct system
        melt_session.callback_period = PERIOD_SNAPSHOTS
        for i in xrange(coarse_graining_levels):
            melt_session.callback = self.get_snapshot_callback('reconstruct.{}'.format(melt_session.e() - 1))
            result = melt_session.reconstruct(
                save_config = self.save_config,
                )
            converted_result = convert_keys_to_string(result)
            self._set(
                'reconstruction_analysis.{}'.format(melt_session.e()),
                converted_result)
        
        # Map result into first image box
        import spmc.utility
        session = melt_session.session()
        self.save_config('reconstructed', session.config)
        config_mapped = spmc.utility.map_into_box(session.config)
        self.save_config('reconstructed_mapped', config_mapped)

def main():
    logging.basicConfig(level = logging.INFO)

# Define parameters
    n_molecules = 5
    N_p = pow(2, 10)
    density = 0.05
    parameters = {
        'n_molecules':  n_molecules,
        'N_p': N_p,
        'density': density,
        'cubic_box_length' : pow(n_molecules * N_p / density, 1.0 / 3),
        'coarse_graining_levels' : 6,
    }

    from pymongo import MongoClient
    client = MongoClient()
    db = client[DATABASE]
    mc = db[COLLECTION_PRIMARY]
    # Because of the MongoDB size limiation on single documents,
    # we need to use a secondary collection for large production runs,
    # to store large amounts of data.
    mc_secondary = db[COLLECTION_SECONDARY]
    # Comment the next two lines to prevent removal of previous data.
    mc.remove()
    mc_secondary.remove()

    with WaveletMeltEquilibrateCGJob(mc, parameters) as job:
        job.set_secondary_collection(mc_secondary)
        job.start()

    query = {
        'config.cg_sampled':    {'$exists': True},
        'config.reconstructed': {'$exists': False}
        }
    docs = mc.find(query)
    for doc in docs:
        with WaveletMeltReconstructJob(mc, parameters = {}, _id = doc['_id']) as job:
            job.set_secondary_collection(mc_secondary)
            job.start()

    query = {
        'config.reconstructed': {'$exists': True}}
    docs = mc.find(query)
    for doc in docs:
        with WaveletMeltReFitJob(mc, parameters = {}, _id = doc['_id']) as job:
            job.set_secondary_collection(mc_secondary)
            job.start()


if __name__ == '__main__':
    main()
