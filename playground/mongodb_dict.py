#!/usr/bin/env python 

class MongoDBDict(dict):
    
    def __init__(self, collection, _id = None, folder = None):
        self._id = _id
        self._collection = collection

    def _update(self):
        query = {'_id': self._id}
        return self._collection.update(query, self)

    def id(self):
        return self._id

    def __enter__(self):
        try:
            if self._id is None:
                self._id = self._collection.insert(self)
            else:
                result = self._collection.find_one(self._id)
                if result is None:
                    msg = "Did not find database entry with _id = '{}'."
                    raise KeyError(msg.format(self._id))
                self.update(result)
        except:
            raise
            return None
        else:
            return self

    def __exit__(self, exception_type, exception_value, traceback):
        try:
            result = self._update()
        except:
            raise
            return False
        else:
            return True

def main():
    from pymongo import MongoClient
    client = MongoClient()
    db = client['testing']
    mc = db['update']

    with MongoDBDict(mc) as my_dict:

        my_dict_id = my_dict.id()
        my_dict.update({
            'a' : 0,
            'b': {'b_a': 0, 'b_b': 1},
        })

        my_dict['a'] = 1

    with MongoDBDict(mc, my_dict_id) as my_dict:
        my_dict['a'] = 2

    try:
        with MongoDBDict(mc, '0') as my_dict: # should fail
            my_dict['a'] = 2
    except KeyError:
        pass
    else:
        assert(False)
    print 'Success!'

    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main())
