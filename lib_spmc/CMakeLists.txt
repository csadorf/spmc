cmake_minimum_required(VERSION 2.6)
project(lib_spmc)

cmake_policy(SET CMP0015 NEW)


find_package(OpenMP)
if (OPENMP_FOUND)
  set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()

#set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=c++0x")
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
  message(WARNING "Building with DEBUG flags!")
  add_definitions(-DDEBUG)
  set (PROFILING_FLAGS "-pg")
  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${PROFILING_FLAGS}") 
  set (CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${PROFILING_FLAGS}")
endif(CMAKE_BUILD_TYPE STREQUAL "Debug")

#add_definitions( -DCHECK_POSITIONS )

set(BUILD_SHARED_LIBS true)

IF(PYTHON_LIBRARY AND PYTHON_INCLUDE_PATH)
  MESSAGE(STATUS "Using custom python installation.")
ELSE()
  MESSAGE(STATUS "Trying to find default python installation.")
  FIND_PACKAGE(PythonLibs 3.3)
ENDIF()
IF(NOT PythonLibs)
  FIND_PACKAGE(PythonLibs 2.7 REQUIRED)
ENDIF()
MESSAGE(STATUS "Python Lib: ${PYTHON_LIBRARY}")
INCLUDE_DIRECTORIES(${PYTHON_INCLUDE_DIRS})

set(BOOST_MIN_VER "1.49.0")

set(BOOST_NO_SYSTEM_PATHS TRUE)
set(BOOST_USE_MULTITHREADED ON)
set(BOOST_USE_STATIC_LIBS OFF)
FIND_PACKAGE(Boost ${BOOST_MIN_VER} COMPONENTS python)
INCLUDE_DIRECTORIES("${Boost_INCLUDE_DIRS}")

include_directories(src)
include_directories(src/config)
include_directories(src/utility)
include_directories(src/simulation_config)
include_directories(src/reverse_mapping)
include_directories(src/parameters)
include_directories(src/simulator)
link_directories(src/utility)
link_directories(src/simulation_config)
link_directories(src/reverse_mapping)

add_subdirectory(src)

