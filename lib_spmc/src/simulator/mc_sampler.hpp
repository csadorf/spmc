//=====================================================================================
//
//       Filename:  mc_sampler.hpp
//
//    Description:  Uses session information to explore phase space.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_MC_SAMPLER_H_
#define MDST_MC_SAMPLER_H_

#include "simulator.hpp"

namespace mdst{

  namespace simulator{

    class MonteCarloSampler : public Simulator{
     public:
      MonteCarloSampler() {};
      MonteCarloSampler(
        shared_ptr<RandomNumberGenerator> rng):
        rng_(rng) {};
      virtual ~MonteCarloSampler() {};

      virtual void
      run(
        Session & session,
        unsigned int num_steps,
        TagSequenceConstIterator sample_sequence_first,
        TagSequenceConstIterator sample_sequence_last);

     protected:

      shared_ptr<RandomNumberGenerator> rng_;

      virtual void
      sample_(
        Session & session,
        unsigned int num_steps,
        TagSequenceConstIterator sample_sequence_first,
        TagSequenceConstIterator sample_sequence_last,
        const Attribute adjust) = 0;
    };

    MonteCarloMoves
    determine_active_moves(
      const MonteCarloMoves & moves,
      const unsigned int cycle);

  } // namespace simulator

} // namespace mdst

#endif // MDST_MC_SAMPLER_H_
