//=====================================================================================
//
//       Filename:  forces.hpp
//
//    Description:  Define forces.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_FORCES_H_
#define MDST_SIMULATOR_FORCES_H_

#include <utility/misc.hpp>
#include <simulation_config/reduced_units.hpp>
#include <simulation_config/simulation_config.hpp>
#include <simulation_config/n_list.hpp>

namespace mdst{

  namespace simulator{
    
    // Virtual force templates

    class Force{
     public:
      Force() {};
      Force(
        shared_ptr<ReducedUnitsSet> units):
        units_(units) {};
      virtual ~Force() {};

      virtual Attribute
      calculate(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const = 0;

      void
      set_units(
        shared_ptr<ReducedUnitsSet> units)
        { units_ = units;}
     protected:
      shared_ptr<ReducedUnitsSet> units_;
    };

    typedef std::vector<shared_ptr<Force> > Forces;
    typedef Forces::const_iterator ForcesConstIterator;
    typedef Forces::iterator ForcesIterator;

    class IntraMolecularForce : public Force{
     public:
      IntraMolecularForce() {};
      IntraMolecularForce(
        shared_ptr<ReducedUnitsSet> units):
        Force(units) {};
      virtual ~IntraMolecularForce() {};

      virtual Attribute
      calculate(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const = 0;
    };

    typedef std::vector<shared_ptr<IntraMolecularForce> > IntraMolecularForces;
    typedef IntraMolecularForces::const_iterator IntraMolecularForcesConstIterator;
    typedef IntraMolecularForces::iterator IntraMolecularForcesIterator;

    class InterMolecularForce : public Force{
     public:
      InterMolecularForce () {};
      InterMolecularForce(
        shared_ptr<ReducedUnitsSet> units):
        Force(units) {};
      virtual ~InterMolecularForce() {};

      virtual Attribute
      calculate(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const = 0;
    };

    typedef std::vector<shared_ptr<InterMolecularForce> > InterMolecularForces;
    typedef InterMolecularForces::const_iterator InterMolecularForcesConstIterator;
    typedef InterMolecularForces::iterator InterMolecularForcesIterator;

    class Pairforce : public InterMolecularForce{
     public:
      explicit
      Pairforce(
        const SortedTypePair & sorted_type_pair);
      explicit
      Pairforce(
        const SortedTypePair & sorted_type_pair,
        unsigned int exclude_bonded,
        const Length & r_cut);
      Pairforce(
        const SortedTypePair & sorted_type_pair,
        unsigned int exclude_bonded,
        const Length & r_cut,
        const Length & r_skin);
      virtual ~Pairforce() {};

      virtual Attribute
      calculate(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const;

      virtual void
      init(
        const SimulationConfiguration::SimulationConfig & config) const;

      SortedTypePair sorted_type_pair_;
      
     private:
      unsigned int exclude_bonded_;
      Length r_cut_;
      Length r_skin_;
      unsigned int factor_;
      mutable std::vector<TagSequence> tags_cache;

     public:
      virtual Attribute
      evaluate(Attribute squared_distance) const = 0;

      SimulationConfiguration::NeighborListKey
      n_list_key() const;

     protected:
      Length
      r_cut() const
      { return r_cut_;}

      SimulationConfiguration::CellGrid &
      get_grid_(
        const SimulationConfiguration::SimulationConfig & config,
        const SimulationConfiguration::ReducedCutOff reduced_r_cut,
        SimulationConfiguration::Resolution & resolution) const;
    };

    typedef std::vector<shared_ptr<Pairforce> > Pairforces;
    typedef Pairforces::const_iterator PairforcesConstIterator;
    typedef Pairforces::iterator PairforcesIterator;

    class FieldForce : public Force{
     public:

      explicit
      FieldForce(
        const ParticleType & particle_type):
      particle_type_(particle_type) {};

      explicit
      FieldForce(
        const ParticleType & particle_type,
        shared_ptr<ReducedUnitsSet> units):
      Force(units),
      particle_type_(particle_type) {};

      virtual Attribute
      calculate(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const;

     private:
      ParticleType particle_type_;

      Attribute
      virtual calculate_(
        const SimulationConfiguration::SimulationConfig & config,
        const ParticleTag & particle) const = 0;
    };

    // E = EvaluationFunct(r)
    template<class EvaluationFunct>
    class HesseFieldForce : public FieldForce{
     public:
      explicit
      HesseFieldForce(
        const ParticleType & particle_type,
        const Tuple3 & normal_vector,
        const Attribute & distance_from_origin,
        const EvaluationFunct & evaluation_funct):
        FieldForce(particle_type),
        evaluation_funct_(evaluation_funct),
        normal_vector_(normal_vector),
        distance_from_origin_(distance_from_origin) {};

      explicit
      HesseFieldForce(
        const ParticleType & particle_type,
        const Tuple3 & normal_vector,
        const Attribute & distance_from_origin,
        const EvaluationFunct & evaluation_funct,
        shared_ptr<ReducedUnitsSet> units):
        FieldForce(particle_type, units),
        evaluation_funct_(evaluation_funct),
        normal_vector_(normal_vector),
        distance_from_origin_(distance_from_origin) {};

      EvaluationFunct evaluation_funct_;

     private:
      Tuple3 normal_vector_;
      Attribute distance_from_origin_;
      
      virtual Attribute
      calculate_(
        const SimulationConfiguration::SimulationConfig & config,
        const ParticleTag & particle) const
      {
        const Attribute r(
          config.particle_position(particle) *
            normal_vector_ - distance_from_origin_);
        return units_->reduce(evaluation_funct_(r));
      }
    };

    // E = EvaluationFunct(r^2)
    template<class EvaluationFunct>
    class PointFieldForce : public FieldForce{
     public:
      explicit
      PointFieldForce(
        const ParticleType & particle_type,
        const Tuple3 & point,
        const EvaluationFunct & evaluation_funct):
        FieldForce(particle_type),
        evaluation_funct_(evaluation_funct),
        point_(point)
      {};

      PointFieldForce(
        const ParticleType & particle_type,
        const Tuple3 & point,
        const EvaluationFunct & evaluation_funct,
        shared_ptr<ReducedUnitsSet> units):
        FieldForce(particle_type, units),
        evaluation_funct_(evaluation_funct),
        point_(point)
      {};

     private:
      EvaluationFunct evaluation_funct_;
      Tuple3 point_;

      virtual Attribute
      calculate_(
        const SimulationConfiguration::SimulationConfig & config,
        const ParticleTag & particle) const
      {
        const Attribute r_squared(abs_squared(
          config.particle_position(particle) - point_));
        return units_->reduce(evaluation_funct_(r_squared));
      }
    };

    template<class EvaluationFunct>
    class PseudoBondForce : public FieldForce{
     public:
      explicit
      PseudoBondForce(
        const ParticleType & particle_type,
        const Length & r_0,
        const EvaluationFunct & evaluation_funct):
        FieldForce(particle_type),
        r_0_(r_0),
        evaluation_funct_(evaluation_funct)
      {};

      PseudoBondForce(
        const ParticleType & particle_type,
        const Length & r_0,
        const EvaluationFunct & evaluation_funct,
        shared_ptr<ReducedUnitsSet> units):
        FieldForce(particle_type, units),
        r_0_(r_0),
        evaluation_funct_(evaluation_funct)
      {};

      void
      clear()
      {
        map_.clear();
      }

      void
      add_pseudo_bond(
        const ParticleTag & particle,
        const Tuple3 & position)
      {
        map_.insert(std::make_pair(particle, position));
      }

      void
      remove_pseudo_bonds(
        const ParticleTag & particle)
      {
        map_.erase(particle);
      }

      Length
      r_0()
      {
        return r_0_;
      }

      void
      set_r_0(
        const Length & r_0)
      {
        r_0_ = r_0;
      }

     private:
      Length r_0_;
      EvaluationFunct evaluation_funct_;

      typedef std::multimap<ParticleTag, Tuple3> PseudoBondMap_;
      PseudoBondMap_ map_;

      virtual Attribute
      calculate_(
        const SimulationConfiguration::SimulationConfig & config,
        const ParticleTag & particle) const
      {
        typedef PseudoBondMap_::const_iterator PBMConstIterator;
        typedef std::pair<PBMConstIterator, PBMConstIterator> PBMRange;
        PBMRange range(map_.equal_range(particle));
        Attribute result = 0;
        const Attribute r_0 = units_->reduce(r_0_);
        for(PBMConstIterator it = range.first; it != range.second; ++it)
        {
          result += units_->reduce(evaluation_funct_(
            //abs(config.particle_position(it->first) - it->second) - r_0));
            abs(config.particle(it->first).remapped_position() - it->second) - r_0));
        }
        return result;
      }
    };
        

    class BondForce: public IntraMolecularForce{
     public:
      BondForce(
        const BondType & bond_type):
        bond_type_(bond_type) {};
      BondForce(
        const BondType & bond_type,
        shared_ptr<ReducedUnitsSet> units):
        IntraMolecularForce(units),
        bond_type_(bond_type) {};
      virtual ~BondForce() {};

      virtual Attribute
      calculate(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const;

      virtual void
      init(
        const SimulationConfiguration::SimulationConfig & config) const {};

     protected:
      BondType bond_type_;

      virtual Attribute
      calculate_force_(
        const SimulationConfiguration::SimulationConfig & config,
        const BondTag bond) const;

      virtual Attribute
      evaluate(
        Attribute squared_distance) const = 0;
    };

    class AngleForce: public IntraMolecularForce{
     public:
      AngleForce(
        const AngleType & angle_type):
        angle_type_(angle_type) {};
      AngleForce(
        const AngleType & angle_type,
        shared_ptr<ReducedUnitsSet> units):
        IntraMolecularForce(units),
        angle_type_(angle_type) {};
      virtual ~AngleForce() {};

      virtual Attribute
      calculate(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const;

      virtual void
      init(
        const SimulationConfiguration::SimulationConfig & config) const {};

     protected:
      AngleType angle_type_;

      virtual Attribute
      calculate_force_(
        const SimulationConfiguration::SimulationConfig & config,
        const AngleTag angle) const;

      virtual Attribute
      evaluate(
        Attribute theta) const = 0;
    };

    class DihedralForce: public IntraMolecularForce{
     public:
      DihedralForce(
        const DihedralType & dihedral_type):
        dihedral_type_(dihedral_type) {};
      DihedralForce(
        const DihedralType & dihedral_type,
        shared_ptr<ReducedUnitsSet> units):
        IntraMolecularForce(units),
        dihedral_type_(dihedral_type) {};
      virtual ~DihedralForce() {};

      virtual Attribute
      calculate(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const;

      virtual void
      init(
        const SimulationConfiguration::SimulationConfig & config) const {};

     protected:
      DihedralType dihedral_type_;

      virtual Attribute
      calculate_force_(
        const SimulationConfiguration::SimulationConfig & config,
        const DihedralTag dihedral) const;

      virtual Attribute
      evaluate(
        Attribute squared_distance) const = 0;
    };

    class NeighborForce: public IntraMolecularForce{
     public:
      explicit NeighborForce(
       const ParticleType & particle_type,
       const unsigned int skip = 0,
       const unsigned int steps = 1):
       particle_type_(particle_type),
       skip_(skip),
       steps_(steps) { check_();}
      explicit NeighborForce(
       const ParticleType & particle_type,
       const unsigned int skip,
       const unsigned int steps,
       shared_ptr<ReducedUnitsSet> units):
       IntraMolecularForce(units),
       particle_type_(particle_type),
       skip_(skip),
       steps_(steps) { check_();}
      virtual ~NeighborForce() {};

      virtual void
      init(
        const SimulationConfiguration::SimulationConfig & config) const {};

      virtual Attribute
      calculate(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const;

     protected:
      ParticleType particle_type_;
      unsigned int skip_;
      unsigned int steps_;

      typedef SimulationConfiguration::SimulationConfig::TagIterator TagIterator;

      virtual Attribute
      calculate_force_(
       const SimulationConfiguration::SimulationConfig & config,
       TagSequenceConstIterator token,
       TagSequenceConstIterator neighborhood_last) const;

      virtual Attribute
      evaluate(
        Attribute squared_distance) const = 0;
     private:
      void
      check_() const;
    };

    // Actual forces

    class HarmonicBondForce: public BondForce{
      // 0.5 * c_0 * (r - r_0)^2
     public:
      HarmonicBondForce(
        const BondType & bond_type,
        const HarmonicBondParameters & parameters):
        BondForce(bond_type),
        parameters_(parameters) {};
      HarmonicBondForce(
        const BondType & bond_type,
        const HarmonicBondParameters & parameters,
        shared_ptr<ReducedUnitsSet> units):
        BondForce(bond_type, units),
        parameters_(parameters) {};
      HarmonicBondForce(
        const BondType & bond_type,
        const Length & r_0,
        const BondStretchingForce & c_0):
        BondForce(bond_type),
        parameters_(r_0, c_0) {};
      virtual ~HarmonicBondForce() {};

      void
      set_r_0(
        const Length & r_0)
      { parameters_.r_0 = r_0;}

      Length
      r_0() const
      { return parameters_.r_0;}

      void
      set_c_0(
        const BondStretchingForce & c_0)
      { parameters_.c_0 = c_0;}

      BondStretchingForce
      c_0() const
      { return parameters_.c_0;}

     private:
      HarmonicBondParameters parameters_;

      virtual Attribute
      evaluate(
        Attribute squared_distance) const;

    };

    class HarmonicAngleForce : public AngleForce{
      // 0.5 * k ( theta - theta_0)^2
     public:
      HarmonicAngleForce(
        const AngleType & angle_type,
        const HarmonicAngleParameters & parameters):
        AngleForce(angle_type),
        parameters_(parameters) {};
      HarmonicAngleForce(
        const AngleType & angle_type,
        const PlaneAngle & theta_0,
        const BondBendingForce & k):
        AngleForce(angle_type),
        parameters_(theta_0, k) {};
      virtual ~HarmonicAngleForce() {};

     private:
      HarmonicAngleParameters parameters_;

      virtual Attribute
      evaluate(
        Attribute theta) const;
    };

    class HarmonicDihedralForce : public DihedralForce{
      // 0.5 * k * (1 + d * cos(n * phi(r)))
      // phi is angle between two sides of the dihedral
      // k - strength of force (in energy units)
      // d - sign factor (unitless)
      // n - angle scaling factor (unitless)
     public:
      HarmonicDihedralForce(
        const DihedralType & dihedral_type,
        const HarmonicDihedralParameters & parameters):
        DihedralForce(dihedral_type),
        parameters_(parameters) {};
       HarmonicDihedralForce(
        const DihedralType & dihedral_type,
        const Energy & k,
        int d,
        Attribute n):
        DihedralForce(dihedral_type),
        parameters_(k, d, n) {};
       virtual ~HarmonicDihedralForce() {};

      private:
       HarmonicDihedralParameters parameters_;

       virtual Attribute
       evaluate(
         Attribute phi) const;
    };

    class LennardJonesPairforce : public Pairforce{
      /*
       * r <  r_cut: 4 * eps [(sigma / r)^12 - alpha * (sigma / r)^6]
       * r >= r_cut: 0
       */
     public:
      LennardJonesPairforce(
        const SortedTypePair & sorted_type_pair,
        const LJPairforceParameters & parameters,
        const Length & r_cut = - Length(),
        unsigned int exclude_bonded = 0):
        Pairforce(sorted_type_pair, exclude_bonded, r_cut),
        parameters_(parameters) {};
      LennardJonesPairforce(
        const SortedTypePair & sorted_type_pair,
        const LJPairforceParameters & parameters,
        const Length & r_cut,
        unsigned int exclude_bonded,
        shared_ptr<ReducedUnitsSet> reduced_units):
        Pairforce(sorted_type_pair, exclude_bonded, r_cut),
        parameters_(parameters) {};
      LennardJonesPairforce(
        const SortedTypePair & sorted_type_pair,
        const Energy & epsilon,
        const Length & sigma,
        const Attribute alpha,
        const Length & r_cut,
        unsigned int exclude_bonded = 0):
        Pairforce(sorted_type_pair, exclude_bonded, r_cut),
        parameters_(epsilon, sigma, alpha) {};
      virtual ~LennardJonesPairforce() {};

     private:
      LJPairforceParameters parameters_;

     public:
      virtual Attribute
      evaluate(Attribute squared_distance) const;
    };

    //template calculate_squared_distances_pbc<LennardJonesPairforce::evaluate>;


    class TabulatedPairforce: public Pairforce{
     public:
      TabulatedPairforce(
        const SortedTypePair & sorted_type_pair,
        const TabulatedPairforceParameters & parameters):
        Pairforce(sorted_type_pair),
        parameters_(parameters) {};
      TabulatedPairforce(
        const SortedTypePair & sorted_type_pair,
        const TabulatedPairforceParameters & parameters,
        unsigned int exclude_bonded,
        const Length r_cut):
        Pairforce(sorted_type_pair, exclude_bonded, r_cut),
        parameters_(parameters) {};
      TabulatedPairforce(
        const SortedTypePair & sorted_type_pair,
        const TabulatedPairforceParameters & parameters,
        unsigned int exclude_bonded,
        const Length r_cut,
        const Length r_skin):
        Pairforce(sorted_type_pair, exclude_bonded, r_cut, r_skin),
        parameters_(parameters) {};
      virtual ~TabulatedPairforce() {};

     private:
      TabulatedPairforceParameters parameters_;

     public:
      Attribute
      virtual evaluate(
        const Attribute squared_distance) const;
    };

    class TabulatedNeighborForce : public NeighborForce{
     public:
      explicit TabulatedNeighborForce(
        const ParticleType & particle_type,
        const unsigned int skip,
        const unsigned int steps,
        const TabulatedNeighborForceParameters & parameters):
        NeighborForce(particle_type, skip, steps),
        parameters_(parameters) {};
      explicit TabulatedNeighborForce(
        const ParticleType & particle_type,
        const unsigned int skip,
        const unsigned int steps,
        const TabulatedNeighborForceParameters & parameters,
        shared_ptr<ReducedUnitsSet> reduced_units):
        NeighborForce(particle_type, skip, steps),
        parameters_(parameters) {};
      explicit TabulatedNeighborForce(
        const ParticleType & particle_type,
        const unsigned int skip,
        const unsigned int steps,
        const TabulatedEnergy & table):
        NeighborForce(particle_type, skip, steps),
        parameters_(table) {};
      virtual ~TabulatedNeighborForce() {};

     private:
      TabulatedNeighborForceParameters parameters_;

     public:
      virtual Attribute
      evaluate(
        Attribute squared_distance) const;
    };

    class HarmonicNeighborForce : public NeighborForce{
    // 0.5 * c_0 * (r - r_0)^2
     public:
      explicit HarmonicNeighborForce(
        const ParticleType & particle_type,
        const unsigned int skip,
        const unsigned int steps,
        const HarmonicBondParameters & parameters):
        NeighborForce(particle_type, skip, steps),
        parameters_(parameters) {};
      explicit HarmonicNeighborForce(
        const ParticleType & particle_type,
        const unsigned int skip,
        const unsigned int steps,
        const HarmonicBondParameters & parameters,
        shared_ptr<ReducedUnitsSet> reduced_units):
        NeighborForce(particle_type, skip, steps),
        parameters_(parameters) {};
      explicit HarmonicNeighborForce(
        const ParticleType & particle_type,
        const unsigned int skip,
        const unsigned int steps,
        const Length & r_0,
        const BondStretchingForce & c_0):
        NeighborForce(particle_type, skip, steps),
        parameters_(r_0, c_0) {};
      virtual ~HarmonicNeighborForce() {};

     private:
      HarmonicBondParameters parameters_;

     public:
      virtual Attribute
      evaluate(
        Attribute squared_distance) const;
    };

    template<class ForceType>
    Forces
    determine_forces_from_type(
      const Forces & forces)
    {
      Forces determined_forces(forces.size());
      size_t i = 0;
      for(ForcesConstIterator force = forces.begin();
          force != forces.end(); ++force){
        if(dynamic_cast<ForceType *>(force->get()))
          determined_forces[i++] = * force;
      }
      determined_forces.resize(i);
      return determined_forces;
    }

  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_FORCES_H_
