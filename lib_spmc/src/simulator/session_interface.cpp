// =====================================================================================
//
//       Filename:  session_interface.cpp
//
//    Description:  Prepare all data, necessary to run a simulation.
//
//        Version:  1.0
//        Created:  01/20/2013 07:45:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "session_interface.hpp"
#include "session_modules.hpp"
#include "simulator.hpp"
#include "random_number_generation.hpp"

namespace mdst{

  namespace simulator{

    SessionInterface::SessionInterface():
      init_(          new interface::Init(this)),
      system_(        new interface::System(this)),
      dump_(          new interface::Dump(this)),
      forcefield_(    new interface::Forcefield(this)),
      moves_(         new interface::Moves(this))
    {
      // pass
    }

  } // namespace simulator

} // namespace mdst
