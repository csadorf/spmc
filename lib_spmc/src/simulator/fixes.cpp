//=====================================================================================
//
//       Filename:  fixes.cpp
//
//    Description:  Apply an operation during runtime.
//
//        Version:  1.0
//        Created:  09/09/13
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "fixes.hpp"
#include "session.hpp"
#include "simulator.hpp"
#include <utility/misc.hpp>
#include <utility/math.hpp>

#include <algorithm>

namespace mdst{

  namespace simulator{
    
    void
    DetectLowAcceptanceRatioFix::apply(
      Session & session)
    {
      RuntimeData & rt_data = * session.runtime_data();
      if ((session.current_step() / session.num_steps()) / 0.1 &&
          rt_data.acceptance_ratio <= threshold_){
        session.log(Session::LOG_MISC)
            << session.current_step() << ":"
            << "Breaking session. Configuration seems "
            << "to be equilibrated. "
            << "Acceptance ratio = " << rt_data.acceptance_ratio
            << std::endl;
        session.status_set(Session::STATUS_STOP);
      }
    }

    void
    DetectEquilibrationFix::init(
      unsigned int num_steps)
    {
      const size_t interval = std::max( static_cast<unsigned int>(2),
                                        num_steps / num_blocks_);
      energies_->resize(interval);
      sp_mean_covariance_.resize(interval);
      i_ = -1;
    }

    void
    DetectEquilibrationFix::apply(
      Session & session)
    {
      RuntimeData & rt_data = * session.runtime_data();
      unsigned int cycle = session.current_step();
      const size_t & interval = energies_->size();
      if (cycle > 0 && cycle % interval == 0)
      {
        i_ = -1;
// new implementation from here
        sp_mean_covariance_.clear();

        Attributes const & energies = * energies_;
        const size_t n = energies.size();
        const size_t n_split = n / 2;
        AttributesConstIterator split = energies.begin() + n_split;
        AttributesConstIterator last = energies.begin() + 2 * n_split;
        /*
        AttributesIterator even_last, odd_last;
        math::arrange_even_odd(
          energies.begin(), energies.end(),
          energies_even_.begin(), even_last,
          energies_odd_.begin(), odd_last);
        */
        const Attribute cov = 
          //math::covariance(energies_even_.begin(), even_last, energies_odd_.begin(), odd_last);
          math::covariance(energies.begin(), split, split, last);
        const Attribute var_a = 
          //math::variance(energies_even_.begin(), even_last);
          math::variance(energies.begin(), split);
        const Attribute var_b =
          //math::variance(energies_odd_.begin(), odd_last);
          math::variance(split, last);
        const Attribute r_squared = (cov * cov) / (var_a * var_b);
        print_iterable(energies.begin(), energies.end(), session.log(Session::LOG_MISC)) << std::endl;
        session.log(Session::LOG_MISC)
          << "n = " << n << std::endl
          << "r_squared = " << r_squared << std::endl
          << "cov = " << cov << std::endl
          << "var_a = " << var_a << std::endl
          << "var_b = " << var_b << std::endl
          << "mean = " << math::mean(energies.begin(), energies.end()) << std::endl
          //<< "mean_by_sitmo = " << sp_mean_covariance_.mean(1) << std::endl
          //<< "variance_by_sitmo = " << sp_mean_covariance_.var() << std::endl
        ;
        if (var_a == var_b || r_squared < threshold_)
        {
          session.log(Session::LOG_MISC)
            << "Energy seems to be uncorrelated. "
            << "Aborting simulation." << std::endl
          ;
          session.status_set(Session::STATUS_STOP);
        }
// end of new implementation
        /*
        last_std_dev_ = current_std_dev_;
        const Attribute mu = mean(energies_->begin(), energies_->end());
        current_std_dev_ = sqrt(variance(energies_->begin(), energies_->end(), mu));
        Attribute delta = std::abs(last_std_dev_ - current_std_dev_);
        if (last_std_dev_ >= 0 && (delta / last_std_dev_ <= threshold_))
        {
          session.log(Session::LOG_MISC)
              << rt_data.cycle << ": "
              << "Configuration appears to be equilibrated."
              << std::endl
              << "Standard deviance of energy = " << current_std_dev_
              << std::endl;
          session.status_set(Session::STATUS_STOP);
        }
        */
      }
      (*energies_)[++i_] = rt_data.energy_total();
      /*
      double * e = new double;
      *e = rt_data.energy_total();
      sp_mean_covariance_.add(e);
      delete e;
      */
    }


  } // namespace simulator

} // namespace mdst
