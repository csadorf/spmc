//=====================================================================================
//
//       Filename:  field_potential.hpp
//
//    Description:  Field potentials.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_POTENTIAL_FIELD_POTENTIAL_H_
#define MDST_SIMULATOR_POTENTIAL_FIELD_POTENTIAL_H_

#include <simulator/forces.hpp>

namespace mdst{

  namespace simulator{

    class CutOffEvaluator
    {
     public:

      explicit
      CutOffEvaluator(
        const Attribute cut_off_,
        const Energy & epsilon_):
        cut_off(cut_off_),
        epsilon(epsilon_) {};

      Energy
      operator()(
        const Attribute d) const
      {
        if (d <= cut_off)
          return epsilon;
        else 
          return Energy(0);
      }

      Attribute cut_off;
      Energy epsilon;
    };

    /// Evaluation in the form of 
    /// r <  r_cut: epsilon * r^d
    class PolynomialEvaluator
    {
     public:

      explicit
      PolynomialEvaluator(
       const int d_,
       const Attribute cut_off_,
       const Energy & epsilon_):
       d(d_),
       cut_off(cut_off_),
       epsilon(epsilon_)
      {};

      Energy
      operator()(
        const Attribute r_squared) const
      {
        const Attribute r(sqrt(r_squared));
        if (r <= cut_off){
          return epsilon * pow(r, d);
        } else {
          return Energy(0);
        }
      }

      int d;
      Attribute cut_off;
      Energy epsilon;
    };

    typedef HesseFieldForce<CutOffEvaluator> HardFieldPotential_;
    class HardFieldPotential : public HardFieldPotential_
    {
     public:
      HardFieldPotential(
        const ParticleType & particle_type,
        const Tuple3 & normal_vector,
        const Attribute & distance_from_origin,
        const Attribute & cut_off,
        const Energy & epsilon):
      HardFieldPotential_(
        particle_type,
        normal_vector,
        distance_from_origin,
        CutOffEvaluator(cut_off, epsilon))
      {};
    };

    typedef PointFieldForce<PolynomialEvaluator> PolynomialPointField_;
    class PolynomialPointPotential : public PolynomialPointField_
    {
     public:
      PolynomialPointPotential(
        const ParticleType & particle_type,
        const Tuple3 & point,
        const int d,
        const Attribute cut_off,
        const Energy & epsilon):
        PolynomialPointField_(
          particle_type,
          point,
          PolynomialEvaluator(d, cut_off, epsilon))
        {};
    };

    typedef PseudoBondForce<PolynomialEvaluator> PolynomialPseudoBondField_;
    class PolynomialPseudoBondPotential : public PolynomialPseudoBondField_
    {
     public:
      PolynomialPseudoBondPotential(
        const ParticleType & particle_type,
        const Length & r_0,
        const int d,
        const Attribute cut_off,
        const Energy & epsilon):
        PolynomialPseudoBondField_(
          particle_type,
          r_0,
          PolynomialEvaluator(d, cut_off, epsilon))
        {};
    };

  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_POTENTIAL_FIELD_POTENTIAL_H_
