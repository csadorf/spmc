//=====================================================================================
//
//       Filename:  hard_sphere.hpp
//
//    Description:  Define forces.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_POTENTIAL_HARD_SPHERE_H_
#define MDST_SIMULATOR_POTENTIAL_HARD_SPHERE_H_

#include <simulator/forces.hpp>

namespace mdst{

  namespace simulator{
    
    const Attribute EPSILON = 10e-13;
    
    class HardSpherePotential : public Pairforce
    {
     public:
      
      explicit
      HardSpherePotential(
        const SortedTypePair & sorted_type_pair,
        const Energy & epsilon):
        Pairforce(
          sorted_type_pair),
        epsilon_(epsilon)
        {};

      explicit
      HardSpherePotential(
        const SortedTypePair & sorted_type_pair,
        const Energy & epsilon,
        unsigned int exclude_bonded,
        const Length & r_cut):
        Pairforce(
          sorted_type_pair,
          exclude_bonded,
          r_cut),
        epsilon_(epsilon)
        {};

      explicit
      HardSpherePotential(
        const SortedTypePair & sorted_type_pair,
        const Energy & epsilon,
        unsigned int exclude_bonded,
        const Length & r_cut,
        const Length & r_skin):
        Pairforce(
          sorted_type_pair,
          exclude_bonded,
          r_cut,
          r_skin),
        epsilon_(epsilon)
        {};

      virtual ~HardSpherePotential() {};

      /// r <  r_cut: eps
      /// r >= r_cut: 0
      virtual Attribute
      evaluate(
        Attribute squared_distance) const
      {
        const Attribute squared_r_cut =
          units_->reduce(r_cut()) * units_->reduce(r_cut());
        if (squared_distance < squared_r_cut)
          //if (std::abs(squared_distance - squared_r_cut) > EPSILON)
            return units_->reduce(epsilon_);
        return 0;
      }

     private:
      const Energy epsilon_;
    };

  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_POTENTIAL_HARD_SPHERE_H_
