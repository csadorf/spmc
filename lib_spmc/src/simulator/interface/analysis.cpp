// =====================================================================================
//
//       Filename:  analysis.cpp
//
//    Description:  A python analysis interface.
//
//        Version:  1.0
//        Created:  01/19/2013 07:26:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <memory.hpp>
#include <types.hpp>
#include <session.hpp>
#include <session_utils.hpp>
#include <simulator.hpp>
#include <config_analysis.hpp>
#include <energy_calculation.hpp>
#include <computes.hpp>

#include <boost/python.hpp>
 
namespace mdst{

  namespace simulator{

    struct Analysis{

      static Attribute
      potential_energy(
          Session & session,
          const TagSet & subset){
        //session.run(0);
        TagSequence sub_sequence(vector_from_set(subset));
        return calculate_potential_energy(
          * session.config(),
          session.forces(),
          sub_sequence.begin(),
          sub_sequence.end()
          );
      }

      static Attribute
      potential_energy(
          Session & session){
        return potential_energy(
          session,
          session.config()->all());
      }

      static Attribute
      inter_molecular_potential_energy(
          Session & session,
          const TagSet & subset){
        TagSequence sub_sequence(vector_from_set(subset));
        return calculate_inter_molecular_potential_energy(
          * session.config(),
          session.forces(),
          sub_sequence.begin(),
          sub_sequence.end());
      }

      static Attribute
      inter_molecular_potential_energy(
          Session & session){
        return inter_molecular_potential_energy(
          session,
          session.config()->all());
      }

      static Attribute
      intra_molecular_potential_energy(
          Session & session,
          const TagSet & subset){
        TagSequence sub_sequence(vector_from_set(subset));
        return calculate_intra_molecular_potential_energy(
          * session.config(),
          session.forces(),
          sub_sequence.begin(),
          sub_sequence.end());
      }

      static Attribute
      intra_molecular_potential_energy(
          Session & session){
        return intra_molecular_potential_energy(
          session,
          session.config()->all());
      }

      static Attribute
      mean_squared_distance(
          const SimulationConfiguration::SimulationConfig & config_a,
          const SimulationConfiguration::SimulationConfig & config_b){
        return ::mdst::analysis::mean_squared_distance(config_a, config_b);
      }

      static Attribute
      osmotic_pressure(
        const Session & session)
      {
        shared_ptr<OsmoticPressureCompute> osm_compute
          = find_compute<OsmoticPressureCompute>(session);
        if (osm_compute)
        {
          //osm_compute->evaluate(session);
          return osm_compute->last_result_as_value();
        }
        else
          throw RuntimeError("No osmotic pressure compute installed.");
      }

      static Tuple3
      minimal_position(
        const SimulationConfiguration::SimulationConfig & config,
        const TagSet & subset)
      {return ::mdst::analysis::minimal_position(config, subset);}

      static Tuple3
      minimal_position(
        const SimulationConfiguration::SimulationConfig & config)
      {return ::mdst::analysis::minimal_position(config);}

      static Tuple3
      maximal_position(
        const SimulationConfiguration::SimulationConfig & config,
        const TagSet & subset)
      {return ::mdst::analysis::maximal_position(config, subset);}

      static Tuple3
      maximal_position(
        const SimulationConfiguration::SimulationConfig & config)
      {return ::mdst::analysis::maximal_position(config);}

      static Tuple3
      dimension(
        const SimulationConfiguration::SimulationConfig & config,
        const TagSet & subset)
      {return ::mdst::analysis::dimension(config, subset);}

      static Tuple3
      dimension(
        const SimulationConfiguration::SimulationConfig & config)
      {return ::mdst::analysis::dimension(config);}

      static Attribute
      volume(
        const SimulationConfiguration::SimulationConfig & config,
        const TagSet & subset)
      {return ::mdst::analysis::volume(config, subset);}

      static Attribute
      volume(
        const SimulationConfiguration::SimulationConfig & config)
      {return ::mdst::analysis::volume(config);}

      static Attribute
      box_volume(
        const SimulationConfiguration::SimulationConfig & config)
      {return ::mdst::analysis::box_volume(config);}

      static Tuple3
      center_of_mass(
        const SimulationConfiguration::SimulationConfig & config,
        const TagSet & subset)
      {return ::mdst::analysis::center_of_mass(config, subset);}

      static Tuple3
      center_of_mass(
        const SimulationConfiguration::SimulationConfig & config)
      {return ::mdst::analysis::center_of_mass(config);}

      static Attribute
      number_density(
        const SimulationConfiguration::SimulationConfig & config,
        const TagSet & subset)
      {return ::mdst::analysis::number_density(config, subset);}

      static Attribute
      number_density(
        const SimulationConfiguration::SimulationConfig & config)
      {return ::mdst::analysis::number_density(config);}

      static Attribute
      density(
        const SimulationConfiguration::SimulationConfig & config,
        const TagSet & subset)
      {return ::mdst::analysis::density(config, subset);}

      static Attribute
      density(
        const SimulationConfiguration::SimulationConfig & config)
      {return ::mdst::analysis::density(config);}
      
      static AttributeMap
      radial_distribution_function(
        const SimulationConfiguration::SimulationConfig & config)
      {return ::mdst::analysis::radial_distribution_function(config);}

    };
        
    namespace pythoninterface{

      using namespace boost::python;
      using namespace SimulationConfiguration;

      BOOST_PYTHON_FUNCTION_OVERLOADS(potential_energy_ol, Analysis::potential_energy, 1, 2)
      BOOST_PYTHON_FUNCTION_OVERLOADS(inter_molecular_potential_energy_ol, Analysis::inter_molecular_potential_energy, 1,2 )
      BOOST_PYTHON_FUNCTION_OVERLOADS(intra_molecular_potential_energy_ol, Analysis::intra_molecular_potential_energy, 1,2 )
      BOOST_PYTHON_FUNCTION_OVERLOADS(minimal_position_ol, Analysis::minimal_position, 1, 2)
      BOOST_PYTHON_FUNCTION_OVERLOADS(maximal_position_ol, Analysis::maximal_position, 1, 2)
      BOOST_PYTHON_FUNCTION_OVERLOADS(dimension_ol, Analysis::dimension, 1, 2)
      BOOST_PYTHON_FUNCTION_OVERLOADS(volume_ol, Analysis::volume, 1, 2)
      BOOST_PYTHON_FUNCTION_OVERLOADS(center_of_mass_ol, Analysis::center_of_mass, 1, 2)
      BOOST_PYTHON_FUNCTION_OVERLOADS(number_density_ol, Analysis::number_density, 1, 2)
      BOOST_PYTHON_FUNCTION_OVERLOADS(density_ol, Analysis::density, 1, 2)

      Attribute (*pe)(Session &) = & Analysis::potential_energy;
      Attribute (*pe_sub)(Session &, const TagSet &) =
        & Analysis::potential_energy;

      BOOST_PYTHON_MODULE(libanalysis)
      {
        scope outer
          = class_<Analysis>("Analysis")
        .def("potential_energy", static_cast<Attribute(*)
            (Session &, const TagSet &)>
            (& Analysis::potential_energy),
            potential_energy_ol(args("session", "subset"), "Calculate potential energy."))
        .staticmethod("potential_energy")
        .def("inter_molecular_potential_energy", static_cast<Attribute(*)
            (Session &, const TagSet &)>
            (& Analysis::inter_molecular_potential_energy),
            inter_molecular_potential_energy_ol(args("session", "subset"),
            "Calculate inter molecular potential energy."))
        .staticmethod("inter_molecular_potential_energy")
        .def("intra_molecular_potential_energy", static_cast<Attribute(*)
            (Session &, const TagSet &)>
            (& Analysis::intra_molecular_potential_energy),
            intra_molecular_potential_energy_ol(args("session", "subset"),
            "Calculate intra molecular potential energy."))
        .staticmethod("intra_molecular_potential_energy")
        .def("mean_squared_distance", & Analysis::mean_squared_distance)
        .staticmethod("mean_squared_distance")
        .def("osmotic_pressure", & Analysis::osmotic_pressure)
        .staticmethod("osmotic_pressure")
        .def("minimal_position", static_cast<Tuple3(*)
            (const SimulationConfig &, const TagSet &)>
            (& Analysis::minimal_position),
            minimal_position_ol(args("config", "subset"), "Determine minimal position."))
        .staticmethod("minimal_position")
        .def("maximal_position", static_cast<Tuple3(*)
            (const SimulationConfig &, const TagSet &)>
            (& Analysis::maximal_position),
            maximal_position_ol(args("config", "subset"), "Determine maximal position."))
        .staticmethod("maximal_position")
        .def("dimension", static_cast<Tuple3(*)
            (const SimulationConfig &, const TagSet &)>
            (& Analysis::dimension),
            dimension_ol(args("config", "subset"), "Determine dimension."))
        .staticmethod("dimension")
        .def("volume", static_cast<Attribute(*)
            (const SimulationConfig &, const TagSet &)>
            (& Analysis::volume),
            volume_ol(args("config", "subset"), "Calculate volume."))
        .staticmethod("volume")
        .def("box_volume", & Analysis::box_volume, args("config"))
        .staticmethod("box_volume")
        .def("center_of_mass", static_cast<Tuple3(*)
            (const SimulationConfig &, const TagSet &)>
            (& Analysis::center_of_mass),
            center_of_mass_ol(args("config", "subset"), "Calculate center of mass."))
        .staticmethod("center_of_mass")
        .def("number_density", static_cast<Attribute(*)
            (const SimulationConfig &, const TagSet &)>
            (& Analysis::number_density),
            number_density_ol(args("config", "subset"), "Calculate number density."))
        .staticmethod("number_density")
        .def("density", static_cast<Attribute(*)
            (const SimulationConfig &, const TagSet &)>
            (& Analysis::density),
            density_ol(args("config", "subset"), "Calculate density."))
        .staticmethod("density")
        .def("radial_distribution_function", static_cast<AttributeMap(*)
            (const SimulationConfig &)>
            (& Analysis::radial_distribution_function),
            arg("config"))
        .staticmethod("radial_distribution_function")
        /*
        .def("evaluate_compute_scalar", & Analysis::evaluate_compute_scalar)
        .staticmethod("evaluate_compute_scalar")
        .def("evaluate_compute_tuple", & Analysis::evaluate_compute_tuple)
        .staticmethod("evaluate_compute_tuple")
        */
        ;
      }

    } // namespace pythoninterface

  } // namespace simulator

} // namespace mdst
