// =====================================================================================
//
//       Filename:  units.cpp
//
//    Description:  Provides a python interface to reduced units.
//
//        Version:  1.0
//        Created:  01/19/2013 07:26:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <simulation_config/reduced_units.hpp>
#include <memory.hpp>

#include <boost/python.hpp>
 
namespace mdst{

  namespace simulator{

    namespace pythoninterface{

      using namespace boost::python;
      using namespace units;
      using reduced_units::ReducedUnitsSet;

      BOOST_PYTHON_MODULE(libreduced_units)
      {

        class_<Length>("Length");
        class_<Area>("Area");
        class_<Mass>("Mass");
        class_<Energy>("Energy");
        class_<PlaneAngle>("PlaneAngle");
        class_<BondStretchingForce>("BondStretchingForce");
        class_<BondBendingForce>("BondBendingForce");

        class_<ReducedUnitsSet, shared_ptr<ReducedUnitsSet> >("ReducedUnitsSet")
        .def(init<Length, Mass, Energy>())
        .def("length", & ReducedUnitsSet::expand_length)
        .def("area", & ReducedUnitsSet::expand_area)
        .def("mass", & ReducedUnitsSet::expand_mass)
        .def("energy", & ReducedUnitsSet::expand_energy)
        .def("plane_angle", & ReducedUnitsSet::expand_plane_angle)
        .def("bond_stretching_force", & ReducedUnitsSet::expand_bond_stretching_force)
        .def("bond_bending_force", & ReducedUnitsSet::expand_bond_bending_force)
        ;

        scope().attr("argon") = reduced_units::argon;

      }

    } // namespace pythoninterface

  } // namespace simulator

} // namespace mdst
