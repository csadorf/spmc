// =====================================================================================
//
//       Filename:  interface.cpp
//
//    Description:  Provides a python interface to simulator.
//
//        Version:  1.0
//        Created:  01/19/2013 07:26:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

//#include <boost/shared_ptr.hpp>

#include <session.hpp>
#include <simulator.hpp>
#include <mc_sampler.hpp>
#include <mc_nvt_sampler.hpp>
#include <forces.hpp>
#include <potentials.hpp>
#include <parameters.hpp>
#include <reduced_units.hpp>
#include <tuple3_arithmetic.hpp>
#include <property_view.hpp>
#include <simulation_config/config_analysis.hpp>
#include <reverse_mapping/reverse_mapping_library.hpp>
#include <analyzer.hpp>

#include <boost/python.hpp>
#include <boost/python/exception_translator.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <indexing_suite/set.hpp>
#include <indexing_suite/vector.hpp>
#include <indexing_suite/map.hpp>
 
namespace mdst{

  namespace simulator{

    namespace pythoninterface{

      void
      translate_exception(const Exception & e)
      {
        PyErr_SetString(PyExc_RuntimeError, e.what());
      }

      void
      translate_warning(const RuntimeWarning & w)
      {
        PyErr_SetString(PyExc_RuntimeWarning, w.what());
      }

      using namespace boost::python;
      using reverse_mapping::ReverseMappingLibrary;
      using reverse_mapping::Structure3;
      using reverse_mapping::ReverseMappingStructure3;
      using SimulationConfiguration::SimulationConfig;
      using SimulationConfiguration::PropertyView;
      using SimulationConfiguration::ParticleView;
      using SimulationConfiguration::BondView;
      using SimulationConfiguration::AngleView;
      using SimulationConfiguration::DihedralView;

      BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(
        session_init_ol, Session::init, 0, 1)
      BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(
        session_run_ol, Session::run, 1, 2)
      BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(
        session_run_upto_ol, Session::run_upto, 1, 2)
      BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(
        session_warn_on_id_none_ol,
        Session::warn_on_id_none, 0, 1)
      BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(
        session_dumper_add_by_id_ol,
        Session::dumper_add_by_id, 2, 3)
      BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(
        simconfig_append_ol, 
        SimulationConfig::append, 1, 2)
      BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(
        property_view_defined_walks_ol, 
        PropertyView::generate_defined_walks, 0, 1);
      BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(
        property_view_unique_defined_walks_ol,
        PropertyView::generate_unique_defined_walks, 0, 1)
      BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(
        property_view_unique_unified_walk_ol,
        PropertyView::generate_unique_unified_walk, 0, 1)

      struct tuple3_pickle_suite : boost::python::pickle_suite
      {
        static
        boost::python::tuple
        getinitargs(const Tuple3 & t)
        {
          return boost::python::make_tuple(t.x(), t.y(), t.z());
        }
      };


      BOOST_PYTHON_MODULE(libinterface)
      {
        docstring_options local_docstring_options(true, true, false);

        register_exception_translator<Exception>(& translate_exception);
        register_exception_translator<RuntimeWarning>(& translate_warning);
        
        class_< TagSet>(
                //shared_ptr<TagSet> >(
          "TagSet",
          "A set of tags, which may refer to a collection particles, bonds, etc..")
        .def(indexing::set_suite<TagSet>())
        ;

        /*
        class_< TagSet,
                shared_ptr<TagSet> >(
          "TagSetPtr")
        .def(indexing::set_suite<TagSet>())
        ;*/

        class_<TagVector>(
          "TagVector",
          "A list of tags, which may refer to a indexed molecule etc.")
        .def(indexing::vector_suite<TagVector>())
        ;

        class_<TagVectorSet>(
          "TagVectorSet",
          "A set of TagVectors,")
        .def(indexing::set_suite<TagVectorSet>())
        ;

        class_<TagSetSet>(
          "TagSetSet")
        .def(indexing::set_suite<TagSetSet>())
        ;

        class_<Attributes>(
          "Attributes")
        .def(indexing::vector_suite<Attributes>())
        ;

        class_<Strings>(
          "Strings")
        .def(indexing::vector_suite<Strings>())
        ;

        class_<Type>("Type", init<Type>());
        class_<BondType>("BondType", init<Type>());
        class_<AngleType>("AngleType", init<Type>());
        class_<DihedralType>("DihedralType", init<Type>());

        class_<SortedTypePair>("SortedTypePair",
          init<const std::string &, const std::string &>())
        ;

        class_< AttributeMap,
                shared_ptr<AttributeMap> >("AttributeMap")
        .def(
          map_indexing_suite<AttributeMap, true>())
        ;

        class_< TagSetMap,
                shared_ptr<TagSetMap> >("TagSetMap")
        .def(map_indexing_suite<TagSetMap, true>())
        ;

        class_<Tuple3>("Tuple3", "A 3-tuple of attributes.", init<>())
        .def(init<Attribute, Attribute, Attribute>())
        .def(init<Attribute>())
        .def(init<const Tuple3 &>())
        .def_pickle(tuple3_pickle_suite())
        .add_property("x", & Tuple3::x, & Tuple3::set_x)
        .add_property("y", & Tuple3::y, & Tuple3::set_y)
        .add_property("z", & Tuple3::z, & Tuple3::set_z)
        .def("abs_squared", & Tuple3::abs_squared_)
        .def(self_ns::str(self_ns::self))
        .def(self_ns::abs(self_ns::self))
        // interoperate with self
        .def(self_ns::self + self_ns::self)
        .def(self_ns::self - self_ns::self)
        .def(self_ns::self * self_ns::self)
        .def(self_ns::self == self_ns::self)
        .def(self_ns::self != self_ns::self)
        // interoperate with Attribute
        .def(Attribute() * self_ns::self)
        .def(self_ns::self * Attribute())
        .def(self_ns::self / Attribute())
        .def(self_ns::str(self_ns::self))
        .def(self_ns::repr(self_ns::self))
        ;

        class_< PropertyView,
                boost::noncopyable>
//                shared_ptr<PropertyView> >
                ("PropertyView", no_init)
        .def( "molecules", 
              & PropertyView::molecules,
              "Return a set of set of tags, where each sets holds all tags "
              "of one molecule")
        .def( "molecule",
              & PropertyView::molecule,
              "Return a set of tags containing all tags, which are in the same "
              "molecule as 'tag'.")
        .def( "defined_walks",
              static_cast<Walks(PropertyView::*)(const TagSet &) const>
                (& PropertyView::generate_defined_walks),
              property_view_defined_walks_ol(args("molecule")))
        .def( "unique_defined_walks", 
              static_cast<Walks(PropertyView::*)(const TagSet &) const>
                (& PropertyView::generate_unique_defined_walks),
              property_view_unique_defined_walks_ol(args("molecule")))
        .def( "unique_unified_walk", 
              static_cast<TagVector(PropertyView::*)(const TagSet &) const>
                (& PropertyView::generate_unique_unified_walk),
              property_view_unique_unified_walk_ol(args("molecule")))
        ;

        class_< ParticleView>
                ("ParticleView", no_init)
        .def("clone", & ParticleView::clone)
        .add_property("type",
                      & ParticleView::type,
                      & ParticleView::set_type)
        .add_property("position",
                      & ParticleView::position,
                      & ParticleView::set_position)
        .add_property("image",
                      & ParticleView::image,
                      & ParticleView::set_image)
        .add_property("mass",
                      & ParticleView::mass,
                      & ParticleView::set_mass)
        .add_property("velocity",
                      & ParticleView::velocity,
                      & ParticleView::set_velocity)
        .add_property("acceleration",
                      & ParticleView::acceleration,
                      & ParticleView::set_acceleration)
        .def("remapped_position", & ParticleView::remapped_position)
        .def("reduced_position", & ParticleView::reduced_position)
        .def("set_reduced_position", & ParticleView::set_reduced_position)
        ;

        class_< BondView>
                ("BondView", no_init)
        .add_property("type",
                      & BondView::type,
                      & BondView::set_type)
        .add_property("a", & BondView::a, & BondView::set_a)
        .add_property("b", & BondView::b, & BondView::set_b)
        ;

        class_< AngleView>
                ("AngleView", no_init)
        .add_property("type",
                      & AngleView::type,
                      & AngleView::set_type)
        .add_property("a", & AngleView::a, & AngleView::set_a)
        .add_property("b", & AngleView::b, & AngleView::set_b)
        .add_property("c", & AngleView::c, & AngleView::set_c)
        ;

        class_< DihedralView>
                ("DihedralView", no_init)
        .add_property("type",
                      & DihedralView::type,
                      & DihedralView::set_type)
        .add_property("a", & DihedralView::a, & DihedralView::set_a)
        .add_property("b", & DihedralView::b, & DihedralView::set_b)
        .add_property("c", & DihedralView::c, & DihedralView::set_c)
        .add_property("d", & DihedralView::d, & DihedralView::set_d)
        ;

        
        ParticleView (SimulationConfig::*particle_non_const)
          (const Tag &) 
          = & SimulationConfig::particle;
        BondView (SimulationConfig::*bond_non_const)(const Tag &)
          = & SimulationConfig::bond;
        AngleView (SimulationConfig::*angle_non_const)(const Tag &)
          = & SimulationConfig::angle;
        DihedralView (SimulationConfig::*dihedral_non_const)(const Tag &)
          = & SimulationConfig::dihedral;
        TagSet (SimulationConfig::*group_by_type)
          (const ParticleType &) const
          = & SimulationConfig::group;
        TagSet (SimulationConfig::*group_by_tag)
          (const ParticleTag &, const ParticleTag &) const
          = & SimulationConfig::group;
        TagSet (SimulationConfig::*group_by_position)
          (const Tuple3 &, const Tuple3 &) const
          = & SimulationConfig::group;
        
        class_< SimulationConfig, 
                shared_ptr<SimulationConfig> >
                ("SimulationConfig", init<>())
        .def(init<const SimulationConfig &>())
        .def(init<const SimulationConfig &, const TagSet &>())
        .def("append", static_cast<TagSet(SimulationConfig::*)
            (const SimulationConfig &, bool)>
            (& SimulationConfig::append),
            simconfig_append_ol())
        .add_property("num_atoms",
                      & SimulationConfig::num_atoms,
                      & SimulationConfig::set_num_atoms)
        .def("num_atom_types", & SimulationConfig::num_atom_types)
        .add_property("num_bonds",
                      & SimulationConfig::num_bonds)
        .def("num_bond_types", & SimulationConfig::num_bond_types)
        .add_property("num_angles",
                      & SimulationConfig::num_angles)
        .def("num_angle_types", & SimulationConfig::num_angle_types)
        .add_property("num_dihedrals",
                      & SimulationConfig::num_dihedrals)
        .def("num_dihedral_types", & SimulationConfig::num_dihedral_types)
        .add_property("num_impropers",
                      & SimulationConfig::num_impropers)
        .def("num_improper_types", & SimulationConfig::num_improper_types)
        .def("all", & SimulationConfig::all)
        .def("group_by_type", group_by_type)
        .def("group_by_tag", group_by_tag)
        .def("group_by_position", group_by_position)
        .add_property("box",  & SimulationConfig::box,
                              & SimulationConfig::set_box)
        .def("lock_particle", &  SimulationConfig::lock_particle)
        .def("unlock_particle", & SimulationConfig::unlock_particle)
        .def("locked_particles", & SimulationConfig::locked_particles)
        .add_property(
          "particles_locked_silent",
          & SimulationConfig::particles_locked_silent,
          & SimulationConfig::set_particles_locked_silent)
        .def("stretch_box", & SimulationConfig::stretch_box)
        .def_readonly("property", & SimulationConfig::property)
        .def("particle", particle_non_const)
        .def("bond", bond_non_const)
        .def("bonds", & SimulationConfig::bonds)
        .def("angle", angle_non_const)
        .def("angles", & SimulationConfig::angles)
        .def("dihedral", dihedral_non_const)
        .def("dihedrals", & SimulationConfig::dihedrals)
        .def("check", & SimulationConfig::check)
        .def( "make_bond", & SimulationConfig::make_bond)
        .def( "bond_all_particles", 
              & SimulationConfig::bond_all_particles)
        .def( "generate_angles",
              & SimulationConfig::generate_angles)
        .def( "generate_dihedrals",
              & SimulationConfig::generate_dihedrals)
        .add_property(
          "developer_mode",
          & SimulationConfig::developer_mode,
          & SimulationConfig::set_developer_mode)
        .def( "add_bond", & SimulationConfig::add_bond)
        .def( "update_internal",
              & SimulationConfig::update_internal)
        ;

        class_<TabulatedEnergy>("TabulatedEnergy")
        .def(map_indexing_suite<TabulatedEnergy>())
        ;

        class_< Simulator,
                boost::noncopyable,
                shared_ptr<Simulator> >("Simulator", no_init)
        .def("run", & Simulator::run)
        ;

        class_< MonteCarloSampler,
                bases<Simulator>,
                boost::noncopyable,
                shared_ptr<MonteCarloSampler> >("MonteCarloSampler", no_init)
        ;

        class_< MonteCarloNVTSampler,
                bases<MonteCarloSampler>,
                shared_ptr<MonteCarloNVTSampler> >("MonteCarloNVTSampler", init<const Attribute>())
        .add_property("beta",
                      & MonteCarloNVTSampler::beta,
                      & MonteCarloNVTSampler::set_beta)
        ;

        class_<Lengths>("Lengths")
        .def(map_indexing_suite<Lengths>())
        ;

        class_<Angles>("Angles")
        .def(map_indexing_suite<Angles>())
        ;

        class_< MonteCarloMove,
                boost::noncopyable,
                shared_ptr<MonteCarloMove> >("MonteCarloMove", no_init)
        .def("execute", & MonteCarloMove::execute)
        .def("set_frequency", & MonteCarloMove::set_frequency)
        .def("frequency", & MonteCarloMove::frequency)
        ;

        class_< RandomDisplacementMove,
                bases<MonteCarloMove>,
                shared_ptr<RandomDisplacementMove> >
                ("RandomDisplacementMove", init<unsigned int>())
        .def(init<unsigned int, Attribute>())
        ;

        class_< RotateBeadsMove,
                bases<MonteCarloMove>,
                shared_ptr<RotateBeadsMove> >
                ("RotateBeadsMove", init<unsigned int>())
        .def(init<unsigned int, Attribute>())
        ;

        class_< ChangeBondLengthMove,
                bases<MonteCarloMove>,
                shared_ptr<ChangeBondLengthMove> >
                ("ChangeBondLengthMove", init<unsigned int>())
        .def(init<unsigned int, Attribute>())
        ;

        class_< TranslateJiggleMove,
                bases<MonteCarloMove>,
                shared_ptr<TranslateJiggleMove> >
                ("TranslateJiggleMove", init<unsigned int>())
        .def(init<unsigned int, Attribute>())
        .add_property(
          "do_reverse",
          & TranslateJiggleMove::do_reverse,
          & TranslateJiggleMove::set_do_reverse)
        .add_property(
          "section",
          & TranslateJiggleMove::section,
          & TranslateJiggleMove::set_section)
        ;

        class_< TranslateJiggleBiasedMove,
                bases<MonteCarloMove>,
                shared_ptr<TranslateJiggleBiasedMove> >
                ("TranslateJiggleBiasedMove", init<unsigned int>())
        .def(init<unsigned int, Attribute>())
        .add_property(
          "do_reverse",
          & TranslateJiggleBiasedMove::do_reverse,
          & TranslateJiggleBiasedMove::set_do_reverse)
        .def("set_parameters", & TranslateJiggleBiasedMove::set_parameters)
        ;
        
        class_< ChangeBondLengthBiasedMove,
                bases<MonteCarloMove>,
                shared_ptr<ChangeBondLengthBiasedMove> >
                ("ChangeBondLengthBiasedMove", init<unsigned int>())
        .def(init<unsigned int, Attribute>())
        .def("set_parameters", & ChangeBondLengthBiasedMove::set_parameters)
        ;

        class_< TranslateMoleculeMove,
                bases<MonteCarloMove>,
                shared_ptr<TranslateMoleculeMove> >
                ("TranslateMoleculeMove", init<unsigned int>())
        .def(init<unsigned int, Attribute>())
        ;

        class_< RotateMoonMove,
                bases<MonteCarloMove>,
                shared_ptr<RotateMoonMove> >
                ("RotateMoonMove", init<Type, Type, unsigned int>())
        .def(init<Type, Type, unsigned int, Attribute>())
        ;

        class_< CrankShaftMove,
                bases<MonteCarloMove>,
                shared_ptr<CrankShaftMove> >
                ("CrankShaftMove", init<unsigned int>())
        .def(init<unsigned int, Attribute>())
        ;

        class_< Force,
                boost::noncopyable,
                shared_ptr<Force> >("Force", no_init)
        .def("calculate", & Force::calculate)
        ;

        class_<Forces>("Forces")
//        .def(indexing::vector_suite<Forces>())
        .def(vector_indexing_suite<Forces>())
        ;


        class_< Pairforce,
                bases<Force>,
                boost::noncopyable,
                shared_ptr<Pairforce> >("Pairforce", no_init)
        ;

        class_< NeighborForce,
                bases<Force>,
                boost::noncopyable,
                shared_ptr<NeighborForce> >("NeighborForce", no_init)
        ;

        class_< FieldForce,
                bases<Force>,
                boost::noncopyable,
                shared_ptr<FieldForce> >("FieldForce", no_init)
        ;

        class_< HardSpherePotential,
                bases<Pairforce>,
                shared_ptr<HardSpherePotential> >
                ("HardSpherePotential",
                 init<const SortedTypePair &,
                      const Energy &>())
        .def(init<  const SortedTypePair &,
                    const Energy &,
                    unsigned int,
                    const Length &>())
        .def(init<  const SortedTypePair &,
                    const Energy &,
                    unsigned int,
                    const Length &,
                    const Length &>())
        ;

        class_< LennardJonesPairforce,
                bases<Pairforce>,
                shared_ptr<LennardJonesPairforce> >
                ("LennardJonesPairforce",
                 init<const SortedTypePair &,
                      const LJPairforceParameters &>())
        .def(init<  const SortedTypePair &,
                    const Energy &,
                    const Length &,
                    const Attribute,
                    const Length &>())
        .def(init<  const SortedTypePair &,
                    const Energy &,
                    const Length &,
                    const Attribute,
                    const Length &,
                    int>())
        ;

        class_< TabulatedPairforce,
                bases<Pairforce>,
                shared_ptr<TabulatedPairforce> >
                ("TabulatedPairforce",
                 init<const SortedTypePair &,
                      const TabulatedEnergy &>())
        .def(init<  const SortedTypePair &,
                    const TabulatedEnergy &,
                    unsigned int,
                    const Length &,
                    const Length &>())
        .def(init<  const SortedTypePair &,
                    const TabulatedEnergy &,
                    unsigned int,
                    const Length &>())
        ;

        class_< BondForce,
                bases<Force>,
                boost::noncopyable,
                shared_ptr<BondForce> >("BondForce", no_init)
        ;

        class_< AngleForce,
                bases<Force>,
                boost::noncopyable,
                shared_ptr<AngleForce> >("AngleForce", no_init)
        ;

        class_< DihedralForce,
                bases<Force>,
                boost::noncopyable,
                shared_ptr<DihedralForce> >("DihedralForce", no_init)
        ;

        class_< HarmonicBondForce,
                bases<BondForce>,
                shared_ptr<HarmonicBondForce> >
                ("HarmonicBondForce",
                 init<const BondType &,
                      const HarmonicBondParameters &>())
        .def(init<  const BondType &,
                    const Length &,
                    const BondStretchingForce &>())
        .add_property("r_0",
          & HarmonicBondForce::r_0,
          & HarmonicBondForce::set_r_0)
        .add_property("c_0",
          & HarmonicBondForce::c_0,
          & HarmonicBondForce::set_c_0)
        ;

        class_< HarmonicAngleForce,
                bases<AngleForce>,
                shared_ptr<HarmonicAngleForce> >
                ("HarmonicAngleForce",
                 init<const AngleType &,
                      const HarmonicAngleParameters &>())
        .def(init<  const AngleType &,
                    const PlaneAngle &,
                    const BondBendingForce &>())
        ;

        class_< HarmonicDihedralForce,
                bases<DihedralForce>,
                shared_ptr<HarmonicDihedralForce> >
                ("HarmonicDihedralForce",
                 init<const DihedralType &,
                      const HarmonicDihedralParameters &>())
        .def(init<  const DihedralType &,
                    const Energy &,
                    int,
                    Attribute>())
        ;

        class_< TabulatedNeighborForce,
                bases<NeighborForce>,
                shared_ptr<TabulatedNeighborForce> >
                ("TabulatedNeighborForce",
                 init<
                  const ParticleType &,
                  const unsigned int,
                  const unsigned int,
                  const TabulatedEnergy &>())
        .def(init<  
                    const ParticleType &, 
                    const unsigned int,
                    const unsigned int,
                    const TabulatedNeighborForceParameters &>())
        .def(init<  
                    const ParticleType &,
                    const unsigned int,
                    const unsigned int,
                    const TabulatedNeighborForceParameters &,
                    shared_ptr<ReducedUnitsSet> >())
        ;

        class_< HarmonicNeighborForce,
                bases<NeighborForce>,
                shared_ptr<HarmonicNeighborForce> >
                ("HarmonicNeighborForce",
                 init<
                      const ParticleType &,
                      const unsigned int,
                      const unsigned int,
                      const HarmonicBondParameters &>())
        .def(init<  const ParticleType &,
                    const unsigned int,
                    const unsigned int,
                    const HarmonicBondParameters &,
                    shared_ptr<ReducedUnitsSet> >())
        .def(init<  const ParticleType &,
                    const unsigned int,
                    const unsigned int,
                    const Length &,
                    const BondStretchingForce &>())
        ;

        class_< HardFieldPotential,
                bases<FieldForce>,
                shared_ptr<HardFieldPotential> >
                ("HardFieldPotential",
                 init<const ParticleType &,
                 const Tuple3 &,
                 const Attribute &,
                 const Attribute &,
                 const Energy &>())
        ;

        class_< PolynomialPointPotential,
                bases<FieldForce>,
                shared_ptr<PolynomialPointPotential> >
                ("PolynomialPointPotential",
                init<const ParticleType &,
                const Tuple3 &,
                const int,
                const Attribute &,
                const Energy &>())
        ;

        class_< PolynomialPseudoBondPotential,
                bases<FieldForce>,
                shared_ptr<PolynomialPseudoBondPotential> >
                ("PolynomialPseudoBondPotential",
                init<const ParticleType &,
                const Length &,
                const int,
                const Attribute,
                const Energy &>())
        .add_property("r_0",
          & PolynomialPseudoBondPotential::r_0,
          & PolynomialPseudoBondPotential::set_r_0)
        .def("clear", & PolynomialPseudoBondPotential::clear)
        .def("add_pseudo_bond", & PolynomialPseudoBondPotential::add_pseudo_bond)
        .def("remove_pseudo_bonds", & PolynomialPseudoBondPotential::remove_pseudo_bonds)
        ;

        class_< Analyzer,
                shared_ptr<Analyzer>,
                boost::noncopyable>
                ("Analyzer", no_init)
        .def( "data",
              & Analyzer::data,
              "Collected data from last run.")
        .def( "id",
              & Analyzer::id,
              "The analyzers' unique id.")
        ;

        class_< Analyzers>("Analyzers")
        .def(vector_indexing_suite<Analyzers, true>())
        ;

        class_< PotentialEnergyAnalyzer,
                bases<Analyzer>,
                shared_ptr<PotentialEnergyAnalyzer> >
                ("PotentialEnergyAnalyzer")
        ;

        typedef ComputeAnalyzer<OsmoticPressureCompute> OsmoticPressureAnalyzer;
        class_< OsmoticPressureAnalyzer,
                bases<Analyzer>,
                shared_ptr<OsmoticPressureAnalyzer> >
                ("OsmoticPressureAnalyzer",
                 init<const std::string>())
        ;

        typedef ConfigAnalyzerCompute<analysis::RadiusOfGyrationAnalyzer> RadiusOfGyrationCompute;
        typedef ComputeAnalyzer<RadiusOfGyrationCompute> RadiusOfGyrationAnalyzerInterface;
        class_< RadiusOfGyrationAnalyzerInterface,
                bases<Analyzer>,
                shared_ptr<RadiusOfGyrationAnalyzerInterface> >
                ("RadiusOfGyrationAnalyzer",
                 init<const std::string>())
        ;

        class_< Fix,
                boost::noncopyable,
                shared_ptr<Fix> >("Fix", no_init)
        ;

        class_< DetectLowAcceptanceRatioFix,
                bases<Fix>,
                shared_ptr<DetectLowAcceptanceRatioFix> >
                ("DetectLowAcceptanceRatioFix")
        ;

        class_< DetectEquilibrationFix,
                bases<Fix>,
                shared_ptr<DetectEquilibrationFix> >
                ("DetectEquilibrationFix",
                 init<>())
        .def(init<Attribute>(args("threshold")))
        .def(init<Attribute, unsigned int>(args("threshold", "num_blocks")))
        ;

        class_ <Compute,
                boost::noncopyable,
                shared_ptr<Compute> >
                ("Compute", no_init)
                .def("evaluate", & Compute::evaluate)
        ;

        typedef TypedCompute<Attribute> AttributeCompute;
        class_ <AttributeCompute,
                boost::noncopyable,
                shared_ptr<AttributeCompute> >
                ("AttributeCompute", no_init)
                .def("last_result_as_value", & AttributeCompute::last_result_as_value)
        ;

        class_ <RadiusOfGyrationCompute,
                bases<AttributeCompute>,
                shared_ptr<RadiusOfGyrationCompute> >
               ("RadiusOfGyrationCompute")
        ;

        class_ <OsmoticPressureCompute,
                bases<AttributeCompute>,
                shared_ptr<OsmoticPressureCompute> >
                ("OsmoticPressureCompute")
        ;

        shared_ptr<SimulationConfig> (Session::*config_non_const)() =
          & Session::config;
        shared_ptr<Analyzer> (Session::*analyzer_add_str)(
          const std::string &) = & Session::analyzer_add;
        shared_ptr<Analyzer> (Session::*analyzer_add_obj)(
          shared_ptr<Analyzer>) = & Session::analyzer_add;
        Analyzers (Session::*analyzers_const)() const =
          & Session::analyzers;

        class_<Session>(
          "Session", 
          "A session includes all necessary elements to run,"
          " store and restart a simulation.")
        .def(init<const Session &>(args("rhs")))
        .def(init<const Session &, const std::string &>(
          args("rhs", "id")))
        .def(init<const std::string &>(
          args("id")))
        .def("id", & Session::id,
             "Constant life-time id of session.")
        .def("warn_on_id_none", 
             & Session::warn_on_id_none,
             session_warn_on_id_none_ol())
        //.def("init", & Session::init)
        .def("init", static_cast<void(Session::*)(unsigned int)>
          (& Session::init),
          session_init_ol())
        .def("status", & Session::status)
        .def("status_set", & Session::status_set)
        .def("status_unset", & Session::status_unset)
        .def("clear_status", & Session::clear_status)
        .def("current_step", & Session::current_step)
        .def("run", static_cast<void(Session::*)
            (unsigned int, const TagSet &)>
            (& Session::run),
            session_run_ol())
        .def("run_upto", static_cast<void(Session::*)
            (unsigned int, const TagSet &)>
            (& Session::run_upto),
            session_run_upto_ol())
        .def("set_callback", & Session::set_callback)
        .def("clear_callback", & Session::clear_callback)
        .def("read", & Session::init_read)
        .add_property("config",
                      config_non_const,
                      & Session::set_config)
        .def("write", & Session::dump_write)
        .def("dump", & Session::dump)
        .def("dump_available_formats", & Session::dump_available_formats)
        .staticmethod("dump_available_formats")
        .def("add_move", & Session::moves_add)
        .def("add_analyzer", analyzer_add_str)
        .def("add_analyzer_as_object", analyzer_add_obj)
        .add_property("analyzer_frequency",
                      & Session::analyzer_frequency,
                      & Session::analyzer_set_frequency)
        .add_property("analyzer_padding",
                      & Session::analyzer_padding,
                      & Session::analyzer_set_padding)
        .def("set_analyzer_callback", & Session::analyzer_set_callback)
        .def("analyzers", analyzers_const)
        .def("add_potential", & Session::potential_add)
//        .def("forces", & Session::forces, return_value_policy<return_by_value>())
        .def("add_fix", & Session::fix_add)
        .def("add_compute", & Session::compute_add)
        .def("add_dumper",
             & Session::dumper_add_by_id,
             session_dumper_add_by_id_ol())
        .add_property("reduced_units",
                      & Session::reduced_units_set,
                      & Session::set_reduced_units_set)
        .add_property("simulator",
                      static_cast<shared_ptr<Simulator>(Session::*)()>(
                      & Session::simulator),
                      & Session::set_simulator)
        .def("random_seed", & Session::random_seed)
        .add_property("use_neighbor_lists",
                      & Session::use_neighbor_lists,
                      & Session::set_use_neighbor_lists)
        .add_property("num_threads",
                      & Session::num_threads,
                      & Session::set_num_threads)
        .def("execute_in_parallel", & Session::execute_in_parallel,
             "Set number of threads to maximum number of threads.")
        .def("execute_serial", & Session::execute_serial,
             "Execute session in serial mode.")
        .add_property("log_flushing_frequency",
                      & Session::log_flushing_frequency,
                      & Session::log_set_flushing_frequency)
        .def("log_redirect_to_std_cout", 
             & Session::log_redirect_to_std_cout)
        .def("log_redirect_to_std_cerr",
             & Session::log_redirect_to_std_cerr)
        .def("log_redirect_to_std_clog", 
             & Session::log_redirect_to_std_clog)
        .def("log_redirect_to_file", & Session::log_redirect_to_file)
        ;

      }

    } // namespace pythoninterface

  } // namespace simulator

} // namespace mdst
