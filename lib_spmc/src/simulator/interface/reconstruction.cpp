// =====================================================================================
//
//       Filename:  reconstruction.cpp
//
//    Decsription:  Provides python interface to reconstruction library.
//
//        Version:  1.0
//        Created:  04/15/2014
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <reverse_mapping/wavelet_reverse_mapping_library.hpp>

#include <boost/python.hpp>
//#include <boost/python/suite/indexing/map_indexing_suite.hpp>
//#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
//#include <indexing_suite/set.hpp>
//#include <indexing_suite/vector.hpp>
 
namespace mdst{

  namespace reverse_mapping{

    namespace pythoninterface{

      using namespace boost::python;

      BOOST_PYTHON_MODULE(libreconstruction)
      {
        docstring_options local_docstring_options(true, true, false);

        class_< ReverseMappingLibrary,
                boost::noncopyable>(
          "ReverseMappingLibrary",
          "A library to improve reverse-mapping processes.",
          no_init)
        .def( "size",
              & ReverseMappingLibrary::size,
              "Return number of records in the library.")
        .def( "__len__",
              & ReverseMappingLibrary::size,
              "Return number of records in the library.")
        .def( "learn_from_subset",
              & ReverseMappingLibrary::learn_from_subset,
              "Extend library with information from subset.")
        .def( "learn_from",
              & ReverseMappingLibrary::learn_from,
              "Extend library with information from configuration.")
        .def( "improve_subset",
              & ReverseMappingLibrary::improve_subset,
              "Improve subset of configuration with information from library.")
        .def( "improve",
              & ReverseMappingLibrary::improve,
              "Improve configuration with information from library.")
        .def( "check_transformation", 
              & ReverseMappingLibrary::check_transformation)
        ;

        class_< WaveletReverseMappingLibrary,
                bases<ReverseMappingLibrary>
                >(
          "WaveletReverseMappingLibrary",
          "A library to improve wavelet-based coarse-grained reverse-mapping processes."
          )
        .add_property(
              "default_rod_size",
              & WaveletReverseMappingLibrary::default_rod_size,
              & WaveletReverseMappingLibrary::set_default_rod_size)
        ;

      }
      

    } // namespace pythoninterface

  } // namespace reverse_mapping

} // namespace mdst
