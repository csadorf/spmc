// =====================================================================================
//
//       Filename:  system.cpp
//
//    Description:  Provides a python interface to simulator.
//
//        Version:  1.0
//        Created:  01/19/2013 07:26:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <boost/python.hpp>

namespace mdst{

  namespace simulator{

    namespace pythoninterface{



    } // namespace pythoninterface

  } // namespace simulator

} // namespace mdst

char const* greet()
{
  return "hello, world.";
}

BOOST_PYTHON_MODULE(libinterface)
{
  using namespace boost::python;
  def("greet", greet);
}
