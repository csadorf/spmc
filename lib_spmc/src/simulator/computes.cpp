//=====================================================================================
//
//       Filename:  computes.cpp
//
//    Description:  Compute attributes during runtime.
//
//        Version:  1.0
//        Created:  09/09/13
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "computes.hpp"
#include "session.hpp"
#include "simulator.hpp"

namespace mdst{

  namespace simulator{

    using SimulationConfiguration::SimulationConfig;

    void
    Compute::init(
      Session & session)
    {
      counter_ = 0;
      init_(session);
    }

    void
    Compute::compute(
      const Session & session)
    {
      ++counter_;
      compute_(session);
    }

    void
    Compute::evaluate(
      const Session & session)
    {
      evaluate_(session, last_result_);
      reset();
    }

    void
    Compute::reset()
    {
      counter_ = 0;
    }

    void
    OsmoticPressureCompute::init_(
      Session & session)
    {
      osmotic_0_ = 0;
      osmotic_1_ = 0;
    }

    void
    OsmoticPressureCompute::compute_(
      const Session & session)
    {
      SimulationConfig const & config = * session.config();
      const Tuple3 & box = config.box();
      static Attribute k1 = 2 * M_PI / box.x();
      static Attribute k2 = 2 * k1;
      const static ComplexAttribute number_i(0, 1);
      //static TagSetSet const & molecules = config.property.molecules();
      ComplexAttribute osm_0(0, 0);
      ComplexAttribute osm_1(0, 0);
      Attribute r;
      const size_t N = config.num_atoms();
      for(ParticleTag p = 0; p < int(N); ++p)
      {
      //for(TagSetConstIterator p = all.begin(); p != all.end(); ++p){
/*      for(TagSetSetConstIterator molecule = molecules.begin();
          molecule != molecules.end(); ++molecule){
        for(TagSetConstIterator p = molecule->begin();
            p != molecule->end(); ++p){*/
          r = config.particle(p).position().x();
          //std::cerr << "r=" << r << std::endl;
          //std::cerr << "log(osm_0)=" << number_i * k1 * r << std::endl;
          //std::cerr << "osm_0=" << exp(number_i * k1 * r) << std::endl;
          osm_0 += exp(number_i * k1 * r);
          osm_1 += exp(number_i * k2 * r);
          if (osm_0 != osm_0 || osm_1 != osm_1)
          {
            std::stringstream msg;
            msg
              << "p = " << p << "; "
              << "position(p) = " << config.particle(p).position() << "; "
              << "k1 = " << k1 << "; "
              << "k2 = " << k2 << "; "
              << "r = " << r << "; "
              << "osm_0 = " << osm_0 << "; "
              << "osm_1 = " << osm_1 << "; "
            ;
            //print_iterable(all.begin(), all.end(), msg);
            session.dump_write("_crashed_!.xml", "hoomd_blue_xml");
            session.dump_write("_crashed_!.xyz", "xyz");
            throw RuntimeError(msg.str());
          }
//        }
      }
      using std::pow;
      using std::abs;
//      std::cerr << "sum osm_0=" << osm_0 << std::endl;
//      std::cerr << "sum osm_1=" << osm_1 << std::endl;
      osmotic_0_ += pow(abs(osm_0), 2);
      osmotic_1_ += pow(abs(osm_1), 2);
//      std::cerr << "sum osm_0=" << osmotic_0_ << std::endl;
//      std::cerr << "sum osm_1=" << osmotic_1_ << std::endl;
    }

    void
    OsmoticPressureCompute::evaluate_(
      const Session & session,
      shared_ptr<void> & result)
    {
      if (counter() > 0){
        SimulationConfig const & config = * session.config();
        const size_t N_molecules = config.property.molecules().size();
        Attribute r(0);
        if (N_molecules > 0)
        {
          const size_t N_all = config.num_atoms();
          const size_t a = counter() * N_molecules * pow((N_all / N_molecules + 1), 2);
          const Attribute s1 = osmotic_0_ / a;
          const Attribute s2 = osmotic_1_ / a;
          /*
          std::cerr << "osmotic_0_=" << osmotic_0_ << " "
                    << "osmotic_1_=" << osmotic_1_ << std::endl;
          std::cerr << "s1=" << s1 << " s2=" << s2 << std::endl;*/
          const Attribute L_x = config.box().x();
          const Attribute b1 = sin(M_PI / L_x);
          const Attribute b2 = sin(2 * M_PI / L_x);
          const Attribute p1 = 4 * b1 * b1;
          const Attribute p2 = 4 * b2 * b2;
          r = (p2 - p1) / (p1 * s2 - p2 * s1);
          if (r != r)
          {
            std::stringstream msg;
            msg
              << "OsmoticPressure: NaN value! "
              << "b1 = " << b1 << "; "
              << "b2 = " << b2 << "; "
              << "p1 = " << p1 << "; "
              << "p2 = " << p2 << "; "
              << "a = " << a << "; "
              << "s1 = " << s1 << "; "
              << "s2 = " << s2 << "; "
              << "N_molecules = " << N_molecules << "; "
              << "N_all = " << N_all << "; "
              << "counter() = " << counter() << "; "
              << "osmotic_0_ = " << osmotic_0_ << "; "
              << "osmotic_1_ = " << osmotic_1_ << "; "
              ;
            throw RuntimeError(msg.str());
          }
        }
        result = shared_ptr<void>(new Attribute(r));
      }
      osmotic_0_ = 0;
      osmotic_1_ = 0;
    }

  } // namespace simulator

} // namespace mdst
