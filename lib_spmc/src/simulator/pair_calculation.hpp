// =====================================================================================
//
//       Filename:  pair_calculation.hpp
//
//    Description:  Calculate forcefield forces.
//
//        Version:  1.0
//        Created:  11/21/2012 10:20:55 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_PAIR_CALCULATION_H_
#define MDST_SIMULATOR_PAIR_CALCULATION_H_

#include <types.hpp>
#include <simulation_config.hpp>
#include <simulation_config/n_list.hpp>
#include <simulation_config/arithmetics.hpp>
#include <simulation_config/neighbors.hpp>
#include <cell_grid.hpp>
#include <omp.h>
#include <forces.hpp>

//#define DEBUG_ENERGY_CALCULATION

namespace mdst{

  namespace simulator{

    typedef Attributes SquaredDistances;
    typedef SquaredDistances::iterator SquaredDistancesIterator;
    typedef SquaredDistances::const_iterator
      SquaredDistancesConstIterator;
    typedef std::map<SortedTypePair, SquaredDistances>
      SquaredDistancesMap;

    

    // pair_potential calculation functions
    //
    struct PairwiseDistance{
      Attribute distance;
      SortedTypePair sorted_type_pair;
    };

    typedef std::vector<PairwiseDistance> PairwiseDistances;
    typedef PairwiseDistances::const_iterator
      PairwiseDistancesConstIterator;

    PairwiseDistances
    calculate_pairwise_distances_(
      const SimulationConfiguration::SimulationConfig & config,
      const TagSet& group,
      const unsigned int exclude_bonded = 1);

    typedef std::vector<PairwiseDistances> PairwiseDistancesVector;
    typedef PairwiseDistancesVector::const_iterator
      PairwiseDistancesVectorConstIterator;

    // Returns the iterator pointing to the last element in the
    // resulting vector.
    // result_vector stores the resulting PairwiseDistances.
    PairwiseDistancesVector
    calculate_pairwise_bonded_distances_(
      const SimulationConfiguration::SimulationConfig & config,
      const TagSet& group,
      const unsigned int i_max);

    inline
    Attribute
    absolute_squared_distance_pbc(
      const SimulationConfiguration::SimulationConfig & config,
      const ParticleTag a,
      const ParticleTag b)
    {
      const Tuple3 pos_a = config.particle_position(a);
      const Tuple3 pos_b = config.particle_position(b);
      const Tuple3 & box = config.box();
      const Tuple3 delta = pos_b - pos_a;
      using std::abs;
      const Tuple3 delta_abs = Tuple3(
        abs(delta.x()), abs(delta.y()), abs(delta.z()));
      const Tuple3 d_pbc = Tuple3(
        abs(delta_abs.x() - int(delta_abs.x()/box.x()) * 2 * box.x()),
        abs(delta_abs.y() - int(delta_abs.y()/box.y()) * 2 * box.y()),
        abs(delta_abs.z() - int(delta_abs.z()/box.z()) * 2 * box.z()));
      const Attribute d = abs_squared(Tuple3(
        d_pbc.x() - int(d_pbc.x() / (2 * box.x())) * d_pbc.x(),
        d_pbc.y() - int(d_pbc.y() / (2 * box.y())) * d_pbc.y(),
        d_pbc.z() - int(d_pbc.z() / (2 * box.z())) * d_pbc.z()));
//      std::cerr << "pos_a=" << pos_a << "; pos_b=" << pos_b << std::endl;
//      std::cerr << "distance(" << a << ", " << b << ")=" << d << std::endl;
      return d;
    }

    template<class PairforceIterator>
    inline
    Attribute
    calculate_potential_energy_with_neighbor_lists(
      const SimulationConfiguration::SimulationConfig & config,
      const TagSequenceConstIterator particle,
      const TagSet & count_once,
      const SimulationConfiguration::NeighborListKey & n_list_key,
      PairforceIterator forces_first,
      PairforceIterator forces_last)
    {
      using namespace SimulationConfiguration;
      Attribute result = 0;
      const bool check_once_or_twice = count_once.size() > 0;
      ParticleTag const & particle_a  = * particle;
      bool required_update;
      TagVectorConstIterator n_list_first, n_list_last;
      n_list_last = config.neighbor_list(n_list_key, particle_a, n_list_first, required_update);
#ifdef DEBUG_NEIGHBOR_LIST
      if (required_update){
        std::cerr
          << "particle " << * particle << " required an update of the neighbor lists."
          << std::endl
          ;
      }
#endif

#ifdef DEBUG_ENERGY_CALCULATION
      const TagSet molecule = config.property.molecule(*particle);
      std::cerr << "molecule(" << * particle << ") = ";
      print_iterable(molecule.begin(), molecule.end()) << std::endl;
      std::cerr << "n_list(" << * particle << ") = ";
      print_iterable(n_list_first, n_list_last) << std::endl;
#endif

      for(; n_list_first != n_list_last; ++n_list_first){
        ParticleTag const & particle_b = * n_list_first;
        assert(particle_a != particle_b);
        Attribute r_squared = minimum_image_squared_distance(config, particle_a, particle_b);
        bool once = true;
        if (r_squared <= n_list_key.squared_r_cut()){
          if(check_once_or_twice)
            if (! count_once.count(particle_b))
              once = false;
          for(PairforceIterator i = forces_first; i != forces_last; ++i){
            Pairforce const & force = static_cast<Pairforce const &>(**i);

            if(once)
              result += force.evaluate(r_squared);
            else
              result += force.evaluate(r_squared) * 2;

#ifdef DEBUG_ENERGY_CALCULATION
            Attribute e = force.evaluate(r_squared);
            if (!once)
              e *= 2;
            std::cerr << "r(" << * particle << ", " << * n_list_first << ") = "
                      << sqrt(r_squared) << " ";
            std::cerr << "e(" << sqrt(r_squared) << ")=" << e << std::endl;
#endif
          }
        }
      }
      return result;
    }

    template<class Pairforce>
    inline
    Attribute
    calculate_squared_distances_pbc(
      const SimulationConfiguration::SimulationConfig & config,
      const TagSequenceConstIterator particle,
      const TagSet & count_once,
//      const SortedTypePair & sorted_type_pair,
      const int sorted_type_pair_first, int sorted_type_pair_second,
      const Attribute squared_reduced_cut,
      const unsigned int exclude_bonded,
      const SimulationConfiguration::CellGrid & cell_grid,
      const SimulationConfiguration::Resolution & resolution,
      TagSequenceIterator first,
      Pairforce * force)
    {
      Attribute result = 0;
      using namespace SimulationConfiguration;
      const size_t N = config.num_atoms();
      if (N <= 0)
        return 0;
      const ParticleTag p_a = * particle;
      TagSequenceIterator last = calculate_neighboring_particles_pbc(
        config,
        cell_grid,
        resolution,
        p_a,
        first);
      const bool check_once_or_twice = count_once.size() > 0;
 //     int sorted_type_pair_first = config.translate_type_to_int_type(sorted_type_pair.first());
   //   int sorted_type_pair_second = config.translate_type_to_int_type(sorted_type_pair.second());
      int compare_sorted_type_pair_first = config.particle(*particle).int_type();
      if (sorted_type_pair_first != compare_sorted_type_pair_first) return result;
      for(TagSequenceConstIterator it = first; it != last; ++it)
      {
        const ParticleTag p_b = *it;
        bool check_not_correct_types = sorted_type_pair_second != config.particle(p_b).int_type();
        if (check_not_correct_types)
          continue;
        if(exclude_bonded > 0)
          if (config.indirectly_bonded(p_a, p_b, exclude_bonded))
            continue;
        const Attribute r_squared = 
          absolute_squared_distance_pbc(config, p_a, p_b);
        if (r_squared <= squared_reduced_cut){
          bool once = true;
          if (check_once_or_twice)
            if (! count_once.count(p_b))
              once = false;

          if (once)
            result += force->evaluate(r_squared);
          else
            result += force->evaluate(r_squared) * 2;

#ifdef DEBUG_ENERGY_CALCULATION
          Attribute e = force->evaluate(r_squared);
          if (!once)
            e *= 2;
          std::cerr
            << "e(" << * particle << ", " << p_b << ")="
            << e << std::endl;
#endif
        }
      }
      return result;
    }

    size_t
    calculate_squared_distances_pbc_no_cell_lists(
      const SimulationConfiguration::SimulationConfig & config,
      TagSequenceConstIterator first,
      TagSequenceConstIterator last,
      const TagSequenceConstIterator particle,
      const SortedTypePair & sorted_type_pair,
      const Attribute reduced_r_cut,
      const unsigned int exclude_bonded,
      SquaredDistances & result);

    Attribute
    absolute_squared_distance(
      const SimulationConfiguration::SimulationConfig & config,
      const ParticleTag a,
      const ParticleTag b);

  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_PAIR_CALCULATION_H_
