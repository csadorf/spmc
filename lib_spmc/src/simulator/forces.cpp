//=====================================================================================
//
//       Filename:  forces.cpp
//
//    Description:  Define forces.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "forces.hpp"
#include "misc.hpp"
#include "simulator.hpp"
#include "energy_calculation.hpp"
#include <utility/coordinates.hpp>
//#include <parser_writer/writers.hpp>

#include <assert.h>

namespace mdst{

  namespace simulator{

    using namespace SimulationConfiguration;

    Pairforce::Pairforce(
      const SortedTypePair & sorted_type_pair):
      sorted_type_pair_(sorted_type_pair),
      exclude_bonded_(0),
      r_cut_( - Length()),
      r_skin_( - Length()),
      factor_(1)
      {};

    Pairforce::Pairforce(
      const SortedTypePair & sorted_type_pair,
      unsigned int exclude_bonded,
      const Length & r_cut):
      sorted_type_pair_(sorted_type_pair),
      exclude_bonded_(exclude_bonded),
      r_cut_(r_cut),
      r_skin_(0.1 * r_cut),
      factor_(1)
    {};

    Pairforce::Pairforce(
      const SortedTypePair & sorted_type_pair,
      unsigned int exclude_bonded,
      const Length & r_cut,
      const Length & r_skin):
      sorted_type_pair_(sorted_type_pair),
      exclude_bonded_(exclude_bonded),
      r_cut_(r_cut),
      r_skin_(r_skin),
      factor_(1)
    {};


    CellGrid &
    Pairforce::get_grid_(
        const SimulationConfig & config,
        const ReducedCutOff reduced_r_cut,
        Resolution & resolution = default_resolution()) const
    {
      return config.cell_grid(
        calculate_key(
          config.box(), 
          reduced_r_cut,
          resolution,
          1000 * config.num_atoms()
          ));
    }

    void
    Pairforce::init(
        const SimulationConfig & config) const
    {
      const ReducedCutOff reduced_r_cut = units_->reduce(r_cut_);
      get_grid_(config, reduced_r_cut);
    }

    NeighborListKey
    Pairforce::n_list_key() const
    {
      return NeighborListKey(
        units_->reduce(r_cut_),
        units_->reduce(r_skin_),
        exclude_bonded_,
        sorted_type_pair_);
    }

    Attribute
    Pairforce::calculate(
        const SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last) const
    {
      Attribute energy = 0;
      if (first == last) return 0.;
      ReducedCutOff reduced_r_cut = units_->reduce(r_cut_);
      
      if (reduced_r_cut > (2.0/3.0) * min(config.box())){
        std::stringstream msg;
        msg << "Warning: Cutoff radius is too big for box size. "
            << "r_c=" << reduced_r_cut <<", min(box)=" << min(config.box())
            << std::endl << "Aborting.";
        throw SimulatorError(msg.str());
      }

      Resolution resolution(3);
      for(unsigned int d = 0; d < 3; ++d)
        resolution[d] = 1;

      TagSet count_once;
      if (last - first < config.num_atoms())
        count_once = TagSet(first, last);

      const CellGrid & cell_grid = get_grid_(config, reduced_r_cut, resolution);
      const Attribute squared_reduced_r_cut = reduced_r_cut * reduced_r_cut;
      const size_t N_p = last - first;

      const int sorted_type_pair_first = config.translate_type_to_int_type(sorted_type_pair_.first());
      const int sorted_type_pair_second = config.translate_type_to_int_type(sorted_type_pair_.second());

      #ifdef _OPENMP
        int max_threads = omp_get_max_threads();
      #else
        int max_threads = 1;
      #endif
      int num_atoms = config.num_atoms();
      tags_cache.resize(max_threads);
      for (int i = 0; i < max_threads; i++) {
        tags_cache[i].resize(num_atoms);
      }
        
//      std::cout << "1234 " << N_p << std::endl;
      #pragma omp parallel if(N_p > 4)
      {
      //  TagSequence tags;
        #ifdef _OPENMP
          int thread_id = omp_get_thread_num();
        #else
          int thread_id = 0;
        #endif
        #pragma omp for reduction(+:energy) schedule(dynamic,1) //private(tags)
        for(size_t i = 0; i < N_p; ++i)
        {
          TagSequence &tags = tags_cache[thread_id];
        //  tags.resize(config.num_atoms());
          energy += 
            calculate_squared_distances_pbc(
              config, first + i, count_once, 
              sorted_type_pair_first, sorted_type_pair_second,
              squared_reduced_r_cut, exclude_bonded_, cell_grid, 
              resolution, tags.begin(), this);
        }
      }
      return energy / 2;
    }

    Attribute
    FieldForce::calculate(
        const SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const
    {
      Attribute result = 0;
      TagSequence match(molecule_last - molecule_first);
      for(; molecule_first != molecule_last; ++molecule_first)
      {
        if (config.particle(*molecule_first).type() == particle_type_)
        {
          result += calculate_(config, * molecule_first);
          //match[++i] = * molecule_first;
        }
      }

      /*for(TagSequenceConstIterator it = match.begin();
          it != match.end(); ++it)
      {
        result += calculate_( it);
      }*/
      return result;
    }

    Attribute
    BondForce::calculate(
        const SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const
    {
     TagSet subset(molecule_first, molecule_last);
     TagSet matching;
     for(unsigned int b = 0; b < config.num_bonds(); ++b){
       const BondView bond_view(config.bond(b));
       if (bond_view.type() != bond_type_)
         continue;
       if (!intersect(subset, bond_view.tags()))
         continue;
       matching.insert(b);
     }
     Attribute energy = 0;
     TagSequence matching_sequence(vector_from_set(matching));
     for(TagSequenceConstIterator it = matching_sequence.begin();
         it != matching_sequence.end(); ++it){
       energy += calculate_force_(config, *it);
     }
//     std::cerr << "energy_sum=" << energy << std::endl;
     return energy;
    }

    Attribute
    BondForce::calculate_force_(
        const SimulationConfig & config,
        const BondTag bond) const
    {
      const BondView bond_view(config.bond(bond));
      const Tuple3 pos_a = config.particle(bond_view.a()).remapped_position();
      const Tuple3 pos_b = config.particle(bond_view.b()).remapped_position();
      Attribute d(abs_squared(pos_b - pos_a));
      Attribute energy = evaluate(d);
      return energy;
    }

    Attribute
    AngleForce::calculate(
        const SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const
    {
      TagSet subset(molecule_first, molecule_last);
      const size_t N_angles = config.num_angles();
      TagSequence matching(N_angles);
      size_t j = 0;

      for(size_t i = 0; i < N_angles; ++i){
        const AngleView angle_view(config.angle(i));
        if (angle_view.type() != angle_type_)
          continue;
        if (!intersect(subset, angle_view.tags()))
          continue;
        matching[j] = i;
        ++j;
      }

      Attribute energy = 0;
      for(size_t i = 0; i < j; ++i)
        energy += calculate_force_(config, matching[i]);

      return energy;
    }

    Attribute
    AngleForce::calculate_force_(
        const SimulationConfig & config,
        const AngleTag angle) const
    {
      const AngleView angle_view(config.angle(angle));
      TagVector const & sorted_tags = angle_view.sorted_tags();
      const ParticleView p_A(config.particle(sorted_tags[0]));
      const ParticleView p_B(config.particle(sorted_tags[1]));
      const ParticleView p_C(config.particle(sorted_tags[2]));
      const PlaneAngle theta = calculate_angle(
        p_A.remapped_position(),
        p_B.remapped_position(),
        p_C.remapped_position());
      return evaluate(units_->reduce(theta));
    }

    Attribute
    DihedralForce::calculate_force_(
        const SimulationConfig & config,
        const DihedralTag dihedral) const
    {
      const DihedralView dihedral_view(config.dihedral(dihedral));
      TagVector const & sorted_tags = dihedral_view.sorted_tags();
      const PlaneAngle phi = calculate_torsion(
        config.particle(sorted_tags[0]).remapped_position(),
        config.particle(sorted_tags[1]).remapped_position(),
        config.particle(sorted_tags[2]).remapped_position(),
        config.particle(sorted_tags[3]).remapped_position());
      return evaluate(units_->reduce(phi));
    }

    void
    NeighborForce::check_() const{
    }

    Attribute
    DihedralForce::calculate(
        const SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const
    {
      TagSet subset(molecule_first, molecule_last);
      const size_t N_dihedrals = config.num_dihedrals();
      TagSequence matching(N_dihedrals);
      size_t j = 0;

      for(size_t i = 0; i < N_dihedrals; ++i){
        const DihedralView dihedral_view(config.dihedral(i));
        if (dihedral_view.type() != dihedral_type_)
          continue;
        if (!intersect(subset, dihedral_view.tags()))
          continue;
        matching[j] = i;
        ++j;
      }

      Attribute energy = 0;
      for(size_t i = 0; i < j; ++i)
        energy += calculate_force_(config, matching[i]);
      return energy;
    }

    Attribute
    NeighborForce::calculate(
        const SimulationConfig & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last) const
    {
      while(molecule_first != molecule_last)
      {
        if (config.particle(* molecule_first).type() == particle_type_)
          break;
        ++molecule_first;
      }
      if (molecule_first == molecule_last)
        return 0;
      return calculate_force_(config, molecule_first, molecule_last);
    }

    Attribute
    NeighborForce::calculate_force_(
        const SimulationConfig & config,
        TagSequenceConstIterator token,
        TagSequenceConstIterator neighborhood_last) const
    {
      /*std::cerr << "Calculating neighbor force with step="
                << steps_ << " and skip=" << skip_ << ". " 
                << "nh_last = " << * neighborhood_last << " "
                << "token = " << * token
                << std::endl;*/
      if ((neighborhood_last - token) < skip_ + steps_)
        return 0;
      //assert((neighborhood_last - token) >= skip_ + steps_);
      Attribute energy = 0;
      for(unsigned int i = 0; i < skip_ + steps_; ++i){
        ++token;
      }
      TagSequenceConstIterator token_b;
      while (token != neighborhood_last){
        token_b = token;
        for (unsigned int i = 0; i < skip_; ++i){
          --token_b;
        }
        for(unsigned int i = 0; i < steps_; ++i){
          const Tuple3 pos_a(config.particle(* token).remapped_position());
          const Tuple3 pos_b(config.particle(* --token_b).remapped_position());
          energy += evaluate(abs_squared(pos_b - pos_a));
          //std::cerr << "energy(" << abs(pos_b - pos_a) << ")=" 
          //          << evaluate(abs_squared(pos_b - pos_a)) << std::endl;
        }
        ++token;
      }
      return energy;
    }

    Attribute
    HarmonicBondForce::evaluate(
        const Attribute squared_distance) const{
      const Attribute d = sqrt(squared_distance);
      const Attribute delta = d - units_->reduce(parameters_.r_0);
      const Attribute energy = 0.5 * units_->reduce(parameters_.c_0) * delta * delta;
/*      std::cerr << "U(" << d << ")=" << energy << std::endl
                << "U(" << units_->expand_length(d) << ")="
                << units_->expand_energy(energy) << std::endl
                << "r_0=" << parameters_.r_0
                << " (" << units_->reduce(parameters_.r_0) << ")"
                << std::endl;*/
      return energy;
    }

    Attribute
    HarmonicAngleForce::evaluate(
      const Attribute theta) const{
        const Attribute delta_theta = theta - units_->reduce(parameters_.theta_0);
        const Attribute energy = 0.5 * units_->reduce(parameters_.k) * delta_theta * delta_theta;
        /*
        std::cerr << "Evaluate " << theta << " to " << units_->expand_energy(energy)
                  << " (" << energy << ")" << std::endl;
        std::cerr << "theta_0 = " << units_->reduce(parameters_.theta_0) << std::endl;
        std::cerr << "k = " << parameters_.k
                  << " (" << units_->reduce(parameters_.k) << ")"  << std::endl
                  << "k_B = " << mdst::reduced_units::k_B
                 << std::endl;*/
        // validated (CSA)
        return energy;
    }

    Attribute
    HarmonicDihedralForce::evaluate(
      const Attribute phi) const{
        const Attribute energy =
          0.5 * units_->reduce(parameters_.k) * 
          (1 + parameters_.d * cos(parameters_.n * phi));
          /*
        std::cerr << "U(" << phi << ")=" << units_->expand_energy(energy)
                  << " (" << energy << ")" << std::endl
                  << "k = " << parameters_.k << " ("
                  << units_->reduce(parameters_.k) << ")" << std::endl
                  << "d = " << parameters_.d << "; "
                  << "n = " << parameters_.n
                  << std::endl;*/
        // validated (CSA)
        return energy;
    }

    Attribute
    LennardJonesPairforce::evaluate(
        const Attribute squared_distance) const{
      const Attribute epsilon = units_->reduce(parameters_.epsilon);
      const Attribute sigma = units_->reduce(parameters_.sigma);
      const Attribute & alpha = parameters_.alpha;
      const Attribute summand =
        pow((pow(sigma, 2) / squared_distance), 3);
      const Attribute energy = 4 * epsilon * (pow(summand, 2) - alpha * summand);
      /*
      std::cerr << "U(" << sqrt(squared_distance) << ")=" << units_->expand_energy(energy)
                << " (" << energy << ")" << std::endl
                << "epsilon = " << parameters_.epsilon << " (" << epsilon << ")" << std::endl
                << "sigma = " << parameters_.sigma << " (" << sigma << ")" << std::endl
                << "alpha = " << parameters_.alpha
                << std::endl;
      validated (CSA) */
      return energy;
    }

    Energy
    lookup_table_low(
        const TabulatedEnergy & table,
        const Area squared_distance){
      TabulatedEnergyConstIterator iter_low =
        table.lower_bound(squared_distance);
      if (iter_low == table.end()){
        --iter_low;
      }
      return iter_low->second;
    }

    Energy
    lookup_table(
        const TabulatedEnergy & table,
        const Area squared_distance){
      TabulatedEnergyConstIterator iter_high =
        table.upper_bound(squared_distance);
      if (iter_high == table.begin()){
        return iter_high->second;
      } else {
        return (--iter_high)->second;
      }
    }

    Energy
    lookup_table_animesh(
        const TabulatedEnergy & table,
        const Area squared_distance){
      TabulatedEnergyConstIterator iter_high =
        table.upper_bound(squared_distance);
      if (iter_high == table.end()){
        return (--iter_high)->second; // modified this in order to
                                      // make sure, I can't "fall off the cliff"
//        return Energy(0);
      }
      if (iter_high != table.begin()){
        const Area & d = squared_distance;
        const Area & d1 = (iter_high)->first;
        const Area & d0 = (--iter_high)->first;
        if (((d - d0) / (d1 - d0)) >= (0.5))
          ++iter_high;
      }
      return iter_high->second;
    }

    Energy
    lookup_table_max(
        const TabulatedEnergy & table)
    {
      Energy max_energy = table.begin()->second;
      for(TabulatedEnergyConstIterator it = table.begin();
          it != table.end(); ++it){
        if (it->second > max_energy)
          max_energy = it->second;
      }
      return max_energy;
    }

    Attribute
    TabulatedPairforce::evaluate(
        const Attribute squared_distance) const{
      Attribute energy = units_->reduce(
              lookup_table_animesh(
                parameters_.table,
                squared_distance * units_->area));
//      std::cerr << "Evaluate squared distance " << squared_distance
//                << " to " << energy << "." << std::endl;
      return energy;
    }

    Attribute
    TabulatedNeighborForce::evaluate(
        const Attribute squared_distance) const{
      const Area sq_distance = units_->area * squared_distance;
      Attribute energy = units_->reduce(
              lookup_table_animesh(
                parameters_.table,
                sq_distance));
      return energy;
    }

    Attribute
    HarmonicNeighborForce::evaluate(
      const Attribute squared_distance) const
    {
      const Attribute d = sqrt(squared_distance);
      const Attribute delta = d - units_->reduce(parameters_.r_0);
      const Attribute energy = 0.5 * units_->reduce(parameters_.c_0) * delta * delta;
      /*
      std::cerr << "Evaluate " << sqrt(squared_distance) << " to "
                << energy << "." << std::endl;
      std::cerr << "r_0=" << units_->reduce(parameters_.r_0) << std::endl;
      */ 
      return energy;
    }

  } // namespace simulator

} // namespace mdst
