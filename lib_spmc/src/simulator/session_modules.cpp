// =====================================================================================
//
//       Filename:  session._modules.cpp
//
//    Description:  Prepare all data, necessary to run a simulation.
//
//        Version:  1.0
//        Created:  01/20/2013 07:45:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "session_modules.hpp"
#include "session_interface.hpp"
#include <parser_writer/parsers.hpp>
//#include <parser_writer/writers.hpp>


namespace mdst{

  namespace simulator{

    namespace interface{

      using SimulationConfiguration::SimulationConfig;
      using SimulationConfiguration::ParserWriter::get_parser;

      void
      Init::read(
          const std::string & filename){
        session_->init_read(filename);
      }

      void
      Init::read(
          const std::string & filename,
          const std::string & format){
        session_->set_config(get_parser(format)->parse(filename));
      }

      void
      Dump::write(
          const std::string & filename,
          const std::string & format){
        session_->dump_write(filename, format);
      }

      void
      Moves::add(
          shared_ptr<MonteCarloMove> move){
        session_->moves_add(move);
      }

      void
      System::set_reduced_units(
          shared_ptr<ReducedUnitsSet> reduced_units_set){
        session_->set_reduced_units(reduced_units_set);
      }
      
      shared_ptr<SimulationConfig>
      System::config(){
        return session_->config();
      }

      void
      System::set_simulator(
          shared_ptr<Simulator> simulator){
        session_->set_simulator(simulator);
      }

      shared_ptr<RandomNumberGenerator>
      System::random_number_generator(){
        return session_->random_number_generator();
      }

      void
      Forcefield::add(shared_ptr<Force> force){
        session_->potential_add(force);
      }

    } // namespace interface
      
  } // namespace simulator

} // namespace mdst
