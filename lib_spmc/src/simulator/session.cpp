// =====================================================================================
//
//       Filename:  session.cpp
//
//    Description:  Prepare all data, necessary to run a simulation.
//
//        Version:  1.0
//        Created:  01/20/2013 07:45:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "session.hpp"
#include "simulator.hpp"
#include "random_number_generation.hpp"
#include "valgrind/callgrind.h"
#include <parser_writer/guess_parser.hpp>
#include <parser_writer/writers.hpp>
#include <parser_writer/parsers.hpp>
#include <simulation_config/dump.hpp>
#include <computes.hpp>
#include <boost/python.hpp>

namespace mdst{

  namespace simulator{

    using SimulationConfiguration::SimulationConfig;
    using SimulationConfiguration::ParserWriter::Parser;
    using SimulationConfiguration::ParserWriter::Writer;
    using SimulationConfiguration::ParserWriter::guess_parser;
    using SimulationConfiguration::ParserWriter::get_parser;
    using SimulationConfiguration::ParserWriter::get_writer;

    int Session::num_n_list_updates_ = 0;
    const std::string Session::ID_NONE = std::string("");

    Session::Session(
      const Session & rhs,
      const std::string id):
      id_(id),
      id_warn_(rhs.id_warn_),
      status_(rhs.status_),
      logger_(rhs.logger_),
      log_flushing_frequency_(rhs.log_flushing_frequency_),
      step_(rhs.step_),
      num_steps_(rhs.num_steps_),
      config_(shared_ptr<SimulationConfig>(new SimulationConfig(* rhs.config_))),
      simulator_(rhs.simulator_),
      rng_(rhs.rng_),
      reduced_units_set_(rhs.reduced_units_set_),
      runtime_data_(shared_ptr<RuntimeData>(new RuntimeData(* rhs.runtime_data_))),
      forces_(rhs.forces_),
      mc_moves_(rhs.mc_moves_),
      analyzers_(rhs.analyzers_),
      analyzer_frequency_(rhs.analyzer_frequency_),
      analyzer_padding_(rhs.analyzer_padding_),
      analyzer_callback_(rhs.analyzer_callback_),
      callback_period_(rhs.callback_period_),
      callback_(rhs.callback_),
      fixes_(rhs.fixes_),
      computes_(rhs.computes_),
      dumpers_(rhs.dumpers_),
      use_neighbor_lists_(rhs.use_neighbor_lists_),
      omp_num_threads_(rhs.omp_num_threads_)
    {;}

    Session::~Session(){
      log_flush_all();
      //std::cerr << "Destroyed session." << std::endl;
    }

    bool
    Session::check_abort_condition()
    {
      if (check_interrupt_state())
      {
        throw SessionInterrupt("Signal interruption.");
        return true;
      }
      if (status_ & STATUS_STOP)
      {
//        std::cerr << "Breaking because of status." << std::endl;
        return true;
      }
      return false;
    }

    void
    Session::log_redirect(
      size_t id,
      std::ostream & stream)
    {
      logger_.redirect(id, stream);
    }


    void
    Session::log_redirect_to_file(
      size_t id,
      const std::string & filename){
      log() << "Redirecting output of channel " << id
            << " to file '" << filename << "'." << std::endl;
      logger_.redirect_to_file(id, filename);
    }

    void
    Session::init_read(
        const std::string & filename){
      shared_ptr<Parser> parser = guess_parser(filename);
      config_ = parser->parse(filename);
      step_ = parser->determine_step(filename);
      init(step_);
    }

    void
    Session::dump_write(
        const std::string & filename,
        const std::string & format) const{
      if (! config_)
        throw SessionError("No config set.");
      shared_ptr<SimulationConfiguration::ParserWriter::Writer> writer
        = get_writer(format);
      if (writer)
        writer->write(* config_, filename, step_);
      else
      {
        std::stringstream msg;
        msg << 
          "Failed to obtain writer for format '" << format << "'.";
        throw SessionError(msg.str());
      }
    }

    void
    Session::dump(
        const std::string & format) const{
      if (! config_)
        throw SessionError("No config set.");
      shared_ptr<SimulationConfiguration::ParserWriter::Writer> writer
        = get_writer(format);
      if (writer)
        writer->write(* config_, std::cout, step_);
      else
      {
        std::stringstream msg;
        msg << 
          "Failed to obtain writer for format '" << format << "'.";
        throw SessionError(msg.str());
      }
    }

    Strings
    Session::dump_available_formats()
    {
      return SimulationConfiguration::ParserWriter::available_output_formats();
    }

    void
    Session::moves_add(
        shared_ptr<MonteCarloMove> move){
      mc_moves_.push_back(move);
    }

    void
    Session::potential_add(
        shared_ptr<Force> force){
      forces_.push_back(force);
    }

    void
    Session::fix_add(
        shared_ptr<Fix> fix){
      fixes_.push_back(fix);
    }

    void
    Session::compute_add(
        shared_ptr<Compute> compute){
      computes_.push_back(compute);
      status_set(STATUS_NO_INIT);
    }

    void
    Session::set_reduced_units_set(
        shared_ptr<ReducedUnitsSet> reduced_units_set){
      reduced_units_set_ = reduced_units_set;
    }

    void
    Session::set_simulator(
        shared_ptr<Simulator> simulator){
      simulator_ = simulator;
    }

    void
    Session::init(unsigned int step)
    {
      init_(0);
      step_ = step;
      runtime_data_->init();
    }

    void
    Session::init_(
      unsigned int num_steps)
    {
      num_steps_ = num_steps;
      runtime_data_->reserve(num_steps, num_steps * mc_moves_.size());
      init_computes_();
      init_analyzers_();
      init_fixes_(num_steps);
      init_dumpers_();
      potential_update_();
      mc_moves_update_();
      num_n_list_updates_ = 0;
      status_unset(STATUS_NO_INIT);
    }

    void
    Session::close()
    {
      close_dumpers_();
    }

    void
    Session::run_upto(
      unsigned int num_steps,
      const TagSet & sample_set)
    {
      if (step_ >= num_steps) {
        log() << "Requesting run upto time step, which has already passed. Skipping." << std::endl;
      } else {
        run(num_steps - step_, sample_set);
      }
    }

    void
    Session::run(
      unsigned int num_steps,
      const TagSet & sample_set)
    {
      if (! config_)
        throw SessionError("No config set.");

      if (status_ & STATUS_NO_INIT)
      {
        log(LOG_GENERAL) 
          << "Warning: Session was not initialized. Initializing..."
          << std::endl;
        init();
      }

      clear_status();
      // check necessary components
      reduced_units_set();      
      random_number_generator();
      
      if (forces_.size() == 0)
        throw SessionWarning("No forces or potentials defined!");

      // Session initialization
      init_(num_steps);

      // Setting up simulator
      if (simulator_){
        if (omp_num_threads_ > 0){
          omp_set_num_threads(omp_num_threads_);
        }

        log(LOG_SIMULATOR)
          << "Simulate system with " << sample_set.size() << " particles for "
          << num_steps << " step(s)"
          ;
        if (omp_get_max_threads() > 1){
          log(LOG_SIMULATOR) 
            <<" using " << omp_get_max_threads() << " threads"
            ;
        }
        log(LOG_SIMULATOR) << "..." << std::endl;
        if (step_ == 0)
          log(LOG_THERMAL) << analysis_headline(* this) << std::endl;
        init_pairforces(* config_, forces_);
        TagSequence sequence(vector_from_set(sample_set));
        config_->set_neighbor_list_on_update_counter(& Session::num_n_list_updates_);
        CALLGRIND_TOGGLE_COLLECT;
        simulator_->run(* this, num_steps, sequence.begin(), sequence.end());
        CALLGRIND_TOGGLE_COLLECT;
        close();
        log_flush_all();
        CALLGRIND_DUMP_STATS;
      } else {
        std::stringstream error;
        error << "No simulator set.";
        throw SessionError(error.str());
      }
    }

    void
    Session::execute_callback() const
    {
      if (callback_ and callback_period_ > 0)
      {
        if (current_step() % callback_period_ == 0)
        {
          PyEval_InitThreads();
          PyGILState_STATE state = PyGILState_Ensure();
          boost::python::call<void>(
            callback_, step_, callback_period_, this);
          PyGILState_Release(state);
        }
      }
    }

    void
    Session::random_seed(
        unsigned int seed){
      typedef shared_ptr<RandomNumberGenerator> RngPtr;
      rng_ = RngPtr(new RandomNumberGenerator(seed));
    }

    shared_ptr<RandomNumberGenerator>
    Session::random_number_generator(){
      if (rng_){
        return rng_;
      } else {
        std::stringstream error;
        error << "No random number generator initialized.";
        throw SessionError(error.str());
      }
      return shared_ptr<RandomNumberGenerator>();
    }

    shared_ptr<ReducedUnitsSet>
    Session::reduced_units_set(){
      if (reduced_units_set_){
        return reduced_units_set_;
      } else {
        std::stringstream error;
        error << "No units set!";
        throw SessionError(error.str());
      }
      return shared_ptr<ReducedUnitsSet>();
    }

    // helper functions
    void
    Session::potential_update_(){
      pairforce_map_.clear();
      for(ForcesConstIterator force = forces_.begin();
          force != forces_.end(); ++force){
        (*force)->set_units(reduced_units_set_);
        add_pairforce_to_map_(*force);
      }
    }

    void
    Session::add_pairforce_to_map_(
      shared_ptr<Force> force)
    {
      if (dynamic_cast<Pairforce *>(force.get())){
        Pairforce const & pairforce = static_cast<Pairforce const &>(* force);
        const SimulationConfiguration::NeighborListKey key = pairforce.n_list_key();
        if (pairforce_map_.count(key) == 0)
          pairforce_map_.insert(std::make_pair(key, Forces()));
        pairforce_map_[key].push_back(force); 
      }
    }

    void
    Session::mc_moves_update_(){
      for(MonteCarloMovesConstIterator mc_move = mc_moves_.begin();
          mc_move != mc_moves_.end(); ++mc_move){
        (*mc_move)->set_random_number_generator(rng_);
        (*mc_move)->set_units(reduced_units_set_);
      }
    }

    void
    Session::init_computes_()
    {
      for(ComputesConstIterator compute = computes_.begin();
          compute != computes_.end(); ++compute){
        (*compute)->init(* this);
      }
    }

    void
    Session::init_fixes_(
      const unsigned int num_steps){
      for(FixesConstIterator fix = fixes_.begin();
          fix != fixes_.end(); ++fix){
        (*fix)->init(num_steps);
      }
    }

  } // namespace simulator

} // namespace mdst
