//=====================================================================================
//
//       Filename:  fixes.hpp
//
//    Description:  Apply an operation during runtime.
//
//        Version:  1.0
//        Created:  09/09/13
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_FIXES_H_
#define MDST_SIMULATOR_FIXES_H_

#include <types.hpp>
#include <memory.hpp>
#include <utility/sp_mean_covariance.hpp>

namespace mdst{

  namespace simulator{

    class Session;

    class Fix{
     public:
      // Fix();
      virtual ~Fix() {};

      virtual void 
      apply(
        Session & session) = 0;

      virtual void
      init(
        unsigned int num_steps) {};
    };

    class DetectLowAcceptanceRatioFix : public Fix{
     public:
      DetectLowAcceptanceRatioFix():
       threshold_(.001) {};

      virtual
      ~DetectLowAcceptanceRatioFix() {};

      virtual void
      apply(
        Session & session);

     private:
      Attribute threshold_;
    };

    class DetectEquilibrationFix : public Fix{
     public:
      DetectEquilibrationFix(
        Attribute threshold = 0.1,
        unsigned int num_blocks = 10):
        threshold_(threshold),
        num_blocks_(num_blocks),
        i_(-1),
        current_std_dev_(-1),
        last_std_dev_(-1),
        energies_(shared_ptr<Attributes>(new Attributes()))
      {};

      virtual
      ~DetectEquilibrationFix() {};

      virtual void
      apply(
        Session & session);

      virtual void
      init(
        unsigned int num_steps);
     private:
      typedef sitmo::sp_mean_covariance SpMeanCovariance;
      
      Attribute threshold_;
      const unsigned int num_blocks_;

      size_t i_;
      Attribute current_std_dev_;
      Attribute last_std_dev_;
      shared_ptr<Attributes> energies_;
      SpMeanCovariance sp_mean_covariance_;

    };


  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_FIXES_H_
