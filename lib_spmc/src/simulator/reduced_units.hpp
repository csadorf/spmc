// =====================================================================================
//
//       Filename:  reduced_units.hpp
//
//    Description:  Generate parameter from reduced_units_set.
//
//        Version:  1.0
//        Created:  01/20/2013 07:45:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_REDUCED_UNITS_H_
#define MDST_SIMULATOR_REDUCED_UNITS_H_

#include "forces.hpp"
#include "mc_moves.hpp"
#include "exception.hpp"

namespace mdst{

  namespace SimulationConfiguration{class SimulationConfig;}
  class RandomNumberGenerator;
  class ReducedUnitsSet;

  namespace simulator{

    // forward-declarations
    class Simulator;


  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_REDUCED_UNITS_H_
