//=====================================================================================
//
//       Filename:  simulator.hpp
//
//    Description:  Uses session information to explore phase space.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_H_
#define MDST_SIMULATOR_H_

#include "session.hpp"
#include "simulation_config.hpp"

namespace mdst{

  namespace simulator{

    class SimulatorError : public RuntimeError{
     public:
      SimulatorError() {};
      explicit SimulatorError(
        const std::string & e):
        RuntimeError(e) {};
      virtual ~SimulatorError() throw() {};
    };

    class SimulatorWarning : public RuntimeWarning{
     public:
      SimulatorWarning() {};
      explicit SimulatorWarning(
        const std::string & e):
        RuntimeWarning(e) {};
      virtual ~SimulatorWarning() throw() {};
    };

    class Simulator{
     public:
//      Simulator() {};
      virtual ~Simulator() {};

      virtual void
      run(Session & session,
          unsigned int num_steps,
          TagSequenceConstIterator sample_sequence_first,
          TagSequenceConstIterator sample_Sequence_last) = 0;

      virtual Attribute
      beta() const = 0;
    };

    void
    init_pairforces(
      const SimulationConfiguration::SimulationConfig & config,
      const Forces & forces);

    const unsigned int PADDING      = 12;
    const unsigned int PADDING_STEP = 10;

    void
    start_benchmark();

    std::string
    analysis_headline(
      Session & session);

    std::string
    update_analysis(
      Session & session);

    void
    apply_fixes(
      Session & session);

    void
    compute_computes(
      const Session & session);

    void
    execute_dumping(
      const Session & session);

    bool
    check_interrupt_state();

    TagSequenceIterator
    sort_by_molecules(
      const SimulationConfiguration::SimulationConfig & config,
      TagSequenceIterator result);

    TagSequenceIterator
    sort_by_molecules(
      const SimulationConfiguration::SimulationConfig & config,
      TagSequenceIterator mol_index_first,
      TagSequenceIterator & mol_index_last,
      TagSequenceIterator result);

    TagSequenceIterator
    sort_by_molecules(
      const SimulationConfiguration::SimulationConfig & config,
      TagSequenceConstIterator sample_first,
      TagSequenceConstIterator sample_last,
      TagSequenceIterator mol_index_first,
      TagSequenceIterator & mol_index_last,
      TagSequenceIterator result);

  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_H_
