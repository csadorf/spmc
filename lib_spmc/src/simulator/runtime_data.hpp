//=====================================================================================
//
//       Filename:  runtime_data.hpp
//
//    Description:  Provide data storage for runtimed data.
//
//        Version:  1.0
//        Created:  09/09/13
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_RUNTIME_DATA_H_
#define MDST_SIMULATOR_RUNTIME_DATA_H_

#include <numeric>

namespace mdst{

  struct KahanAccumulation
  {
    double sum;
    double correction;
  };

  inline
  KahanAccumulation KahanSum(KahanAccumulation accumulation, double value)
  {
     KahanAccumulation result;
     double y = value - accumulation.correction;
     double t = accumulation.sum + y;
     result.correction = (t - accumulation.sum) - y;
     result.sum = t;
     return result;
  }


  namespace simulator{

    typedef unsigned int DataId;

    struct RuntimeData{
      unsigned int cycle_deprecated;
      unsigned int num_cycles;
      TagSequenceConstIterator first;
      TagSequenceConstIterator last;
      Attribute acceptance_ratio;
      Attribute adjust;
      Attributes energy_deltas;

      RuntimeData():
        cycle_deprecated(0),
        num_cycles(0),
        adjust(1)
      {};

      void
      reserve(
        size_t num_cycles_,
        size_t num_entries)
      {
        num_cycles += num_cycles_;
        energy_deltas.reserve(energy_deltas.size() + num_entries);
      }

      void
      init()
      {
        cycle_deprecated = 0;
        num_cycles = 0;
        energy_deltas.clear();
      }

      void
      reset_energy()
      {
        energy_deltas.clear();
      }

      void
      add_delta(
        Attribute value)
      {
        energy_deltas.push_back(value);
      }

      Attribute
      energy_total() const
      {
        KahanAccumulation init = {0};
        KahanAccumulation result = std::accumulate(
          energy_deltas.begin(), energy_deltas.end(), init, KahanSum);
        return result.sum;
      }

    };

  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_RUNTIME_DATA_H_
