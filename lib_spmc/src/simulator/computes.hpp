//=====================================================================================
//
//       Filename:  computes.hpp
//
//    Description:  Compute attributes during runtime.
//
//        Version:  1.0
//        Created:  09/09/13
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_COMPUTES_H_
#define MDST_SIMULATOR_COMPUTES_H_

#include <types.hpp>
#include <memory.hpp>
#include <utility/exception.hpp>
#include <simulation_config/config_analysis.hpp>
#include <session.hpp>


namespace mdst{

  namespace simulator{

    //class Session;

    class Compute{
     public:
      Compute():
        counter_(0),
        frequency_(1) {};

      explicit 
      Compute(
        shared_ptr<void> init_result,
        unsigned int frequency = 1):
        last_result_(init_result),
        frequency_(frequency)
        {};

      void
      init(
        Session & session);

      void
      compute(
        const Session & session);

      void
      evaluate(
        const Session & session);

      shared_ptr <void> 
      last_result()
      {
        return last_result_;
      }

      void
      reset();

      unsigned int
      frequency() const
      { return frequency_;}


     protected:
      unsigned int
      counter(){
        return counter_;
      }

      shared_ptr<void> last_result_;

     private:
      unsigned int counter_;
      unsigned int frequency_;

      virtual void
      init_(
        Session & session) = 0;

      virtual void
      compute_(
        const Session & session) = 0;

      virtual void
      evaluate_(
        const Session & session,
        shared_ptr<void> & result) = 0;
    };

    template <class T>
    T
    last_result_of_compute_as(
      Compute & compute)
    {
      return * static_pointer_cast<T>(compute.last_result());
    }

    template<class T>
    class TypedCompute : public Compute{
     public:
      TypedCompute() {};
      explicit TypedCompute(
        T init_value):
        Compute(shared_ptr<T>(new T(init_value))) {};
      virtual ~TypedCompute() {};

      T
      last_result_as_value()
      {
        if (last_result_)
          return * static_pointer_cast<T>(last_result_);
        else
        {
          std::stringstream msg;
          msg << "TypedCompute: Tried to obtain last result, "
              << "but there was no result available. "
              << "Evaluate compute first!";
          throw RuntimeError(msg.str());
        }
      }

      T
      last_result_as_value_or_default()
      {
        if (last_result_)
          return * static_pointer_cast<T>(last_result_);
        else
          return T();
      }
     
     private:
      void
      test(){counter();}

    };

    template<class A>
    class ConfigAnalyzerCompute : public TypedCompute<typename A::ValueType>{
     public:
      typedef typename A::ValueType ValueType;
      typedef TypedCompute<ValueType> BaseClass;
      
      explicit
      ConfigAnalyzerCompute(
        A a = A(),
        ValueType zero = ValueType()):
        BaseClass(zero),
        a_(a),
        zero_(zero)
        {};

      virtual ~ConfigAnalyzerCompute() {};

     private:

      virtual void
      compute_(
        const Session & session)
      { data_ += a_(* session.config());}

      virtual void
      evaluate_(
        const Session & session,
        shared_ptr<void> & result)
      { 
        result = shared_ptr<void>(new ValueType(data_ / (this->counter() + 1)));
        data_ = a_(* session.config());
      }

      virtual void
      init_(
        Session & session)
      {
        data_ = a_(* session.config());
      }

      A a_;

      ValueType zero_;
      ValueType data_;
    };

    template class ConfigAnalyzerCompute<analysis::RadiusOfGyrationAnalyzer>;

    class OsmoticPressureCompute : public TypedCompute<Attribute>{
     public:
      virtual ~OsmoticPressureCompute() {};

     private:
      Attribute osmotic_0_;
      Attribute osmotic_1_;
     
      virtual void
      init_(
        Session & session);

      virtual void
      compute_(
        const Session & session);

      virtual void
      evaluate_(
        const Session & session,
        shared_ptr<void> & result);

    };

  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_COMPUTES_H_
