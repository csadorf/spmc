// =====================================================================================
//
//       Filename:  mc_nvt_sampler.cpp
//
//    Description:  Uses session information to explore phase space.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "mc_nvt_sampler.hpp"
#include "random_number_generation.hpp"
#include <simulation_config/parser_writer/writers.hpp>
#include "energy_calculation.hpp"

namespace mdst{

  namespace simulator{

    using SimulationConfiguration::SimulationConfig;
    using SimulationConfiguration::ParticleView;

    void
    MonteCarloNVTSampler::sample_(
        Session & session,
        const unsigned int num_cycles,
        TagSequenceConstIterator sample_molecule_first,
        TagSequenceConstIterator sample_molecule_last,
        Attribute adjust){
      // Copy sesssion configuration
      shared_ptr<SimulationConfig> config(session.config());
      TagSequence sequence(vector_from_set(config->all()));

      // Gather necessary session data
      const Forces & forces = session.forces();
      const PairforceMap & pairforce_map = session.pairforce_map();
      const MonteCarloMoves & mc_moves = session.mc_moves();

      // Statistical data
      Attribute num_accepted = 0;
      Attribute num_attempts = 0;
      static const unsigned int adjust_freq = 10;

      // Prepare cycles
      const size_t N_all = sequence.size();
      const size_t N_sample = sample_molecule_last - sample_molecule_first;

      TagSequence sorted_sequence(N_all);
      TagSequence molecule_index(N_sample);
      TagSequenceIterator molecule_index_last;

      TagSequenceIterator sorted_molecule_last =
        sort_by_molecules(
          * config, sample_molecule_first, sample_molecule_last,
          molecule_index.begin(), molecule_index_last,
          sorted_sequence.begin());

      const size_t N_molecules = molecule_index_last - molecule_index.begin();
      molecule_index.resize(N_molecules + 1);
      molecule_index[N_molecules] = sorted_molecule_last - sorted_sequence.begin();

      // Write runtime data
      RuntimeData & rt_data = * session.runtime_data();
      rt_data.first      = sorted_sequence.begin();
      rt_data.last       = sorted_molecule_last;
      
      //Initial analysis
      if (session.current_step() == 0)
        session.log(Session::LOG_THERMAL) 
          << update_analysis(session) << std::endl;

      for(unsigned int cycle = 0; cycle < num_cycles; ++cycle){
        //std::cerr << "cycles: " << float(cycle + 1) / num_cycles * 100 << "%" << std::endl;
        //rt_data.cycle = cycle;
        rt_data.adjust = adjust;

        const MonteCarloMoves active_moves =
          determine_active_moves(mc_moves, cycle);
        //Tuples positions(N_sample);
        //Tuples images(N_sample);
        Tuples positions(N_all);
        Tuples images(N_all);
        for(MonteCarloMovesConstIterator move = active_moves.begin();
            move != active_moves.end(); ++move){
          //#pragma omp parallel for
          for(size_t i_mol = 0; i_mol < N_molecules; ++i_mol){
            rt_data.add_delta(perform_attempt_(config,
                          sorted_sequence.begin() + molecule_index[i_mol],
                          sorted_sequence.begin() + molecule_index[i_mol + 1],
                          beta_,
                          forces,
                          pairforce_map,
                          **move,
                          adjust,
                          num_accepted,
                          positions,
                          images,
                          session.use_neighbor_lists(),
                          false,
                          false
                            ));
            ++num_attempts;
          }
        }
        session.do_step();

        const bool check_energy_calculation = false;
        if (check_energy_calculation){
          const Attribute pe_calculated = calculate_potential_energy(*config, forces);
          //const Attribute pe_calculated = calculate_potential_energy(* config, forces, pairforce_map);
          const Attribute pe_accumulated = rt_data.energy_total();
          const Attribute delta = pe_calculated - pe_accumulated;
          const Attribute error = std::abs(delta) / std::abs(pe_calculated) * 100;
          std::cerr << "cycle " << cycle 
                    << " error = " << error 
                    << "%, delta = " << delta 
                    << ", pe_real = " << pe_calculated
                    << ", pe_accu = " << pe_accumulated
                    << std::endl;
        }

        if (cycle % adjust_freq == 0){
          const Attribute ratio = num_accepted / num_attempts;
          if (ratio > 1.0){
            throw RuntimeError("Adjust ratio must be smaller or equal to 1.0!");
          }
          adjust_movement(
            ratio, adjust);
          num_accepted = 0;
          num_attempts = 0;
          rt_data.acceptance_ratio = ratio;
        }
        apply_fixes(session);
        compute_computes(session);
        execute_dumping(session);

        if ((cycle + 1) % session.analyzer_frequency() == 0){
          session.log(Session::LOG_THERMAL)
            << update_analysis(session) << std::endl;
        }
        
        if (session.check_abort_condition() == true)
        {
          session.log(Session::LOG_SIMULATOR) 
            << "Breaking, status: " << session.status() << std::endl;
          break;
        }
      }
      // final analysis
      //session.log(Session::LOG_THERMAL)
        //<< update_analysis(session) << std::endl;

      // Replace session configuration with sampled config
      //session.set_config(config);
      /*
      const Attribute pe_calculated = calculate_potential_energy(*config, forces);
      const Attribute pe_accumulated = rt_data.energy_total();
      std::cerr << "pe calculated  = " << pe_calculated << std::endl;
      std::cerr << "pe accumulated = " << pe_accumulated << std::endl;
      std::cerr << "delta = " << pe_calculated - pe_accumulated << std::endl;
      */
    }

    Attribute
    MonteCarloNVTSampler::perform_attempt_(
        shared_ptr<SimulationConfig> & config,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last,
        const Attribute beta,
        const Forces & forces,
        const PairforceMap & pairforce_map,
        const MonteCarloMove & mc_move,
        Attribute & adjust,
        Attribute & num_accepted,
        Tuples & positions_copy,
        Tuples & images_copy,
        const bool use_neighbor_lists,
        const bool full_copy,
        const bool full_calculation)
    {  
//      std::cerr << "Execute MC cycle # " << cycle << std::endl;
//
      /*
      std::cerr << "perform attempt: molecule = ";
      print_iterable(molecule_first, molecule_last, std::cerr) << std::endl;
      */

      const size_t random_particle = 
        size_t(rng_->random_number() * (molecule_last - molecule_first));
      const TagSequenceConstIterator picked = molecule_first + random_particle;

      TagSequenceConstIterator first_affected, last_affected;
      last_affected = mc_move.affected(
        * config, molecule_first, molecule_last, picked, first_affected);
      const size_t N_affected = last_affected - first_affected;
//      std::cerr << "affected :"; print_iterable(first_affected, last_affected) << std::endl;

      static shared_ptr<SimulationConfig> config_copy;
      #pragma omp critical
      {
        if (full_copy){
          // Make a full copy
          config_copy = shared_ptr<SimulationConfig>(new SimulationConfig(* config));
        } else {
          // Save original positions and images
          for(size_t i = 0; i < N_affected; ++i){
            positions_copy[i] = config->particle(*(first_affected + i)).reduced_position();
            images_copy[i] = config->particle(*(first_affected + i)).image();
          }
        }
      }
      
      Attribute original_energy, new_energy;
      if (full_calculation)
        original_energy = calculate_potential_energy(*config, forces);
      else
        original_energy = calculate_potential_energy_only_affected(
          * config, forces, pairforce_map,
          molecule_first, molecule_last,
          first_affected, last_affected,
          use_neighbor_lists);

      mc_move.execute(
        *config,
        molecule_first,
        molecule_last,
        picked,
        adjust);

      if (full_calculation)
        new_energy = calculate_potential_energy(
          *config, forces, pairforce_map, use_neighbor_lists);
      else
        new_energy = calculate_potential_energy_only_affected(
          * config, forces, pairforce_map,
          molecule_first, molecule_last,
          first_affected, last_affected,
          use_neighbor_lists);

      const Attribute rnd = rng_->random_number();
      const Attribute delta = new_energy - original_energy;
      const bool accepted = sample_nvt_accepted_(delta, beta, rnd);

      #pragma omp critical
      {
        if (!accepted){
          if (full_copy){
            // Restore original config from copy
            config = config_copy;
          } else {
            for(size_t i = 0; i < N_affected; ++i){
              ParticleView p(config->particle(*(first_affected + i)));
              p.set_reduced_position(positions_copy[i]);
              p.set_image(images_copy[i]);
            }
          }
        }
      }
      if (accepted){
        //std::cerr << "accepted" << std::endl;
        //std::cerr << "delta=" << delta << std::endl;
        num_accepted++;
        return delta;
      } else {
        //std::cerr << "not accepted." << std::endl;
        return 0;
      }
    }

    bool
    MonteCarloNVTSampler::sample_nvt_accepted_(
        const Attribute delta,
        const Attribute beta,
        const Attribute rnd) const
    {
      const Attribute a = exp( - beta * (delta));
      if (a > 1){
        return true;
      } else {
        return rnd <= a;
      }
    }

    void
    adjust_movement(
      const Attribute ratio,
      Attribute & adjust){
      adjust *= (0.95 + int(ratio / 0.5) * 0.1);
      adjust = std::min(std::max(adjust, 0.01), 100.0);
    }

  } // namespace simulator

} // namespace mdst



//        if (significantly_different(actual_delta, limited_delta, 4) && actual_delta != 0){
/*        if (accept_safe != accept_expr){
        std::cerr << "Sequence Iteration step: " << i
                  << " (particle=" << particle_i << ")"
                  << std::endl;
          std::cerr << "ERROR: delta value discrepancy!" << std::endl;
          std::cerr << "affected: ";
          print_iterable(affected) << std::endl;
          std::cerr << "actual delta = " << new_energy << " - " << original_energy
                    << " = " << actual_delta << std::endl;
          std::cerr << "limited delta = " << limited_delta << std::endl;
        }*/
