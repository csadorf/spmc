// =====================================================================================
//
//       Filename:  session.hpp
//
//    Description:  Manages session data.
//
//        Version:  1.0
//        Created:  01/20/2013 07:45:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

/*!
 * \file session.hpp
 * \brief Manage session data.
 *
 * A session may be attached with a simulator, a simulation configuration,
 * force fields, analyzers, computes, fixes and all other elements, which
 * are necessary to run a specific simulation.
 */

#ifndef MDST_SESSION_H_
#define MDST_SESSION_H_

#include "forces.hpp"
#include "mc_moves.hpp"
#include "fixes.hpp"
//#include "computes.hpp"
//#include "dumping/dumper.hpp"
//#include "analyzer.hpp"
#include "exception.hpp"
#include "runtime_data.hpp"
#include "utility/logging.hpp"
#include <simulation_config/n_list.hpp>

#include <omp.h>
#include <Python.h>

namespace mdst{

  namespace SimulationConfiguration{class SimulationConfig;}
  class RandomNumberGenerator;
  class ReducedUnitsSet;
    

  namespace simulator{

    // forward-declarations
    class Simulator;
    class Compute;
    class Analyzer;
    typedef shared_ptr<Analyzer> AnalyzerPtr_;
    typedef std::vector<AnalyzerPtr_> Analyzers;
    typedef Analyzers::const_iterator AnalyzersConstIterator;
    typedef Analyzers::iterator AnalyzersIterator;

    class Dumper;

    typedef std::vector<shared_ptr<Fix> > Fixes;
    typedef Fixes::const_iterator FixesConstIterator;
    typedef Fixes::iterator FixesIterator;
    typedef std::vector<shared_ptr<Compute> > Computes;
    typedef Computes::const_iterator ComputesConstIterator;
    typedef Computes::iterator ComputesIterator;
    typedef std::vector<shared_ptr<Dumper> > Dumpers;
    typedef Dumpers::const_iterator DumpersConstIterator;
    typedef Dumpers::iterator DumpersIterator;
    typedef std::map<SimulationConfiguration::NeighborListKey, Forces> PairforceMap;
    typedef PairforceMap::const_iterator PairforceMapConstIterator;
    typedef PairforceMap::iterator PairforceMapIterator;

    namespace interface{
      class Init;
      class System;
      class Dump;
      class Forcefield;
      class Moves;
    }

    class SessionError : public Exception{
     public:
      SessionError() {};
      SessionError(
        const std::string & e):
        Exception(e) {};
      virtual ~SessionError() throw() {};
    };

    class SessionWarning : public RuntimeWarning{
     public:
      SessionWarning() {};
      SessionWarning(
        const std::string & e):
        RuntimeWarning(e) {};
      virtual ~SessionWarning() throw() {};
    };

    class SessionIdNoneWarning : public SessionWarning{
     public:
      SessionIdNoneWarning() {};
      virtual ~SessionIdNoneWarning() throw() {};
    };

    class SessionAnalyzerNotFoundWarning : public SessionWarning{
     public:
      SessionAnalyzerNotFoundWarning() {};
      SessionAnalyzerNotFoundWarning(
        const std::string & e):
        SessionWarning(e) {};
      virtual ~SessionAnalyzerNotFoundWarning() throw() {};
    };

    class SessionDumperNotFoundWarning : public SessionWarning{
     public:
      SessionDumperNotFoundWarning() {};
      SessionDumperNotFoundWarning(
        const std::string & e):
        SessionWarning(e) {};
      virtual ~SessionDumperNotFoundWarning() throw() {};
    };

    class SessionInterrupt : public Exception{
     public:
      SessionInterrupt() {};
      SessionInterrupt(
        const std::string & e):
        Exception(e) {};
      virtual ~SessionInterrupt() throw() {};
    };

    /*! \brief A session includes all necessary elements
     *         to run, store and restart a simulation.
     *
     * A session may be attached with a simulator, a simulation configuration,
     * force fields, analyzers, computes, fixes and all other elements, which
     * are necessary to run a specific simulation.
     * 
     * A session can be copied and stored.
     */
    class Session{
     public:
      static const std::string ID_NONE;
      
      static const int STATUS_OK            = 0x00000000;
      static const int STATUS_STOP          = 0x00000001;
      static const int STATUS_ERROR         = 0x00000010;
      static const int STATUS_WARNING       = 0x00000100;
      static const int STATUS_NO_INIT       = 0x00001000;

      static const size_t LOG_GENERAL       = 0;
      static const size_t LOG_SIMULATOR     = 1;
      static const size_t LOG_THERMAL       = 2;
      static const size_t LOG_COMPUTES      = 3;
      static const size_t LOG_MISC          = 4;
      static const size_t LOG_NUM_CHANNELS  = 5;

      static const unsigned int STD_FREQ_FLUSHING = 1;
      static const unsigned int STD_FREQ_ANALYZER = 100;
      static const unsigned int STD_FREQ_COMPUTE  = 100;

      static const unsigned int STD_ANALYZER_PADDING = 20;

     private:
      typedef SimulationConfiguration::SimulationConfig sc_type_;

     public:
      Session(
        const std::string id = ID_NONE):
        id_(id),
        id_warn_(true),
        status_(STATUS_NO_INIT),
        logger_(LOG_NUM_CHANNELS),
        log_flushing_frequency_(STD_FREQ_FLUSHING),
        step_(0),
        num_steps_(0),
        config_(new SimulationConfiguration::SimulationConfig()),
        reduced_units_set_(new ReducedUnitsSet(reduced_units::argon)),
        runtime_data_(new RuntimeData()),
        analyzer_frequency_(STD_FREQ_ANALYZER),
        analyzer_padding_(STD_ANALYZER_PADDING),
        analyzer_callback_(NULL),
        callback_period_(0),
        callback_(NULL),
        use_neighbor_lists_(false),
        omp_num_threads_(0)
        {};

      Session(
        const Session & rhs,
        const std::string id = ID_NONE);

      ~Session();

      int
      status(){
        return status_;
      }
      
      void
      clear_status(){
        status_ = Session::STATUS_OK;
      }

      void
      init(unsigned int step = 0);

      int
      status_set(
        int bits){
        return status_ |= bits;
      }

      int
      status_unset(
        int bits){
        return status_ &= ~(bits);
      }

      bool
      check_abort_condition();

      std::ostream &
      log(
        size_t id = LOG_GENERAL){
        try{
          return logger_.channel(id);
        } catch (...) {
          std::stringstream msg;
          msg << "Invalid channel: " << id;
          throw RuntimeError(msg.str());
        }
      }

      void
      log_flush(
        size_t id){
        logger_.flush(id);
      }

      void
      log_flush_all(){
        logger_.flush_all();
      }

      void
      log_redirect(
        size_t id,
        std::ostream & stream);

      void
      log_redirect_to_std_cout(
        size_t id){
        logger_.redirect(id, std::cout);
      }

      void
      log_redirect_to_std_cerr(
        size_t id){
        logger_.redirect(id, std::cerr);
      }

      void
      log_redirect_to_std_clog(
        size_t id){
        logger_.redirect(id, std::clog);
      }

      void
      log_redirect_to_file(
        size_t id,
        const std::string & filename);

      unsigned int
      log_flushing_frequency() const{
        return log_flushing_frequency_;
      }

      void
      log_set_flushing_frequency(
        const unsigned int value){
        log_flushing_frequency_ = value;
      }

      void
      close();

      void
      run(
        unsigned int num_steps,
        const TagSet & sample_set);

      void
      run(
        unsigned int num_steps){
        if (! config_)
          throw SessionError("No config set.");
        run(num_steps, 
            config_->all());
        }

      void
      run_upto(
        unsigned int num_steps,
        const TagSet & sample_set);

      void
      run_upto(
        unsigned int num_steps){
        if (! config_)
          throw SessionError("No config set.");
        run_upto(num_steps,
            config_->all());
        }

      shared_ptr<sc_type_>
      config(){
        return config_;}

      shared_ptr<const sc_type_>
      config() const{
        return config_;
      }

      void
      set_config(
        shared_ptr<sc_type_> config){
        config_ = config;}

      shared_ptr<RuntimeData>
      runtime_data() const{
        return runtime_data_;}

      void
      init_read(
        const std::string & filename);

      void
      dump_write(
        const std::string & filename,
        const std::string & format) const;

      void
      dump(
        const std::string & format) const;

      static Strings
      dump_available_formats();

      void
      moves_add(
        shared_ptr<MonteCarloMove> move);

      void
      potential_add(
        shared_ptr<Force> force);

//      void
//      potential_add_pairforce(
//        shared_ptr<Pairforce> pairforce);

      shared_ptr<Analyzer>
      analyzer_add(
        shared_ptr<Analyzer> analyzer);

      shared_ptr<Analyzer>
      analyzer_add(
        const std::string & analyzer_id);

      void
      analyzer_set_frequency(
        const unsigned int frequency)
        { analyzer_frequency_ = frequency; }

      unsigned int
      analyzer_frequency()
        { return analyzer_frequency_;}

      PyObject *
      analyzer_set_callback(
        PyObject * callable)
      {
        analyzer_callback_ = callable;
        return Py_None;
      }

      PyObject *
      set_callback(
        unsigned int period,
        PyObject * callable)
      {
        callback_period_ = period;
        callback_ = callable;
        return Py_None;
      }

      PyObject *
      clear_callback()
      {
        callback_period_ = 0;
        callback_ = NULL;
        return Py_None;
      }

      void
      execute_analyzer_callback() const;

      void
      execute_callback() const;

      void
      analyzer_set_padding(
        const unsigned int value)
        { analyzer_padding_ = value;}

      unsigned int
      analyzer_padding() const
        { return analyzer_padding_; }

      void
      fix_add(
        shared_ptr<Fix> fix);

      void
      compute_add(
        shared_ptr<Compute> compute);

      void
      dumper_add(
        shared_ptr<Dumper> dumper);

      void
      dumper_add_by_id(
        const std::string & filename,
        const std::string & id,
        unsigned int frequency = 1);

      void
      set_reduced_units_set(
        shared_ptr<ReducedUnitsSet> rds);

      void
      set_simulator(
        shared_ptr<Simulator> simulator);

      shared_ptr<Simulator>
      simulator(){
        return simulator_;
      }

      shared_ptr<const Simulator>
      simulator() const
      {
        return simulator_;
      }

      Forces &
      forces()
        { return forces_;}

      Forces
      forces() const
        { return forces_;}

      PairforceMap &
      pairforce_map()
        { return pairforce_map_;}

      PairforceMap
      pairforce_map() const
        { return pairforce_map_;}

      Analyzers &
      analyzers()
        { return analyzers_;}

      Analyzers
      analyzers() const
      {  return analyzers_;}

      Fixes &
      fixes()
        { return fixes_;}

      Computes
      computes() const
        { return computes_;}

      Dumpers
      dumpers() const
        { return dumpers_;}
 
      MonteCarloMoves &
      mc_moves()
        { return mc_moves_;}
 
      void
      random_seed(
        unsigned int seed);
 
      shared_ptr<RandomNumberGenerator>
      random_number_generator();
 
      void
      set_reduced_units(
        shared_ptr<ReducedUnitsSet> units)
        { reduced_units_set_ = units;}
 
      shared_ptr<ReducedUnitsSet>
      reduced_units_set();
      
      // Parallelism

      void
      set_num_threads(
        int num_threads){
        omp_num_threads_ = num_threads;}

      int
      num_threads(){
        return omp_num_threads_;
      }

      void
      execute_in_parallel(){
        set_num_threads(omp_get_max_threads());
      }

      void
      execute_serial(){
        set_num_threads(1);
      }

      int
      num_n_list_updates() const
      { return num_n_list_updates_;}

      bool
      use_neighbor_lists() const
      { return use_neighbor_lists_;}

      void
      set_use_neighbor_lists(
        bool value)
      { use_neighbor_lists_ = value;}

      std::string
      id() const
      {
        if (id_ == ID_NONE && id_warn_)
          throw SessionIdNoneWarning();
        return id_;
      }

      void
      warn_on_id_none(bool warn = true)
      { id_warn_ = warn;}

      unsigned int
      current_step() const
      { return step_;}

      unsigned int
      num_steps() const
      { return num_steps_;}

      void
      do_step()
      {
        execute_callback();
        ++step_;
      }

     private:
      void
      init_(
        unsigned int num_steps = 0);

      // Internal state and logging
      const std::string id_;
      bool id_warn_;
      int status_;
      logging::Logger logger_;
      unsigned int log_flushing_frequency_;
      static int num_n_list_updates_;

      unsigned int step_;
      unsigned int num_steps_;

      // Aggregative session information
      shared_ptr<sc_type_> config_;
      shared_ptr<Simulator> simulator_;
      shared_ptr<RandomNumberGenerator> rng_;
      shared_ptr<ReducedUnitsSet> reduced_units_set_;
      shared_ptr<RuntimeData> runtime_data_;

      // Composite session information
      Forces forces_;
      PairforceMap pairforce_map_;
      MonteCarloMoves mc_moves_;
      Analyzers analyzers_;
      unsigned int analyzer_frequency_;
      unsigned int analyzer_padding_;
      PyObject * analyzer_callback_;
      unsigned int callback_period_;
      PyObject * callback_;
      Fixes fixes_;
      Computes computes_;
      Dumpers dumpers_;
      bool use_neighbor_lists_;
      
      // Parallelism
      int omp_num_threads_;

      // initialization functions
      void
      potential_update_();

      void
      add_pairforce_to_map_(
        shared_ptr<Force> force);

      void
      mc_moves_update_();
      
      void
      init_computes_();

      void
      init_analyzers_();

      void
      init_fixes_(
        const unsigned int num_steps);

      void
      init_dumpers_();

      void
      close_dumpers_();

    };

  } // namespace simulator

} // namespace mdst

#endif // MDST_SESSION_H_
