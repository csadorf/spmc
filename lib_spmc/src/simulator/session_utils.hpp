// =====================================================================================
//
//       Filename:  session_utils.hpp
//
//    Description:  Manipulate sessions.
//
//        Version:  1.0
//        Created:  11/19/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SESSION_UTILS_H_
#define MDST_SESSION_UTILS_H_

#include "session.hpp"

namespace mdst{

  namespace simulator{

    using SimulationConfiguration::SimulationConfig;

    template<class ComputeType>
    Computes
    find_computes(
      const Session & session)
    {
      Computes all_computes = session.computes();
      Computes result(all_computes.size());
      ComputesIterator last =
        find_type_in_iterable_of_shared_ptr<ComputeType>(
          all_computes.begin(), all_computes.end(),
          result.begin());
      result.resize(last - result.begin());
      return result;
    }

    template<class ComputeType>
    shared_ptr<ComputeType>
    find_or_install_compute(
      Session & session)
    {
      Computes found = find_computes<ComputeType>(session);
      if (found.size() > 0){
        return static_pointer_cast<ComputeType>(* found.begin());
      } else {
        shared_ptr<ComputeType> new_compute(new ComputeType());
        session.compute_add(new_compute);
        return new_compute;
      }
    }

    template<class ComputeType>
    shared_ptr<ComputeType>
    find_compute(
      const Session & session)
    {
      Computes found = find_computes<ComputeType>(session);
      if (found.size() > 0)
        return static_pointer_cast<ComputeType>(* found.begin());
      else
        return shared_ptr<ComputeType>();
    }

    template<class Analyzer>
    AnalyzersIterator
    find_analyzers(
      const Session & session,
      AnalyzersIterator first)
    {
      Analyzers all_analyzers = session.analyzers();
      return find_type_in_iterable_of_shared_ptr<Analyzer>(
        all_analyzers.begin(), all_analyzers.end(),
        first);
    }

    template<class Analyzer>
    shared_ptr<Analyzer>
    find_analyzer(
      const Session & session)
    {
      Analyzers found = session.analyzers();
      AnalyzersIterator last = find_analyzers<Analyzer>(session, found.begin());
      if (last != found.begin())
        return static_pointer_cast<Analyzer>(* found.begin());
      else
        return shared_ptr<Analyzer>();
    }

    template<class Analyzer>
    shared_ptr<Analyzer>
    find_or_install_analyzer(
      Session & session)
    {
      shared_ptr<Analyzer> found = find_analyzer<Analyzer>(session);
      if (found){
        return static_pointer_cast<Analyzer>(found);
      } else {
        shared_ptr<Analyzer> new_analyzer = boost::make_shared<Analyzer>();
        session.analyzer_add(new_analyzer);
        return new_analyzer;
      }
    }

    template<class Analyzer>
    shared_ptr<Analyzer>
    find_or_install_analyzer(
      Session & session,
      const std::string analyzer_id)
    {
      shared_ptr<Analyzer> found = find_analyzer<Analyzer>(session);
      if (found){
        return static_pointer_cast<Analyzer>(found);
      } else {
        shared_ptr<Analyzer> new_analyzer = boost::make_shared<Analyzer>(analyzer_id);
        session.analyzer_add(new_analyzer);
        return new_analyzer;
      }
    }

  } // namespace simulator

} // namespace mdst

#endif // MDST_SESSION_UTILS_H_
