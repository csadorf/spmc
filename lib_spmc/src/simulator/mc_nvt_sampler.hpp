//=====================================================================================
//
//       Filename:  mc_nvt_sampler.hpp
//
//    Description:  Uses session information to explore phase space.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_MC_NVT_SAMPLER_H_
#define MDST_MC_NVT_SAMPLER_H_

#include "mc_sampler.hpp"

namespace mdst{

  namespace simulator{

    class MonteCarloNVTSampler : public MonteCarloSampler{
     public:
      MonteCarloNVTSampler(
        const Attribute beta):
        beta_(beta) {};
      MonteCarloNVTSampler(
        const Attribute beta,
        shared_ptr<RandomNumberGenerator> rng):
        MonteCarloSampler(rng),
        beta_(beta) {};
      virtual ~MonteCarloNVTSampler() {};

      void
      set_beta(const Attribute beta)
        { beta_ = beta;}

      virtual Attribute
      beta() const
        { return beta_;}

     private:
      Attribute beta_;

      virtual void
      sample_(
          Session & session,
          const unsigned int num_cycles,
          TagSequenceConstIterator sample_sequence_first,
          TagSequenceConstIterator sample_sequence_last,
          Attribute adjust);

      Attribute
      perform_attempt_(
          shared_ptr<SimulationConfiguration::SimulationConfig> & config,
          TagSequenceConstIterator molecule_first,
          TagSequenceConstIterator molecule_last,
          const Attribute beta,
          const Forces & forces,
          const PairforceMap & pairforce_map,
          const MonteCarloMove & mc_move,
          Attribute & adjust,
          Attribute & num_accepted,
          Tuples & positions_copy,
          Tuples & images_copy,
          const bool use_neighbor_lists,
          const bool full_copy = false,
          const bool full_calculation = false);

      bool
      sample_nvt_accepted_(
          const Attribute delta,
          const Attribute beta,
          const Attribute random_number) const;
    };

    void
    adjust_movement(
        const Attribute ratio,
        Attribute & adjust);

  } // namespace simulator

} // namespace mdst

#endif // MDST_MC_NVT_SAMPLER_H_
