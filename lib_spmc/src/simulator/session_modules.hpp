// =====================================================================================
//
//       Filename:  session._modules.hpp
//
//    Description:  Prepare all data, necessary to run a simulation.
//
//        Version:  1.0
//        Created:  01/20/2013 07:45:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SESSION_MODULES_H_
#define MDST_SESSION_MODULES_H_

#include "forces.hpp"

#include <string>
#include <memory.hpp>

namespace mdst{

  namespace SimulationConfiguration{ class SimulationConfig;}
  class RandomNumberGenerator;
  class ReducedUnitsSet;

  namespace simulator{

    class SessionInterface;
    class Strings;
    class Force;
    class MonteCarloMove;
    class Simulator;

    namespace interface{

      class SessionInterfaceModule{
       public:
        SessionInterfaceModule(SessionInterface * session):
          session_(session) {};

        void
        setSessionInterface(SessionInterface * session){
          session_ = session;}
       protected:
        SessionInterface * session_;
      };

      class Init: public SessionInterfaceModule{
       public:
        Init(SessionInterface * session):
         SessionInterfaceModule(session) {};

       void
       reset();

       void
       read(
        const std::string & filename);

       void
       read(
        const std::string & filename,
        const std::string & format);

      };

      class Dump: public SessionInterfaceModule{
       public:
        explicit Dump(
          SessionInterface * session):
          SessionInterfaceModule(session) {};

        void
        write(
          const std::string & filename,
          const std::string & format);

        void
        thermal(
          const std::string & filename,
          const Strings & properties);
      };

      class Forcefield: public SessionInterfaceModule{
        friend class System;
       public:
        Forcefield(
          SessionInterface * session):
          SessionInterfaceModule(session) {};

        void
        add(
          shared_ptr<Force> force);
       private:
        void
        update_units_();
      };

      class Moves: public SessionInterfaceModule{
       public:
        Moves(
          SessionInterface * session):
          SessionInterfaceModule(session) {};

        void
        add(
          shared_ptr<MonteCarloMove> move);

       private:
        void
        update_rng_();
      };

      class System: public SessionInterfaceModule{
       public:
        System(
          SessionInterface * session):
          SessionInterfaceModule(session) {};

        shared_ptr<SimulationConfiguration::SimulationConfig>
        config();

        void
        set_reduced_units(
          shared_ptr<ReducedUnitsSet> reduced_units_set);
        
        void
        set_simulator(
          shared_ptr<Simulator> simulator);

        shared_ptr<RandomNumberGenerator>
        random_number_generator();
      };

    } // namespace interface
      
  } // namespace simulator

} // namespace mdst

#endif // MDST_SESSION_MODULES_H_
