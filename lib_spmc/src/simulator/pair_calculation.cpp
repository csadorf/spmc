// =====================================================================================
//
//       Filename:  pair_calculation.cpp
//
//    Description:  Pairwise calculations.
//
//        Version:  1.0
//        Created:  11/21/2012 10:20:55 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "pair_calculation.hpp"
#include <view.hpp>
#include <misc.hpp>
#include <tuple3_arithmetic.hpp>
#include <cell_grid.hpp>

namespace mdst{

  template std::ostream & 
  print_array(
    unsigned int *,
    const unsigned int,
    std::ostream &);

  namespace simulator{

    using namespace SimulationConfiguration;


    // distance calculations
    PairwiseDistances
    calculate_pairwise_distances_(
        const SimulationConfig & config,
        const TagSequence & group,
        const unsigned int exclude_bonded){
      const int N = config.num_atoms();
      PairwiseDistances result(N * (N-1) / 2);
      unsigned int n = -1;
      for(TagSequenceConstIterator it = group.begin(); it != group.end(); ++it){
        for(TagSequenceConstIterator it2 = ++TagSequenceConstIterator(it);
            it2 != group.end();
            ++it2){
          const ParticleTag i = *it;
          const ParticleTag j = *it2;
          if (i == j) continue;
          if (exclude_bonded > 0)
            if (config.property.bonded(i , j, exclude_bonded))
              continue;
          const ParticleView p_A = config.particle(i);
          const ParticleView p_B = config.particle(j);
          const Attribute d = abs(  p_B.remapped_position() 
                                  - p_A.remapped_position());
          SortedTypePair pair(p_A.type(), p_B.type());
          result[++n] = (PairwiseDistance){d, pair};
        }
      }
      return result;
    }
    
    unsigned int
    sum_num_cells_(
        const CellGrid & cell_grid){
      return    cell_grid.num_cells[0] 
              * cell_grid.num_cells[1] 
              * cell_grid.num_cells[2];
    }

    PairwiseDistancesVector
    calculate_pairwise_bonded_distances_(
      const SimulationConfig & config,
      const TagSequence & group,
      const unsigned int i_max){

      PairwiseDistancesVector result;
      for (TagSequenceConstIterator it = group.begin(); it != group.end(); ++it){
        typedef std::vector<TagSet> TagSetVector;
        typedef TagSetVector::const_iterator TagSetVectorConstIterator;
        TagSetVector neighbourhoods(i_max-1);
        // Generate vector of neighbourhoods including sub neighbourhoods
        for (unsigned int i = 0; i < i_max; ++i){
          neighbourhoods[i] = config.property.neighbourhood(*it, i+1);
        }
        // Subtract sub neighbourhoods
        for (unsigned int i = i_max; i > 1; --i){
          neighbourhoods[i-1] = 
            difference(neighbourhoods[i-1], neighbourhoods[i-2]);
        }
        // Iterate all neighbourhoods and calculate distances
        for(unsigned int i = 0; i < i_max; ++i){
          for(TagSetConstIterator n_it = neighbourhoods[i].begin();
              n_it != neighbourhoods[i].end();
              ++n_it){
            const ParticleTag i = *it;
            const ParticleTag j = *n_it;
            const ParticleView p_A = config.particle(i);
            const ParticleView p_B = config.particle(j);
            Attribute distance = 
              abs(  p_B.remapped_position()
                  - p_A.remapped_position());
            SortedTypePair pair(p_A.type(), p_B.type());
            result[i].push_back(
              (PairwiseDistance){distance, pair});
          }
        }
      }
      return result;
    }

    size_t
    calculate_squared_distances_pbc_no_cell_lists(
        const SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator particle,
        const SortedTypePair & sorted_type_pair,
        const Attribute reduced_r_cut,
        const unsigned int exclude_bonded,
        SquaredDistances & result){
      const Attribute squared_reduced_r_cut = reduced_r_cut * reduced_r_cut;
      const size_t N = last - first;
      if (result.size() < 0.5 * N * (N - 1))
        result.resize(0.5 * N * (N - 1));
      unsigned int n = -1;
      for(TagSequenceConstIterator it = first; it != last; ++it){
        for(TagSequenceConstIterator it2 = ++TagSequenceConstIterator(it);
            it2 != last; ++it2){
          const ParticleTag & i = *it;
          const ParticleTag & j = *it2;
//          std::cerr << "calc. " << i << " " << j << std::endl;
          SortedTypePair current_sorted_type_pair(
            config.particle(i).type(),
            config.particle(j).type());
          if (current_sorted_type_pair != sorted_type_pair)
            continue;
          if (particle != last){
            if(!(i ==  * particle) && !(j == * particle)){
              continue;
            }
          }
          if (i == j) continue;
          if (exclude_bonded > 0)
            if (config.property.bonded(i, j, exclude_bonded))
              continue;
//          std::cerr << "no exclusion." << std::endl;
          const Attribute r = absolute_squared_distance_pbc(
              config, i, j);
          /*std::cerr << "pp w/o cl.: "
                    << "r(" << i << ", " << j << ")="
                    << sqrt(r);*/
          if (r <= squared_reduced_r_cut){
            result[++n] = r;
          } else {
//            std::cerr << " excluded (>" << sqrt(squared_reduced_r_cut) << ")";
          }
//          std::cerr << std::endl;
        }
      }
      return ++n;
    }

/*
    Attribute
    absolute_squared_distance_pbc(
        const SimulationConfig & config,
        const ParticleTag a,
        const ParticleTag b){
      const Tuple3 pos_a = config.particle_position(a);
      const Tuple3 pos_b = config.particle_position(b);
      const Tuple3 & box = config.box();
      const Tuple3 delta = pos_b - pos_a;
      using std::abs;
      const Tuple3 delta_abs = Tuple3(
        abs(delta.x()), abs(delta.y()), abs(delta.z()));
      const Tuple3 d_pbc = Tuple3(
        abs(delta_abs.x() - int(delta_abs.x()/box.x()) * 2 * box.x()),
        abs(delta_abs.y() - int(delta_abs.y()/box.y()) * 2 * box.y()),
        abs(delta_abs.z() - int(delta_abs.z()/box.z()) * 2 * box.z()));
      const Attribute d = abs_squared(Tuple3(
        d_pbc.x() - int(d_pbc.x() / (2 * box.x())) * d_pbc.x(),
        d_pbc.y() - int(d_pbc.y() / (2 * box.y())) * d_pbc.y(),
        d_pbc.z() - int(d_pbc.z() / (2 * box.z())) * d_pbc.z()));
//      std::cerr << "pos_a=" << pos_a << "; pos_b=" << pos_b << std::endl;
//      std::cerr << "distance(" << a << ", " << b << ")=" << d << std::endl;
      return d;
    }
    */

    Attribute
    absolute_squared_distance(
        const SimulationConfig & config,
        const ParticleTag a,
        const ParticleTag b){
      return
        abs_squared(
          config.particle_position(b) - config.particle_position(a));
    }

  } // namespace simulator

} // namespace mdst
