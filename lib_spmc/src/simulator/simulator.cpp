// =====================================================================================
//
//       Filename:  simulator.cpp
//
//    Description:  Uses session information to explore phase space.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <Python.h>
#include "simulator.hpp"
#include "pair_calculation.hpp"
#include <iomanip>
#include <analyzer.hpp>
#include <dumping.hpp>

namespace mdst{

  namespace simulator{

    using SimulationConfiguration::SimulationConfig;

    void
    init_pairforces(
      const SimulationConfig & config,
      const Forces & forces)
    {
      for(ForcesConstIterator force = forces.begin();
          force != forces.end(); ++force){
        if (Pairforce * f = dynamic_cast<Pairforce*>(force->get())){
          f->init(config);
        }
      }
    }

    static std::time_t begin_time;
    static clock_t begin_ticks;

    void
    start_benchmark()
    {
      begin_time  = std::time(NULL);
      begin_ticks = std::clock();
    }

    std::string
    analysis_headline(
        Session & session)
        //const Analyzers & analyzers,
        //const unsigned int padding_step = PADDING_STEP)
    {
      const unsigned int padding = session.analyzer_padding();
      const unsigned int padding_step = PADDING_STEP;
      Analyzers const & analyzers = session.analyzers();

      // output
      std::stringstream out;
      out << std::setiosflags(std::ios::fixed)
          << std::left << std::setw(padding_step) << "step";
      for(AnalyzersConstIterator analyzer = analyzers.begin();
          analyzer != analyzers.end(); ++analyzer){
        std::string truncated_id((*analyzer)->id());
        if (truncated_id.size() > (padding - 1))
          truncated_id.resize(padding - 1, 'x');
        out << std::right << std::setw(padding)
            << truncated_id;
        //out << std::right << std::setw(padding)
         //   << truncated_id;
            //<< (*analyzer)->id();
      }
      return out.str();
    }

    std::string
    format_time(const std::time_t & time)
    {
      std::stringstream out;
      out << std::setfill('0') << std::setw(2);
      int hours = int(time / 3600);
      int minutes = int((time / 60) % 60);
      int seconds = time % 60;
      if (hours)
        out << hours << "h:";
      if (minutes)
        out << minutes << "m:";
      out << seconds << "s";
      return out.str();
    }

    std::string
    update_analysis(
      Session & session)
    {
      const unsigned int padding = session.analyzer_padding();
      const unsigned int padding_step = PADDING_STEP;
      Analyzers & analyzers = session.analyzers();
      // benchmarking
      std::time_t elapsed_time = std::time(NULL) - begin_time;
      std::time_t overall_time;
      if (session.current_step() > 0){
        overall_time = double(elapsed_time) / session.current_step() * session.num_steps();
      }
      
      // output
      std::stringstream out;
      // set table layout
      out 
          << std::setiosflags(std::ios::scientific)
          //<< std::setiosflags(std::ios::fixed)
          //<< std::setprecision(3)
          << std::left << std::setw(padding_step) << session.current_step();
      for(AnalyzersConstIterator analyzer = analyzers.begin();
          analyzer != analyzers.end(); ++analyzer){
        out << std::right << std::setw(padding)
            << (*analyzer)->compute(session);
      }
      
      // Execute registered analyzer callback function
      session.execute_analyzer_callback();

      if(session.current_step() > 0 && overall_time > 5){
        out << " (" << format_time(elapsed_time);
        if (session.current_step() > 0)
          out << "/" << format_time(overall_time);
        out << ")";
      }

      return out.str();
    }

    void
    apply_fixes(
      Session & session){
      Fixes & fixes = session.fixes();
      for(FixesConstIterator fix = fixes.begin();
          fix != fixes.end(); ++fix){
        (*fix)->apply(session);
      }
    }

    void
    compute_computes(
      const Session & session){
      Computes computes = session.computes();
      for(ComputesConstIterator compute = computes.begin();
          compute != computes.end(); ++compute){
        if (session.current_step() % (*compute)->frequency() == 0)
          (*compute)->compute(session);
      }
    }

    void
    execute_dumping(
      const Session & session)
    {
      Dumpers dumpers = session.dumpers();
      for(DumpersConstIterator dumper = dumpers.begin();
          dumper != dumpers.end(); ++dumper)
      {
        if (session.current_step() % (*dumper)->frequency() == 0)
          (*dumper)->dump(session);
      }
    }

    bool
    check_interrupt_state(){
      return PyErr_CheckSignals() < 0;
    }

    TagSequenceIterator
    sort_by_molecules(
      const SimulationConfig & config,
      TagSequenceIterator result)
    {
      TagSetSet molecules = config.property.molecules();
      for(TagSetSetConstIterator it = molecules.begin();
          it != molecules.end(); ++it){
        result = std::copy(it->begin(), it->end(), result);
      }
      return result;
    }

    TagSequenceIterator
    sort_by_molecules(
      const SimulationConfig & config,
      TagSequenceIterator mol_index_first,
      TagSequenceIterator & mol_index_last,
      TagSequenceIterator result)
    {
      TagSetSet molecules = config.property.molecules();
      size_t mol_index = 0;
      for(TagSetSetConstIterator it = molecules.begin();
          it != molecules.end(); ++it){
        result = std::copy(it->begin(), it->end(), result);
        * mol_index_first = mol_index;
        ++mol_index_first;
        mol_index += it->size();
      }
      mol_index_last = mol_index_first;
      return result;
    }

    TagSequenceIterator
    sort_by_molecules(
      const SimulationConfig & config,
      TagSequenceConstIterator sample_first,
      TagSequenceConstIterator sample_last,
      TagSequenceIterator mol_index_first,
      TagSequenceIterator & mol_index_last,
      TagSequenceIterator result)
    {
      const size_t N_sample = sample_last - sample_first;
      if (N_sample == config.num_atoms())
        return sort_by_molecules(config, mol_index_first, mol_index_last, result);

      TagSetSet all_molecules = config.property.molecules();
      TagSetSet sample_molecules;

      //#pragma omp parallel for shared(sample_molecules)
      for(size_t i = 0; i < N_sample; ++i){
        sample_molecules.insert(config.property.molecule(*(sample_first + i)));
//      while(sample_first != sample_last){
//        sample_molecules.insert(config.property.molecule(*sample_first));
//        ++sample_first;
      }

      size_t molecule_index = 0;
      for(TagSetSetConstIterator molecule = all_molecules.begin();
          molecule != all_molecules.end(); ++molecule){
        result = std::copy(molecule->begin(), molecule->end(), result);
        for(TagSetSetConstIterator sample_molecule = sample_molecules.begin();
            sample_molecule != sample_molecules.end(); ++sample_molecule){
          if (* sample_molecule == * molecule){     // mol_index_first to *_last indexes
            *(mol_index_first++) = molecule_index;  // all molecules, which are within the sample
            break;
          }
        }
        molecule_index += molecule->size();
      }
      mol_index_last = mol_index_first;
      return result;
    }


  } // namespace simulator

} // namespace mdst
