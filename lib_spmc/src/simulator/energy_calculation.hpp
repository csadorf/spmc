//=====================================================================================
//
//       Filename:  energy_calculation.hpp
//
//    Description:  Calculate energies.
//
//        Version:  1.0
//        Created:  11/14/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_ENERGY_CALCULATION_H_
#define MDST_ENERGY_CALCULATION_H_

#include <simulation_config.hpp>
#include "forces.hpp"
#include "pair_calculation.hpp"
#include "session.hpp"

namespace mdst{

  namespace simulator{

    //class Session;
    //class PairforceMap;

    Attribute
    calculate_potential_energy(
        const SimulationConfiguration::SimulationConfig & config,
        const Forces & forces);

    Attribute
    calculate_potential_energy(
        const SimulationConfiguration::SimulationConfig & config,
        const Forces & forces,
        const PairforceMap & pairforce_map,
        const bool use_neighbor_lists);

    Attribute
    calculate_potential_energy(
        const SimulationConfiguration::SimulationConfig & config,
        const Forces & forces,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last);

    Attribute
    calculate_potential_energy_only_affected(
        const SimulationConfiguration::SimulationConfig & config,
        const Forces & forces,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last,
        TagSequenceConstIterator affected_first,
        TagSequenceConstIterator affected_last);

    Attribute
    calculate_potential_energy_only_affected(
        const SimulationConfiguration::SimulationConfig & config,
        const Forces & forces,
        const PairforceMap & pairforce_map,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last,
        TagSequenceConstIterator affected_first,
        TagSequenceConstIterator affected_last,
        const bool use_neighbor_lists);


    template<class ForceType>
    inline
    Attribute
    calculate_specific_potential_energy(
        const SimulationConfiguration::SimulationConfig & config,
        const Forces & forces)
    {
      return calculate_potential_energy(
        config,
        determine_forces_from_type<ForceType>(forces));
    }


    template<class ForceType>
    inline
    Attribute
    calculate_specific_potential_energy(
        const SimulationConfiguration::SimulationConfig & config,
        const Forces & forces,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last)
    {
      Attribute energy = 0;
      for(ForcesConstIterator force = forces.begin();
          force != forces.end(); ++force){
        if (ForceType * f = dynamic_cast<ForceType*>(force->get())){
          energy += f->calculate(config, first, last);
        }
      }
      return energy;
    }


    inline
    Attribute
    calculate_inter_molecular_potential_energy(
        const SimulationConfiguration::SimulationConfig & config,
        const Forces & forces,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last)
    {
      return calculate_specific_potential_energy<InterMolecularForce>(
        config, forces, first, last);
    }

    Attribute
    calculate_inter_molecular_potential_energy(
        const SimulationConfiguration::SimulationConfig & config,
        const PairforceMap & pairforce_map,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last);

    Attribute
    calculate_intra_molecular_potential_energy(
        const SimulationConfiguration::SimulationConfig & config,
        const Forces & forces,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last);

    inline
    Attribute
    calculate_intra_molecular_potential_energy_molecule(
        const SimulationConfiguration::SimulationConfig & config,
        const Forces & forces,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last)
    {
      return calculate_specific_potential_energy<IntraMolecularForce>(
        config, forces, molecule_first, molecule_last);
    }

    inline
    Attribute
    calculate_field_potential_energy(
        const SimulationConfiguration::SimulationConfig & config,
        const Forces & forces,
        TagSequenceConstIterator molecule_first,
        TagSequenceConstIterator molecule_last)
    {
      return calculate_specific_potential_energy<FieldForce>(
        config, forces, molecule_first, molecule_last);
    }

  } // namespace simulator

} // namespace mdst

#endif // MDST_ENERGY_CALCULATION_H_
