//=====================================================================================
//
//       Filename:  mc_moves.cpp
//
//    Description:  Monte Carlo Sample Moves.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "mc_moves.hpp"
#include "simulation_config.hpp"
#include "random_number_generation.hpp"
#include "coordinates.hpp"
#include "tuple3_arithmetic.hpp"

#include <assert.h>
namespace mdst{

  namespace simulator{

    using SimulationConfiguration::SimulationConfig;
    using SimulationConfiguration::ParticleView;

    TagSequenceConstIterator
    MonteCarloMove::affected(
      const SimulationConfig & config,
      TagSequenceConstIterator first,
      TagSequenceConstIterator last,
      const TagSequenceConstIterator picked,
      TagSequenceConstIterator & result) const
    {
      result = first;
      return last;
    }

    void
    RotateMoonMove::execute(
        SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const{
      const size_t N = last - first;
      if (N <= 1)
        return;
      TagSequenceConstIterator planet = last;
      if (picked != last &&
          config.particle(*picked).type() == planet_type_){
        planet = picked;
      } else {
        TagSequenceConstIterator planets[N];
        size_t num_planets = 0;
        for(TagSequenceConstIterator iter = first;
            first != last; ++first){
          const Type t = config.particle(*iter).type();
          if (t == planet_type_)
            planets[num_planets++] = iter;
        }
        if (num_planets > 0){
          const size_t r = size_t(rng_->random_number() * num_planets);
          planet = planets[r];
        }
      }
      if (planet != last){
        const TagSet possible_moons = config.property.neighbours(*planet);
        TagVector moons(N);
        size_t num_moons = 0;
        for(TagSetConstIterator iter = possible_moons.begin();
            iter != possible_moons.end(); ++iter){
          const Type t = config.particle(*iter).type();
          if (t == moon_type_)
            moons[num_moons++] = *iter;
        }
        const size_t r = size_t(rng_->random_number() * num_moons);
        move_(config, planet, moons[r]);
      }
      std::cerr << "WARNING: RotateMoonMove not varified!" << std::endl;
    }

    void
    RotateMoonMove::move_(
        SimulationConfig & config,
        const TagSequenceConstIterator planet,
        const ParticleTag moon) const{
      const Tuple3 planet_position = 
        config.particle(* planet).remapped_position();
      const Attribute x0 = rng_->random_number();
      const Attribute x1 = rng_->random_number();
      const Attribute x2 = rng_->random_number();
      const Quaternion q = uniform_random_rotation(x0, x1, x2);
      ParticleView moon_v = config.particle(moon);
      moon_v.set_position(rotation(planet_position, moon_v.remapped_position(), q));
    }

    void
    RandomDisplacementMove::execute(
        SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const{
      const Tuple3 d_max = config.box() * adjust * adjust_;
      const Tuple3 delta(
        rng_->random_number() - 0.5,
        rng_->random_number() - 0.5,
        rng_->random_number() - 0.5);
      ParticleView particle_view(config.particle(* picked));
      particle_view.set_position(
        particle_view.position() + element_product(delta, d_max));
    }

    TagSequenceConstIterator
    RandomDisplacementMove::affected(
      const SimulationConfig & config,
      TagSequenceConstIterator first,
      TagSequenceConstIterator last,
      const TagSequenceConstIterator picked,
      TagSequenceConstIterator & result) const
    {
      result = picked;
      return result + 1;
    }

    void
    RotateBeadsMove::execute(
        SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const{
      const size_t k = (*picked) - (*picked) % 2;
      ParticleView p_k = config.particle(k);
      ParticleView p_k_1 = config.particle(k + 1);
      const Tuple3 center_of_gravity = 0.5 * (
        p_k.remapped_position() + p_k_1.remapped_position());
      const Attribute x0 = rng_->random_number();
      const Attribute x1 = rng_->random_number();
      const Attribute x2 = rng_->random_number();
      const Quaternion q = uniform_random_rotation(x0, x1, x2);
      p_k.set_position(
        rotation(center_of_gravity, p_k.remapped_position(), q));
      p_k_1.set_position(
        rotation(center_of_gravity, p_k_1.remapped_position(), q));
    }

    TagSequenceConstIterator
    RotateBeadsMove::affected(
      const SimulationConfig & config,
      TagSequenceConstIterator first,
      TagSequenceConstIterator last,
      const TagSequenceConstIterator picked,
      TagSequenceConstIterator & result) const
    {
      result = picked - (*picked) % 2;
      return result + 2;
    }

    void
    ChangeBondLengthMove::execute(
        SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const
    {
      const size_t k = (*picked) - (*picked) % 2;
      ParticleView p_k  = config.particle(k);
      ParticleView p_k_1 = config.particle(k + 1);
      const Tuple3 center_of_gravity = 0.5 * (
        p_k.remapped_position() + p_k_1.remapped_position());
      const Attribute x0 = rng_->random_number();
      const Attribute new_bond_length = x0 * adjust_;
      const Tuple3 p_k_r = p_k.remapped_position() - center_of_gravity;
      const Tuple3 p_k_1_r = p_k_1.remapped_position() - center_of_gravity;
      p_k.set_position(
          normalize(p_k_r) * new_bond_length + center_of_gravity);
      p_k_1.set_position(
          normalize(p_k_1_r) * new_bond_length + center_of_gravity);
    }

    TagSequenceConstIterator
    ChangeBondLengthMove::affected(
      const SimulationConfig & config,
      TagSequenceConstIterator first,
      TagSequenceConstIterator last,
      const TagSequenceConstIterator picked,
      TagSequenceConstIterator & result) const
    {
      result = picked - (*picked) % 2;
      return result + 2;
    }

    void
    ChangeBondLengthBiasedMove::execute(
        SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const
    {
      const size_t k = (*picked) - (*picked) % 2;
      ParticleView p_k  = config.particle(k);
      ParticleView p_k_1 = config.particle(k + 1);
      const Tuple3 center_of_gravity = 0.5 * (
        p_k.remapped_position() + p_k_1.remapped_position());
      //const Attribute x0 = rng_->random_number();

      Attribute new_bond_length = units_->reduce(
        interpolate_linear(length_cd_, rng_->random_number()));
      assert(new_bond_length == new_bond_length);
      assert(center_of_gravity == center_of_gravity);

      const Tuple3 p_k_r = p_k.remapped_position() - center_of_gravity;
      const Tuple3 p_k_1_r = p_k_1.remapped_position() - center_of_gravity;
      p_k.set_position(
          normalize(p_k_r) * 0.5 * new_bond_length + center_of_gravity);
      p_k_1.set_position(
          normalize(p_k_1_r) * 0.5 * new_bond_length + center_of_gravity);
    }

    TagSequenceConstIterator
    ChangeBondLengthBiasedMove::affected(
      const SimulationConfig & config,
      TagSequenceConstIterator first,
      TagSequenceConstIterator last,
      const TagSequenceConstIterator picked,
      TagSequenceConstIterator & result) const
    {
      result = picked - (*picked) % 2;
      return result + 2;
    }


    template<class InputIterator>
    void
    check_bond_length_(
      const SimulationConfig & config,
      InputIterator first,
      InputIterator last)
    {
      ++first;
      for(; first != last; ++first)
      {
        const ParticleView p0 = config.particle(*(first - 1));
        const ParticleView p1 = config.particle(*(first));
        const Attribute bl = abs(p1.remapped_position() - p0.remapped_position());
        if (significantly_different(bl, 1, 4))
        {
          std::stringstream msg;
          msg
            << "Incorrect bond length(" 
            << *(first - 1) << ", " << *first << ") = "
            << bl
          ;
          throw RuntimeError(msg.str());
        }
      }
    }

    template<class InputIterator>
    void
    generic_jiggle(
      SimulationConfig & config,
      InputIterator picked,
      InputIterator last,
      const Attribute adjust,
      const ReducedUnitsSet & units,
      RandomNumberGenerator & rng,
      bool section = false)
    {
      InputIterator section_last(last);
      InputIterator jiggled_last;
      const ParticleView p0 = config.particle(*picked);
      //const ParticleView p1 = config.particle(*(picked + 1));
      const Tuple3 pivot(p0.remapped_position());
      const Quaternion q = uniform_random_rotation(
        rng.random_number(), rng.random_number(), rng.random_number());

      static Tuple3 delta;
      if (section){
        const double x = rng.random_number() * adjust;
        size_t n_section = std::max(x * (last - picked), 1.0);
        if (n_section >= size_t (last-picked) )
          n_section = last-picked - 1;
        jiggled_last = picked + n_section;
        section_last = picked + n_section + 1;
        delta = -1 * config.particle(* jiggled_last).remapped_position();
      }

      for(InputIterator i = picked + 1; i != section_last; ++i){
        ParticleView p_i1 = config.particle(*i);
        p_i1.set_position(rotation(pivot, p_i1.remapped_position(), q));
      }
      if (section){
        delta += config.particle(* jiggled_last).remapped_position();
      }
      for(InputIterator i = section_last; i != last; ++i){
        ParticleView p_i = config.particle(*i);
        p_i.set_position(p_i.remapped_position() + delta);
      }
    }

    void
    TranslateJiggleMove::execute(
        SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const
    {
      //check_bond_length_(config, first, last);
      if (reverse_){
        TagSequence::const_reverse_iterator rpicked(picked + 1);
        TagSequence::const_reverse_iterator rlast(first);
        if (rlast - rpicked > 1)
          generic_jiggle(
            config, rpicked, rlast, adjust * adjust_,
            *units_, *rng_, section_);
      } else {
        if (last - picked > 1)
          generic_jiggle(
            config, picked, last, adjust * adjust_,
            *units_, *rng_, section_);
      }
      //check_bond_length_(config, first, last);
      if (do_reverse_)
        if (rng_->random_number() < 0.5)
          reverse_ = !reverse_;
    }

    TagSequenceConstIterator
    TranslateJiggleMove::affected(
        const SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        TagSequenceConstIterator & result) const
    {
      if (reverse_){
        if (picked - first > 0){
          result = first;
          return picked;
        }
      } else {
        if (last - picked > 1){
          result = picked + 1;
          return last;
        }
      }
      result = last;
      return last;
    }

    void
    TranslateJiggleMove::jiggle_(
        SimulationConfig & config,
        TagSequenceConstIterator picked,
        TagSequenceConstIterator last,
        const Attribute adjust) const{
      // use template function 'generig_jiggle(..)' instead
      throw RuntimeError("Deprecated.");
    }

    void
    TranslateJiggleBiasedMove::jiggle_(
      SimulationConfig & config,
      TagSequenceConstIterator picked,
      TagSequenceConstIterator last,
      const Attribute adjust) const
    {
      // use template function 'generic_biased_jiggle(..)' instead
      throw RuntimeError("Deprecated.");
    }

/*    template<class InputIterator>
    void
    generic_biased_jiggle(
      SimulationConfiguration::SimulationConfig & config,
      InputIterator picked,
      InputIterator last,
      const Attribute adjust,
      const ReducedUnitsSet & units,
      RandomNumberGenerator & rng,
      const Lengths & length_cd_,
      const Angles & angle_small_cd_,
      const Angles & angle_big_cd_,
      const Angles & torsion_cd_,
      const Area & bead_size_squared_)
    {
      const ParticleView p0 = config.particle(*picked);
      const ParticleView p1 = config.particle(*(picked + 1));
      const ParticleView p2 = config.particle(*(picked + 2));
      const ParticleView p3 = config.particle(*(picked + 3));

      const Tuple3 d0(p1.remapped_position() - p0.remapped_position());
      const Tuple3 d1(p2.remapped_position() - p1.remapped_position());
      const Tuple3 d2(p3.remapped_position() - p2.remapped_position());

      const Attribute r = units.reduce(
        interpolate_linear(length_cd_, rng.random_number()));

      PlaneAngle phi;
      const Attribute p = std::max(r*r, abs_squared(d1));
      if (p < 2.25 * units.reduce(bead_size_squared_))
        phi = interpolate_linear(angle_small_cd_, rng.random_number());
      else
        phi = interpolate_linear(angle_big_cd_, rng.random_number());

      const PlaneAngle theta =
        interpolate_linear(torsion_cd_, rng.random_number());

//      std::cerr << "r=" << r << ", phi=" << phi << ", theta=" << theta << std::endl;

      const Tuple3 d2_new = oriented_position(d1, d0, r, phi, theta);
//      std::cerr << "d2=" << d2 << " (" << abs(d2) << ")" << std::endl;
//      std::cerr << "d2_new=" << d2_new << " (" << abs(d2_new) << ")" << std::endl;

      const Tuple3 pivot = p2.remapped_position();
      const Tuple3 from = p3.remapped_position();
      const Tuple3 to = p2.remapped_position() + d2_new;
      //std::cerr << "pivot=" << pivot << ", from=" << from << ", to=" << to << std::endl;

      for(InputIterator i = picked + 3; i != last; ++i){
        ParticleView particle = config.particle(*i);
        particle.set_position(rotation(pivot, particle.remapped_position(), from, to));
      }
    }*/

    template<class InputIterator>
    void
    generic_biased_jiggle(
      SimulationConfiguration::SimulationConfig & config,
      InputIterator picked,
      InputIterator last,
      const Attribute adjust,
      const ReducedUnitsSet & units,
      RandomNumberGenerator & rng,
      const Lengths & length_cd_,
      const Angles & angle_small_cd_,
      const Angles & angle_big_cd_,
      const Angles & torsion_cd_,
      const Area & bead_size_squared_,
      const bool section = false)
    {
      InputIterator section_last(last);
      InputIterator jiggled_last;

      Tuple3 delta;
      if (section){
        const double x = rng.random_number() * adjust;
        size_t n_section = std::max(x * (last - picked), 5.0);
        if (n_section >= size_t(last - picked))
          n_section = last-picked - 1;
        jiggled_last = picked + n_section;
        section_last = picked + n_section + 1;
        delta = -1 * config.particle(* jiggled_last).remapped_position();
      }

      ParticleView p00 = config.particle(*picked);
      ParticleView p10 = config.particle(*(picked + 1));
      ParticleView p20 = config.particle(*(picked + 2));


      Tuple3 d2(p10.remapped_position() - p00.remapped_position());
      Tuple3 d1(p20.remapped_position() - p10.remapped_position());


      Attribute r = units.reduce(
      interpolate_linear(length_cd_, rng.random_number()));

      PlaneAngle phi;
      const Attribute p = std::max(r*r, abs_squared(d1));
      if (p < 2.25 * sqrt(units.reduce(bead_size_squared_)))
        phi = interpolate_linear(angle_small_cd_, rng.random_number());
      else
        phi = interpolate_linear(angle_big_cd_, rng.random_number());

       Attribute rand_th = 2 * rng.random_number();
      PlaneAngle theta;
      if (rand_th > 1)
         theta = -interpolate_linear(torsion_cd_, rand_th - 1);
      else
         theta = interpolate_linear(torsion_cd_, rand_th);

      const Tuple3 carte = cartesian(r, phi, theta);
      spherical(carte, r, phi, theta);

      const size_t N_molecule = last - picked;
      Attributes r_particle(N_molecule);
      Attributes phi_particle(N_molecule);
      Attributes theta_particle(N_molecule);

      size_t counter = 0;

       for (InputIterator i = picked + 3; i != section_last; ++i){
         if (counter >= r_particle.size()){
           std::stringstream msg;
           msg
            << "Error in translate_jiggle_biased move. "
            << "Out of bounds. "
            << "last - picked = " << last - picked << "; "
            << "counter = " << counter << "; "
            << "Affected: ";
           print_iterable(picked, last, msg) << "; ";
           throw RuntimeError(msg.str());
         }
         if (*i < 0 or *i > int(config.num_atoms())){
           std::stringstream msg;
           msg
            << "Error in translate_jiggle_biased move. "
            << "Particle out of bounds. "
           ;
           throw RuntimeError(msg.str());
         }

         ParticleView p0 = config.particle(*(i - 3));
         ParticleView p1 = config.particle(*(i - 2));
         ParticleView p2 = config.particle(*(i - 1));
         ParticleView p3 = config.particle(*(i));

         r_particle[counter] = abs(p3.remapped_position() - p2.remapped_position());
         phi_particle[counter] = calculate_angle(
          p1.remapped_position(),
          p2.remapped_position(),
          p3.remapped_position()) / si::radian;
         theta_particle[counter] = calculate_torsion(
          p0.remapped_position(), 
          p1.remapped_position(), 
          p2.remapped_position(), 
          p3.remapped_position()) / si::radian;
         counter++;
      }


      r_particle[0] = r;
      phi_particle[0] = phi / si::radian;
      theta_particle[0] = theta / si::radian;

      counter = 0;
      for (InputIterator i = picked + 3; i != section_last; ++i){
        Attribute rand_theta_phi = M_PI * rng.random_number();
        Tuple3 d2_new = oriented_position(
           d1, d2, r_particle[counter], 
           phi_particle[counter] * si::radian,
           theta_particle[counter] * si::radian,
           rand_theta_phi);
        ParticleView previous = config.particle(*(i-1));
        ParticleView particle = config.particle(*(i));
        const Tuple3 to = previous.remapped_position() + d2_new;
        particle.set_position(to);
        d2 = d1;
        d1 = d2_new;
        counter++;
      }

      if (section){
        delta += config.particle(* jiggled_last).remapped_position();
      }
      for (InputIterator i = section_last; i != last; ++i){
        ParticleView p_i = config.particle(* i);
        p_i.set_position(p_i.remapped_position() + delta);
      }
    }

    void
    TranslateJiggleBiasedMove::execute(
        SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const
    {
      if (last - first < 5){
        std::stringstream error;
        error
          << "Translate Jiggle Biased Move cannot be used for molecules "
          << "with less than 5 beads.";
        throw RuntimeError(error.str());
      }
      if (reverse_){
        TagSequence::const_reverse_iterator rpicked(picked + 1);
        TagSequence::const_reverse_iterator rlast(first);
        if (rlast - rpicked > 3)
          generic_biased_jiggle( config, rpicked, rlast, adjust, *units_, *rng_,
                          length_cd_, angle_small_cd_, angle_big_cd_, 
                          torsion_cd_, bead_size_squared_);
      } else {
        if (last - picked > 3)
          generic_biased_jiggle( config, picked, last, adjust, * units_, * rng_,
                          length_cd_, angle_small_cd_, angle_big_cd_, 
                          torsion_cd_, bead_size_squared_);
      }
      if (do_reverse_)
        if (rng_->random_number() < 0.5)
          reverse_ = !reverse_;
    }

    TagSequenceConstIterator
    TranslateJiggleBiasedMove::affected(
      const SimulationConfig & config,
      TagSequenceConstIterator first,
      TagSequenceConstIterator last,
      const TagSequenceConstIterator picked,
      TagSequenceConstIterator & result) const
    {
      if (reverse_){
        TagSequence::const_reverse_iterator rpicked(picked + 1);
        TagSequence::const_reverse_iterator rlast(first);
        if (rlast - rpicked > 3)
        {
          result = first;
          return picked;
        }
        /*if (picked - first > 3){  // varified
          result = first;         // ''
          return picked;
          return picked - 2;      // ''
        }*/
      } else {
        if(last - picked > 3){    // varified
          result = picked + 3;    // ''
          return last;            // ''
        }
      }
      result = last;
      return last;
    }

    void
    TranslateMoleculeMove::execute(
        SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        TagSequenceConstIterator picked,
        const Attribute adjust) const
    {
      translate_(config, first, last, adjust);
    }

    void
    TranslateMoleculeMove::translate_(
      SimulationConfig & config,
      TagSequenceConstIterator first,
      TagSequenceConstIterator last,
      const Attribute adjust) const
    {
      const Tuple3 d_max(adjust * adjust_);

      Tuple3 delta(
        rng_->random_number() - 0.5,
        rng_->random_number() - 0.5,
        rng_->random_number() - 0.5);
      delta = element_product(delta, d_max);
      while(first != last){
        ParticleView particle_i(config.particle(*first));
        particle_i.set_position(particle_i.remapped_position() + delta);
        ++first;
      }
    }

    void
    CrankShaftMove::execute(
      SimulationConfig & config,
      TagSequenceConstIterator first,
      TagSequenceConstIterator last,
      const TagSequenceConstIterator picked,
      const Attribute adjust) const{
      const size_t N = last - first;
      if (N <= 2 || picked == first || picked + 1 == last)
        return;
      rotate_(config, picked);
    }

    TagSequenceConstIterator
    CrankShaftMove::affected(
      const SimulationConfig & config,
      TagSequenceConstIterator first,
      TagSequenceConstIterator last,
      const TagSequenceConstIterator picked,
      TagSequenceConstIterator & result) const
    {
      const size_t N = last - first;
      if (N <= 2 || picked == first || picked + 1 == last){
        result = last;
        return last;
      } else {
        result = picked;
        return picked + 1;
      }
    }

    void
    CrankShaftMove::rotate_(
//    rotate_(
      SimulationConfig & config,
      TagSequenceConstIterator middle) const{
      const ParticleView p0 = config.particle(*(--middle));
      ParticleView p1 = config.particle(*(++middle));
      const ParticleView p2 = config.particle(*(++middle));
      const Attribute phi = rng_->random_half_turn();

      const Tuple3 b = 0.5 * (p0.remapped_position() + p2.remapped_position());
      const Tuple3 x = p1.remapped_position() - b;
      const Tuple3 z = p0.remapped_position() - b;
      const Attribute x_abs = abs(x);
      const Attribute z_abs = abs(z);
      if (x_abs == 0 || z_abs == 0)
        return;
      const Tuple3 y = normalize(cross_product(x, z));
      const Attribute x_ = x_abs * cos(phi);
      const Attribute y_ = - x_abs * sin(phi);
      const Tuple3 & box = config.box();
      const Tuple3 x_new = element_division(x, box) * (x_ /x_abs) + element_division(y, box) * y_;
      const Tuple3 p_new = element_division(b, box) + x_new;
      p1.set_position(element_product(p_new, box));
    }

  } // namespace simulator

} // namespace mdst
