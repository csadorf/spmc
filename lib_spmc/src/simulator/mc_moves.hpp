//=====================================================================================
//
//       Filename:  mc_moves.hpp
//
//    Description:  Monte Carlo Sample Moves.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_MC_MOVES_H_
#define MDST_SIMULATOR_MC_MOVES_H_

#include <utility/misc.hpp>
#include <simulation_config/parameters.hpp>
#include <simulation_config/reduced_units.hpp>

namespace mdst{

  class RandomNumberGenerator;
  namespace SimulationConfiguration{ class SimulationConfig;}

  namespace simulator{
    // Cummulated distributions
    typedef std::map<Attribute, Length> Lengths;
    typedef Lengths::const_iterator LengthsConstIterator;
    typedef Lengths::iterator LengthsIterator;
    typedef std::map<Attribute, PlaneAngle> Angles;
    typedef Angles::const_iterator AnglesConstIterator;
    typedef Angles::iterator AnglesIterator;

    // Move definitions

    class MonteCarloMove{
     public:
      explicit MonteCarloMove(
        unsigned int frequency = 1,
        Attribute adjust = 1):
        frequency_(frequency),
        adjust_(adjust) {};
      explicit MonteCarloMove(
        shared_ptr<RandomNumberGenerator> rng,
        unsigned int frequency = 1,
        Attribute adjust = 1):
        frequency_(frequency),
        adjust_(adjust),
        rng_(rng) {};
      virtual ~MonteCarloMove() {};

      virtual void
      execute(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const = 0;

      virtual TagSequenceConstIterator
      affected(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        TagSequenceConstIterator & result) const;

      void
      set_frequency(unsigned int frequency)
        {frequency_ = frequency;}

      unsigned int
      frequency() const
        { return frequency_;}

      void
      set_random_number_generator(
        shared_ptr<RandomNumberGenerator> rng)
        {rng_ = rng;}

      void
      set_units(
        shared_ptr<ReducedUnitsSet> units)
        {units_ = units;}

     protected:
      unsigned int frequency_;
      Attribute adjust_;
      shared_ptr<RandomNumberGenerator> rng_;
      shared_ptr<ReducedUnitsSet> units_;
    };

    typedef shared_ptr<MonteCarloMove> MonteCarloMovePtr_;
    typedef std::vector<MonteCarloMovePtr_> MonteCarloMoves;
    typedef MonteCarloMoves::const_iterator 
      MonteCarloMovesConstIterator;
    typedef MonteCarloMoves::iterator MonteCarloMovesIterator;

    class RotateMoonMove: public MonteCarloMove{
     public:
      explicit RotateMoonMove(
        const Type planet_type,
        const Type moon_type,
        unsigned int frequency = 1,
        Attribute adjust = 1):
        MonteCarloMove(frequency, adjust),
        planet_type_(planet_type),
        moon_type_(moon_type) {};
      explicit RotateMoonMove(
        const Type planet_type,
        const Type moon_type,
        shared_ptr<RandomNumberGenerator> rng,
        unsigned int frequency = 1,
        Attribute adjust = 1):
        MonteCarloMove(rng, frequency, adjust),
        planet_type_(planet_type),
        moon_type_(moon_type) {};
      virtual ~RotateMoonMove() {};

      virtual void
      execute(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const;

     private:
      void
      move_(
        SimulationConfiguration::SimulationConfig & config,
        const TagSequenceConstIterator planet,
        const ParticleTag moon) const;

      Type planet_type_;
      Type moon_type_;
    };

    class RandomDisplacementMove: public MonteCarloMove{
     public:
      explicit RandomDisplacementMove(
        unsigned int frequency = 1,
        Attribute adjust = 1):
        MonteCarloMove(frequency, adjust) {};
      explicit RandomDisplacementMove(
        shared_ptr<RandomNumberGenerator> rng,
        unsigned int frequency = 1,
        Attribute adjust = 1):
        MonteCarloMove(rng, frequency, adjust) {};
      virtual ~RandomDisplacementMove() {};
      
      virtual void
      execute(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const;

      virtual TagSequenceConstIterator
      affected(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        TagSequenceConstIterator & first_affected) const;
    };

    class RotateBeadsMove: public MonteCarloMove{
     // Assumes, that we are operating with chains of even size.
     public:
      explicit RotateBeadsMove(
        unsigned int frequency = 1,
        Attribute adjust = 1):
        MonteCarloMove(frequency, adjust) {};
      explicit RotateBeadsMove(
        shared_ptr<RandomNumberGenerator> rng,
        unsigned int frequency = 1,
        Attribute adjust = 1):
        MonteCarloMove(rng, frequency, adjust) {};
      virtual ~RotateBeadsMove() {};
      
      virtual void
      execute(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const;

      virtual TagSequenceConstIterator
      affected(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        TagSequenceConstIterator & result) const;
    };

    class ChangeBondLengthMove: public MonteCarloMove{
     // Assumes, that we are operating with chains of even size.
     public:
      explicit ChangeBondLengthMove(
        unsigned int frequency = 1,
        Attribute adjust = 1):
        MonteCarloMove(frequency, adjust) {};
      explicit ChangeBondLengthMove(
        shared_ptr<RandomNumberGenerator> rng,
        unsigned int frequency = 1,
        Attribute adjust = 1):
        MonteCarloMove(rng, frequency, adjust) {};
      virtual ~ChangeBondLengthMove() {};
      
      virtual void
      execute(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const;

      virtual TagSequenceConstIterator
      affected(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        TagSequenceConstIterator picked,
        TagSequenceConstIterator & first_affected) const;
    };

    class ChangeBondLengthBiasedMove: public MonteCarloMove{
     // Assumes, that we are operating with chains of even size.
     public:
      explicit ChangeBondLengthBiasedMove(
        unsigned int frequency = 1,
        Attribute adjust = 1):
        MonteCarloMove(frequency, adjust) {};
      explicit ChangeBondLengthBiasedMove(
        shared_ptr<RandomNumberGenerator> rng,
        unsigned int frequency = 1,
        Attribute adjust = 1):
        MonteCarloMove(rng, frequency, adjust) {};
      virtual ~ChangeBondLengthBiasedMove() {};
      
      virtual void
      execute(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const;

      virtual TagSequenceConstIterator
      affected(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        TagSequenceConstIterator picked,
        TagSequenceConstIterator & first_affected) const;

      void
      set_parameters(
        Lengths & length_cd)
      { length_cd_ = length_cd;}

      private:
        Lengths length_cd_;
    };

    class TranslateJiggleMove: public MonteCarloMove{
     public:
      explicit TranslateJiggleMove(
       unsigned int frequency = 1,
       Attribute adjust = 1):
       MonteCarloMove(frequency, adjust),
       do_reverse_(true),
       reverse_(false),
       section_(false)
       {};
      explicit TranslateJiggleMove(
       shared_ptr<RandomNumberGenerator> rng,
       unsigned int frequency = 1,
       Attribute adjust = 1):
       MonteCarloMove(rng, frequency, adjust),
       do_reverse_(true),
       reverse_(false),
       section_(false)
       {};
      virtual ~TranslateJiggleMove() {};

      virtual void
      execute(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const;

      virtual TagSequenceConstIterator
      affected(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        TagSequenceConstIterator & result) const;

      bool
      do_reverse() const
      { return do_reverse_;}

      bool
      set_do_reverse(
        bool reverse)
      { return do_reverse_ = reverse;}

      bool
      section() const
      { return section_;}

      bool
      set_section(
        bool section)
      { return section_ = section;}

     private:
      bool do_reverse_;
      mutable bool reverse_;
      bool section_;

      void
      jiggle_(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator picked,
        TagSequenceConstIterator last,
        const Attribute adjust) const;
    };

    class TranslateJiggleBiasedMove: public MonteCarloMove{
     public:
      explicit TranslateJiggleBiasedMove(
       unsigned int frequency = 1,
       Attribute adjust = 1):
       MonteCarloMove(frequency, adjust),
       do_reverse_(true),
       reverse_(false),
       section_(false)
       {};
      explicit TranslateJiggleBiasedMove(
       shared_ptr<RandomNumberGenerator> rng,
       unsigned int frequency = 1,
       Attribute adjust = 1):
       MonteCarloMove(rng, frequency, adjust),
       do_reverse_(true),
       reverse_(false),
       section_(false)
       {};
      virtual ~TranslateJiggleBiasedMove() {};

      virtual void
      execute(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const;

      virtual TagSequenceConstIterator
      affected(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        TagSequenceConstIterator & result) const;

      void
      set_parameters(
        Lengths & length_cd,
        Angles & angle_small_cd,
        Angles & angle_big_cd,
        Angles & torsion_cd,
        Area & bead_size_squared)
      { length_cd_ = length_cd;
        angle_small_cd_ = angle_small_cd;
        angle_big_cd_ = angle_big_cd;
        torsion_cd_ = torsion_cd;
        bead_size_squared_ = bead_size_squared;}

      bool
      do_reverse() const
      { return do_reverse_;}

      bool
      set_do_reverse(
        bool reverse)
      { return do_reverse_ = reverse;}

      bool
      section() const
      { return section_;}

      bool
      set_section(
        bool section)
      { return section_ = section;}

     private:
      Lengths length_cd_;
      Angles angle_small_cd_;
      Angles angle_big_cd_;
      Angles torsion_cd_;
      Area bead_size_squared_;

      bool do_reverse_;
      mutable bool reverse_;
      bool section_;

      void
      jiggle_(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator picked,
        TagSequenceConstIterator last,
        const Attribute adjust) const;
    };

    class TranslateMoleculeMove: public MonteCarloMove{
     public:
      explicit TranslateMoleculeMove(
       unsigned int frequency = 1,
       Attribute adjust = 1):
       MonteCarloMove(frequency, adjust) {};
      explicit TranslateMoleculeMove(
       shared_ptr<RandomNumberGenerator> rng,
       unsigned int frequency = 1,
       Attribute adjust = 1):
       MonteCarloMove(rng, frequency, adjust) {};
      virtual ~TranslateMoleculeMove() {};

      virtual void
      execute(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const;
     private:
      void
      translate_(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const Attribute adjust) const;
    };

    class CrankShaftMove: public MonteCarloMove{
     public:
      explicit CrankShaftMove(
       unsigned int frequency = 1,
       Attribute adjust = 1):
       MonteCarloMove(frequency, adjust) {};
      explicit CrankShaftMove(
       shared_ptr<RandomNumberGenerator> rng,
       unsigned int frequency = 1,
       Attribute adjust = 1):
       MonteCarloMove(rng, frequency, adjust) {};
      virtual ~CrankShaftMove() {};

      virtual void
      execute(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        const Attribute adjust) const;
      private:

      virtual TagSequenceConstIterator
      affected(
        const SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last,
        const TagSequenceConstIterator picked,
        TagSequenceConstIterator & result) const;

       void
       rotate_(
        SimulationConfiguration::SimulationConfig & config,
        TagSequenceConstIterator middle) const;
    };
        
  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_MC_MOVES_H_
