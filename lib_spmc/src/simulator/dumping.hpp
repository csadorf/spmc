//=====================================================================================
//
//       Filename:  dumping.hpp
//
//    Description:  Dumping of trajectories/ snapshots.
//
//        Version:  1.0
//        Created:  09/09/13
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_DUMPING_H_
#define MDST_SIMULATOR_DUMPING_H_

#include "dumping/xyz_dumper.hpp"
//#include "dumping/xml_dumper.hpp"

namespace mdst{

  namespace simulator{


  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_DUMPING_H_
