// =====================================================================================
//
//       Filename:  mc_sampler.cpp
//
//    Description:  Uses session information to explore phase space.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "mc_sampler.hpp"
#include "session_modules.hpp"
#include "random_number_generation.hpp"
#include "energy_calculation.hpp"

namespace mdst{

  namespace simulator{

    using SimulationConfiguration::SimulationConfig;

    void
    MonteCarloSampler::run(
        Session & session,
        unsigned int num_steps,
        TagSequenceConstIterator sample_sequence_begin,
        TagSequenceConstIterator sample_sequence_end)
    {
      rng_ = session.random_number_generator();
      if (session.mc_moves().size() == 0)
      {
        throw RuntimeWarning("No moves defined!");
      }
      session.runtime_data()->reset_energy();
      session.runtime_data()->add_delta(
        calculate_potential_energy(
          * session.config(),
          session.forces(),
          session.pairforce_map(),
          session.use_neighbor_lists()));
      start_benchmark();
      sample_(
        session, num_steps,
        sample_sequence_begin, sample_sequence_end,
        session.runtime_data()->adjust);
    }

    MonteCarloMoves
    determine_active_moves(
      const MonteCarloMoves & moves,
      const unsigned int cycle)
    {
      MonteCarloMoves active_moves(moves.size());
      size_t i = 0;
      for(MonteCarloMovesConstIterator iter = moves.begin();
          iter != moves.end(); ++iter){
        MonteCarloMove & move = **iter;
        if (cycle % move.frequency() == 0){
          active_moves[++i - 1] = *iter;
        }
      }
      active_moves.resize(i);
      return active_moves;
    }

  } // namespace simulator

} // namespace mdst
