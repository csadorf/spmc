// =====================================================================================
//
//       Filename:  energy_calculation.cpp
//
//    Description:  Calculate energies.
//
//        Version:  1.0
//        Created:  11/14/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "energy_calculation.hpp"
#include "pair_calculation.hpp"
#include "session.hpp"

namespace mdst{

  namespace simulator{

    using SimulationConfiguration::SimulationConfig;

    Attribute
    calculate_potential_energy(
      const SimulationConfig & config,
      const Forces & forces)
    {
      Attribute energy = 0;
      TagSequence all(vector_from_set(config.all()));
      energy += calculate_intra_molecular_potential_energy(
        config, forces, all.begin(), all.end());
      energy += calculate_inter_molecular_potential_energy(
        config, forces, all.begin(), all.end());
      energy += calculate_field_potential_energy(
        config, forces, all.begin(), all.end());
      return energy;
    }

    Attribute
    calculate_potential_energy(
      const SimulationConfig & config,
      const Forces & forces,
      const PairforceMap & pairforce_map,
      const bool use_neighbor_lists)
    {
      Attribute energy = 0;
      TagSequence all(vector_from_set(config.all()));
      energy += calculate_intra_molecular_potential_energy(
        config, forces, all.begin(), all.end());
      if (use_neighbor_lists)
        energy += calculate_inter_molecular_potential_energy(
          config, pairforce_map, all.begin(), all.end());
      else
        energy += calculate_inter_molecular_potential_energy(
          config, forces, all.begin(), all.end());
      energy += calculate_field_potential_energy(
        config, forces, all.begin(), all.end());
      return energy;
    }

    Attribute
    calculate_intra_molecular_potential_energy(
        const SimulationConfiguration::SimulationConfig & config,
        const Forces & forces,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last)
    {
        Attribute energy = 0;
        typedef std::set<shared_ptr<TagSet> > TagSetPtrSet;
        TagSetPtrSet molecules;
        for(TagSequenceConstIterator it = first;
            it != last; ++it){
          molecules.insert(config.property.molecule_ptr(* it));
        }
        long int N = 0;
        for(TagSetPtrSet::const_iterator molecule = molecules.begin();
            molecule != molecules.end(); ++molecule){
          TagSequence s = vector_from_set(** molecule);
          N += s.size();
          energy += calculate_intra_molecular_potential_energy_molecule(
            config, forces, s.begin(), s.end());
        }
        if (N != last - first)
          throw RuntimeError("Illegal subset.");
        return energy;
    }

    Attribute
    calculate_potential_energy(
        const SimulationConfig & config,
        const Forces & forces,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last)
    {
        //return calculate_potential_energy_only_affected(
          //config, forces, first, last, first, last);

        Attribute energy = 0;
        energy += calculate_intra_molecular_potential_energy(
          config, forces, first, last);
        energy += calculate_inter_molecular_potential_energy(
          config, forces, first, last);
        energy += calculate_field_potential_energy(
          config, forces, first, last);
        return energy;
    }

    Attribute
    calculate_potential_energy_only_affected(
      const SimulationConfig & config,
      const Forces & forces,
      TagSequenceConstIterator molecule_first,
      TagSequenceConstIterator molecule_last,
      TagSequenceConstIterator affected_first,
      TagSequenceConstIterator affected_last)
    {
      Attribute energy = 0;
      energy += calculate_intra_molecular_potential_energy_molecule(
        config, forces, molecule_first, molecule_last);
      Attribute inter_energy = calculate_inter_molecular_potential_energy(
        config, forces, affected_first, affected_last);
      energy += inter_energy;
      Attribute field_energy = calculate_field_potential_energy(
        config, forces, affected_first, affected_last);
      energy += field_energy;
      return energy;
    }

    Attribute
    calculate_potential_energy_only_affected(
      const SimulationConfig & config,
      const Forces & forces,
      const PairforceMap & pairforce_map,
      TagSequenceConstIterator molecule_first,
      TagSequenceConstIterator molecule_last,
      TagSequenceConstIterator affected_first,
      TagSequenceConstIterator affected_last,
      const bool use_neighbor_lists)
    {
      Attribute energy = 0;
      energy += calculate_intra_molecular_potential_energy_molecule(
        config, forces, molecule_first, molecule_last);
      if (use_neighbor_lists)
        energy += calculate_inter_molecular_potential_energy(
          config, pairforce_map, affected_first, affected_last);
      else
        energy += calculate_inter_molecular_potential_energy(
          config, forces, affected_first, affected_last);
      energy += calculate_field_potential_energy(
        config, forces, affected_first, affected_last);
      return energy;
    }

    Attribute
    calculate_inter_molecular_potential_energy(
        const SimulationConfiguration::SimulationConfig & config,
        const PairforceMap & pairforce_map,
        TagSequenceConstIterator first,
        TagSequenceConstIterator last)
    {
      Attribute energy = 0;
      TagSet count_once;
      if (last - first < config.num_atoms())
        count_once = TagSet(first, last);
      for(PairforceMapConstIterator iter = pairforce_map.begin();
          iter != pairforce_map.end(); ++iter)
          for(ParticleTag i = 0; i < last - first; ++i){
            energy += calculate_potential_energy_with_neighbor_lists(
              config, first + i, count_once, iter->first,
              iter->second.begin(), iter->second.end());
          }
      return energy / 2;
    }


  } // namespace simulator

} // namespace mdst
