// =====================================================================================
//
//       Filename:  session_interface.hpp
//
//    Description:  Manages session data.
//
//        Version:  1.0
//        Created:  01/20/2013 07:45:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_SESSION_INTERFACE_H_
#define MDST_SIMULATOR_SESSION_INTERFACE_H_

#include "session.hpp"
#include "forces.hpp"
#include "mc_moves.hpp"
#include "exception.hpp"

namespace mdst{

  namespace simulator{

      class SessionInterface : public Session{
        friend class interface::Init;
        friend class interface::System;
        friend class interface::Dump;
        friend class interface::Forcefield;
        friend class interface::Moves;

       public:
         SessionInterface();

         // Sub modules
         interface::Init &
         init()       { return * init_;}

         interface::System &
         system()     { return * system_;}

         interface::Dump &
         dump()       { return * dump_;}

         interface::Forcefield &
         forcefield() { return * forcefield_;}

         interface::Moves &
         moves()      { return * moves_;}

       private:
        // Sub modules
        shared_ptr<interface::Init> init_;
        shared_ptr<interface::System> system_;
        shared_ptr<interface::Dump> dump_;
        shared_ptr<interface::Forcefield> forcefield_;
        shared_ptr<interface::Moves> moves_;
      };

  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_SESSION_INTERFACE_H_
