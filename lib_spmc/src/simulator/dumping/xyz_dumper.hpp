//=====================================================================================
//
//       Filename:  xyz.hpp
//
//    Description:  Simple xyz dumping of trajectories. 
//
//        Version:  1.0
//        Created:  09/09/13
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_DUMP_XYZ_H_
#define MDST_SIMULATOR_DUMP_XYZ_H_

#include "dumper.hpp"

namespace mdst{

  namespace simulator{
    
    const int DEFAULT_PRECISION = 10;

    class XyzTrajectoryDumper : public TrajectoryDumper
    {
     public:
      XyzTrajectoryDumper(
        const std::string & filename,
        unsigned int frequency = 1):
        TrajectoryDumper(filename, frequency)
     {};

     private:
      virtual void
      init_(
        Session & session);

      virtual void
      dump_(
        const Session & session,
        std::ostream & steam);

      virtual void
      close_();

    };

  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_DUMP_XYZ_H_
