//=====================================================================================
//
//       Filename:  xml_dump.hpp
//
//    Description:  Periodic xml file dumping for restart.
//
//        Version:  1.0
//        Created:  12/28/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_DUMP_XML_H_
#define MDST_SIMULATOR_DUMP_XML_H_

#include "dumper.hpp"

namespace mdst{

  namespace simulator{

    class XmlPeriodicDumper : public PeriodicDumper
    {
     public:
      XmlPeriodicDumper(
        const std::string & filename,
        unsigned int frequency = 1):
        PeriodicDumper(filename, frequency)
     {};

     private:
      virtual void
      init_(
        Session & session);

      virtual void
      dump_(
        const Session & session,
        std::ostream & steam);

      virtual void
      close_();

    };

  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_DUMP_XML_H_
