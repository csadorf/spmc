//=====================================================================================
//
//       Filename:  dumper.cpp
//
//    Description:  TrajectoryDumper class for dumping of trajectories/ snapshots.
//
//        Version:  1.0
//        Created:  12/25/13
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "dumper.hpp"
#include <session.hpp>
#include <iostream>

namespace mdst{

  namespace simulator{

    Dumper::Dumper(
      const std::string & filename,
      unsigned int frequency):
      frequency_(frequency),
      filename_(filename)
    {};
 
    TrajectoryDumper::TrajectoryDumper(
       const std::string & filename,
       unsigned int frequency):
       Dumper(filename, frequency)
    {
      fstream_.open(
       filename.c_str(),
       std::fstream::out | std::fstream::app);
    }
 
    void
    TrajectoryDumper::dump(
       const Session & session)
    {
      dump_(session, fstream_);
    }
 
    void
    TrajectoryDumper::init(
       Session & session)
    {
      init_(session);
    }
 
    void
    TrajectoryDumper::close()
    {
      close_();
    }
 
    TrajectoryDumper::~TrajectoryDumper()
    {
      fstream_.close();
    }
 
  } // namespace simulator

} // namespace mdst
