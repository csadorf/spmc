//=====================================================================================
//
//       Filename:  xml_dumper.cpp
//
//    Description:  Periodic xml file dumping for restart.
//
//        Version:  1.0
//        Created:  12/28/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "xml_dumper.hpp"
#include <session.hpp>
#include <simulation_config/parser_writer/writers.hpp>

namespace mdst{

  namespace simulator{

    using SimulationConfiguration::SimulationConfig;
    using SimulationConfiguration::ParticleView;

    void
    XmlPeriodicDumper::init_(
      Session & session)
    {};

    void
    XmlPeriodicDumper::dump_(
      const Session & session,
      std::ostream & stream)
    {
      SimulationConfig const & config = * session.config();
      using SimulationConfiguration::ParserWriter::get_writer;
      get_writer("hoomd_blue_xml")->write(config, stream, session.current_step());
    }

    void
    XmlPeriodicDumper::close_()
    {};

  } // namespace simulator

} // namespace mdst
