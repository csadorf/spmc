//=====================================================================================
//
//       Filename:  xyz.cpp
//
//    Description:  Simple xyz dumping of trajectories. 
//
//        Version:  1.0
//        Created:  09/09/13
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "xyz_dumper.hpp"
#include <session.hpp>

namespace mdst{

  namespace simulator{

    using SimulationConfiguration::SimulationConfig;
    using SimulationConfiguration::ParticleView;

    void
    XyzTrajectoryDumper::init_(
      Session & session)
    {
      if (session.current_step() == 0)
      {
        std::stringstream filename;
        filename
         << filename_ << ".xml";
        session.dump_write(filename.str(), "hoomd_blue_xml");
      }
    }

    void
    XyzTrajectoryDumper::dump_(
      const Session & session,
      std::ostream & stream)
    {
      SimulationConfig const & config = * session.config();
      const size_t N = config.num_atoms();
      stream 
        << N << std::endl
        << " frame " << session.current_step() << std::endl
      ;
      for(size_t i = 0; i < N; ++i)
      {
        ParticleView p = config.particle(i);
        const Tuple3 pos = p.remapped_position() - 0.5 * config.box();
        //const Tuple3 img = p.image();
        stream
          << p.type() << " "
          << std::setprecision(DEFAULT_PRECISION) << pos.x() << " " 
          << std::setprecision(DEFAULT_PRECISION) << pos.y() << " " 
          << std::setprecision(DEFAULT_PRECISION) << pos.z() << " "
          << std::endl
        ;
      }
    }

    void
    XyzTrajectoryDumper::close_()
    {};

  } // namespace simulator

} // namespace mdst
