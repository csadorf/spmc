//=====================================================================================
//
//       Filename:  dumper.hpp
//
//    Description:  Dumper class for dumping of trajectories/ snapshots.
//
//        Version:  1.0
//        Created:  09/09/13
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_DUMPER_H_
#define MDST_SIMULATOR_DUMPER_H_

#include <fstream>
#include <parser_writer/writers.hpp>
#include <session.hpp>

namespace mdst{

  namespace simulator{

    //class Session;

    class Dumper{
     public:

      explicit
      Dumper(
        const std::string & filename,
        unsigned int frequency = 1);

      virtual
      ~Dumper() {};

      virtual void
      init(
        Session & session)
      { init_(session);}

      virtual void
      dump(
        const Session & session) = 0;

      virtual void
      close()
      { close_();}

      unsigned int
      frequency() const
      { return frequency_;}

      void
      set_frequency(
        unsigned int value)
      { frequency_ = value;}

     protected:

      virtual void
      init_(
        Session & session) = 0;

      virtual void
      dump_(
        const Session & session,
        std::ostream & stream) = 0;

      virtual void
      close_() = 0;

      unsigned int frequency_;
      std::string filename_;

    };
    
    class TrajectoryDumper : public Dumper{
     public:

      explicit
      TrajectoryDumper(
        const std::string & filename,
        unsigned int frequency = 1);

      virtual
      ~TrajectoryDumper();

      virtual void
      init(
        Session & session);

      virtual void
      dump(
        const Session & session);

      virtual void
      close();

      unsigned int
      frequency() const
      { return frequency_;}

      void
      set_frequency(
        unsigned int value)
      { frequency_ = value;}

     protected:
      std::fstream fstream_;

    };

    template<class Writer>
    class PeriodicDumper : public Dumper{
     public:
      explicit
      PeriodicDumper(
        const std::string & filename,
        unsigned int frequency = 100):
        Dumper(filename, frequency)
      {};

      virtual
      ~PeriodicDumper() {};

      virtual void
      dump(
        const Session & session)
      {
        std::string filename = filename_;
        std::stringstream period;
        period << session.current_step();
        const size_t index_period_char = filename_.find(std::string("#"));
        if (index_period_char != std::string::npos)
          filename.replace(
           index_period_char, 1, period.str());
        std::fstream file;
        file.open(filename.c_str(), std::fstream::out);
        dump_(session, file);
      }

     protected:

      std::fstream fstream_;
      Writer writer_;

      void
      init_(
        Session & session) {};

      void
      dump_(
        const Session & session,
        std::ostream & stream)
      {
        writer_.write(* session.config(), stream, session.current_step());
      }

      void
      close_() {};

    };

    typedef PeriodicDumper<SimulationConfiguration::ParserWriter::XYZ> XyzPeriodicDumper;
    template class PeriodicDumper<SimulationConfiguration::ParserWriter::XYZ>;

    typedef PeriodicDumper<SimulationConfiguration::ParserWriter::HoomdBlueXml> HoomdBlueXmlPeriodicDumper;
    typedef HoomdBlueXmlPeriodicDumper XmlPeriodicDumper;
    template class PeriodicDumper<SimulationConfiguration::ParserWriter::HoomdBlueXml>;


  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_DUMPER_H_
