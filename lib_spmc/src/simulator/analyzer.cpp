//=====================================================================================
//
//       Filename:  analyzer.cpp
//
//    Description:  Analyze simulation configuration data.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "analyzer.hpp"
#include "session.hpp"
#include "session_utils.hpp"
#include "computes.hpp"
#include "energy_calculation.hpp"
#include "simulator.hpp"

namespace mdst{

  namespace simulator{

    using SimulationConfiguration::SimulationConfig;

    void
    Analyzer::init(
        Session & session)
    {
      data_.clear();
      init_(session);
    }
    
    Attribute
    Analyzer::compute(
        const Session & session)
    {
      Attribute d = compute_(session);
      data_.push_back(d);
      return d;
    }
    
    Attribute
    PotentialEnergyAnalyzer::compute_(
        const Session & session) const
    {
      return session.runtime_data()->energy_total();
    }

    template<class ForceType>
    Attribute
    SpecificPotentialEnergyAnalyzer<ForceType>::compute_(
      const Session & session) const
    {
      const SimulationConfig & config = * session.config();
      const Forces forces = session.forces();
      return calculate_specific_potential_energy<ForceType>(config, forces);
    }
    
    template class SpecificPotentialEnergyAnalyzer<Pairforce>;
    template class SpecificPotentialEnergyAnalyzer<IntraMolecularForce>;
    template class SpecificPotentialEnergyAnalyzer<InterMolecularForce>;

    template<class ComputeType>
    void
    ComputeAnalyzer<ComputeType>::init_(
      Session & session)
    {
      installed_compute_ = find_or_install_compute<ComputeType>(session);
    }

    template<class ComputeType>
    Attribute
    ComputeAnalyzer<ComputeType>::compute_(
      const Session & session) const
    {
      installed_compute_->evaluate(session);
      return installed_compute_->last_result_as_value_or_default();
    }

    template class ComputeAnalyzer<OsmoticPressureCompute>;

    Attribute
    AdjustAnalyzer::compute_(
      const Session & session) const
    {
      return session.runtime_data()->adjust;
    }

    Attribute
    AcceptanceRatioAnalyzer::compute_(
      const Session & session) const
    {
      return session.runtime_data()->acceptance_ratio;
    }

    Attribute
    BetaAnalyzer::compute_(
        const Session & session) const
    {
      return session.simulator()->beta();
    }

    Attribute
    VolumeAnalyzer::compute_(
        const Session & session) const
    {
      const Tuple3 box = session.config()->box();
      return box[0] * box[1] * box[2];
    }

    Attribute
    NListUpdateAnalyzer::compute_(
        const Session & session) const
    {
      return session.num_n_list_updates();
    }

  } // namespace simulator

} // namespace mdst
