// =====================================================================================
//
//       Filename:  session_dumper.cpp
//
//    Description:  Add dumper to session class with filename and id.
//
//        Version:  1.0
//        Created:  12/26/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "session.hpp"
#include "dumping.hpp"

namespace mdst{

  namespace simulator{

    void
    Session::dumper_add(
      shared_ptr<Dumper> dumper)
    {
      dumpers_.push_back(dumper);
    }

    std::ostream &
    log_(
      const std::string filename,
      const unsigned int freq,
      std::ostream & stream)
    {
      stream
        << "Dumping every " << freq << " steps to '"
        << filename << "'." << std::endl;
      return stream;
    }

    void
    Session::dumper_add_by_id(
        const std::string & filename,
        const std::string & id,
        unsigned int frequency)
    {
      bool found = false;
      typedef shared_ptr<Dumper> d_ptr;
      if (id == "xyz_trajectory"){
        dumper_add(d_ptr(new XyzTrajectoryDumper(filename, frequency)));
        found = true;
      }
      if (id == "xyz" || id == "xyz_periodic"){
        dumper_add(d_ptr(new XyzPeriodicDumper(filename, frequency)));
        found = true;
      }
      if (id == "xml" || id == "xml_periodic"){
        dumper_add(d_ptr(new XmlPeriodicDumper(filename, frequency)));
        found = true;
      }
      if (found)
        log_(filename, frequency, log(LOG_GENERAL));
      else
        throw SessionDumperNotFoundWarning(id);
    }

    void
    Session::init_dumpers_()
    {
      for(DumpersConstIterator dumper = dumpers_.begin();
          dumper != dumpers_.end(); ++dumper)
      {
        (*dumper)->init(* this);
      }
    }

    void
    Session::close_dumpers_()
    {
      for(DumpersConstIterator dumper = dumpers_.begin();
          dumper != dumpers_.end(); ++dumper)
      {
        (*dumper)->close();
      }
    }


  } // namespace simulator

} // namespace mdst
