//=====================================================================================
//
//       Filename:  potentials.hpp
//
//    Description:  All potentials.
//
//        Version:  1.0
//        Created:  12/25/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_POTENTIAL_H_
#define MDST_SIMULATOR_POTENTIAL_H_

#include "potentials/hard_sphere.hpp"
#include "potentials/field_potential.hpp"

namespace mdst{

  namespace simulator{

  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_POTENTIAL_H_
