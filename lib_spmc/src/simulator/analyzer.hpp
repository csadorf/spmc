//=====================================================================================
//
//       Filename:  analyzer.hpp
//
//    Description:  Analyzer simulation configuration data.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATOR_ANALYZER_H_
#define MDST_SIMULATOR_ANALYZER_H_

#include <simulation_config.hpp>
#include <simulation_config/config_analysis.hpp>
#include "forces.hpp"
#include "runtime_data.hpp"
#include "computes.hpp"
//#include "energy_calculation.hpp"
#include "session.hpp"

namespace mdst{

  namespace simulator{
    // Forward-declaration
    //class Session;
    class Compute;
    
    class Analyzer{
     public:
      virtual ~Analyzer() {};

      virtual std::string
      id() const = 0;

      void
      init(
        Session & session);

      Attribute
      compute(
        const Session & session);

      Attributes
      data() const
      { return data_;}

     private:

      virtual void
      init_
        (Session & session) {};
      
      virtual Attribute
      compute_(
        const Session & session) const = 0;

      Attributes data_;
    };

    typedef shared_ptr<Analyzer> AnalyzerPtr_;
    typedef std::vector<AnalyzerPtr_> Analyzers;
    typedef Analyzers::const_iterator AnalyzersConstIterator;
    typedef Analyzers::iterator AnalyzsersIterator;

    template<class A>
    class ConfigAnalyzer : public Analyzer{
     public:
      virtual ~ConfigAnalyzer() {};

      explicit
      ConfigAnalyzer(
        A a,
        const std::string id):
        a_(a),
        id_(id) {};

      virtual std::string
      id() const
      { return id_;}

      virtual Attribute
      compute_(
        const Session & session) const
      { return a_(* session.config());}

     private:
      A a_;
      std::string id_;
    };


    class PotentialEnergyAnalyzer : public Analyzer{
     public:
      virtual ~PotentialEnergyAnalyzer() {};

      virtual std::string
      id() const
        { return "potential_energy";}
     

     private:

      virtual Attribute
      compute_(
        const Session & session) const;
    };

    template<class ForceType>
    class SpecificPotentialEnergyAnalyzer : public Analyzer{
     public:
      SpecificPotentialEnergyAnalyzer(
        const std::string & id):
        id_(id) {};
      virtual ~SpecificPotentialEnergyAnalyzer() {};

      virtual std::string
      id() const
        { return "pe_" + id_;}

     private:

      virtual Attribute
      compute_(
        const Session & session) const;

     private:
      std::string id_;
    };

    template<class ComputeType>
    class ComputeAnalyzer : public Analyzer{
     public:
      ComputeAnalyzer(
        const std::string & id):
        id_(id) {};
      virtual ~ComputeAnalyzer() {};

      virtual std::string
      id() const
        { return id_;}

     private:

      virtual Attribute
      compute_(
        const Session & session) const;

      virtual void
      init_(
        Session & session);
           
     private:
      std::string id_;
      shared_ptr<ComputeType> installed_compute_;
    };

    //template class ConfigAnalyzerCompute<analysis::RadiusOfGyrationAnalyzer>;
    template class ComputeAnalyzer<ConfigAnalyzerCompute<analysis::RadiusOfGyrationAnalyzer> >;

    class BetaAnalyzer : public Analyzer{
     public:
      virtual ~BetaAnalyzer() {};

      virtual std::string
      id() const
      { return "beta";}

     private:
      virtual Attribute
      compute_(
        const Session & session) const;
    };

    class VolumeAnalyzer : public Analyzer{
     public:
      virtual ~VolumeAnalyzer() {};

      virtual std::string
      id() const
        { return "volume";}

     private:
      virtual Attribute
      compute_(
        const Session & session) const;
    };

    class AdjustAnalyzer : public Analyzer{
     public:
      virtual ~AdjustAnalyzer() {};

      virtual std::string
      id() const
       { return "adjust";}

     private:
      virtual Attribute
      compute_(
        const Session & session) const;
    };

    class AcceptanceRatioAnalyzer : public Analyzer{
     public:
      virtual ~AcceptanceRatioAnalyzer() {};

      virtual std::string
      id() const
        { return "acceptance ratio";}

     private:
      virtual Attribute
      compute_(
        const Session & session) const;
    };

    class NListUpdateAnalyzer : public Analyzer{
     public:
      virtual ~NListUpdateAnalyzer() {};

      virtual std::string
      id() const
      { return "n_list_updates";}

     private:
      virtual Attribute
      compute_(
        const Session & session) const;
    };

  } // namespace simulator

} // namespace mdst

#endif // MDST_SIMULATOR_ANALYZER_H_
