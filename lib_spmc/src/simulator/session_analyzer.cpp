// =====================================================================================
//
//       Filename:  session.cpp
//
//    Description:  Prepare all data, necessary to run a simulation.
//
//        Version:  1.0
//        Created:  01/20/2013 07:45:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "session.hpp"
#include "session_utils.hpp"
#include <simulation_config/config_analysis.hpp>
#include <analyzer.hpp>
#include <boost/python.hpp>

namespace mdst{

  namespace simulator{

    shared_ptr<Analyzer>
    Session::analyzer_add(
        shared_ptr<Analyzer> analyzer){
      analyzers_.push_back(analyzer);
      return analyzer;
    }
    
    shared_ptr<Analyzer>
    Session::analyzer_add(
        const std::string & analyzer_id){
      typedef shared_ptr<Analyzer> a_ptr;
      if (analyzer_id == "potential_energy" ||
          analyzer_id == "pe"){
        return find_or_install_analyzer<PotentialEnergyAnalyzer>(* this);
      }
      if (analyzer_id == "beta"){
        return find_or_install_analyzer<BetaAnalyzer>(* this);
      }
      if (analyzer_id == "volume"){
        return find_or_install_analyzer<VolumeAnalyzer>(* this);
      }
      if (analyzer_id == "pair_potential"){
        return find_or_install_analyzer<SpecificPotentialEnergyAnalyzer<Pairforce> >(* this, analyzer_id);
      }
      if (analyzer_id == "pe_intra"){
        return find_or_install_analyzer<SpecificPotentialEnergyAnalyzer<IntraMolecularForce> >(* this, analyzer_id);
      }
      if (analyzer_id == "pe_inter"){
        return find_or_install_analyzer<SpecificPotentialEnergyAnalyzer<InterMolecularForce> >(* this, analyzer_id);
      }
      if (analyzer_id == "adjust"){
        return find_or_install_analyzer<AdjustAnalyzer>(* this);
      }
      if (analyzer_id == "acceptance_ratio"){
        return find_or_install_analyzer<AcceptanceRatioAnalyzer>(* this);
      }
      if (analyzer_id == "osmotic_pressure"){
        return find_or_install_analyzer<ComputeAnalyzer<OsmoticPressureCompute> >(* this, analyzer_id);
      }
      if (analyzer_id == "n_list_updates"){
        return find_or_install_analyzer<NListUpdateAnalyzer>(* this);
      }
      if (analyzer_id == "radius_of_gyration"){
        typedef analysis::RadiusOfGyrationAnalyzer RG_A;
        return analyzer_add(
          a_ptr(new ConfigAnalyzer<RG_A>(RG_A(), analyzer_id)));
      }
      throw SessionAnalyzerNotFoundWarning(analyzer_id);
    }

    void
    Session::init_analyzers_()
    {
      for(AnalyzersConstIterator analyzer = analyzers_.begin();
          analyzer != analyzers_.end(); ++analyzer){
        (*analyzer)->init(* this);
      }
    }

    void
    Session::execute_analyzer_callback() const
    {
      if (analyzer_callback_)
      {
        boost::python::dict data;
        for(AnalyzersConstIterator analyzer = analyzers_.begin();
          analyzer != analyzers_.end(); ++analyzer)
        {
          data[(* analyzer)->id()] = (* analyzer)->data().back();
        }
        PyEval_InitThreads();
        PyGILState_STATE state = PyGILState_Ensure();
        boost::python::call<void>(
          analyzer_callback_, step_, analyzer_frequency_, data);
        PyGILState_Release(state);
      }
    }

  } // namespace simulator

} // namespace mdst
