/*
 * =====================================================================================
 *
 *       Filename:  config_manipulation.cpp
 *
 *    Description:  Provide functions to manipulate a SimulationConfig object.
 *
 *        Version:  1.0
 *        Created:  10/19/2012 04:56:39 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
 *   Organization:  RWTH Aachen University
 *
 * =====================================================================================
 */

#include "config_manipulation.hpp"
#include "coordinates.hpp"
#include "tuple3_arithmetic.hpp"
#include "random_number_generation.hpp"

namespace mdst{

  using SimulationConfiguration::SimulationConfig;
  using SimulationConfiguration::ParticleView;

  void
  ConfigManipulator::set_config(const shared_ptr<SimulationConfig>& config){
    config_ = config;
    analyzer_ = deprecated::ConfigAnalyzer(config_);
  }

  void
  ConfigManipulator::move(const Tuple3& new_position,
                          const Tuple3& image,
                          const TagSet& group){
    const Tuple3 delta = new_position - analyzer_.center_of_mass(group);
    for(TagSetConstIterator it = group.begin(); it != group.end(); ++it){
      ParticleView particle = config_->particle(*it);
      particle.set_position(particle.position() + delta);
      particle.set_image(image);
    }
  }

  void
  ConfigManipulator::move(const Tuple3& new_position,
                          const TagSet& group){
    move(new_position, Tuple3(0), group);
  }

  void
  ConfigManipulator::move(const Tuple3& new_position,
                          const Tuple3 image){
    move(new_position, image, config_->all());
  }

  void
  ConfigManipulator::rotate(const Tuple3& pivot,
                            const PlaneAngle& delta_theta,
                            const PlaneAngle& delta_phi,
                            const TagSet& group){
    for(TagSetConstIterator it = group.begin(); it != group.end(); ++it){
      ParticleView moon(config_->particle(*it));
      if (moon.position() == pivot)
        continue;
      moon.set_position(rotation(pivot, moon.position(), delta_theta, delta_phi));
    }
  }

  void
  ConfigManipulator::rotate(const Tuple3& pivot,
                            const Tuple3& from,
                            const Tuple3& to,
                            const TagSet& group){
    for (TagSetConstIterator it = group.begin(); it != group.end(); ++it){
      ParticleView p_view(config_->particle(*it));
      p_view.set_position(rotation(pivot,
                                   p_view.position(),
                                   pivot + from,
                                   pivot + to));
    }
  }

  void
  ConfigManipulator::rotate(const Tuple3& pivot,
                            const Tuple3& to,
                            const TagSet& group){
    rotate(pivot, analyzer_.center_of_mass(group), to, group);
  }

  void
  ConfigManipulator::rotate(const Tuple3& pivot,
                            const Tuple3& to){
    rotate(pivot, to, config_->all());
  }



  void
  ConfigManipulator::rename(const ParticleType& new_type,
                            const TagSet& group){
    for(TagSetConstIterator it = group.begin(); it != group.end(); ++it)
      config_->particle(*it).set_type(new_type);
    config_->update_bond_types();
    config_->generate_angles();
    config_->generate_dihedrals();
  }

  void
  ConfigManipulator::remap(const TagSet & group){
    for (TagSetConstIterator it = group.begin(); it != group.end(); ++it){
      ParticleView p = config_->particle(*it);
      const Tuple3 image_delta(
        int(p.position().x() / (config_->box().x() / 2)),
        int(p.position().y() / (config_->box().y() / 2)),
        int(p.position().z() / (config_->box().z() / 2)));
      p.set_image(p.image() + image_delta);
      p.set_position(
        p.position() - element_product(config_->box(), image_delta));
    }
  }

  void
  ConfigManipulator::remap(){
    remap(config_->all());
  }

  void
  ConfigManipulator::set_temperature(
      Attribute reduced_temperature,
      unsigned int random_seed,
      const TagSet & group){
    Attribute & T = reduced_temperature;
    RandomNumberGenerator random(random_seed);
    Tuple3 p_mom(0);
    for(TagSetConstIterator it = group.begin(); it != group.end(); ++it){
      ParticleView p = config_->particle(*it);
      p.set_velocity(
        Tuple3(
          random.normal_number() * sqrt(T / p.mass()),
          random.normal_number() * sqrt(T / p.mass()),
          random.normal_number() * sqrt(T / p.mass())
          )
        );
      p_mom += p.mass() * p.velocity();
    }

    // Compute average momentum
    p_mom = p_mom / config_->num_atoms();

    // Subtract average momentum from each particle
    for(TagSetConstIterator it = group.begin(); it != group.end(); ++it){
      ParticleView p = config_->particle(*it);
      p.set_velocity(
        p.velocity() - p_mom / p.mass());
    }
  }

  void
  ConfigManipulator::set_temperature(Attribute reduced_temperature,
                                     unsigned int random_seed){
    set_temperature(
      reduced_temperature,
      random_seed,
      config_->all());
  }

} // namespace mdst
