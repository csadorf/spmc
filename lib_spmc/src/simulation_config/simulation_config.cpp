/*
 * =====================================================================================
 *
 *       Filename:  simulation_config.cpp
 *
 *    Description:  Interace to access and manipulate a simulation configuration.
 *
 *        Version:  1.0
 *        Created:  10/19/2012 04:54:33 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
 *   Organization:  RWTH Aachen University
 *
 * =====================================================================================
 */

#include "simulation_config.hpp"
#include "cell_grid.hpp"
#include "coordinates.hpp"
#include "misc.hpp"
#include <utility/tuple3_arithmetic.hpp>

#include <sstream>
#include <boost/unordered_set.hpp>
#include <exception>

namespace mdst{

  namespace SimulationConfiguration{

    typedef std::map<ParticleTag, ParticleTag> ParticleTagMap;
    typedef ParticleTagMap::const_iterator ParticleTagMapConstIterator;

    bool
    SimulationConfig::check() const
    {
      return check_internal_();
    }

    bool
    SimulationConfig::check_internal_() const
    {
#if defined(DEBUG) || defined(CHECK_POSITIONS)
      for(unsigned int i = 0; i < positions_->size(); ++i)
      {
        TuplesConstIterator it = positions_->begin() + i;
        if(max(*it) > 1){
          abort();
          std::stringstream msg;
          msg
            << "Particle " << i << " out of bounds: " << *it
            ;
          throw DataCorruptedError(msg.str());
        }
        if(*it != *it){
          abort();
          std::stringstream msg;
          msg 
            << "Particle " << i << " has a corrupted position: " << *it
            ;
          throw DataCorruptedError(msg.str());
        }
      }
#endif
      return true;
    }

    Bond
    reversed_bond(const Bond & bond){
      return Bond(bond.second, bond.first);
    }

    TagSet
    tag_set_from_walk(const TagVector& walk){
      TagSet result;
      for(TagVectorConstIterator it = walk.begin(); it != walk.end(); ++it)
        result.insert(*it);
      return result;
    }

    ParticleTagMap
    generate_subset_index(const TagSet& subset){
      ParticleTagMap result;
      int index = -1;
      for(TagSetConstIterator it = subset.begin(); it != subset.end(); ++it)
        result[*it] = ++index;
      return result;
    }

    shared_ptr<TagSet>
    shift_molecule_(
      const TagSet & molecule,
      const size_t offset)
    {
      shared_ptr<TagSet> shifted = shared_ptr<TagSet>(new TagSet());
      for(TagSetConstIterator it = molecule.begin();
          it != molecule.end(); ++it)
        shifted->insert(*it + offset);
      return shifted;
    }

    SimulationConfig::SimulationConfig(const Parameters& parameters):
      property(PropertyView(this)),
      parameters_(parameters),
      dimension_(3),
      box_(0, 0, 0),
      map_types_to_int_types_(),
      n_types_(0),
      types_(new Types()),
      int_types_(new std::vector<int>()),
      masses_(new Attributes()),
      charges_(new Attributes()),
      positions_(new Tuples()),
      velocities_(new Tuples()),
      accelerations_(new Tuples()),
      images_(new Tuples()),
      bond_types_(new Types()),
      angle_types_(new Types()),
      dihedral_types_(new Types()),
      bonds_(new TagMap()),
      bonds_sorted_(new TagVectorMap()),
      angles_(new TagMap()),
      angles_sorted_(new TagVectorMap()),
      dihedrals_(new TagMap()),
      dihedrals_sorted_(new TagVectorMap()),
      particle_map_(new TagMap()),
      reversed_bonds_(new TagMap()),
      reversed_angles_(new TagMap()),
      reversed_dihedrals_(new TagMap()),
      molecule_map_(TagSetMap()),
      molecules_(TagSetPtrSet()),
      particles_locked_(new TagSet()),
      particles_locked_silent_(false),
      neighbor_list_max_displacement_(0),
      n_list_update_counter_(0),
      developer_mode_(false)
      {};

    SimulationConfig::SimulationConfig(const SimulationConfig & rhs,
                                       const TagSet& subset):
      property(PropertyView(this)),
      parameters_(rhs.parameters_),
      dimension_(rhs.dimension_),
      box_(rhs.box_),
      map_types_to_int_types_(rhs.map_types_to_int_types_),
      n_types_(rhs.n_types_),
      types_(new Types(*rhs.types_)),
      int_types_(new std::vector<int>(*rhs.int_types_)),
      masses_(new Attributes(*rhs.masses_)),
      charges_(new Attributes(*rhs.charges_)),
      positions_(new Tuples(*rhs.positions_)),
      velocities_(new Tuples(*rhs.velocities_)),
      accelerations_(new Tuples(*rhs.accelerations_)),
      images_(new Tuples(*rhs.images_)),
      bond_types_(new Types(*rhs.bond_types_)),
      angle_types_(new Types(*rhs.angle_types_)),
      dihedral_types_(new Types(*rhs.dihedral_types_)),
      bonds_(new TagMap(*rhs.bonds_)),
      bonds_sorted_(new TagVectorMap(*rhs.bonds_sorted_)),
      angles_(new TagMap(*rhs.angles_)),
      angles_sorted_(new TagVectorMap(*rhs.angles_sorted_)),
      dihedrals_(new TagMap(*rhs.dihedrals_)),
      dihedrals_sorted_(new TagVectorMap(*rhs.dihedrals_sorted_)),
      particle_map_(new TagMap(*rhs.particle_map_)),
      reversed_bonds_(new TagMap(*rhs.reversed_bonds_)),
      reversed_angles_(new TagMap(*rhs.reversed_angles_)),
      reversed_dihedrals_(new TagMap(*rhs.reversed_dihedrals_)),
      molecule_map_(rhs.molecule_map_),
      molecules_(rhs.molecules_),
      particles_locked_(new TagSet(* rhs.particles_locked_)),
      particles_locked_silent_(rhs.particles_locked_silent_),
      cell_grids_(rhs.cell_grids_),
      //cell_index_dirty_(rhs.cell_index_dirty_),
      cell_index_dirty_sparse_(rhs.cell_index_dirty_sparse_),
      cell_index_dirty_dense_(rhs.cell_index_dirty_dense_),
      cell_index_dirty_count_(rhs.cell_index_dirty_count_),
      neighbor_lists_map_(rhs.neighbor_lists_map_),
      neighbor_list_max_displacement_(rhs.neighbor_list_max_displacement_),
      n_list_update_counter_(rhs.n_list_update_counter_),
      developer_mode_(rhs.developer_mode_)
    {
      if (!subset.empty()){
        // Lock both configurations
        UniqueLock lhs_lock(mutex_, boost::defer_lock);
        UniqueLock rhs_lock(rhs.mutex_, boost::defer_lock);
        boost::lock(lhs_lock, rhs_lock);
        // Iterate bonds
        cut_bonds_(bonds_, bond_types_, 2, subset);
        cut_bonds_(angles_, angle_types_, 3, subset);
        cut_bonds_(dihedrals_, dihedral_types_, 4, subset);
        /*
        bonds_->clear();
        bond_types_->clear();
        angles_->clear();
        angle_types_->clear();
        dihedrals_->clear();
        dihedral_types_->clear();*/
        // Update particles
        int index = -1;
        for(TagSetConstIterator it = subset.begin(); it != subset.end(); ++it){
          particle(++index).clone(rhs.particle(*it));
        }
        check_internal_();
        // Delete the rest
        check_internal_();
        set_num_atoms_(subset.size());
        particles_locked_->clear();
        std::set_intersection(
          rhs.particles_locked_->begin(), rhs.particles_locked_->end(),
          subset.begin(), subset.end(), 
          std::inserter(* particles_locked_, particles_locked_->begin()));
        check_internal_();
      }
      check_internal_();
    }
    
    SimulationConfig::~SimulationConfig()
    {
    }

    void
    SimulationConfig::cut_bonds_(shared_ptr<TagMap>& bonds,
                                 shared_ptr<Types>& bond_types,
                                 const int N,
                                 const TagSet& subset){
      // copy old lists
      shared_ptr<TagMap> new_bonds(new TagMap(*bonds));
      shared_ptr<Types> new_bond_types(new Types(*bond_types));
      // remap old particles to subset
      const ParticleTagMap particle_map = generate_subset_index(subset);
      TagVectorMap pseudo_map;

      // Iterate bonds
      for(NBondTag tag = 0; tag < (NBondTag)bond_types->size(); ++tag){
        NBondView n_bond(this, tag, *bonds, *bond_types, pseudo_map, N);
        const TagSet bond_tags = n_bond.tags();
        TagSet bond_tags_copy = bond_tags;
        for(TagSetConstIterator it = subset.begin(); it != subset.end(); ++it)
          bond_tags_copy.erase(*it);
        if (bond_tags.empty()){ // the bond is part of the subset
          // Append new bond
          new_bond_types->push_back(n_bond.type());
          const int index = new_bond_types->size() - 1;
          for(TagSetConstIterator it = bond_tags.begin(); it != bond_tags.end(); ++it){
            new_bonds->insert(std::make_pair(index, particle_map.find(*it)->second));
          }
        }
      }
      std::swap(bonds, new_bonds);
      std::swap(bond_types, new_bond_types);
      check_internal_();
    }

    void
    SimulationConfig::cut_bonds_(shared_ptr<TagMap>& bonds,
                                 shared_ptr<Types>& bond_types,
                                 const int N,
                                 const ParticleTag& max_tag){
      shared_ptr<Types> new_bond_types(new Types());
      shared_ptr<TagMap> new_bonds(new TagMap());
      TagVectorMap pseudo_map;
      const int n = bond_types->size();
      for(NBondTag bond_tag = 0; bond_tag < n; ++bond_tag){
        NBondView n_bond(this, bond_tag, *bonds, *bond_types, pseudo_map, N);
        const TagSet tags = n_bond.tags();
        bool copy = true;
        for(TagSetConstIterator it = tags.begin(); it != tags.end(); ++it){
          if (*it > max_tag){
            copy = false;
            break;
          }
        }
        if (copy){
          new_bond_types->push_back(n_bond.type());
          const int index = new_bond_types->size() - 1;
          for(TagSetConstIterator it = tags.begin(); it != tags.end(); ++it){
            new_bonds->insert(std::make_pair(index, *it));
          }
        }
      }
      std::swap(bonds, new_bonds);
      std::swap(bond_types, new_bond_types);
      check_internal_();
    }

    void
    SimulationConfig::update_particle_map_() const{
      if (bonds_->size() == 0)
        return;
      TagMap & map = *particle_map_;
      map.clear();
      for(TagMapConstIterator it_a = bonds_->begin(); it_a != bonds_->end(); ++it_a){
        TagMapConstIterator it_b = it_a;
        // This condition should never be true by design, but we prevent a segment fault here.
        if (++it_b == bonds_->end()) break;
        map.insert(Bond(it_a->second, it_b->second));
        map.insert(Bond(it_b->second, it_a->second));
        // We always jump twice
        ++it_a;
      }
    }

    void
    SimulationConfig::update_reversed_n_bonds_(const unsigned int n) const{
      const TagMap& n_bonds = n_bond_map_(n);
      TagMap& reversed_n_bonds = const_cast<TagMap&>(reversed_n_bonds_(n));
      reversed_n_bonds.clear();
      for(TagMapConstIterator it = n_bonds.begin(); it != n_bonds.end(); ++it)
        reversed_n_bonds.insert(std::make_pair(it->second, it->first));
    }

    void
    SimulationConfig::update_molecule_map_() const{
      molecule_map_.clear();
      molecules_.clear();
      TagSet skip;
      for(unsigned int t = 0; t < num_atoms(); ++t)
        if (skip.count(t) == 0)
          map_molecule_(t, skip);
    }

    void
    SimulationConfig::update_internal_() const{
      indirect_bond_map_1_.clear();
      indirect_bond_map_2_.clear();
      indirect_bond_map_3_.clear();
      update_particle_map_();
      update_molecule_map_();
      update_sorted_n_bonds_(
        * bond_types_, * bonds_, * bonds_sorted_);
      update_sorted_n_bonds_(
        * angle_types_, * angles_, * angles_sorted_);
      update_sorted_n_bonds_(
        * dihedral_types_, * dihedrals_, * dihedrals_sorted_);
      update_reversed_n_bonds_(3);
      update_reversed_n_bonds_(4);
      cell_grids_.clear();
      //cell_index_dirty_.clear();
      int num = num_atoms();
      cell_index_dirty_dense_.resize(num);
      cell_index_dirty_count_ = 0;
      std::fill(cell_index_dirty_sparse_.begin(), cell_index_dirty_sparse_.end(), -1);
      cell_index_dirty_sparse_.resize(num, -1);
      neighbor_lists_map_.clear();
      neighbor_list_max_displacement_ = 0;
      is_linear_flag_ = is_linear_();
      for (int i = 0; i < types_->size(); i++){
        int_types_->operator[](i) = map_type_to_int_type_(types_->operator[](i));
      }
      check_internal_();
    }

    void
    SimulationConfig::set_developer_mode(
      bool mode){
      developer_mode_ = mode;
      if (not mode){
        update_internal_();
        check();
      }
    }

    void
    SimulationConfig::update_internal() const{
      if (developer_mode_){
        update_internal_();
      } else {
        throw RuntimeError("Requires admin mode.");
      }
    }

    int
    SimulationConfig::calculate_num_bonds_(const TagMap& n_bonds) const{
      int num_of_n_bonds = 0;
      for(TagMapConstIterator it = n_bonds.begin(); it != n_bonds.end();){
        ++num_of_n_bonds;
        it = n_bonds.upper_bound(it->first);
      }
      return num_of_n_bonds;
    }

    void
    SimulationConfig::set_num_atoms(int num_atoms)
    {
      //std::cerr << "set_num_atoms" << std::endl;
      LockGuard _(mutex_);
      set_num_atoms_(num_atoms);
    }

    void
    SimulationConfig::set_num_atoms_( int num_atoms){
      // Modifying the number of atoms is critical, because all
      // data fields have to be resized and updated.
      // The procedure involves the copying of all fields, to 
      // guarantee strong(!) exception safety.
      int old_num_atoms = this->num_atoms();
      if (old_num_atoms == num_atoms)
        // No change -> return!
        return;

      // Copy old lists
      shared_ptr<Types> new_types(new Types(*types_));
      shared_ptr<std::vector<int> > new_int_types(new std::vector<int>(*int_types_));
      shared_ptr<Attributes> new_masses(new Attributes(*masses_));
      shared_ptr<Attributes> new_charges(new Attributes(*charges_));
      shared_ptr<Tuples> new_positions(new Tuples(*positions_));
      shared_ptr<Tuples> new_velocities(new Tuples(*velocities_));
      shared_ptr<Tuples> new_accelerations(new Tuples(*accelerations_)); 
      shared_ptr<Tuples> new_images(new Tuples(*images_));
      shared_ptr<Types> new_bond_types(new Types(*bond_types_));
      shared_ptr<Types> new_angle_types(new Types(*angle_types_));
      shared_ptr<Types> new_dihedral_types(new Types(*dihedral_types_));
      shared_ptr<TagMap> new_bonds(new TagMap(*bonds_));
      shared_ptr<TagMap> new_angles(new TagMap(*angles_));
      shared_ptr<TagMap> new_dihedrals(new TagMap(*dihedrals_));

      // Try to resize
      new_types->resize(num_atoms);
      new_int_types->resize(num_atoms);
      new_masses->resize(num_atoms);
      new_charges->resize(num_atoms);
      new_positions->resize(num_atoms);
      new_velocities->resize(num_atoms);
      new_accelerations->resize(num_atoms);
      new_images->resize(num_atoms);

      Type uninitialized_type = "no_type";
      int uninitialized_int_type = map_type_to_int_type_(uninitialized_type);
      // Initialize new particles
      for(int i = old_num_atoms; i < num_atoms; ++i){
        new_types->operator[](i) = uninitialized_type;
        new_int_types->operator[](i) = uninitialized_int_type;
        new_positions->operator[](i) = Tuple3(0);
        new_images->operator[](i) = Tuple3(0);
      }

      // Update the bonds, angles and dihedrals
      const ParticleTag max_tag = new_types->size() - 1;
      cut_bonds_(new_bonds, new_bond_types, 2, max_tag);
      cut_bonds_(new_angles, new_angle_types, 3, max_tag);
      cut_bonds_(new_dihedrals, new_dihedral_types, 4, max_tag);

      // From here we are exception safe
      using std::swap;
      swap(types_, new_types);
      swap(int_types_, new_int_types);
      swap(masses_, new_masses);
      swap(charges_, new_charges);
      swap(positions_, new_positions);
      swap(velocities_, new_velocities);
      swap(accelerations_, new_accelerations);
      swap(images_, new_images);
      swap(bond_types_, new_bond_types);
      swap(angle_types_, new_angle_types);
      swap(dihedral_types_, new_dihedral_types);
      swap(bonds_, new_bonds);
      swap(angles_, new_angles);
      swap(dihedrals_, new_dihedrals);
      update_internal_();
      // Note: The old lists will be deleted automatically, once we leave this scope.
    }

    TagSet
    SimulationConfig::append(const SimulationConfig & rhs, bool update_internal){
      if (!(update_internal or developer_mode_)){
        throw RuntimeError("Requires admin mode.");
      }

      //std::cerr << "append" << std::endl;
      LockGuard _(mutex_);
      // rhs represents other_config
      const size_t offset = num_atoms();

      // Copy old lists
      shared_ptr<Types> new_types(new Types(*types_));
      shared_ptr<std::vector<int> > new_int_types(new std::vector<int>(*int_types_));
      shared_ptr<Attributes> new_masses(new Attributes(*masses_));
      shared_ptr<Attributes> new_charges(new Attributes(*charges_));
      shared_ptr<Tuples> new_positions(new Tuples(*positions_));
      shared_ptr<Tuples> new_velocities(new Tuples(*velocities_));
      shared_ptr<Tuples> new_accelerations(new Tuples(*accelerations_));
      shared_ptr<Tuples> new_images(new Tuples(*images_));
      shared_ptr<Types> new_bond_types(new Types(*bond_types_));
      shared_ptr<Types> new_angle_types(new Types(*angle_types_));
      shared_ptr<Types> new_dihedral_types(new Types(*dihedral_types_));
      shared_ptr<TagMap> new_bonds(new TagMap(*bonds_));
      shared_ptr<TagMap> new_angles(new TagMap(*angles_));
      shared_ptr<TagMap> new_dihedrals(new TagMap(*dihedrals_));

      // Append other_config attributes and types
      new_types->insert(new_types->end(), rhs.types_->begin(), rhs.types_->end());
      new_int_types->insert(new_int_types->end(), rhs.int_types_->begin(), rhs.int_types_->end());
      new_masses->insert(new_masses->end(),rhs.masses_->begin(), rhs.masses_->end());
      new_charges->insert(new_charges->end(), rhs.charges_->begin(), rhs.charges_->end());
      new_positions->insert(new_positions->end(), rhs.positions_->begin(), rhs.positions_->end());
      new_velocities->insert(new_velocities->end(), rhs.velocities_->begin(), rhs.velocities_->end());
      new_accelerations->insert(new_accelerations->end(), rhs.accelerations_->begin(), rhs.accelerations_->end());
      new_images->insert(new_images->end(), rhs.images_->begin(), rhs.images_->end());
      new_bond_types->insert(new_bond_types->end(), rhs.bond_types_->begin(), rhs.bond_types_->end());
      new_angle_types->insert(new_angle_types->end(), rhs.angle_types_->begin(), rhs.angle_types_->end());
      new_dihedral_types->insert(new_dihedral_types->end(), rhs.dihedral_types_->begin(), rhs.dihedral_types_->end());

      // Transform to correct int types.
      //for(unsigned int i = int_types_->size(); i < new_int_types->size(); i++){
      //  new_int_types->operator[](i) = map_type_to_int_type_(new_types->operator[](i));
      //}

      // Transform and append bonds, angles and dihedrals
      const int bond_offset = bond_types_->size();
      for(TagMapConstIterator it = rhs.bonds_->begin(); it != rhs.bonds_->end(); ++it){
        std::pair<Tag, NBondTag> new_pair(it->first + bond_offset, it->second + offset);
        new_bonds->insert(new_pair);
      }

      const int angle_offset = angle_types_->size();
      for(TagMapConstIterator it = rhs.angles_->begin(); it != rhs.angles_->end(); ++it){
        std::pair<Tag, NBondTag> new_pair(it->first + angle_offset, it->second + offset);
        new_angles->insert(new_pair);
      }

      const int dihedral_offset = dihedral_types_->size();
      for(TagMapConstIterator it = rhs.dihedrals_->begin(); it != rhs.dihedrals_->end(); ++it){
        std::pair<Tag, NBondTag> new_pair(it->first + dihedral_offset, it->second + offset);
        new_dihedrals->insert(new_pair);
      }


      // From here we are exception safe
      using std::swap;
      swap(types_, new_types);
      swap(int_types_, new_int_types);
      swap(masses_, new_masses);
      swap(charges_, new_charges);
      swap(positions_, new_positions);
      swap(velocities_, new_velocities);
      swap(accelerations_, new_accelerations);
      swap(images_, new_images);
      swap(bond_types_, new_bond_types);
      swap(angle_types_, new_angle_types);
      swap(dihedral_types_, new_dihedral_types);
      swap(bonds_, new_bonds);
      swap(angles_, new_angles);
      swap(dihedrals_, new_dihedrals);

      // Append molecule data
      const TagSetPtrSet molecules = rhs.molecules();
      for(TagSetPtrSetConstIterator it = molecules.begin();
          it != molecules.end(); ++it)
      {
        TagSet & molecule = **it;
        shared_ptr<TagSet> shifted = shared_ptr<TagSet>(new TagSet());
        molecules_.insert(shifted);
        for(TagSetConstIterator particle = molecule.begin();
            particle != molecule.end(); ++particle)
        {
          Tag new_index = *particle + offset;
          shifted->insert(new_index);
          molecule_map_[new_index] = shifted;
        }
      }

      if (update_internal){
        update_internal_();
      }

      // Return set of new tags
      return generate_subset(num_atoms() - offset, offset);
    }

    // Particle Selection
    TagSet
    SimulationConfig::group() const{
      return generate_subset(num_atoms());
    }

    TagSet
    SimulationConfig::group(const ParticleType& type) const{
      TagSet result;
      //std::cerr << "group" << std::endl;
      SharedLock _(mutex_);
      for(unsigned int i = 0; i < num_atoms(); ++i)
        if (particle(i).type() == type)
          result.insert(i);
      return result;
    }

    TagSet
    SimulationConfig::group(const ParticleTag& lower_bound,
                            const ParticleTag& upper_bound) const{
      TagSet result;
      for(int i = 0; i < static_cast<int>(num_atoms()); ++i)
        if (i >= lower_bound && i < upper_bound)
          result.insert(i);
      return result;
    }

    TagSet
    SimulationConfig::group(const Tuple3& lower_bound,
                            const Tuple3& upper_bound) const{
      TagSet result;
      for(unsigned int i = 0; i < num_atoms(); ++i){
        const Tuple3 position = particle(i).position();
        if (position >= lower_bound && position < upper_bound)
          result.insert(i);
      }
      return result;
    }

    TagVector
    SimulationConfig::sort_bond_tags_(const TagSet& tags) const{
      // Generate result vector
      ParticleTag next = *property.find_ends(tags).begin();
      TagVector result;
      TagSet result_check;
      for(unsigned int i = 0; i < tags.size(); ++i){
        result_check.insert(next);
        result.push_back(next);
        // Find next
        const TagSet neighbours(property.neighbourhood(next, 1));
        for(TagSetConstIterator it = neighbours.begin(); it != neighbours.end(); ++it){
          if(tags.count(*it) > 0 && result_check.count(*it) == 0){
            next = *it;
            break;
          }
        }
      }
      return result;
    }

    void
    SimulationConfig::update_sorted_n_bonds_(
      const Types & types,
      const TagMap & map,
      TagVectorMap & sorted_n_bonds) const
    {
      for(size_t i = 0; i < types.size(); ++i)
      {
        TagMapConstIterator first = map.lower_bound(i);
        TagMapConstIterator last = map.upper_bound(i);
        TagSet tags;
        for(TagMapConstIterator it = first; it != last; ++it)
          tags.insert(it->second);
        sorted_n_bonds.insert(std::make_pair(i, sort_bond_tags_(tags)));
      }
    }

    unsigned int
    SimulationConfig::num_atom_types() const{
      TypeSet set;
      for(TypesConstIterator it = types_->begin();
          it != types_->end(); ++it){
        set.insert(*it);
      }
      return set.size();
    }
   
    TagSet
    SimulationConfig::bonds() const{
      return generate_subset(num_bonds());
    }

    unsigned int
    SimulationConfig::num_bond_types() const{
      TypeSet set;
      for(TypesConstIterator it = bond_types_->begin();
          it != bond_types_->end(); ++it){
        set.insert(*it);
      }
      return set.size();
    }

    TagSet
    SimulationConfig::angles() const{
      return generate_subset(num_angles());
    }

    unsigned int
    SimulationConfig::num_angle_types() const{
      TypeSet set;
      for(TypesConstIterator it = angle_types_->begin();
          it != angle_types_->end(); ++it){
        set.insert(*it);
      }
      return set.size();
    }


    TagSet
    SimulationConfig::dihedrals() const{
      return generate_subset(num_dihedrals());
    }

    unsigned int
    SimulationConfig::num_dihedral_types() const{
      TypeSet set;
      for(TypesConstIterator it = dihedral_types_->begin();
          it != dihedral_types_->end(); ++it){
        set.insert(*it);
      }
      return set.size();
    }


    TagSetSet
    SimulationConfig::generate_n_bond_set_(unsigned int n) const{
      TagSetSet result;
      // Iterate each particle
      for(unsigned int p = 0; p < num_atoms(); ++p){
        const Walks paths = property.paths(p, n - 1);
        // Iterate paths
        for(WalksConstIterator path = paths.begin(); path != paths.end(); ++path){
          TagSet set(tag_set_from_walk(*path));
          if(set.size() == n)
            result.insert(set);
        }
      }
      return result;
    }

    TagSetSet
    SimulationConfig::generate_dihedral_set_() const{
      TagSetSet result;
      TagSetSet blacklist;
      for (unsigned int p = 0; p < num_atoms(); ++p){
        const Walks paths = property.paths(p, 3);
        Walks selected_paths;
        for(WalksConstIterator path = paths.begin(); path != paths.end(); ++path){
          TagSet set(tag_set_from_walk(*path));
          if (set.size() == 4){
            TagSet spine(path->begin(), path->end());
            spine.erase(path->at(0));
            spine.erase(path->at(3));
            if (blacklist.count(spine) > 0)
              continue;
            blacklist.insert(spine);
//            TagSet spine1(path->begin(), path->end());
//            TagSet spine2(spine1);
//            spine1.erase(path->at(0));
//            spine2.erase(path->at(3));
//            if (blacklist.count(spine1) > 0 || blacklist.count(spine2) > 0)
//              continue;
//            blacklist.insert(spine1);
//            blacklist.insert(spine2);
            result.insert(tag_set_from_walk(*path));        
          }
        }
      }
      return result;
    }

    void
    SimulationConfig::generate_n_bonds_(shared_ptr<Types>& types,
                                        shared_ptr<TagMap>& tag_map,
                                        const int n){
      shared_ptr<Types> new_types(new Types());
      shared_ptr<TagMap> new_tag_map(new TagMap());

      TagSetSet sets;
      if (n == 4)
        sets = generate_dihedral_set_();
      else
        sets = generate_n_bond_set_(n);

      for(TagSetSet::const_iterator it = sets.begin(); it != sets.end(); ++it){
        const NBondType type =
            generate_type_name(types_from_tags_(sort_bond_tags_(*it)));
        new_types->push_back(type);
        const int index = new_types->size() - 1;
        for(TagSetConstIterator p = it->begin(); p != it->end(); ++p)
          new_tag_map->insert(std::make_pair(index, *p));
      }
      std::swap(types, new_types);
      std::swap(tag_map, new_tag_map);
      update_internal_();
    }

    void
    SimulationConfig::update_bond_types(){
      //std::cerr << "update bond types " << std::endl;
      LockGuard _(mutex_);
      for(unsigned int i = 0; i < num_bonds(); ++i){
        BondView bond_ = bond(i);
        const BondType new_type = 
          generate_type_name(types_from_tags_(bond_.sorted_tags()));
        bond_.set_type(new_type);
      }
    }

    void
    SimulationConfig::bond_all_particles(){
      //std::cerr << "bond all particles" << std::endl;
      LockGuard _(mutex_);
      if (all().size() > 1){
        // Create new lists
        shared_ptr<Types> new_bond_types(new Types());
        shared_ptr<TagMap> new_bonds(new TagMap());

        TagSet all_particles = all();
        TagSetConstIterator it_b = all_particles.begin();
        TagSetConstIterator it_a = it_b++;
        while (it_b != all_particles.end()){
          const Tag & a = *it_a;
          const Tag & b = *it_b;
          Types types(2);
          types[0] = particle(a).type();
          types[1] = particle(b).type();
          new_bond_types->push_back(generate_type_name(types));
          const int index = new_bond_types->size() - 1;
          new_bonds->insert(std::make_pair(index, a));
          new_bonds->insert(std::make_pair(index, b));
          ++it_a;
          ++it_b;
        }

        // Exception safe
        std::swap(bond_types_, new_bond_types);
        std::swap(bonds_, new_bonds);

        // Update molecule map
        molecule_map_.clear();
        shared_ptr<TagSet> mol = shared_ptr<TagSet>(new TagSet(all()));
        molecules_.clear();
        molecules_.insert(mol);
        for(TagSetConstIterator it = mol->begin(); it != mol->end(); ++it)
          molecule_map_[*it] = shared_ptr<TagSet>(mol);
        // Update internal rest
        update_particle_map_();
        update_reversed_n_bonds_(3);
        update_reversed_n_bonds_(4);
        cell_grids_.clear();
        //cell_index_dirty_.clear();
        cell_index_dirty_count_ = 0;
        std::fill(cell_index_dirty_sparse_.begin(), cell_index_dirty_sparse_.end(), -1);
      }
    }

    Types
    SimulationConfig::types_from_tags_(const TagVector& tags) const{
      Types result;
      for(TagVectorConstIterator it = tags.begin(); it != tags.end(); ++it)
        result.push_back(particle(*it).type());
      return result;
    }

    TagVector
    SimulationConfig::sort_by_angle_(const ParticleTag& planet,
                                     const TagSet& moons) const{
      typedef std::map<PlaneAngle, ParticleTag> SortedTags;
      typedef SortedTags::const_iterator STConstIterator;
      SortedTags sorted_tags;
      const Tuple3 planet_pos = particle(planet).position();
      const Tuple3 origin = planet_pos + Tuple3(1, 1, 1);
      for(TagSetConstIterator moon = moons.begin(); moon != moons.end(); ++moon){
        const Tuple3& moon_pos = particle(*moon).position();
        sorted_tags[calculate_angle(origin, planet_pos, moon_pos)] = *moon;
      }
      TagVector result;
      for(STConstIterator it = sorted_tags.begin(); it != sorted_tags.end(); ++it){
        result.push_back(it->second);
      }
      return result;
    }

    void
    SimulationConfig::set_dimension(int d){
      if (d >=0 && d < 4)
        dimension_ = d;
    }
    
    void
    SimulationConfig::stretch_box(const Tuple3 & factor){
      throw RuntimeError("Deprecated.");
      if(factor == Tuple3(1)){
        return;
      }
      if(min(box_) > 0){
        for(TuplesIterator it = positions_->begin();
            it != positions_->end(); ++it){
          *it = element_product(*it, factor);
        }
      }
      set_box(element_product(box_, factor));
      update_cell_lists();
    }

    void
    SimulationConfig::set_box( const Tuple3 & box){
      //std::cerr << "set_box " << std::endl;
      LockGuard _(mutex_);
      if (box == box_)
        return;
      if (min(box) < 0){
        std::stringstream error;
        error << "Box dimension must be equal or ";
        error << "greater than zero in all dimensions. ";
        error << "Bad argument: " << box;
        throw std::invalid_argument(error.str());
      } else {
        box_ = box;
        update_cell_lists();
      }
    }

    std::ostream &
    SimulationConfig::dump_angles(std::ostream & out) const{
      //std::cerr << "dump_angles " << std::endl;
      SharedLock _(mutex_);
      for(unsigned int i = 0; i < num_angles(); i++){
        AngleView angle_view = angle(i);
        out << angle_view.type();
        const TagVector sorted_tags(angle_view.sorted_tags());
        for(TagVectorConstIterator it = sorted_tags.begin(); it != sorted_tags.end(); ++it){
          out << " " << *it;
        }
        out << std::endl;
      }
      return out;
    }

    std::ostream &
    SimulationConfig::dump_dihedrals(std::ostream & out) const{
      //std::cerr << "dump_dihdreals" << std::endl;
      SharedLock _(mutex_);
      for(unsigned int i = 0; i < num_dihedrals(); i++){
        DihedralView dihedral_view = dihedral(i);
        out << dihedral_view.type();
        const TagVector sorted_tags(dihedral_view.sorted_tags());
        for(TagVectorConstIterator it = sorted_tags.begin(); it != sorted_tags.end(); ++it){
          out << " " << *it;
        }
        out << std::endl;
      }
      return out;
    }

     
    std::ostream &
    SimulationConfig::dump_all_(std::ostream & out, const TagMap & map, const Types & types) const{
      TagMapConstIterator iter = map.begin();
      while (iter != map.end()){
        out << types[iter->first];
        TagMapConstIterator upper_bound = map.upper_bound(iter->first);
        while (iter != upper_bound)
          out << " " << (iter++)->second;
        out << std::endl;
      }
      return out;
    }

    void
    SimulationConfig::map_molecule_(
      const Tag particle,
      TagSet & skip) const
    {
      // Calculate the set, which holds every tag of the molecule
      shared_ptr<TagSet> molecule
        = shared_ptr<TagSet>(new TagSet(property.calculate_molecule(particle)));
      molecules_.insert(molecule);
      // Associate the molecule with each particle, which is part of it.
      for(TagSetConstIterator p = molecule->begin();
          p != molecule->end(); ++p)
      {
        molecule_map_[* p] = molecule;
      }
      // Make sure, that we will not insert this molecule more than once
      skip.insert(molecule->begin(), molecule->end());
    }

    NBondTag
    SimulationConfig::determine_n_bond_(const TagSet& tags) const{
      const unsigned int n = tags.size();
      const TagMap& n_bonds = n_bond_map_(n);
      for(TagMapConstIterator it = n_bonds.begin(); it != n_bonds.end(); ){
        const TagMapConstIterator upper_bound = n_bonds.upper_bound(it->first);
        TagSet compare_set;
        for(TagMapConstIterator it2 = it; it2 != upper_bound; ++it2){
          compare_set.insert(it2->second);
        }
        if (tags == compare_set)
          return it->first;
        it = upper_bound;
      }
      return -1;
    }

    BondTag
    SimulationConfig::determine_bond(const ParticleTag& a,
                                     const ParticleTag& b,
                                     const ParticleTag& c,
                                     const ParticleTag& d) const{
      TagSet set;
      set.insert(a); set.insert(b);
      set.insert(b); set.insert(c);
      set.erase(-1);
      return determine_n_bond_(set);
    }

    TagMap&
    SimulationConfig::n_bond_map_(const unsigned int n){
      switch(n){
        case 2 : return *bonds_;
        case 3 : return *angles_;
        case 4 : return *dihedrals_;
        default:
          std::stringstream error;
          error << "determine_n_bond_(const TagSet& tags): ";
          error << "Arguments tags size must be in range of 2 to 4.";
          throw std::invalid_argument(error.str());
      }
    }

    const TagMap&
    SimulationConfig::n_bond_map_(const unsigned int n) const{
      switch(n){
        case 2 : return *bonds_;
        case 3 : return *angles_;
        case 4 : return *dihedrals_;
        default:
          std::stringstream error;
          error << "determine_n_bond_(const TagSet& tags): ";
          error << "Arguments tags size must be in range of 2 to 4.";
          throw std::invalid_argument(error.str());
      }
    }

    const TagMap&
    SimulationConfig::reversed_n_bonds_(const unsigned int n) const{
      switch(n){
        case 2 : return *reversed_bonds_;
        case 3 : return *reversed_angles_;
        case 4 : return *reversed_dihedrals_;
        default:
          std::stringstream error;
          error << "TagMap& reversed_n_bond_map_(const unsigned int): ";
          error << "Arguments tags size must be in range of 2 to 4.";
          throw std::invalid_argument(error.str());
      }
    }

    TagMap&
    SimulationConfig::reversed_n_bonds_(const unsigned int n){
      switch(n){
        case 2 : return *reversed_bonds_;
        case 3 : return *reversed_angles_;
        case 4 : return *reversed_dihedrals_;
        default:
          std::stringstream error;
          error << "TagMap& reversed_n_bond_map_(const unsigned int): ";
          error << "Arguments tags size must be in range of 2 to 4.";
          throw std::invalid_argument(error.str());
      }
    }

    const Types&
    SimulationConfig::n_bond_types_(const unsigned int n) const{
      switch(n){
        case 2 : return *bond_types_;
        case 3 : return *angle_types_;
        case 4 : return *dihedral_types_;
        default:
          std::stringstream error;
          error << "determine_n_bond_types_(const TagSet& tags): ";
          error << "Arguments tags size must be in range of 2 to 4.";
          throw std::invalid_argument(error.str());
      }

    }

    unsigned int
    SimulationConfig::num_of_n_bonds_(const unsigned int n) const{
      return n_bond_types_(n).size();
    }

    const BondView
    SimulationConfig::bond(const BondTag& tag) const{
      return BondView(this, tag);
    }

    BondView
    SimulationConfig::bond(const BondTag& tag){
      return BondView(this, tag);
    }

    void
    SimulationConfig::make_bond(const ParticleTag& a, const ParticleTag& b,
                                const BondType& type){
      LockGuard _(mutex_);
      // Copy old relevant lists
      shared_ptr<Types> new_bond_types(new Types(*bond_types_));
      shared_ptr<TagMap> new_bonds(new TagMap(*bonds_));

      // Append bond
      new_bond_types->push_back(type);
      const int index = new_bond_types->size() - 1;
      new_bonds->insert(std::make_pair(index, a));
      new_bonds->insert(std::make_pair(index, b));

      // Exception safe
      std::swap(bond_types_, new_bond_types);
      std::swap(bonds_, new_bonds);
      update_internal_();
    }

    void
    SimulationConfig::add_bond(const ParticleTag& a, const ParticleTag& b,
                                const BondType& type){
      if (not developer_mode_){
        throw RuntimeError("Requires admin mode.");
      }
      LockGuard _(mutex_);

      bond_types_->push_back(type);
      const int index = bond_types_->size() - 1;
      bonds_->insert(std::make_pair(index, a));
      bonds_->insert(std::make_pair(index, b));
    }

    // cell grids
    CellGrid &
    SimulationConfig::cell_grid(
        const CellGridsKey & key) const{
      LockGuard _(cell_grid_mutex_);
      clean_cell_index_();
      bool grid_available = cell_grids_.count(key) > 0;
      if (! grid_available){
        CellGrid cell_grid(key, box_, num_atoms());
//        std::cerr << "Calculated grid with " << product(key)
//                  << " cells." << std::endl;
        cell_grids_.insert(std::make_pair(key, cell_grid));
        calculate_cell_list_(cell_grids_[key]);
      }
      return cell_grids_[key];
    }

    void
    SimulationConfig::clean_cell_index_() const{
      for(CellGridsIterator grid_iter = cell_grids_.begin();
          grid_iter != cell_grids_.end();){
        CellGrid & grid = grid_iter->second;
        ++grid_iter;
        bool last = (grid_iter == cell_grids_.end());
        unsigned int old_cell[3];
        unsigned int new_cell[3];
        typedef CellIndexDirtyConstIterator_ CIDConstIter;
//        for(CIDConstIter it = cell_index_dirty_.cbegin();
//            it != cell_index_dirty_.cend(); ++it){
        for (int i = 0; i < cell_index_dirty_count_; i++) {
          const std::pair<int, Tuple3> *it = &cell_index_dirty_dense_[i];
          if(last) cell_index_dirty_sparse_[it->first] = -1; // delete
          calculate_cell( old_cell, it->second, box_,
          //calculate_cell( old_cell, element_product(it->second, box_), box_,
                          grid.cell_dimension);
          calculate_cell( new_cell, (*positions_)[it->first], box_,
          //calculate_cell( new_cell, element_product((*positions_)[it->first], box_), box_,
                          grid.cell_dimension);
          move(
            * grid.cell_list(),
            it->first,
            get_cell_index(grid.num_cells, old_cell),
            get_cell_index(grid.num_cells, new_cell));
        }
      }
      cell_index_dirty_count_ = 0;
      if (cell_grids_.empty())
        std::fill(cell_index_dirty_sparse_.begin(), cell_index_dirty_sparse_.end(), -1);
      //cell_index_dirty_.clear();
    }

    void
    SimulationConfig::calculate_cell_list_2_(
        CellGrid & cell_grid) const{
      CellList & cell_list = * cell_grid.cell_list();
      cell_list.clear();
      const Tuple3 & cell_dimension = cell_grid.cell_dimension;
      unsigned int cell[3];
      const TagSet all_ = all();
      for(TagSetConstIterator i = all_.begin(); i != all_.end(); ++i){
        calculate_cell(cell, (*positions_)[*i], box_, cell_dimension);
        //calculate_cell(cell, element_product((*positions_)[*i], box_), box_, cell_dimension);
        const CellIndex cell_index =
          get_cell_index(cell_grid.num_cells, cell);
        cell_list.add_to_cell(cell_index, *i);
        assert(cell_list.sanity_check());
      }
      //dump_cell_list(cell_list, std::cerr);
    }

    void
    SimulationConfig::calculate_cell_list_(
        CellGrid & cell_grid) const
    {
      CellList & cell_list = * cell_grid.cell_list();
      const Tuple3 & cell_dimension = cell_grid.cell_dimension;
      unsigned int cell[3];
      CellListMap cell_list_map;
      const TagSet all_ = all();
      for(TagSetConstIterator i = all_.begin(); i != all_.end(); ++i)
      {
        calculate_cell(cell, (*positions_)[*i], box_, cell_dimension);
        const CellIndex cell_index =
          get_cell_index(cell_grid.num_cells, cell);
        cell_list_map.insert(std::make_pair(cell_index, *i));
      }
      cell_list.construct_from_map(cell_list_map);
      assert(cell_list.sanity_check());
      //dump_cell_list(cell_list, std::cerr);
    }

    void
    SimulationConfig::update_cell_lists() const
    {
      for(CellGridsIterator it = cell_grids_.begin();
          it != cell_grids_.end(); ++it){
        calculate_cell_list_(it->second);
      }
    }
    
    int
    SimulationConfig::map_type_to_int_type_(const Type &type) const{
      std::map<Type,int>::const_iterator it = map_types_to_int_types_.find(type);
      if (it != map_types_to_int_types_.end()) {
        return it->second;
      }
      int next_int_type = n_types_++;
      map_types_to_int_types_[type] = next_int_type;
      return next_int_type;
    }

  } // namespace SimulationConfiguration

} // namespace mdst
