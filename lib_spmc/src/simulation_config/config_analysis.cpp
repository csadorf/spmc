/*
 * =====================================================================================
 *
 *       Filename:  config_analysis.cpp
 *
 *    Description:  Provide functions to analyze a SimulationConfig object.
 *                  This class considers the SimulationConfig object as constant
 *                  and will never manipulate it in any way.
 *
 *        Version:  1.0
 *        Created:  10/19/2012 04:55:22 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
 *   Organization:  RWTH Aachen University
 *
 * =====================================================================================
 */

#define _USE_MATH_DEFINES
#include <cmath>
#include<limits>
#include "config_analysis.hpp"
#include "tuple3_arithmetic.hpp"

namespace mdst{

  using SimulationConfiguration::SimulationConfig;
  using SimulationConfiguration::ParticleView;
  
  namespace analysis{

    Attribute
    mean_squared_distance(
        const SimulationConfig & config_a,
        const SimulationConfig & config_b){
      if (config_a.num_atoms() != config_b.num_atoms()){
        return -1;
      }

      Attribute squared_sum = 0;
      std::vector<char> blacklist2;
      blacklist2.resize(config_b.num_atoms(), 0);
//      TagSet blacklist2;

  //    int n = -1;
      const TagSet all_a = config_a.all();
      const TagSet all_b = config_b.all();
      for(TagSetConstIterator i = all_a.begin(); i != all_a.end(); ++i){
        Attribute min_distance = std::numeric_limits<Attribute>::max();
        Tag min_b;
        const Tuple3 remapped_position_i = config_a.particle(*i).remapped_position();
        for(TagSetConstIterator j = all_b.begin(); j != all_b.end(); ++j){
//          if (blacklist2.count(*j)) continue;
          if (blacklist2[*j]) continue;

          const Attribute distance = 
            abs(remapped_position_i - config_b.particle(*j).remapped_position());
          if (distance < min_distance) {
            min_distance = distance;
            min_b = *j;
          }
        }        
//        blacklist2.insert(min_b);
        blacklist2[min_b] = 1;
        squared_sum += min_distance * min_distance;
      }

      return sqrt(squared_sum / all_a.size());
    }

    Tuple3
    extremal_position(
        const SimulationConfig & config,
        const TagSet & subset,
        const bool minimal)
    {
      const int n = -1 + 2 * minimal;
      Tuple3 min_pos(0);
      if(subset.size() > 0){
        min_pos = config.particle((* subset.begin())).position();
        for(TagSetConstIterator it = subset.begin();
            it != subset.end(); ++it){
          const Tuple3 & pos = config.particle(*it).position();
          for(unsigned int d = 0; d < 3; ++d){
            min_pos[d] = std::min(n * min_pos[d], n * pos[d]) * n;
          }
        }
      }
      return min_pos;
    }

    Tuple3
    dimension(
        const SimulationConfig & config,
        const TagSet & subset)
    {
      const Tuple3 a = minimal_position(config, subset);
      const Tuple3 b = maximal_position(config, subset);
      using std::abs;
      using std::max;
      Tuple3 result;
      for(unsigned int d = 0; d < 3; ++d)
        result[d] = max(abs(a[0]), abs(b[0]));
      return result;
    }

    Attribute
    volume(
        const SimulationConfig & config,
        const TagSet & subset)
    {
      const Tuple3 a = minimal_position(config, subset);
      const Tuple3 b = maximal_position(config, subset);
      Attribute result = 1.0;
      for(unsigned int d = 0; d < 3; ++d)
        result *= b[0] - a[0];
      return result;
    }

    Attribute
    box_volume(
        const SimulationConfig & config)
    {
      const Tuple3 & box = config.box();
      return Attribute(box[0] * box[1] * box[2]);
    }

    Tuple3
    center_of_mass(
        const SimulationConfig & config,
        const TagSet & subset)
    {
      Attribute mass = 0.0;
      Tuple3 r(0);
      for(TagSetConstIterator it = subset.begin();
          it != subset.end(); ++it){
        ParticleView p = config.particle(*it);
        mass += p.mass();
        r += p.mass() * p.position();
      }
      return r / mass;
    }

    Attribute
    density(
      const SimulationConfig & config,
      const TagSet & subset)
    {
      Attribute mass = 0;
      for(TagSetConstIterator it = subset.begin();
          it != subset.end(); ++it){
        mass += config.particle(*it).mass();
      }
      return mass / box_volume(config);
    }

    AttributeMap
    radial_distribution_function(
      const SimulationConfig & config,
      bool exclude_bonded,
      const Attribute delta_r,
      float epislon)
    {
      // Calculate maximum box length
      // Diagonal of boxdd
      const Tuple3 & L = config.box();
      Attribute r_max = max(L) / 2;
      // Prepare calculation of distances
      const int N = config.num_atoms();
      std::vector<Attribute> radii(N);
      Histogram<std::vector<Attribute> > h;
#ifdef DEBUG_RADIAL_DISTRIBUTION_FUNCTION
      clock_t begin = clock();
#endif
      for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
          ParticleView p_A = config.particle(i);
          ParticleView p_B = config.particle(j);

          if (exclude_bonded && config.property.bonded(i, j)){
            radii[j] = 0;
          } else {
            const Tuple3 pos_A = p_A.position();
            const Tuple3 pos_B = p_B.position();
            const Tuple3 d = pos_B - pos_A;
            Tuple3 d_;
            for(int k  = 0; k < 3; ++k){
              int beta = (int)(std::abs(d[k]) / (L[k]/2));
              d_[k] = beta * L[k] + (1 - 2 * beta) * std::abs(d[k]);
            } // for
            radii[j] = floor(abs(d_) / delta_r) * delta_r;
          } // else
        } // for 
        h.count(radii);
      } // for
#ifdef DEBUG_RADIAL_DISTRIBUTION_FUNCTION
      clock_t end = clock();
      std::cerr << "Time elapsed: " << static_cast<double>((end - begin) / CLOCKS_PER_SEC);
                << "s" << std::endl; 
#endif
      // Calculate distances
      // Normalize binned radii
      // N * N_i(r) = 4 * pi * rho * r^2 * dr * N
      // dr ~ delta_r =>
      // N * N_i(r) ~ 4 * pi * rho * r^2 * delta_r * N
      //           =: k * r^2
      const Attribute rho = number_density(config);
      const Attribute k = 4 * M_PI * rho * delta_r * N;
      Attribute check = 0;
      AttributeMap normalized;
      for(int _r = 1; _r < (int)floor(r_max / delta_r); ++_r){
        Attribute r = _r * delta_r;
        Attribute n_f = k * r * r;
        check += n_f;
        normalized[r] = float(h[r]) / n_f;
      } // for
      Attribute N_check = rho * (float(4) / 3) * M_PI * r_max*r_max*r_max * N;
      check /= N_check;
      if (significantly_different(check, 1, 3)){
        std::stringstream msg;
        msg << "Calculation of radial distribution function "
            << "was not accurate. Error: " << std::abs(check - 1) << " !";
        throw RuntimeWarning(msg.str());
      }
      // Calculate average
      float resolution = 0.1;
      const int freq = (int)floor(resolution / delta_r);
      Attribute tmp_r = 0;
      Attribute tmp_c = 0;
      int n = 0;
      AttributeMap averaged;
      for(AttributeMap::const_iterator it = normalized.begin(); it != normalized.end(); ++it){
        n += 1;
        tmp_r += it->first;
        tmp_c += it->second;
        if (n >= freq){
          averaged[tmp_r / freq] = tmp_c / freq;
          n = 0;
          tmp_r = tmp_c = 0;
        } // if
      } // for
      return averaged;
    }

    int
    expected_number_of_particles(
      const SimulationConfiguration::SimulationConfig & config,
      const AttributeMap & rdf_histogram,
      const Attribute delta_r,
      const Attribute r)
    {
      Attribute N = 0;
      Attribute r_i = 0;
      int n = int(r / delta_r);
      for(int i = 0; i < n; ++i){
        r_i += delta_r;
        N += rdf_histogram.find(r_i)->second * delta_r * r_i * r_i;
      }
      return 4 * M_PI * number_density(config) * N;
    }

    Attribute
    RadiusOfGyrationAnalyzer::operator()(
      const SimulationConfig & config) const
    {
      Attribute result;
      typedef std::vector<TagSet> TagSetVector;
      TagSetVector molecules = vector_from_set(config.property.molecules());
      const size_t N = molecules.size();
      //for(TagSetSetConstIterator molecule = molecules.begin();
          //molecule != molecules.end(); ++molecule)
      #pragma omp parallel
      {
        #pragma omp for reduction(+:result)
        for(size_t i = 0; i < N; ++i)
          result += rg_molecule_(config, *(molecules.begin() + i));
          //result += rg_molecule_(config, * molecule);
      }
      return result / molecules.size();
    }

    Attribute
    RadiusOfGyrationAnalyzer::rg_molecule_(
      const SimulationConfig & config,
      const TagSet & molecule)
    {
      Attribute rg = 0;
      Tuple3 r_sum(0);
      for(TagSetConstIterator p = molecule.begin();
          p != molecule.end(); ++p)
        r_sum += config.particle(* p).remapped_position();
      const Tuple3 r_mean = r_sum / molecule.size();
      for(TagSetConstIterator p = molecule.begin();
          p != molecule.end(); ++p)
        rg += abs_squared(
                  config.particle(*p).remapped_position()
                - r_mean);
      return sqrt(rg / molecule.size());
    }

  } // namespace analysis

  namespace deprecated{

    Tuple3
    ConfigAnalyzer::origin(bool maximum) const{
      int n;
      if (maximum){
        n = -1;
      } else {
        n = 1;
      }

      const Tuple3 start = config_->particle(0).position();
      Attribute x_min = start.x() * n;
      Attribute y_min = start.y() * n;
      Attribute z_min = start.z() * n;

      const TagSet all = config_->all();
      for (TagSetConstIterator it = all.begin(); it != all.end(); ++it){
        const Tuple3 position = config_->particle(*it).position();
        x_min = std::min(x_min, n * position.x());
        y_min = std::min(y_min, n * position.y()); 
        z_min = std::min(z_min, n * position.z());
      }

      return Tuple3(x_min, y_min, z_min) * n;
    }

    Tuple3
    ConfigAnalyzer::dimension() const{
      const Tuple3 a = origin();
      const Tuple3 b = origin(true);
      using std::abs;
      using std::max;
      return Tuple3(
        max(abs(a.x()), abs(b.x())),
        max(abs(a.y()), abs(b.y())),
        max(abs(a.z()), abs(b.z()))
        );
    }

    Attribute
    ConfigAnalyzer::volume() const{
      const Tuple3 a = origin();
      const Tuple3 b = origin(true);
      using std::abs;
      return    abs(b.x() - a.x()) 
              * abs(b.y() - a.y()) 
              * abs(b.z() - a.z());
    }

    Tuple3
    ConfigAnalyzer::box_origin(bool maximum) const{
      const Tuple3 & box = config_->box();
      if (maximum){
        return box * (0.5);
      } else {
        return box * (-0.5);
      }
    }

    Attribute
    ConfigAnalyzer::box_volume() const{
      const Tuple3 & box = config_->box();
      return Attribute(box.x() * box.y() * box.z());
    }

    Tuple3
    ConfigAnalyzer::center_of_mass(const TagSet& group) const{
      Tuple3 result;
      Attribute sum_mass = 0;
      for(TagSetConstIterator it = group.begin(); it != group.end(); ++it){
        const ParticleView particle = config_->particle(*it);
        sum_mass += particle.mass();
        result += particle.mass() * particle.position();
      }
      return result / sum_mass;
    }

    Tuple3
    ConfigAnalyzer::center_of_mass() const{
      return center_of_mass(config_->all());
    }

    Attribute
    ConfigAnalyzer::mean_squared_distance(const SimulationConfig& other) const{
      return mean_squared_distance(other, config_->all());
    }

    Attribute
    ConfigAnalyzer::mean_squared_distance(
        const SimulationConfig& other,
        const TagSet& group) const{
      SimulationConfig config_a(* config_, group);
      return analysis::mean_squared_distance(config_a,  other);
    }

    Attribute
    ConfigAnalyzer::end_to_end_distance(const ParticleTag& tag) const{
      Attribute max_distance = 0;
      const TagSet molecule(config_->property.molecule(tag));
      const TagSet ends(config_->property.find_ends(molecule));
      const std::map<Tag, Tuple3> mapped_into_box(        
                config_->property.remap(*molecule.begin())); 
      const Tuple3 pos_a = mapped_into_box.find(*ends.begin())->second;
      for(TagSetConstIterator it = ++ends.begin(); it != ends.end(); ++it){
        max_distance = std::max(max_distance,
                                abs(mapped_into_box.find(*it)->second - pos_a));
      }
      return max_distance;
    }

  } // namespace deprecated

} // namespace mdst
