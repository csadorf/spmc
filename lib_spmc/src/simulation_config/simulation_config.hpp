/*
 * =====================================================================================
 *
 *       Filename:  simulation_config.hpp
 *
 *    Description:  Interface to access and manipulate a simulation configuration.
 *
 *        Version:  1.0
 *        Created:  10/19/2012 04:54:33 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
 *   Organization:  RWTH Aachen University
 *
 * =====================================================================================
 */

#ifndef MDST_SIMULATION_CONFIG_H_
#define MDST_SIMULATION_CONFIG_H_

#include <list>
#include <vector>
#include <map>
#include <set>
#include <string>
#include <memory.hpp>

// multithreading
#include <boost/thread/locks.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/shared_mutex.hpp>

#include "types.hpp"
#include "iterators.hpp"
#include "tuple3.hpp"
#include "tuple3_arithmetic.hpp"
#include "exception.hpp"
#include "parameters.hpp"
#include "view.hpp"
#include "property_view.hpp"
#include "cell_grid.hpp"
#include "n_list.hpp"

namespace mdst{
   
  namespace SimulationConfiguration{

    // Forward-declarations
    namespace ParserWriter{
      class HoomdBlueXml;
      class XYZ;
    };

    // Global function declarations
    TagSet tag_set_from_walk(const TagVector& walk);

    // Class declarations

    class LookupError : public Exception{
     public:  
      LookupError();
      LookupError(const std::string& e): Exception(e){};
    };

    class DataCorruptedError : public Exception{
     public:
      DataCorruptedError();
      DataCorruptedError(const std::string& e): Exception(e) {};
    };

    /*! \brief SimulationConfig represents a configuration of particles.
     *
     * Store, view and manipulate all necessary information about a 
     * molecular system. The system is concludingly described by the
     * data within this class.
     */
    class SimulationConfig{
      
    // Parsers and dumpers can be friends to improve performance.
    //
     friend class ::mdst::SimulationConfiguration::ParserWriter::HoomdBlueXml;
     friend class ::mdst::SimulationConfiguration::ParserWriter::XYZ;

     friend class View;
     friend class NBondView;
     friend class BondView;
     friend class AngleView;
     friend class DihedralView;
     friend class ParticleView;
     friend class PropertyView;

    // 
    // Declaration of main class
    //
     public:
      typedef internal::TagCounter TagIterator;
      typedef TagSetIterator TagGroupIterator;

      explicit SimulationConfig(const Parameters& parameters = Parameters());
      SimulationConfig(const SimulationConfig & other_config,
                       const TagSet& subset = TagSet());
      ~SimulationConfig();

      // Particle Views
      //
      ParticleView particle(const Tag& tag)
        { return ParticleView(this, tag);}
      ParticleView particle(const Tag& tag) const
        { return ParticleView(this, tag);}
      // Direct high performance access
      Tuple3
      particle_position(const ParticleTag & tag) const
      { return element_product(positions_->operator[](tag), box_);}

      TuplesConstIterator
      particle_positions_cbegin() const
      { return positions_->begin();}

      TuplesConstIterator
      particle_positions_cend() const
      {return positions_->end();}

      TuplesIterator
      particle_positions_begin()
      { return positions_->begin();}
      
      TuplesIterator
      particle_positions_end()
      { return positions_->end();}

      // Bonds, Angles and Dihedral Views
      //
      unsigned int num_bonds() const { return bond_types_->size();};
      unsigned int num_bond_types() const;
      TagSet bonds() const;
      const BondView bond(const BondTag& tag) const;
      BondView bond( const BondTag& tag);
      BondTag determine_bond(const ParticleTag& a,
                             const ParticleTag& b,
                             const ParticleTag& c = ParticleTag(-1),
                             const ParticleTag& d = ParticleTag(-1)) const;
      AngleTag determine_angle(const ParticleTag& a,
                               const ParticleTag& b,
                               const ParticleTag& c) const;
      DihedralTag determine_dihedral(const ParticleTag& a, const ParticleTag& b,
                                     const ParticleTag& c, const ParticleTag& d) const;
      void make_bond(const ParticleTag& a, const ParticleTag& b, const BondType& type);
      void add_bond(const ParticleTag & a, const ParticleTag & b, const BondType & type);

      unsigned int num_angles() const { return angle_types_->size();};
      unsigned int num_angle_types() const;
      TagSet angles() const;
      const AngleView angle(const Tag& tag) const { return AngleView(this, tag);};
      AngleView angle( const Tag& tag) { return AngleView(this, tag);};

      unsigned int num_dihedrals() const { return dihedral_types_->size();};
      unsigned int num_dihedral_types() const;
      TagSet dihedrals() const;
      const DihedralView dihedral( const Tag& tag) const { return DihedralView(this, tag);};
      DihedralView dihedral( const Tag& tag) { return DihedralView(this, tag);};

      unsigned int num_impropers() const { return 0;}
      unsigned int num_improper_types() const { return 0;}

      // Dumping functions
      //
      std::ostream & dump_bonds( std::ostream & out) const
        {return dump_all_(out, *bonds_, *bond_types_);}
      std::ostream & dump_angles( std::ostream & out) const;
      std::ostream & dump_dihedrals( std::ostream & out) const;

      // Basic properties
      //
      unsigned int num_atoms() const { return types_->size();}
      void set_num_atoms( int);
      unsigned int num_atom_types() const;
      int dimension() const { return dimension_;}
      void set_dimension( int);
      Tuple3 box() const { return box_;}

      // Neighborhood properties
      
      /// Returns true if particles a and b share one molecule.
      bool
      bonded(
        const ParticleTag a,
        const ParticleTag b) const;

      /// Returns true if particles a and b share one bond.
      bool
      directly_bonded(
        const ParticleTag a,
        const ParticleTag b) const;

      /// Returns true if particles a and b are bonded by a maximum of depth steps.
      /// Warning: As this function is called within the innermost simulation loop,
      ///  it does *not* lock the SimulationConfig.
      bool
      indirectly_bonded(
        const ParticleTag a,
        const ParticleTag b,
        int depth = 1) const;

      /// Calculate the neighborhood of particle p.
      /// All particles in result will be neighbors of p
      /// until the maximum step size of depth.
      void
      neighborhood(
        const ParticleTag p,
        int depth,
        TagSet & result) const;


      // Bond, Angle and Dihedral properties
      /// Returns true if particles a, b share one bond.
      bool
      share_bond(
        const ParticleTag a,
        const ParticleTag b) const;

      /// Returns true if particles a, b share at least one angle.
      bool
      share_angle(
        const ParticleTag a,
        const ParticleTag b) const;

      /// Returns true if particles a, b share at least one dihedral.
      bool
      share_dihedral(
        const ParticleTag a,
        const ParticleTag b) const;

      // Selection
      //
      /// Returns a TagSet of all particles.
      TagSet group() const;

      /// Returns a TagSet of all particles of type.
      TagSet group(const ParticleType& type) const;

      /// Returns a TagSet of all particles from lower_bound to beneath upper_bound.
      TagSet group(const ParticleTag& lower_bound,
                   const ParticleTag& upper_bound) const;

      /// Returns a TagSet of all particles from lower_bound
      /// till just beneath upper_bound.
      TagSet group(const Tuple3& lower_bound,
                   const Tuple3& upper_bound) const;

      /// Alias for group()
      TagSet all() const {return group();}
      TagSetPtrSet molecules() const {return molecules_;};

      /// Returns iterator to first particle
      TagIterator all_begin() const { return TagIterator(0);}
      /// Returns iterator one past last particle
      TagIterator all_end() const { return TagIterator(num_atoms());}
      /// Returns iterator from tag
      TagIterator particle_iterator(ParticleTag tag) const {
        if (tag >= 0 && tag <= ParticleTag(num_atoms()))
        {
          return TagIterator(tag);
        }
        else
        {
          std::stringstream msg;
          msg
            << "Particle iterator requested for particle with tag '"
            << tag << "' is out of bounds. Min: 0, Max: "
            << num_atoms() << "."
            ;
          throw std::invalid_argument(msg.str());
        }
      }

      // Locking particles from position change
      /// Prohibit particle from position change.
      void
      lock_particle(
        const ParticleTag & particle)
      { particles_locked_->insert(particle);}

      /// Permit particle position change.
      void
      unlock_particle(
        const ParticleTag & particle)
      { particles_locked_->erase(particle);}

      TagSet
      locked_particles() const
      {
        return * particles_locked_;
      }

      bool
      particles_locked_silent() const
      { return particles_locked_silent_;}

      bool
      set_particles_locked_silent(
        const bool silent)
      { return particles_locked_silent_ = silent;}

      // Basic manipulation
      //
      /// Append other_config to this config. NBonds are transformed accordingly.
      TagSet append(
        const SimulationConfig & other_config,
        bool update_internal = true); // DANGEROUS OPTION!!
      void set_box( const Tuple3 &);
      void stretch_box( const Tuple3 & factor);

      // Advanced manipulation
      //
      void update_bond_types();
      void generate_angles()
        {generate_n_bonds_(angle_types_, angles_, 3);}
      void generate_dihedrals()
        {generate_n_bonds_(dihedral_types_, dihedrals_, 4);}
       
      // Bond all particles in succesive order to eachother
      void bond_all_particles();
      
      // PropertyView offers more advanced views of the data.
      //
      const PropertyView property;

      bool
      check() const;

      bool developer_mode() const
        { return developer_mode_;}
      void set_developer_mode(bool mode);
      void update_internal() const;

      int translate_type_to_int_type(const Type &type) const{
        std::map<Type,int>::const_iterator it = map_types_to_int_types_.find(type);
        assert(it != map_types_to_int_types_.end());
        return it->second;
      }

     private:
      // Private functions
      bool
      check_internal_() const;

      void set_num_atoms_(int num_atoms);
      shared_ptr<TagMap> cut_bonds_(TagMap * bonds, Types * types) const;
      void cut_bonds_(shared_ptr<TagMap>& bonds,
                      shared_ptr<Types>& bond_types,
                      const int N,
                      const TagSet& subset);
      void cut_bonds_(shared_ptr<TagMap>& bonds,
                      shared_ptr<Types>& bond_types,
                      const int N,
                      const ParticleTag& max_tag);
      void generate_n_bonds_(shared_ptr<Types>&, shared_ptr<TagMap>&, const int n);
      int calculate_num_bonds_(const TagMap& n_bonds) const;
      NBondTag determine_n_bond_(const TagSet& tags) const;
      const TagMap& n_bond_map_(const unsigned int n) const;
      TagMap& n_bond_map_(const unsigned int n);
      TagMap& reversed_n_bonds_(const unsigned int n);
      const TagMap& reversed_n_bonds_(const unsigned int n) const;
      const Types& n_bond_types_(const unsigned int n) const;
      Types& n_bond_types_(const unsigned int n);
      unsigned int num_of_n_bonds_(const unsigned int n) const;
      void update_particle_map_() const;
      void update_molecule_map_() const;
      void update_reversed_n_bonds_(const unsigned int n) const;
      void update_sorted_n_bonds_(
        const Types & types,
        const TagMap & map,
        TagVectorMap & sorted_n_bonds) const;
      void update_internal_() const;
      void map_molecule_(Tag particle, TagSet & skip) const;
      TagVector sort_bond_tags_(const TagSet& tags) const;
      std::ostream & dump_all_( std::ostream & out, const TagMap & map, const Types & types) const;
      // shift all tags by n
      void shift_bonds_(int n);
      TagSetSet generate_dihedral_set_() const;
      TagSetSet generate_n_bond_set_(unsigned int n) const;
      std::set<Type> generate_type_set_(const TagSet& particles) const;
      Types types_from_tags_(const TagVector& tags) const;
      TagVector sort_by_angle_(const ParticleTag& planet, const TagSet& moons) const;

      typedef std::map<TagPair, bool> IndirectBondMap;
      typedef IndirectBondMap::const_iterator IndirectBondMapConstIterator;

      mutable IndirectBondMap indirect_bond_map_1_;
      mutable IndirectBondMap indirect_bond_map_2_;
      mutable IndirectBondMap indirect_bond_map_3_;

      bool
      directly_bonded_(const ParticleTag a, const ParticleTag b) const;
      bool
      indirectly_bonded_depth_1_(const ParticleTag a, const ParticleTag b) const;
      bool
      indirectly_bonded_depth_1_calc_(const ParticleTag a, const ParticleTag b) const;
      bool
      indirectly_bonded_depth_2_(const ParticleTag a, const ParticleTag b) const;
      bool
      indirectly_bonded_depth_2_calc_(const ParticleTag a, const ParticleTag b) const;
      bool
      indirectly_bonded_depth_3_(const ParticleTag a, const ParticleTag b) const;
      bool
      indirectly_bonded_depth_3_calc_(const ParticleTag a, const ParticleTag b) const;

      /// Check whether the molecule from 'first' to 'last' is linear.
      bool
      is_linear_(
        TagSetConstIterator first,
        TagSetConstIterator last) const;

      /// Check whether the configuration contains only linear molecules.
      bool
      is_linear_() const;

      mutable bool is_linear_flag_;

      bool
      indirectly_bonded_recursive_(
        const ParticleTag a,
        const ParticleTag b,
        int depth) const;

      int
      map_type_to_int_type_(const Type &type) const;
      
      mutable std::map<Type,int> map_types_to_int_types_;
      mutable int n_types_;

      // Private variables
      const Parameters parameters_;
      int dimension_;
      Tuple3 box_;
      /// A list of all particle types. The index of each particle in this list, represents
      /// its unique id. All attributes are associated by their index. 
      /// Therefore the order must be retained!
      shared_ptr<Types> types_;
      shared_ptr<std::vector<int> > int_types_;
      // Note:
      // The particle attributes mass and charge are not related to the particle type, which
      // seems to be counterintuitive, but makes physical sense.
      //
      //
      /// List of all masses for each particle
      shared_ptr<Attributes> masses_;
      /// List of all charges for each particle
      shared_ptr<Attributes> charges_;
      /// List of all particle positions.
      shared_ptr<Tuples> positions_;
      /// List of all particle velocities.
      shared_ptr<Tuples> velocities_;
      /// List of all particle accelerations.
      shared_ptr<Tuples> accelerations_;
      /// List of all particle images.
      shared_ptr<Tuples> images_;
      /// List of all bond types per bond. The number of types is directly related
      /// to the number of bonds.
      shared_ptr<Types> bond_types_;
      /// List of all angle types per angle. The number of types is directly related to the 
      /// number of angles.
      shared_ptr<Types> angle_types_;
      /// List of all dihedral types per dihedral. The number of types is directly related
      /// to the number of dihedrals.
      shared_ptr<Types> dihedral_types_;
      /// Every bond type entry is mapped to the particles which make up the bond.
      shared_ptr<TagMap> bonds_;
      /// Every bond type entry is mapped to the sorted particles which make up the bond.
      mutable shared_ptr<TagVectorMap> bonds_sorted_;
      /// Every angle type entry is mapped to the particles which make up the angle.
      shared_ptr<TagMap> angles_;
      /// Every angle type entry is mapped to the sorted particles which make up the angle.
      mutable shared_ptr<TagVectorMap> angles_sorted_;
      /// Every dihedral type entry is mapped to the particles which make up the dihedral.
      shared_ptr<TagMap> dihedrals_;
      /// Every dihedral type entry is mapped to the sorted particles which make up the dihedral.
      mutable shared_ptr<TagVectorMap> dihedrals_sorted_;
      /// The particle map, associates each particle to
      /// all its directly bonded particles and vice-versa.
      /// Note: The particle is not associated with itself!
      shared_ptr<TagMap> particle_map_;
      /// Every particle is mapped to its corresponding bond indeces.
      shared_ptr<TagMap> reversed_bonds_;
      /// Every particle is mapped to its corresponding angle indeces.
      shared_ptr<TagMap> reversed_angles_;
      /// Every particle is mapped to its corresponding dihedral indeces.
      shared_ptr<TagMap> reversed_dihedrals_;
      /// molecule_map_ maps each particle to a TagSet,
      /// which represents all particles bonded in one molecule.
      mutable TagSetMap molecule_map_;
      /// molecules_ is a set of all registered molecules.
      mutable TagSetPtrSet molecules_;
      /// All particles in this group are blocked from a position change.
      shared_ptr<TagSet> particles_locked_;
      /// Throw an exception if locked particles shall be moved.
      bool particles_locked_silent_;
      

      // cell grid
//      typedef std::map<ParticleTag, Tuple3> CellIndexDirty_;
      typedef std::tr1::unordered_map<ParticleTag, Tuple3> CellIndexDirty_;
      typedef CellIndexDirty_::const_iterator CellIndexDirtyConstIterator_;

      mutable CellGrids cell_grids_;
//      mutable CellIndexDirty_ cell_index_dirty_;
      mutable std::vector<int> cell_index_dirty_sparse_;
      mutable std::vector<std::pair<int,Tuple3> > cell_index_dirty_dense_;
      mutable int cell_index_dirty_count_;

     public:
      CellGrid &
      cell_grid(const CellGridsKey & key) const;
     
      void
      update_cell_lists() const;

     private:
      mutable NeighborListsMap neighbor_lists_map_;
      mutable Attribute neighbor_list_max_displacement_;

      void
      initialize_neighbor_lists_(
        const NeighborListKey & n_list_key) const;
       
      void
      calculate_neighbor_list_(
        const ParticleTag particle) const;

     public:

      TagVectorConstIterator
      neighbor_list(
        const NeighborListKey & n_list_key,
        const ParticleTag particle,
        TagVectorConstIterator & first,
        bool & required_update) const;

      TagVectorConstIterator
      neighbor_list(
        const NeighborListKey & n_list_key,
        const ParticleTag particle,
        TagVectorConstIterator & first) const
      {
        bool tmp;
        return neighbor_list(n_list_key, particle, first, tmp);
      }

      /// Update all neighbor lists.
      void
      update_neighbor_lists() const;

      void
      set_neighbor_list_on_update_counter(
        int * counter)
      { n_list_update_counter_ = counter;}

      mutable int * n_list_update_counter_;

     private:
      void
      update_neighbor_lists_() const;

      void
      calculate_cell_list_(
        CellGrid & cell_grid) const;
      void
      calculate_cell_list_2_(
        CellGrid & cell_grid) const;
      void
      clean_cell_index_() const;

      
      // Multithreading
      typedef boost::shared_mutex Mutex;
      typedef boost::lock_guard<Mutex> LockGuard;
      typedef boost::unique_lock<Mutex> UniqueLock;
      typedef boost::shared_lock<Mutex> SharedLock;

      mutable Mutex mutex_;
      mutable Mutex cell_grid_mutex_;
      //mutable RecursiveMutex recursive_mutex_;

      // Admin mode
      bool developer_mode_;
    };

  } // namespace SimulationConfiguration

} // namespace mdst

#endif // MDST_SIMULATION_CONFIG_H_
