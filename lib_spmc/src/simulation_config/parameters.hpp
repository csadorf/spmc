// =====================================================================================
//
//       Filename:  parameters.hpp
//
//    Description:  Dictionary for simulation parameters.
//
//        Version:  1.0
//        Created:  11/05/2012 08:55:29 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_PARAMETERS_H_
#define MDST_PARAMETERS_H_

#include "reduced_units.hpp"
#include "exception.hpp"

#include <memory.hpp>
#include <boost/units/cmath.hpp>

namespace mdst{

  namespace SimulationConfiguration { class SimulationConfig; }


  using namespace units;

  // Exception class
  class ParameterError : public Exception{
   public:
    ParameterError() {};
    ParameterError(const std::string& e): Exception(e) {};
    virtual ~ParameterError() throw() {};
  };

  struct HarmonicBondParameters{
    Length r_0;
    BondStretchingForce c_0;
    HarmonicBondParameters() {};
    HarmonicBondParameters(
      const Length & r_0_,
      const BondStretchingForce & c_0_):
      r_0(r_0_), c_0(c_0_) {};
  };

  struct HarmonicAngleParameters{
    PlaneAngle theta_0; 
    BondBendingForce k;
    HarmonicAngleParameters() {};
    HarmonicAngleParameters(
      const PlaneAngle & theta_0_,
      const BondBendingForce & k_):
      theta_0(theta_0_), k(k_) {};
  };

  struct HarmonicDihedralParameters{
    Energy k;
    int d;
    Attribute n;
    HarmonicDihedralParameters() {};
    HarmonicDihedralParameters(
      const Energy & k_,
      const int d_,
      const Attribute n_):
      k(k_), d(d_), n(n_) {};
  };

  struct PairforceParameters{
  };

  struct LJPairforceParameters : public PairforceParameters{
    Energy epsilon;
    Length sigma;
    Attribute alpha;
    LJPairforceParameters() {};
    LJPairforceParameters(
      const Energy & e_,
      const Length & s_,
      const Attribute alpha_):
      epsilon(e_), sigma(s_), alpha(alpha_) {};
  };

  typedef std::map<Area, Energy> TabulatedEnergy;
  typedef TabulatedEnergy::const_iterator TabulatedEnergyConstIterator;
  typedef TabulatedEnergy::const_reverse_iterator
    TabulatedEnergyConstReverseIterator;
  
  struct TabulatedPairforceParameters : public PairforceParameters{
    TabulatedEnergy table;
    // constructors
    TabulatedPairforceParameters() {};
    TabulatedPairforceParameters(
      const TabulatedEnergy & table_):
      table(table_) {};
    TabulatedPairforceParameters(
      const TabulatedEnergy & table_,
      const Length & r_cut_,
      const Length & r_min_):
      table(table_) {};
  };

  struct TabulatedNeighborForceParameters{
    TabulatedEnergy table;
    Area r_max;
    // constructors
    TabulatedNeighborForceParameters() {};
    TabulatedNeighborForceParameters(
      const TabulatedEnergy & table_):
      table(table_) {};
  };

  TabulatedEnergy
  parse_tabulated_energy(std::istream & stream,
                         const ReducedUnitsSet & units);

  TabulatedEnergy
  parse_tabulated_energy_file(const std::string & filename,
                              const ReducedUnitsSet & units);

  class ReverseMappingParameters{
   public:
    // constructors
    ReverseMappingParameters():
      replacement_config_(shared_ptr<SimulationConfiguration::SimulationConfig>()) {};
    ReverseMappingParameters(const shared_ptr<SimulationConfiguration::SimulationConfig> rc):
      replacement_config_(rc) {};
    shared_ptr<SimulationConfiguration::SimulationConfig> replacement_config() const;
   private:
    // data members
    shared_ptr<SimulationConfiguration::SimulationConfig> replacement_config_;
  };

  struct ParticleParameters{
   public:
    Length sigma;
    Energy epsilon;
    ReverseMappingParameters reverse_map;
  };

  typedef std::map<SortedTypePair, PairforceParameters const * const>
    PairforceParametersMap;
  typedef std::vector<PairforceParametersMap> PairforceParametersMaps;
  typedef PairforceParametersMap::const_iterator
    PairforceParametersMapConstIterator;
  typedef std::map<SortedTypePair, LJPairforceParameters> LJPairforceParametersMap;
  typedef std::map<ParticleType, ParticleParameters> ParticleParametersMap;
  typedef std::map<BondType, HarmonicBondParameters> BondParametersMap;
  typedef std::map<AngleType, HarmonicAngleParameters> AngleParametersMap;
  typedef std::vector<HarmonicDihedralParameters> HarmonicDihedralParametersVector;
  typedef HarmonicDihedralParametersVector::const_iterator HarmonicDihedralParametersVectorConstIterator;
  typedef std::map<DihedralType, HarmonicDihedralParametersVector> HarmonicDihedralParametersMap;

  typedef std::map<std::string, std::string> ParameterOptions;
  typedef ParameterOptions::const_iterator ParameterOptionsConstIterator;

  std::string
  retrieve_option(const ParameterOptions & options,
                  const std::string & key);

  struct Parameters{
   public:
    ParticleParametersMap particles;
    LJPairforceParametersMap lj_pairforces;
    PairforceParametersMaps pairforce_parameters_maps;
    BondParametersMap bonds;
    AngleParametersMap angles;
    HarmonicDihedralParametersMap dihedrals;
    // member functions
    LJPairforceParametersMap generate_lj_pairforces() const;
    BondType bond_type(const SortedTypePair& type_pair) const;
    AngleType angle_type(const ParticleType& a,
                         const ParticleType& b,
                         const ParticleType& c) const;
    DihedralType dihedral_type(const ParticleType& a,
                               const ParticleType& b,
                               const ParticleType& c,
                               const ParticleType& d) const;
    ParticleParameters particle_parameter(const ParticleType& type) const;
  };

  inline Type
  generate_type_name(Types types){
    if (types.empty()){
      std::stringstream error;
      error << "generate_type_name(Types types): ";
      error << "types argument empty.";
      throw ParameterError(error.str());
    }
    // Making sure we always use the same direction of order
    if (types.at(0) > types.at(types.size() - 1))
      std::reverse(types.begin(), types.end());
    // Generate name
    std::stringstream type_name;
    type_name << types.at(0);
    for(TypesConstIterator it = ++types.begin(); it != types.end(); ++it)
      type_name << "-" << *it;
    return type_name.str();
  }

} // namespace mdst

#endif // MDST_PARAMETERS_H_
