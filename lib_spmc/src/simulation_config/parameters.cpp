// =====================================================================================
//
//       Filename:  parameters.cpp
//
//    Description:  Dictionary for simulation parameters.
//
//        Version:  1.0
//        Created:  11/05/2012 08:55:29 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "parameters.hpp"
#include <sstream>
#include <fstream>

namespace mdst{
 
  std::string
  retrieve_option(const ParameterOptions & options,
                  const std::string & key){
    ParameterOptionsConstIterator match =
      options.find(key);
    if (match == options.end()){
      std::stringstream error;
      error << "Failed to access parameter option ";
      error << "'" << key << "'.";
      throw ParameterError(error.str());
    }
    return match->second;
  }

  LJPairforceParametersMap
  Parameters::generate_lj_pairforces() const{
    LJPairforceParametersMap result;
    typedef ParticleParametersMap::const_iterator PPMConstIterator;
    for(PPMConstIterator itA = particles.begin(); itA != particles.end(); ++itA){
      for(PPMConstIterator itB = itA; itB != particles.end(); ++itB){
        const SortedTypePair pair(itA->first, itB->first);
        const ParticleParameters& a = itA->second;
        const ParticleParameters& b = itB->second;
        LJPairforceParameters pairforce;
        pairforce.epsilon = boost::units::sqrt(a.epsilon * b.epsilon);
        pairforce.sigma = boost::units::sqrt(a.sigma * b.sigma);
        pairforce.alpha = 1;
        result[pair] = pairforce;
      }
    }
    return result;
  }

  BondType
  Parameters::bond_type(const SortedTypePair& type_pair) const{
    std::stringstream type_name;
    type_name << type_pair.first() << "-" << type_pair.second();
    return Type(type_name.str());
  }

  ParticleParameters
  Parameters::particle_parameter(const ParticleType& type) const{
    ParticleParametersMap::const_iterator it = particles.find(type);
    if (it == particles.end()){
      std::stringstream error;
      error << "Unable to find particle parameters for particle of type '";
      error << type << "'.";
      throw ParameterError(error.str());
    } else {
      return it->second;
    }
  }

  AngleType
  Parameters::angle_type(const ParticleType& a,
                         const ParticleType& b,
                         const ParticleType& c) const{
    Types types;
    types.push_back(a);
    types.push_back(b);
    types.push_back(c);
    return AngleType(generate_type_name(types));
  }

  DihedralType
  Parameters::dihedral_type(const ParticleType& a,
                            const ParticleType& b,
                            const ParticleType& c,
                            const ParticleType& d) const{
    Types types;
    types.push_back(a);
    types.push_back(b);
    types.push_back(c);
    types.push_back(d);
    return DihedralType(generate_type_name(types));
  }

  shared_ptr<SimulationConfiguration::SimulationConfig>
  ReverseMappingParameters::replacement_config() const{
    if (replacement_config_){
      return replacement_config_;
    } else {
      std::stringstream error;
      error << "Could not find replacement configuration for particle.";
      throw ParameterError(error.str());
    }
  }

  TabulatedEnergy
  parse_tabulated_energy_file(const std::string & filename,
                              const ReducedUnitsSet & units){
    std::ifstream file(filename.c_str());
    if (file){
      std::stringstream buffer;
      buffer << file.rdbuf();
      file.close();
      return parse_tabulated_energy(buffer, units);
    } else {
      std::stringstream error;
      error << "Failed to read from '" << filename << "'.";
      throw ParameterError(error.str());
    }
  }

  TabulatedEnergy
  parse_tabulated_energy(std::istream & stream,
                         const ReducedUnitsSet & units){
    typedef std::vector<std::string> Lines;
    Lines lines;
    std::string line;
    while (std::getline(stream, line)){
      lines.push_back(line);
    }

    TabulatedEnergy table;
    for(unsigned int i = 1; i < lines.size(); ++i){
      Attribute distance, energy;
      std::istringstream iss(lines[i]);
      iss >> distance >> energy;
      table[distance * distance * Area(units.length * units.length)] = energy * units.energy;
    }
    return table;
  }

} // namespace mdst
