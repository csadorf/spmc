//=====================================================================================
//
//       Filename:  cell_list.hpp
//
//    Description:  Save cell_list content.
//
//        Version:  1.0
//        Created:  06/21/2014
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATION_CONFIG_CELL_LIST_
#define MDST_SIMULATION_CONFIG_CELL_LIST_

#include "types.hpp"
#include <tr1/unordered_map>
#include <iostream>

namespace mdst{

  namespace SimulationConfiguration{

    typedef size_t ParticleIndex;
    typedef size_t CellIndex;

    typedef std::multimap<CellIndex, ParticleTag> CellListMap;
    typedef CellListMap::const_iterator CellListMapConstIterator;
    typedef CellListMap::iterator CellListMapIterator;
    typedef std::pair<CellListMapConstIterator, CellListMapConstIterator> CellListMapConstRange;
    typedef std::pair<CellListMapIterator, CellListMapIterator> CellListMapRange;

    typedef CellListMapConstIterator CellListConstIterator;
    typedef CellListMapIterator CellListIterator;
    typedef CellListMapConstRange CellListConstRange;
    typedef CellListMapRange CellListRange;

    class CellList{
      friend void dump_cell_list(const CellList &, std::ostream &);
     public:
      explicit CellList(
        size_t num_particles,
        size_t num_cells);

      CellList(
        const CellList & other);

      void
      add_to_cell(
        const CellIndex cell_index,
        const ParticleTag particle);

      void
      remove_from_cell(
        const CellIndex cell_index,
        const ParticleTag particle);

      void clear();

      void
      construct_from_map(
        const CellListMap & map);

      size_t
      num_cells() const{
        std::cerr << "Not implemented!" << std::endl;
        return 0;
      }

      bool
      sanity_check() const;

      CellListMapConstRange
      equal_range(
        const CellIndex cell_index) const
      {
        return map_.equal_range(cell_index);
      }

      CellListMapRange
      equal_range(
        const CellIndex cell_index)
      {
        return map_.equal_range(cell_index);
      }

     private:
      CellListMap map_;

    };

  } // namespace SimulationConfiguration

} // namespace mdst

#endif // MDST_SIMULATION_CONFIG_CELL_LIST_
