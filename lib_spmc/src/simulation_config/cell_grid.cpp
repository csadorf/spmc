//=====================================================================================
//
//       Filename:  cell_grid.cpp
//
//    Description:  Calculate cell grids.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "cell_grid.hpp"
#include <tuple3_arithmetic.hpp>
#include <iostream>
#include <math.h>
#include <utility/exception.hpp>

namespace mdst{

  namespace SimulationConfiguration{

    void
    dump_cell_list(
        const CellList & cell_list,
        std::ostream & stream)
    {
      CellListConstIterator it, itlow, itup;
      itlow = cell_list.map_.begin();
      itup = cell_list.map_.upper_bound(itlow->first);
      while (itlow != cell_list.map_.end()){
        stream << "Cell '" << itlow->first << "':" << std::endl;
        for(it = itlow; it != itup; ++it){
          stream << it->second << " ";
        }
        stream << std::endl;
        itlow = itup;
        itup = cell_list.map_.upper_bound(itlow->first);
      }
    }

    CellIndex
    get_cell_index(
        const unsigned int * num_cells,
        const unsigned int * cell){
      assert((cell[0] * num_cells[1] * num_cells[2]
            + cell[1] * num_cells[2] 
            + cell[2] < (num_cells[0] * num_cells[1] * num_cells[2])));
      return  cell[0] * num_cells[1] * num_cells[2]
            + cell[1] * num_cells[2] 
            + cell[2];
    }
      
    CellGrid::CellGrid(
        const CellGridsKey & key,
        const Tuple3 & box,
        const size_t num_atoms)
//        const ReducedCutOff reduced_cut_off):
//        cut_off(reduced_cut_off),
//        factor(1)
    {
      assert(key.size() <= 3);
      for(unsigned int d = 0; d < 3; ++d){
//        num_cells[d] = (unsigned int)(box[d] / grid_size_) * 2;
        num_cells[d] = key[d];
        cell_dimension[d] = box[d] / num_cells[d] * 2;
//        assert(cell_dimension[d] >= reduced_cut_off);
      }
      unsigned int S = num_cells[0] * num_cells[1] * num_cells[2];
      cell_list_ = shared_ptr<CellList>( new CellList(num_atoms, S));
    }

    CellGrid::CellGrid(
        const CellGrid & other):
//        cut_off(other.cut_off),
//        factor(other.factor),
//      cell_list_(shared_ptr<CellList>(new CellList(other.cell_list_)))
        cell_list_(new CellList(* other.cell_list_))
    {
      for(unsigned int d = 0; d < 3; ++d){
        num_cells[d] = other.num_cells[d];
        cell_dimension[d] = other.cell_dimension[d];
      }
    }

    void
    calculate_cell(
        unsigned int cell[],
        const Tuple3 & reduced_pos,
        const Tuple3 & box,
        const Tuple3 & cell_dimension,
        unsigned int dimension){
#ifdef DEBUG
      if (max(reduced_pos) > 1)
      {
        std::stringstream msg;
        msg
          << "calculate_cell() out of bounds! "
          << "new (reduced) position = " << reduced_pos
          ;
        throw RuntimeError(msg.str());
      }
#endif
      const Tuple3 pos = element_product(reduced_pos, box);

      for(unsigned int d = 0; d < dimension; ++d){
#ifdef DEBUG
        if (fabs(pos[d]) > box[d])
        {
          std::stringstream msg;
          msg
            << "calculate_cell() out of bounds! "
            << "pos = " << pos << "; "
            << "box = " << box << "; "
            ;
          throw RuntimeError(msg.str());
        }
#endif
        assert(fabs(pos[d]) <= box[d]);
        //cell[d] = (unsigned int)((box[d] * (pos[d] + 1)) / cell_dimension[d]);
        // TODO restore if necessary
        cell[d] = (unsigned int)((pos[d] + box[d]) / cell_dimension[d]);
      }
/*      std::cerr << "cell of " << pos << " in " << box
                << " with cell dimension " << cell_dimension
                << " is ( ";
      for(unsigned int d = 0; d < dimension; ++d)
        std::cerr << cell[d] << " ";
      std::cerr << ")" << std::endl;*/
    }

    void
    remove_from_cell(
        CellList & cell_list,
        const CellIndex cell_index,
        const ParticleTag & particle)
    {
      cell_list.remove_from_cell(cell_index, particle);
    }

    void
    add_to_cell(
        CellList & cell_list,
        const CellIndex cell_index,
        const ParticleTag & particle)
    {
      cell_list.add_to_cell(cell_index, particle);
    }

    void
    move(
        CellList & cell_list,
        const ParticleTag & particle,
        const CellIndex old_cell_index,
        const CellIndex new_cell_index)
    {
      if (old_cell_index != new_cell_index){
        /*std::cerr << "Moving particle " << particle << " from "
                  << "cell " << old_cell_index << " to cell "
                  << new_cell_index << "." << std::endl;
        std::cerr << "Before move:" << std::endl;
        dump_cell_list(cell_list, std::cout);*/

        cell_list.remove_from_cell(old_cell_index, particle);
        cell_list.add_to_cell(new_cell_index, particle);

        /*std::cerr << "After move:" << std::endl;
        dump_cell_list(cell_list, std::cout);*/
      }
    }

    unsigned int
    calculate_num_cells(
      const ReducedCutOff reduced_cutoff,
      const Resolution & resolution,
      const Tuple3 & box,
      const unsigned int dimension)
    {
      assert(dimension <= 3);
      unsigned int num_cells = 1;
      for(unsigned int d = 0; d < dimension; ++d){
        const Attribute grid_size = reduced_cutoff / resolution[d];
        unsigned int n = (unsigned int)(box[d] / grid_size) * 2;
        if (n == 0)
          ++n;
        num_cells *= n;
      }
      assert(num_cells > 0);
      return num_cells;
    }

    CellGridsKey
    calculate_key(
      const Tuple3 & box,
      const ReducedCutOff reduced_cutoff,
      const Resolution & resolution,
      const unsigned int max_num_cells,
      const unsigned int dimension)
    {
      assert(dimension > 0 && dimension <= 3);
      IntVector factorial = find_prime_numbers(max_num_cells);
      limit_prime_numbers(factorial, dimension);
      unsigned int order[dimension];
      Tuple3 box2(box);
      for(unsigned int d = 0; d < dimension; ++d){
        order[d] = max_index(box2);
        box2[order[d]] = 0;
      }
      int k_max_hard_limit[dimension];
      unsigned int check_num_cells = 0;
      for(unsigned int d = 0; d < dimension; ++d){
        assert((dimension - d - 1) < dimension);
        assert((dimension - d - 1) >= 0);
        assert(factorial[order[dimension - d - 1]] > 0);
        k_max_hard_limit[d] = factorial[order[dimension - d - 1]];
        check_num_cells += k_max_hard_limit[d];
      }
      assert(check_num_cells <= max_num_cells);
      

      CellGridsKey key(dimension);
      for(unsigned int d = 0; d < dimension; ++d){
        const Attribute grid_size = reduced_cutoff / resolution[d];
        int k_max = (int)(box[d] / grid_size) * 2;
        int k_min = 2 * resolution[d] + 1;
        key[d] = std::max(std::min(k_max_hard_limit[d], k_max), k_min);
      }
      return key;
    }

    unsigned int
    calculate_neighbor_list_for_cell_(
        const unsigned int * cell,
        const unsigned int * num_cells,
        const SimulationConfiguration::Resolution & resolution,
        unsigned int * neighbor_list){
      int i[3];
      unsigned int n = 0;
//      std::cerr << "neighbors of cell ";
//      print_array(cell, 3);
      for(i[0] = 0; i[0] < 2 * resolution[0] + 1; ++i[0])
        for(i[1] = 0; i[1] < 2 * resolution[1] + 1; ++i[1])
          for(i[2] = 0; i[2] < 2 * resolution[2] + 1; ++i[2]){
            int n_cell[3];
            unsigned int n_cell_c[3];
            for(int d = 0; d < 3; ++d){
              n_cell[d] = cell[d] + i[d] - resolution[d] + num_cells[d];
              int pbc = (int)(n_cell[d] / num_cells[d]);
              n_cell[d] -= pbc * num_cells[d];
              n_cell_c[d] = (unsigned int) n_cell[d];
              assert(n_cell_c[d] <= num_cells[d]);
            }
//            std::cerr << n << ": ";
//            print_array(n_cell_c, 3);
            neighbor_list[n] = get_cell_index(num_cells, n_cell_c);
            ++n;
          }
//      std::cerr << "n=" << n << std::endl;
      return n;
    }

    unsigned int
    num_neighbors_(
        const Resolution & resolution,
        const unsigned int dimension){
//      return std::pow(2 * factor + 1, dimension) - 1;
//      return std::pow(2 * factor + 1, dimension);
      unsigned int result = 1;
      for(unsigned int d = 0; d < dimension; ++d)
        result *= 2 * resolution[d] + 1;
      return result;
    }

  } // namespace SimulationConfiguration

} // namespace mdst
