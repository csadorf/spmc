// =====================================================================================
//
//       Filename:  units.hpp
//
//    Description:  Unit system for MD-simulations.
//
//        Version:  1.0
//        Created:  11/12/2012 05:31:53 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef UNITS_H_
#define UNITS_H_

#include <boost/units/physical_dimensions.hpp>
#include <boost/units/derived_dimension.hpp>
#include <boost/units/conversion.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/base_units/metric/angstrom.hpp>
#include <string>

namespace mdst{

namespace si = boost::units::si;

namespace units{

namespace u = boost::units;

// TODO
// Cite Matthias Schabel!
// http://search.gmane.org/?author=Matthias+Schabel&sort=date

using boost::units::length_dimension;
using boost::units::area_dimension;
using boost::units::volume_dimension;
using boost::units::mass_dimension;
using boost::units::time_dimension;

using boost::units::derived_dimension;
using boost::units::quantity;
using boost::units::base_unit;
using boost::units::unit;
using boost::units::scaled_base_unit;
using boost::units::scale;
using boost::units::static_rational;


// length
typedef scaled_base_unit<
  si::meter_base_unit,
  scale<10, static_rational<-9> > > nanometer_base_unit;

typedef u::metric::angstrom_base_unit::unit_type angstrom_unit;
BOOST_UNITS_STATIC_CONSTANT(angstrom, angstrom_unit);
BOOST_UNITS_STATIC_CONSTANT(angstroms, angstrom_unit);

// mass
struct amu_base_unit:
  base_unit<amu_base_unit, mass_dimension, 2>
{
  static std::string name() {return "atomic mass unit";}
  static std::string symbol() {return "amu";}
};

// time
typedef scaled_base_unit<
  si::second_base_unit,
  scale<10, static_rational<-12> > > picosecond_base_unit;

// derived dimensions
// bond stretching force dimension
typedef derived_dimension<u::mass_base_dimension,1,
                          u::time_base_dimension,-2>::type bond_stretching_force_dimension;
typedef unit<bond_stretching_force_dimension, si::system> bond_stretching_force_base_unit;

// bond bending force dimension
typedef derived_dimension<u::plane_angle_base_dimension, -2,
                          u::length_base_dimension,2,
                          u::mass_base_dimension,1,
                          u::time_base_dimension,-2>::type bond_bending_force_dimension;
typedef unit<bond_bending_force_dimension, si::system> bond_bending_force_base_unit;

// quantity types
typedef quantity<si::energy> Energy;
typedef quantity<si::plane_angle> PlaneAngle;
typedef quantity<si::time> Time;
typedef quantity<si::volume> Volume;
typedef quantity<si::area> Area;

typedef nanometer_base_unit::unit_type nanometer_unit;
BOOST_UNITS_STATIC_CONSTANT(nanometer, nanometer_unit);
BOOST_UNITS_STATIC_CONSTANT(nanometers, nanometer_unit);
typedef quantity<nanometer_unit> Length;

typedef amu_base_unit::unit_type amu_unit;
BOOST_UNITS_STATIC_CONSTANT(amu, amu_unit);
BOOST_UNITS_STATIC_CONSTANT(dalton, amu_unit);
BOOST_UNITS_STATIC_CONSTANT(daltons, amu_unit);
typedef quantity<amu_unit> Mass;

typedef bond_stretching_force_base_unit::unit_type bond_stretching_force_unit;
typedef quantity<bond_stretching_force_unit> BondStretchingForce;

typedef bond_bending_force_base_unit::unit_type bond_bending_force_unit;
typedef quantity<bond_bending_force_unit> BondBendingForce;

} // namespace units
} // namespace mdst

BOOST_UNITS_DEFINE_CONVERSION_FACTOR(
  mdst::units::amu_base_unit,
  si::kilogram_base_unit,
  double, 1.6605388628e-27);

#endif // UNITS_H_
