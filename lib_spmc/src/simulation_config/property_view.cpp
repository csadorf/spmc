// =====================================================================================
//
//       Filename:  property_view.cpp
//
//    Description:  PropertyView for SimulationConfiguration.
//
//        Version:  1.0
//        Created:  11/27/2012 11:49:37 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "property_view.hpp"

#include "simulation_config.hpp"
#include "misc.hpp"
#include<stack>

namespace mdst{

  namespace SimulationConfiguration{

    Walks
    PropertyView::paths(
        const ParticleTag& parent,
        int depth,
        TagVector* path) const
    {
      Walks result;
      path->push_back(parent);
      int num_of_neighbours = 0;
      if (--depth > -1){
        const TagMapConstRange range = parent_->particle_map_->equal_range(parent);
        for(TagMapConstIterator it = range.first; it != range.second; ++it){
          ++num_of_neighbours;
          if (path->back() == it->second)
            continue;
          // copy path
          TagVector* copied_path = new TagVector(*path);
          Walks collected_paths = paths(it->second, depth, copied_path);
          result.insert(collected_paths.begin(), collected_paths.end());
        }
      }
      if (num_of_neighbours < 2 /*  || depth < 0*/){
        // reached end of path
        result.insert(*path);
        delete path;
      }
      return result;
    }

    TagSet
    PropertyView::neighbourhood(
        const ParticleTag& parent,
        int depth,
        const TagSetPtr& result) const
    {
      if (result->count(parent) > 0)
        return TagSet();
      result->insert(parent);
      if (depth == 1) {
        const TagMapConstRange range = parent_->particle_map_->equal_range(parent);
        for(TagMapConstIterator iter = range.first; iter != range.second; ++iter){
          result->insert(iter->second);
        }
        return *result;
      } else if (depth == -1) {
        std::stack<Tag> stack;
        stack.push(parent);
        while (! stack.empty()) {
          Tag current = stack.top(); stack.pop();
          const TagMapConstRange range = parent_->particle_map_->equal_range(current);
          for(TagMapConstIterator iter = range.first; iter != range.second; ++iter){
            Tag neigh = iter->second;
            if(result->insert(neigh).second) stack.push(neigh);
          }
        }
        return *result;
      }
      // By choosing -1 as start value, this condition is never(!) false
      if (--depth != -1){
        const TagMapConstRange range = parent_->particle_map_->equal_range(parent);
        for(TagMapConstIterator iter = range.first; iter != range.second; ++iter){
          neighbourhood(iter->second, depth, result);
        }
      }
      return *result;
    }

    TagSet
    PropertyView::neighbours(
        const ParticleTag& parent,
        int depth) const
    {
      TagSet result = parent_->property.neighbourhood(parent, depth);
      result.erase(parent);
      return result;
    }

    TagSet
    PropertyView::find_ends(
        const TagSet& tags) const
    {
      if (tags.size() == 1)
        return tags;

      TagSet result;
      for(TagSetConstIterator a = tags.begin();
          a != tags.end(); ++a)
      {
        int num_of_internal_bonds = 0;
        const TagSet neighbours(parent_->property.neighbours(*a, 1));
        for(TagSetConstIterator b = neighbours.begin(); b != neighbours.end(); ++b){
          if(*a == *b) continue;
          if (tags.count(*b) > 0){
            if(++num_of_internal_bonds > 1)
              break;
          }
        }
        if (num_of_internal_bonds == 1)
          result.insert(*a);
      }
      return result;
    }

    TagSet
    PropertyView::molecule(
        const ParticleTag & p) const
    {
      return * molecule_ptr(p);
    }

    shared_ptr<TagSet>
    PropertyView::molecule_ptr(
      const ParticleTag & p) const
    {
      TagSetMapConstIterator ret= parent_->molecule_map_.find(p);
      if (ret == parent_->molecule_map_.end()){
        std::stringstream msg;
        msg << "Did not find molecule of '" << p << "'.";
        throw DataCorruptedError(msg.str());
        return parent_->molecule_map_.find(p)->second;
      } else {
        return parent_->molecule_map_.find(p)->second;
      }
    }

    bool
    PropertyView::bonded(
        const ParticleTag& a,
        const ParticleTag& b) const
    {
      if (a == b)
        return true;
      TagSetMap & molecule_map = parent_->molecule_map_;
      return (molecule_map.find(a)->second->count(b) > 0);
    }

    bool
    PropertyView::bonded(
        const ParticleTag& a,
        const ParticleTag& b,
        unsigned int steps) const
    {
      assert(steps < 4);
      if (steps == 1 && directly_bonded(a, b))
        return true;
      if (steps == 2 && parent_->share_angle(a, b))
        return true;
      if (steps == 3 && parent_->share_dihedral(a, b))
        return true;
      return false;
    }

    TagSetSet
    PropertyView::molecules() const
    {
      TagSetPtrSet const & molecules = parent_->molecules_;
      TagSetSet result;
      for(TagSetPtrSetConstIterator it = molecules.begin();
          it != molecules.end(); ++it)
        result.insert(** it);
      return result;
    }

    typedef std::list<TagVector> WalkList;
    typedef WalkList::const_iterator WalkListConstIterator;

    Walks
    PropertyView::generate_defined_walks(
        const TagSet& molecule) const
    {
      Walks result;
      TagSet ends(find_ends(molecule));
      TagSetConstIterator start = ends.begin();
      if (start != ends.end())
      {
        int i = -1;
        while(parent_->property.add_walk_(molecule, * start, result, ++i));
      }
      return result;
    }

    Walks
    PropertyView::generate_defined_walks() const
    {
      Walks result;
      const TagSetSet molecules = parent_->property.molecules();
      for(TagSetSetConstIterator it = molecules.begin(); it != molecules.end(); ++it){
        Walks new_walks = parent_->property.generate_defined_walks(*it);
        result.insert(new_walks.begin(), new_walks.end());
      }
      return result;
    }

    Walks
    PropertyView::generate_unique_defined_walks(
        const TagSet& molecule) const
    {
      const Walks walks = generate_defined_walks(molecule);
      Walks result;
      TagVector unified_walk;
      for(WalksConstIterator walk = walks.begin(); walk != walks.end(); ++walk){
        // find first unique position
        TagVectorConstIterator it2 = walk->begin();
        for(TagVectorConstIterator it = unified_walk.begin(); it != unified_walk.end(); ++it)
          if(*it != *it2++)
            break;
        // add rest
        unified_walk.insert(unified_walk.end(), it2, walk->end());
        result.insert(TagVector(it2, walk->end()));
      }
      return result;
    }

    Walks
    PropertyView::generate_unique_defined_walks() const
    {
      Walks result;
      const TagSetSet molecules = parent_->property.molecules();
      for(TagSetSetConstIterator it = molecules.begin(); it != molecules.end(); ++it){
        const Walks molecule_walks = generate_unique_defined_walks(*it);
        result.insert(molecule_walks.begin(), molecule_walks.end());
      }
      return result;
    }

    TagVector
    concatenate_walks(const Walks& walks){
      TagVector result;
      // calculate size
      size_t size = 0;
      for(WalksConstIterator walk = walks.begin(); walk != walks.end(); ++walk)
        size += walk->size();
      // concatenate
      for(WalksConstIterator walk = walks.begin(); walk != walks.end(); ++walk)
        result.insert(result.end(), walk->begin(), walk->end());
      return result;
    }

    TagVector
    PropertyView::generate_unique_unified_walk(
        const TagSet& molecule) const
    {
      const Walks unique_walks = generate_unique_defined_walks(molecule);
      return concatenate_walks(unique_walks);
    }


    TagVector
    PropertyView::generate_unique_unified_walk() const
    {
      const Walks unique_walks = generate_unique_defined_walks();
      return concatenate_walks(unique_walks);
    }

    bool
    PropertyView::add_walk_(
        const TagSet& molecule,
        const ParticleTag& start,
        Walks& walks,
        int path) const
    {
      TagVector new_walk;
      ParticleTag next = start;
      ParticleTag last = next;
      bool more_paths = false;
      while(true){
        new_walk.push_back(next);
        TagSet neighbours = parent_->property.neighbours(next);
        neighbours.erase(last);
        last = next;
        if (neighbours.empty()){
          break;
        } else {
          if(neighbours.size() > 1){
          const TagVector sorted_neighbours = parent_->sort_by_angle_(next, neighbours);
          more_paths = (int)sorted_neighbours.size() > (path + 1);
          next = sorted_neighbours[std::max(--path, 0)];
          } else {
            next = *neighbours.begin();
          }
        }
      }
      walks.insert(new_walk);
      // If path < 0, that means there are walks available, which we did not go.
      return more_paths;
    }

    bool
    PropertyView::directly_bonded(
        const ParticleTag& a,
        const ParticleTag& b) const
    {
      return parent_->directly_bonded(a, b);
    }

    bool
    PropertyView::directly_bonded(
        const ParticleTag& a,
        const ParticleTag& b,
        const ParticleTag& c,
        const ParticleTag& d) const
    {
      return (parent_->determine_bond(a, b, c, d) != -1);
    }

    TagSet
    PropertyView::n_bonds_(
        const ParticleTag& particle,
        const unsigned int n) const
    {
      TagSet result;
      const TagMapConstRange range = parent_->reversed_n_bonds_(n).equal_range(particle);
      for(TagMapConstIterator it = range.first; it != range.second; ++it)
        result.insert(it->second);
      return result;
    }

    bool
    PropertyView::in_n_bond_(
        const TagSet& tags,
        const unsigned int n) const
    {
      if (tags.size() > n || tags.empty()){
        std::stringstream error;
        error << "SimulationConfig::PropertyView::in_n_bond_";
        error << "(const TagSet& tags, const unsigned int n): ";
        error << "n must be <= tags.size() and > 0!";
        dump_tagset(tags, error);
        throw std::invalid_argument(error.str());
      }
      const TagSet compare_bond_tags = n_bonds_(*tags.begin(), n);
      for(TagSetConstIterator it = ++tags.begin(); it != tags.end(); ++it){
        const TagSet bond_tags = n_bonds_(*it, n);
        TagVector intersection(std::min(compare_bond_tags.size(), bond_tags.size()));
        TagVectorIterator intersection_end =
            std::set_intersection(compare_bond_tags.begin(),
                                  compare_bond_tags.end(),
                                  bond_tags.begin(),
                                  bond_tags.end(),
                                  intersection.begin());
        if (int(intersection_end - intersection.begin()) == 0)
          return false;
      }
      return true;
    }
 
    // Warning: (csa) Function implementation is not multi-thread safe!
    TupleMap
    PropertyView::remap(
        const ParticleTag& tag,
        const ParticleTag& caller,
        bool init) const
    {
      std::cerr << "'TupleMap (PropertyView::remap)(const ParticleTag &, "
                << "ParticleTag &, bool) const' is deprecated." << std::endl;
      assert(false);
      static TupleMap result;
      if (init)
        result.clear();
      else
        if (result.count(tag) > 0)
          return TupleMap();
      result[tag] = parent_->particle(tag).position(); 
      //result[tag] = parent_->positions_->at(tag);
      const Tuple3 & pos_tag = result[tag];
      // Remap tag if there is a caller
      if ( caller >= 0){
        const Tuple3 & box_l = parent_->box();
        Tuple3 & pos_caller = result[caller];
        Tuple3 delta = pos_tag - pos_caller;
        Tuple3 new_pos;
        for (int i = 0; i < 3; ++i){
          if (delta[i] > (box_l[i] / 2))
            new_pos[i] = pos_tag[i] - box_l[i];
          else
            if (delta[i] < (- box_l[i] / 2))
              new_pos[i] = pos_tag[i] + box_l[i];
            else{
              new_pos[i] = pos_tag[i];
          }
        }
        std::cerr << "new pos " << new_pos << std::endl;
        result[tag] = new_pos;
      }
      // Call function recursively for every bonded particle
      TagMapConstRange range(parent_->particle_map_->equal_range(tag));
      for(TagMapConstIterator iter = range.first; iter != range.second; ++iter)
        remap(iter->second, tag, init = false);
      return result;
    }

    Attribute
    PropertyView::distance(
        const ParticleTag& a,
        const ParticleTag& b) const
    {
      std::cerr << "'Attribute (PropertyView::distance)(const ParticleTag &, "
                << "const ParticleTag &) const' is deprecated." << std::endl;
      assert(false);
      return abs(parent_->particle(b).position() -
                 parent_->particle(a).position());
    }

    Tuples
    PropertyView::expanded_positions() const
    {
      const Tuple3 box = parent_->box();
      Tuples const & positions = * parent_->positions_;
      const size_t N = positions.size();
      Tuples result(N);
      for(size_t i = 0; i < N; ++i)
        result[i] = element_product(box, positions[i]);
      return result;
    }

  } // namespace SimulationConfiguration

} // namespace mdst
