// =====================================================================================
//
//       Filename:  view.hpp
//
//    Description:  Views for SimulationConfigurations.
//
//        Version:  1.0
//        Created:  11/27/2012 11:43:37 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_VIEW_H_
#define MDST_VIEW_H_

#include "types.hpp"
#include "tuple3.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    class SimulationConfig;

    class View{
      public:
        View(SimulationConfig const * parent):
          parent_(parent){};

      protected:
        const SimulationConfig * parent_;
    };

    class IndexedView : public View{
      public:
        IndexedView(
          SimulationConfig const * parent,
          const int index)
#ifndef DEBUG
        :
         View(parent),
         index_(index)
         {}
#endif
         ;

        bool operator==(const IndexedView &rhs) const {return (index_ == rhs.index_);};
        bool operator!=(const IndexedView &rhs) const { return (index_ != rhs.index_);};
        bool operator<(const IndexedView &rhs) const { return (index_ < rhs.index_);};
        bool operator<=(const IndexedView &rhs) const { return (index_ <= rhs.index_);};
        bool operator>(const IndexedView &rhs) const { return (index_ > rhs.index_);};
        bool operator>=(const IndexedView &rhs) const { return (index_ >= rhs.index_);};

      protected:
        const Tag index_;
    };

    class NBondView : public IndexedView{
     public:
      NBondView(SimulationConfig const * parent,
                const NBondTag& index,
                TagMap & map,
                Types & types,
                TagVectorMap & sorted_tags,
                int N);
      Type type() const {return types_.at(index_);}
      void set_type(const Type & type) {types_[index_] = type;}
      std::string dump() const;
      TagSet tags() const;
      TagVector sorted_tags() const;
      NBondView & operator=(const NBondView& rhs);

     protected:
      TagMapIterator particle(const Tag&);
      const TagMapConstIterator particle(const Tag&) const;
      Tag x(const int order) const { return x_iter(order)->second;};
      bool set_x(int order, const Tag & new_tag);

     private:
      // private functions
      TagMapConstIterator x_iter(const int order) const;
      TagMapIterator x_iter(const int order);
      // private data members
      TagMap & map_;
      Types & types_;
      TagVectorMap & sorted_tags_;
      const int N_;
    };

    class BondView : public NBondView{
     public:
      BondView(SimulationConfig const * parent, const int index);
      Tag  a() const { return x(0);}
      bool set_a( const Tag& new_tag) { return set_x(0, new_tag);}
      Tag  b() const { return x(1);}
      bool set_b( const Tag& new_tag) { return set_x(1, new_tag);}
    };

    class AngleView : public NBondView{
     public:
      AngleView(SimulationConfig const * parent, const int index);
      Tag  a() const { return x(0);}
      bool set_a( const Tag& new_tag) { return set_x(0, new_tag);}
      Tag  b() const { return x(1);}
      bool set_b( const Tag& new_tag) { return set_x(1, new_tag);}
      Tag  c() const { return x(2);}
      bool set_c( const Tag& new_tag) { return set_x(2, new_tag);}
    };

    class DihedralView : public NBondView{
     public:
      DihedralView(SimulationConfig const * parent, const int index);
      Tag  a() const { return x(0);};
      bool set_a( const Tag& new_tag) { return set_x(0, new_tag);}
      Tag  b() const { return x(1);};
      bool set_b( const Tag& new_tag) { return set_x(1, new_tag);}
      Tag  c() const { return x(2);};
      bool set_c( const Tag& new_tag) { return set_x(2, new_tag);}
      Tag  d() const { return x(3);};
      bool set_d( const Tag& new_tag) { return set_x(3, new_tag);}
    };

    class ParticleView : public IndexedView{
     public:
      ParticleView(SimulationConfig const * parent, const int index):
        IndexedView(parent, index){};
      Type type() const;
      void set_type(const Type& type);
      int int_type() const;
      Attribute mass() const;
      void set_mass(const Attribute& mass){mass_() = mass;}
      Attribute charge() const;
      void set_charge(const Attribute& charge){charge_() = charge;}
      Tuple3 position() const;
      void set_position(const Tuple3& position);
      Tuple3 remapped_position() const;
      Tuple3 reduced_position() const;
      void set_reduced_position(const Tuple3 & reduced_position);
      Tuple3 velocity() const;
      void set_velocity(const Tuple3& velocity){velocity_() = velocity;}
      Tuple3 acceleration() const;
      void set_acceleration(const Tuple3& acceleration){acceleration_() = acceleration;}
      Tuple3 image() const;
      void set_image(const Tuple3& image){image_() = image;}
      void reset_image(){image_() = Tuple3(0);}

      void clone(const ParticleView & other);
     private:
      Type &type_();
      int &int_type_();
      Attribute &mass_();
      Attribute &charge_();
      Tuple3 &position_();
      Tuple3 &velocity_();
      Tuple3 & acceleration_();
      Tuple3 & image_();

      bool check_lock_();
      void set_reduced_position_(const Tuple3 & reduced_position);
    };

/*    class IterableView : public View{
     public:
      IterableView(SimulationConfig const * parent): View(parent){};
      const Iterable<Types> type() const
        { return Iterable<Types>(*(parent_->types_));};
      const Iterable<Attributes> mass() const
        { return Iterable<Attributes>(*(parent_->masses_));};
      const Iterable<Attributes> charge() const
        { return Iterable<Attributes>(*(parent_->charges_));};
      const Iterable<Tuples> position() const
        { return Iterable<Tuples>(*(parent_->positions_));};
      const Iterable<Tuples> velocity() const
        { return Iterable<Tuples>(*(parent_->velocities_));};
      const Iterable<Tuples> acceleration() const
        { return Iterable<Tuples>(*(parent_->accelerations_));};
    };*/
    
  } // namespace SimulationConfiguration

} // namespace mdst

#endif // MDST_VIEW_H_
