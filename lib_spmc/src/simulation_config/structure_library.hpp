// =====================================================================================
//
//       Filename:  structure_libray.hpp
//
//    Description:  Instantiate HasedLibrary for morphological structures.
//
//        Version:  1.0
//        Created:  04/04/2014
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_STRUCTURE_LIBRARY_H_
#define MDST_STRUCTURE_LIBRARY_H_

#include <types.hpp>
#include <utility/tuple3.hpp>
#include <utility/hashed_library.hpp>
#include <exception.hpp>
#include <cstring>
//#include <array>
#include <boost/array.hpp>
#include <numeric>

extern "C"{
#include "qcprot.h"
}

namespace mdst{

  namespace SimulationConfiguration{

    template<
      class T,
      class RandomAccessIterator,
      std::size_t dim
      >
    class InnerProduct{
     private:
      typedef boost::array<T, dim * dim> Matrix_;
     public:
      T
      operator()(
        Matrix_ & A,
        RandomAccessIterator a,
        RandomAccessIterator b,
        const size_t n
        ) const
      {
        for(size_t i = 0; i < dim; ++i)
          for(size_t j = 0; j < dim; ++j)
            A[i * dim + j] = std::inner_product(a + i * n, a + (i + 1) * n, b + j * n, 0.0);
        const T G1 = std::inner_product(a, a + dim * n, a, 0.0);
        const T G2 = std::inner_product(b, b + dim * n, b, 0.0);
        return 0.5 * (G1 + G2);
      }
    };

    template<
      class T,
      std::size_t dim
      >
    class Structure{
     private:
      typedef std::vector<T> Data_;
      Data_ data_;
      T * matrix_[dim];
      InnerProduct<T, typename Data_::const_iterator, dim> inner_product_;

      void
      init_matrix_(){
        const size_t n_ = n();
        for(size_t i = 0; i < dim; ++i)
          matrix_[i] = data_.data() + (i * n_);
      }
      
     public:

      typedef boost::array<T, dim> Column;
      typedef boost::array<T, dim * dim> RotationalMatrix;

      Structure():
        data_(0)
      {
        init_matrix_();
      }

      Structure(
        const size_t n):
        data_(n * dim)
      {
        init_matrix_();
      }

      Structure(
        Tuples::const_iterator first,
        Tuples::const_iterator last):
        data_((last - first) * dim)
      {
        init_matrix_();
        const size_t n_ = n();
        for(size_t i = 0; i < n_; ++i)
        {
          for(size_t j = 0; j < dim; ++j)
            matrix_[j][i] = (*first)[j];
          ++first;
        }
      }

      Structure(
        const Structure & rhs):
        data_(rhs.data_.begin(), rhs.data_.end())
      {
        init_matrix_();
      }

      template<class ForwardIterator>
      void
      from_tuples(
        ForwardIterator first,
        ForwardIterator last);

      size_t
      m() const {return dim;}

      size_t
      n() const
      {
        return data_.size() / dim;
      }

      T ** matrix()
      {
        return matrix_;
      }

      T
      matrix(
        const size_t i,
        const size_t j) const
      {
        return data_[i * n() + j];
      }

      void
      matrix_set(
        const size_t i,
        const size_t j,
        const T & value)
      {
        data_[i * n() + j] = value;
      }

      void
      rotate(
        const RotationalMatrix & rm)
      {
        Attribute ** mx = matrix_;
        const size_t n_ = n();
        for(size_t i = 0; i < n_; ++i)
        {
          mx[0][i] = rm[0] * mx[0][i] + rm[1] * mx[1][i] + rm[2] * mx[2][i];
          mx[1][i] = rm[3] * mx[0][i] + rm[4] * mx[1][i] + rm[5] * mx[2][i];
          mx[2][i] = rm[6] * mx[0][i] + rm[7] * mx[1][i] + rm[8] * mx[2][i];
        }
      }

      Column
      calculate_center() const
      {
        Column result;
        const size_t n_ = n();
        for(size_t i = 0; i < dim; ++i)
          result[i] = std::accumulate(data_.begin() + i * n_, data_.begin() + (i +1 ) * n_, 0.0) / T(n_);
        return result;
      }

      // TODO, DEBUG FUNCTION, delete later
      void
      dump_center() const
      {
        Column center(calculate_center());
        std::cerr << "(";
        for(size_t i = 0; i < dim; ++i)
          std::cerr << center[i] << " ";
        std::cerr << ")" << std::endl;
      }

      Structure
      centered() const
      {
        Structure ret(* this);
        ret.center();
        return ret;
      }

      void
      center()
      {
        Column center(calculate_center());
        const size_t n_ = n();
        for(size_t i = 0; i < dim; ++i)
          for(size_t j = 0; j < n_; ++j)
            data_[i * n_ + j] -= center[i];
      }

      T
      calculate_rmsd(
        const Structure & rhs) const
      {
        RotationalMatrix rot_mat;
        return calculate_rmsd_rotational_matrix(rhs, rot_mat);
      }

      T
      calculate_rmsd_rotational_matrix(
        const Structure & rhs,
        RotationalMatrix & rot_mat) const
      {
        RotationalMatrix A;
        const T E0 = inner_product_(A, data_.begin(), rhs.data_.begin(), n());
        T rmsd;
        FastCalcRMSDAndRotation(
          rot_mat.data(),
          A.data(),
          & rmsd,
          E0,
          n(),
          -1);
        return rmsd;
      }

      void
      check()
      {
        init_matrix_();
        const size_t n_(n());
        const size_t m_(m());
        for(size_t i = 0; i < n_; ++i)
          for(size_t j = 0; j < m_; ++j)
            std::cerr
              << "data(" << i << ", " << j << ") = "
              << matrix_[j][i]
              << std::endl
              ;
      }

      void
      dump() const
      {
        std::cerr
          << "Dumping " << m() << "x" << n()
          << " matrix.." 
          << std::endl
          ;
        if (matrix_ != NULL)
        {
          const size_t m_ = m();
          const size_t n_ = n();
          std::cerr << "[" << std::endl;
          for(size_t i = 0; i < n_; ++i)
          {
            std::cerr << "[ ";
            for(size_t j = 0; j < m_; ++j)
            {
              assert(matrix_[j]);
              std::cerr << matrix_[j][i] << " ";
            }
            std::cerr << "]" << std::endl;
          }
          std::cerr << "]" << std::endl;
        }
      }
    };

    template<
      class T,
      std::size_t dim>
    template<
      class ForwardIterator>
    void
    Structure<T, dim>::from_tuples(
      ForwardIterator first,
      ForwardIterator last)
    {
      const size_t n_ = n();
      size_t j = 0;
      while(first != last)
      {
        assert(j <= n_);
        for(size_t i = 0; i < dim; ++i)
          data_[i * n_ + j] = (*first)[i];
        ++j;
        ++first;
      }
    }

  } // namespace SimulationConfiguration

} // namespace mdst


#endif //MDST_STRUCTURE_LIBRARY_H_
