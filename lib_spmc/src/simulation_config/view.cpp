// =====================================================================================
//
//       Filename:  view.cpp
//
//    Description:  Views for SimulationConfigurations.
//
//        Version:  1.0
//        Created:  11/27/2012 11:43:37 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <utility/tuple3_arithmetic.hpp>
#include <utility/exception.hpp>
#include "view.hpp"
#include "simulation_config.hpp"

namespace mdst{

  namespace SimulationConfiguration{

#ifdef DEBUG
      IndexedView::IndexedView(
        SimulationConfig const * parent,
        const int index):
       View(parent),
       index_(index)
      {
        if (index < 0 or index > int(parent->num_atoms())){
          std::stringstream msg;
          msg
            << "ParticleView: index out of bounds! "
            << "Index=" << index << "; "
            << "Should be between 0 and " << parent->num_atoms() << " !"
            ;
          throw std::out_of_range(msg.str());
        }
      }
#endif

      NBondView::NBondView(SimulationConfig const * parent,
                const NBondTag& index,
                TagMap & map,
                Types & types,
                TagVectorMap & sorted_tags,
                int N):
         IndexedView(parent, index),
         map_(map),
         types_(types),
         sorted_tags_(sorted_tags),
         N_(N){
        if (static_cast<unsigned int>(index) > types.size()){
          std::stringstream error;
          error << "Index of NBondView is out of range.";
          error << " (index = " << index << " number of bonds = " << types.size() << ")";
          throw std::out_of_range(error.str());
        }
      }

      //  Return the order'th entry of a bond.
      //  Note: The order of tags to be appear in a bond is undetermined
      //        but remains constant as long as no bond in configuration
      //        is changed.
      TagMapConstIterator
      NBondView::x_iter(const int order) const{
        TagMapConstIterator iter = map_.lower_bound(index_);
        for(int i = 0; i < order; ++i)
          ++iter;
        return iter;
      }

      TagSet
      NBondView::tags() const{
        TagSet result;
        TagMapConstIterator first = map_.lower_bound(index_);
        TagMapConstIterator last = map_.upper_bound(index_);
        for(TagMapConstIterator it = first; it != last; ++it)
          result.insert(it->second);
        return result;
      }

      // non_const version
      TagMapIterator
      NBondView::x_iter(const int order){
        TagMapIterator iter = map_.lower_bound(index_);
        for(int i = 0; i < order; ++i)
          ++iter;
        return iter;
      }

      // TODO (csa) Warning: This method is not(!) exception-safe!
      // Change the order'th entry of a bond. The following rules apply:
      //  - Every particle tag in a bond is unique.
      //  - The tag has to be valid.
      //  The function will return false, in case the modification was not
      //  successful.
      bool
      NBondView::set_x( int order, const Tag & new_tag){
        // Check if order is valid
        if (order >= N_)
          return false;
        // Check if tag is valid
        if (new_tag >= ParticleTag(parent_->num_atoms()))
          return false;
        // Check if tag is unique in current bond
        TagMapConstIterator iter = x_iter(0);
        for (int i = 0; i < N_; ++i)
          if (iter->second == new_tag)
            return false;
        // new_tag is valid
        TagMapIterator replace = x_iter(order);
        //Tag old_tag = replace->second;
        replace->second = new_tag;
        parent_->update_internal_();
        return true;
      }

      // Const version of particle()
      const TagMapConstIterator
      NBondView::particle( const Tag & tag) const{
        TagMapConstRange range = map_.equal_range(tag);
        for(TagMapConstIterator iter = range.first; iter != range.second; ++iter)
          if (iter->second == index_)
            return iter;
        return map_.end(); 
      }

      std::string
      NBondView::dump() const{
        std::stringstream s;
        s << "index: " << index_ << " ";
        s << type();
        TagMapConstIterator iter = map_.lower_bound(index_);
        while (iter != map_.upper_bound(index_))
          s << " " << (iter++)->second;
        return s.str();
      }

      TagVector
      NBondView::sorted_tags() const{
        return sorted_tags_[index_];
      }

      BondView::BondView(SimulationConfig const * parent, const int index):
        NBondView(parent,
                  index,
                  *(parent->bonds_),
                  *(parent->bond_types_),
                  *(parent->bonds_sorted_),
                  2)
      {}

      AngleView::AngleView(SimulationConfig const * parent, const int index):
        NBondView(parent,
                  index,
                  *(parent->angles_),
                  *(parent->angle_types_),
                  *(parent->angles_sorted_),
                  3)
      {}

      DihedralView::DihedralView(SimulationConfig const * parent, const int index):
        NBondView(parent,
                  index,
                  *(parent->dihedrals_),
                  *(parent->dihedral_types_),
                  *(parent->dihedrals_sorted_),
                  4)
      {}

      Type
      ParticleView::type() const{
        return parent_->types_->operator[](index_);
      }

      void
      ParticleView::set_type(const Type &type){
        type_() = type; 
        int_type_() = const_cast<SimulationConfig*>(parent_)->map_type_to_int_type_(type); 
      }

      int
      ParticleView::int_type() const{
        return parent_->int_types_->operator[](index_);
      }

      Attribute
      ParticleView::mass() const{
        return parent_->masses_->operator[](index_);
      }

      Attribute
      ParticleView::charge() const{
        return parent_->charges_->operator[](index_);
      }

      Tuple3
      ParticleView::position() const{
        return element_product(parent_->positions_->operator[](index_), parent_->box_);
      }

      void
      ParticleView::set_position(
          const Tuple3 & new_position)
      {
        if (check_lock_())
          return;
        Tuple3 & i = image_();
        //Tuple3 & p = position_();
        assert(
          parent_->box_[0] != 0 &&
          parent_->box_[1] != 0 &&
          parent_->box_[2] != 0);
        i = Tuple3(
          floor(new_position[0] / (parent_->box_[0])),
          floor(new_position[1] / (parent_->box_[1])),
          floor(new_position[2] / (parent_->box_[2])));
        const Tuple3 new_p = element_division(new_position, parent_->box_) - i;
#if defined(DEBUG) || defined(CHECK_POSITIONS)
        if (new_p != new_p || i != i)
        {
          std::stringstream msg;
          msg
            << "Calculated position is 'nan'! "
            << "New position argument: " << new_position << ", "
            << "ParticleTag: " << index_ << ", "
            << "old position: " << position_() << ", "
            << "new_position: " << new_p << ", "
            //<< "old image: " << image_() << ", "
            << "new image: " << i << ", "
            << "box: " << parent_->box_ 
            ;
          throw DataCorruptedError(msg.str());
        }
#endif
        assert(new_p == new_p && i == i);
        set_reduced_position_(new_p);
        //set_reduced_position(element_division(new_position, parent_->box_) - i);
      }

      Tuple3
      ParticleView::remapped_position() const{
        return element_product(
          parent_->positions_->operator[](index_) + parent_->images_->operator[](index_),
          parent_->box_);
      }

      Tuple3
      ParticleView::reduced_position() const
      {
        return parent_->positions_->operator[](index_);
      }

      bool
      ParticleView::check_lock_()
      {
        if (parent_->particles_locked_->size() > 0)
        {
          if (parent_->particles_locked_->count(index_))
          {
            if (parent_->particles_locked_silent_) {
              return true;
            } else {
              std::stringstream msg;
              msg
                << "Particle " << index_ << " is locked. "
                << "Its position cannot be changed!"
                ;
              throw RuntimeWarning(msg.str());
            }
          }
        }
        return false;
      }

      void
      ParticleView::set_reduced_position(
        const Tuple3 & reduced_position)
      {
        if (check_lock_())
          return;
        set_reduced_position_(reduced_position);
      }

      void
      ParticleView::set_reduced_position_(
        const Tuple3 & reduced_position)
      {
#ifdef DEBUG
        if (max(reduced_position) > 1)
        {
          std::stringstream msg;
          msg
            << "Data corrupted. New position out of bounds! "
            << "new positions = " << reduced_position
            ;
          throw DataCorruptedError(msg.str());
        }
        if (reduced_position != reduced_position)
        {
          std::stringstream msg;
          msg
            << "Data corrupted. New position for particle " << index_
            << " is not a number."
            ;
          throw DataCorruptedError(msg.str());
        }
#endif
        assert(reduced_position == reduced_position);
        assert(max(reduced_position) <= 1);
//        parent_->cell_index_dirty_.insert(std::make_pair(index_, position_()));
        {
          int pos = parent_->cell_index_dirty_sparse_[index_];
          if (pos == -1) {
            int count = parent_->cell_index_dirty_count_;
            parent_->cell_index_dirty_dense_[count] = std::make_pair(index_, position_());
            parent_->cell_index_dirty_sparse_[index_] = count;
            parent_->cell_index_dirty_count_ += 1;
          }
        }
        parent_->neighbor_list_max_displacement_ = 
          std::max(abs_squared(element_product(reduced_position - position_(), parent_->box_)),
                    parent_->neighbor_list_max_displacement_);
        position_() = reduced_position;
      }

      Tuple3
      ParticleView::velocity() const{
        return parent_->velocities_->operator[](index_);
      }

      Tuple3
      ParticleView::acceleration() const{
        return parent_->accelerations_->operator[](index_);
      }

      Tuple3
      ParticleView::image() const{
        return parent_->images_->operator[](index_);
      }

      Attribute&
      ParticleView::mass_(){
        return parent_->masses_->operator[](index_);
      }

      Attribute&
      ParticleView::charge_(){
        return parent_->charges_->operator[](index_);
      }

      Tuple3 &
      ParticleView::position_(){
#ifdef DEBUG
        if (max(parent_->positions_->operator[](index_)) > 1)
        {
          std::stringstream msg;
          msg
            << "position_(): "
            << "position of particle " << index_ << " is out ouf bounds. "
            << "position = " << parent_->positions_->at(index_)
            ;
          throw DataCorruptedError(msg.str());
        }
#endif
        return parent_->positions_->operator[](index_);
      }

      Tuple3 &
      ParticleView::velocity_(){
        return parent_->velocities_->operator[](index_);
      }

      Tuple3 &
      ParticleView::acceleration_(){
        return parent_->accelerations_->operator[](index_);
      }

      Tuple3 &
      ParticleView::image_(){
        return parent_->images_->operator[](index_);
      }
      
      Type &
      ParticleView::type_(){
        return parent_->types_->operator[](index_);
      }

      int &
      ParticleView::int_type_(){
        return parent_->int_types_->operator[](index_);
      }

      void
      ParticleView::clone(ParticleView const &other){
        type_() = ParticleType(other.type());
        int_type_() = int(other.int_type());
        mass_() = Attribute(other.mass());
        charge_() = Attribute(other.charge());
        position_() = other.reduced_position();
        velocity_() = Tuple3(other.velocity());
        acceleration_() = Tuple3(other.acceleration());
        image_() = Tuple3(other.image());
      }

  } // SimulationConfiguration

} // namespace mdst
