//=====================================================================================
//
//       Filename:  cell_list.cpp
//
//    Description:  Save cell_list content.
//
//        Version:  1.0
//        Created:  06/21/2014 
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "cell_list.hpp"
#include <iostream>
#include <utility/exception.hpp>

namespace mdst{

  namespace SimulationConfiguration{

    CellList::CellList(
        size_t num_particles_,
        size_t num_cells)
    {
//      clear();
    }

    void
    CellList::clear()
    {
      map_.clear();
    }

    void
    CellList::construct_from_map(
      const CellListMap & map)
    {
      clear();
      map_ = map;
    }

    CellList::CellList(
        const CellList & other)
    {
      map_ = other.map_;
    }

    bool
    CellList::sanity_check() const
    {
      return true;
      throw NotImplementedError("sanity check");
    }

    void
    CellList::add_to_cell(
        const CellIndex cell_index,
        const ParticleTag particle)
    {
      map_.insert(std::make_pair(cell_index, particle));
    }

    void
    CellList::remove_from_cell(
        const CellIndex cell_index,
        const ParticleTag particle)
    {
      CellListMapRange range(equal_range(cell_index));
      CellListMapIterator it = range.first;
      for(; it != range.second; ++it)
      {
        if (it->second == particle)
          break;
      }
      if (it != range.second)
        map_.erase(it);
      else
        throw RuntimeWarning("Tried to erase particle from cell, but not in cell.");
    }

  } // namespace SimulationConfiguration

} // namespace mdst
