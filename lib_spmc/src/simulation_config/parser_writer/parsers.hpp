// =====================================================================================
//
//       Filename:  parsers.hpp
//
//    Description:  Get all registered parsers.
//
//        Version:  1.0
//        Creaated: 03/07/2013 10:14:00 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_PARSERS_H_
#define MDST_PARSERS_H_

#include <memory.hpp>
#include <string>

// all registered parsers
//#include <parser_writers/parser_writer.hpp>
#include <misc.hpp>

#include <parser_writer/hoomd_blue_xml.hpp>
#include <parser_writer/xyz.hpp>
//namespace mdst{ class Strings{};}

namespace mdst{

  namespace SimulationConfiguration{

    namespace ParserWriter{

      class Parser;

      inline shared_ptr<Parser>
      get_parser(
          const std::string format){
        if (format == "hoomd_blue_xml"){
          return shared_ptr<Parser>(new HoomdBlueXml());
        } else if (format == "xyz"){
          return shared_ptr<Parser>(new XYZ());
        } else {
          return shared_ptr<Parser>();
        }
      }

    } // namespace ParserWriter

  } // namespace SimulationConfiguration
    
} // namespace mdst

#endif // MDST_PARSERS_H_
