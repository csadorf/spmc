// =====================================================================================
//
//       Filename:  writers.hpp
//
//    Description:  Get all registered writers.
//
//        Version:  1.0
//        Creaated: 03/07/2013 10:14:00 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_WRITERS_H_
#define MDST_WRITERS_H_

#include <memory.hpp>
#include <string>

// all registered writers
#include "hoomd_blue_xml.hpp"
#include "xyz.hpp"

namespace mdst{
  
  namespace SimulationConfiguration{

    namespace ParserWriter{

      inline shared_ptr<Writer>
      get_writer(
          const std::string format){
        if (format == "hoomd_blue_xml"){
          return shared_ptr<Writer>(new HoomdBlueXml());
        } else if (format == "xyz"){
          return shared_ptr<Writer>(new XYZ());
        } else {
          return shared_ptr<Writer>();
        }
      }

      inline Strings
      available_output_formats()
      {
        Strings result;
        result.push_back("hoomd_blue_xml");
        result.push_back("xyz");
        return result;
      }

      inline std::ostream &
      dump_xyz(
        const SimulationConfig & config,
        const std::string & format = "xyz",
        std::ostream & stream = std::cerr)
      {
        get_writer(format)->write(config, stream);
        return stream;
      }

    } // namespace ParserWriter

  } // namespace SimulationConfiguration
    
} // namespace mdst

#endif // MDST_WRITERS_H_
