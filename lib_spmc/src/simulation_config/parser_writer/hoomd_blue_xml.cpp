// =====================================================================================
//
//       Filename:  hoomd_blue_xml.cpp
//
//    Description:  Parse and dump xml files in hoomd blue format.
//
//        Version:  1.0
//        Created:  10/21/2012 06:46:27 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "hoomd_blue_xml.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "pugixml.hpp"
#include "tuple3.hpp"
#include "tuple3_arithmetic.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    namespace ParserWriter{

      using SimulationConfiguration::SimulationConfig;

      // obsolete
      std::vector<std::string>
      vector_split(const std::string & str, const std::string & delimiters){
        char buffer[str.size()];
        // Copy str into buffer
        sprintf(buffer, "%s", str.c_str());
        // Prepare the return vector
        std::vector<std::string> list;
        char * pch = strtok(buffer, delimiters.c_str());
        while (pch != NULL){
          list.push_back(std::string(pch));
          pch = strtok(buffer, delimiters.c_str());
        }
        return list;
      }

      std::istream&
      operator>>(std::istream& is, Tuple3 & t){
        Attribute a0, a1, a2;
        is >> a0 >> a1 >> a2;
        t.set_x(a0);
        t.set_y(a1);
        t.set_z(a2);
        return is;
      }

      template <class T, class C>
      int
      read_lines_(const shared_ptr<C> & container, std::stringstream & ss){
        T buffer;
        while(! ss.eof()){
          ss >> buffer;
          if (! ss.fail())
            container->push_back(buffer);
        }
        return container->size();
      }

      template <class C>
      std::string
      text(const shared_ptr<C> & container){
        std::stringstream ss;
        for(typename C::const_iterator iter = container->begin(); iter != container->end(); ++iter){
          typename C::value_type t = *iter;
          ss << boost::tuples::set_open(' ') << boost::tuples::set_close(' ') << std::setprecision(DEFAULT_PRECISION) << t << std::endl;
        }
        return ss.str();
      }

      template <class InputIterator>
      std::string
      text(
        InputIterator first,
        InputIterator last)
      {
        typedef std::numeric_limits<Attribute> atr;
        std::stringstream ss;
        while(first != last){
          ss
            << std::setprecision(atr::digits10)
            << boost::tuples::set_open(' ') << boost::tuples::set_close(' ')
            << * first++ << std::endl;
        }
        return ss.str();
      }

      void
      HoomdBlueXml::assert_(bool test) const{
        if (!test){
          throw ParseException("Parsing assertion failed.");
        }
      }

      bool
      HoomdBlueXml::process_result_(const pugi::xml_parse_result& result) const{
       if (! result){
        throw ParseException(result.description());
        return false;
       } else {
        return true;
        }
      }

      shared_ptr<SimulationConfig>
      HoomdBlueXml::parse(const std::string & filename) const{
        pugi::xml_document doc;
        if (process_result_(doc.load_file(filename.c_str())))
          return parse_(doc);
        else
          return shared_ptr<SimulationConfig>(new SimulationConfig());
      }

      shared_ptr<SimulationConfig>
      HoomdBlueXml::parse(std::istream& stream) const{
        pugi::xml_document doc;
        if (process_result_(doc.load(stream)))
          return parse_(doc);
        else
          return shared_ptr<SimulationConfig>(new SimulationConfig());
      }

      unsigned int
      HoomdBlueXml::determine_step(const std::string & filename) const{
        pugi::xml_document doc;
        if (process_result_(doc.load_file(filename.c_str())))
          return determine_step_(doc);
        else
          return 0;
      }

      unsigned int
      HoomdBlueXml::determine_step(std::istream& stream) const{
        pugi::xml_document doc;
        if (process_result_(doc.load(stream)))
          return determine_step_(doc);
        else
          return 0;
      }

      unsigned int
      HoomdBlueXml::determine_step_(const pugi::xml_document & doc) const{
        pugi::xml_node config_node = doc.select_single_node("hoomd_xml/configuration").node();
        return config_node.attribute("time_step").as_int();
      }

      void
      HoomdBlueXml::reduce_positions(
        SimulationConfig & config) const
      {
        Tuples & positions_ = *(config.positions_);
        const size_t N = positions_.size();
        const Tuple3 box = config.box();
        if (box == Tuple3(0))
          return;
        for(size_t i = 0; i < N; ++i){
          static Tuple3 new_pos;
          new_pos = element_division(positions_[i], box);
          if (max(new_pos) > 1)
          {
            std::stringstream msg;
            msg 
              << "Invalid position in xml file: "
              << new_pos << "."
            ;
            throw ParseException(msg.str());
          }
          positions_[i] = new_pos + Tuple3(0.5);
        }
      }

      Tuples
      expand_positions(
        const SimulationConfig & config) 
      {
        const Tuple3 box = config.box();
        const size_t N = config.num_atoms();
        const Tuple3 null_position(0.0);
        Tuples result(N);
        for(size_t i = 0; i < N; ++i)
          if (config.particle(i).reduced_position() == null_position){
            result[i] = null_position;
          } else {
            result[i] = element_product(config.particle(i).reduced_position() - Tuple3(0.5), box);
          }
        return result;
      }

      shared_ptr<SimulationConfig>
      HoomdBlueXml::parse_(const pugi::xml_document& doc) const{
        // Create the result configuration
        shared_ptr<SimulationConfig> config(new SimulationConfig);
        // Parse all the data
        pugi::xml_node config_node = doc.select_single_node("hoomd_xml/configuration").node();
        // Number of atoms
        int N = config_node.attribute("natoms").as_int();
        config->set_num_atoms(N);
        // Generate iterable for all attributes
        pugi::xpath_node_set config_child_nodes = doc.select_nodes("hoomd_xml/configuration/*");
        //  box, type, position, velocity, mass, charge, bond, angle, dihedral
        std::stringstream buffer;
        pugi::xpath_node_set::const_iterator iter = config_child_nodes.begin();
          while (iter < config_child_nodes.end()){
            const pugi::xml_node & node = iter++->node();
      //      std::cerr << "Reading '" << node.name() << "'..." << std::endl;
            std::string name(node.name());
            buffer << node.first_child().text().as_string();
            // Using customized(!) assert_() !
            if (name == "type"){
                 config->types_->clear();
                 assert_((read_lines_<Type, Types>(config->types_, buffer) == N));
            } else if (name == "box"){
                Tuple3 box;
                assert_((box.set_x(node.attribute("lx").as_double()) >= 0));
                assert_((box.set_y(node.attribute("ly").as_double()) >= 0));
                assert_((box.set_z(node.attribute("lz").as_double()) >= 0));
                config->set_box(box);
            } else if (name == "position"){
                config->positions_->clear();
                assert_((read_lines_<Tuple3, Tuples>(config->positions_, buffer) == N));
            } else if (name == "mass"){
                config->masses_->clear();
                assert_((read_lines_<Attribute, Attributes>(config->masses_, buffer) == N));
            } else if (name == "charge"){
                config->charges_->clear();
                assert_((read_lines_<Attribute, Attributes>(config->charges_, buffer) == N));
            } else if (name == "velocity"){
                config->velocities_->clear();
                assert_((read_lines_<Tuple3, Tuples>(config->velocities_, buffer) == N));
            } else if (name == "acceleration"){
                config->accelerations_->clear();
                assert_((read_lines_<Tuple3, Tuples>(config->accelerations_, buffer) == N));
            } else if (name == "image"){
                config->images_->clear();
                assert_((read_lines_<Tuple3, Tuples>(config->images_, buffer) == N));
            } else if (name == "bond"){
                Type type;
                Tag tag0, tag1;
                while (!buffer.eof()){
                  buffer >> type >> tag0 >> tag1;
                  if (!buffer.fail()){
                    config->bond_types_->push_back(type);
                    int index = config->bond_types_->size() - 1;
                    config->bonds_->insert( std::pair<int, Tag>(index, tag0));
                    config->bonds_->insert( std::pair<int, Tag>(index, tag1));
                  }
                }
                assert_((config->bonds_->size() >= config->bond_types_->size()));
            } else if (name == "angle"){
                Type type;
                Tag tag0, tag1, tag2;
                while (!buffer.eof()){
                  buffer >> type >> tag0 >> tag1 >> tag2;
                  if (!buffer.fail()){
                    config->angle_types_->push_back(type);
                    int index = config->angle_types_->size() - 1;
                    config->angles_->insert( std::pair<Tag, int>(index, tag0));
                    config->angles_->insert( std::pair<Tag, int>(index, tag1));
                    config->angles_->insert( std::pair<Tag, int>(index, tag2));
                  }
                }
                assert_((config->angles_->size() >= config->angle_types_->size()));
            } else if (name == "dihedral"){
                Type type;
                Tag tag0, tag1, tag2, tag3;
                while (!buffer.eof()){
                  buffer >> type >> tag0 >> tag1 >> tag2 >> tag3;
                  if (!buffer.fail()){
                    config->dihedral_types_->push_back(type);
                    int index = config->dihedral_types_->size() - 1;
                    config->dihedrals_->insert( std::pair<Tag, int>(index, tag0));
                    config->dihedrals_->insert( std::pair<Tag, int>(index, tag1));
                    config->dihedrals_->insert( std::pair<Tag, int>(index, tag2));
                    config->dihedrals_->insert( std::pair<Tag, int>(index, tag3));
                  }
                }
                assert_((config->dihedrals_->size() >= config->dihedral_types_->size()));
            } else {
              std::cerr << "Attribute unknown: " << name << std::endl;
              buffer.seekg(0, std::ios::end);
            }
            buffer.clear();
          } // while
        reduce_positions(* config);
        config->update_internal_();
        return config;
      }

      void
      HoomdBlueXml::write(const SimulationConfig & config,
                          std::ostream & os,
                          unsigned int step) const{
      // Setting up the document
        pugi::xml_document doc;
        pugi::xml_node hoomd_node = doc.append_child("hoomd_xml");
        hoomd_node.append_attribute("version");
        hoomd_node.attribute("version") = "1.4";
      // Setting up the configuration node
        pugi::xml_node config_node = hoomd_node.append_child("configuration");
        pugi::xml_attribute natoms_attr = config_node.append_attribute("natoms");
        natoms_attr.set_value(config.num_atoms());
        config_node.append_attribute("time_step").set_value(step);
      // box
        pugi::xml_node box_node = config_node.append_child("box");
        box_node.append_attribute("lx");
        box_node.append_attribute("ly");
        box_node.append_attribute("lz");
        box_node.attribute("lx") = config.box().x();
        box_node.attribute("ly") = config.box().y();
        box_node.attribute("lz") = config.box().z();
      // types
        pugi::xml_node types_node = config_node.append_child("type");
        types_node.append_attribute("num") = (int)config.types_->size();
        types_node.text().set(text<Types>(config.types_).c_str());
      // masses
        pugi::xml_node mass_node = config_node.append_child("mass");
        mass_node.append_attribute("num") = (int)config.masses_->size();
        mass_node.text().set(text<Attributes>(config.masses_).c_str());
      // charges
        pugi::xml_node charge_node = config_node.append_child("charge");
        charge_node.append_attribute("num") = (int)config.charges_->size();
        charge_node.text().set(text<Attributes>(config.charges_).c_str());
      // positions
        pugi::xml_node position_node = config_node.append_child("position");
        //Tuples expanded_positions = config.property.expanded_positions();
        Tuples expanded_positions = expand_positions(config);
        position_node.append_attribute("num") = (int)(expanded_positions.size());
        position_node.text().set(text(expanded_positions.begin(), expanded_positions.end()).c_str());
      // velocities
        pugi::xml_node velocity_node = config_node.append_child("velocity");
        velocity_node.append_attribute("num") = (int)config.velocities_->size();
        velocity_node.text().set(text<Tuples>(config.velocities_).c_str());
      // accelerations
        pugi::xml_node acceleration_node = config_node.append_child("acceleration");
        acceleration_node.append_attribute("num") = (int)config.accelerations_->size();
        acceleration_node.text().set(text<Tuples>(config.accelerations_).c_str());
      // images
        pugi::xml_node image_node = config_node.append_child("image");
        image_node.append_attribute("num") = (int)config.images_->size();
        image_node.text().set(text<Tuples>(config.images_).c_str());
      // bonds
        std::stringstream bond_stream;
        config.dump_bonds(bond_stream);
        pugi::xml_node bond_node = config_node.append_child("bond");
        bond_node.append_attribute("num") = config.num_bonds();
        bond_node.text().set(bond_stream.str().c_str());
      // angles
        std::stringstream angle_stream;
        config.dump_angles(angle_stream);
        pugi::xml_node angle_node = config_node.append_child("angle");
        angle_node.append_attribute("num") = config.num_angles();
        angle_node.text().set(angle_stream.str().c_str());
      // dihedrals
        std::stringstream dihedral_stream;
        config.dump_dihedrals(dihedral_stream);
        pugi::xml_node dihedral_node = config_node.append_child("dihedral");
        dihedral_node.append_attribute("num") = config.num_dihedrals();
        dihedral_node.text().set(dihedral_stream.str().c_str());

        doc.save(os);
      }

      void
      HoomdBlueXml::write(const SimulationConfig & config,
                          const std::string & filename,
                          unsigned int step) const{
        std::ofstream xml_file;
        xml_file.open(filename.c_str());
        write(config, xml_file, step);
        xml_file.close();
      }

    } // namespace SimulationConfiguration

  } // namespace ParserWriter

} // namespace mdst
