// =====================================================================================
//
//       Filename:  guess_parser.hpp
//
//    Description:  Guess parser from file extension and file format.
//
//        Version:  1.0
//        Created:  03/13/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_GUESS_PARSER_H_
#define MDST_GUESS_PARSER_H_

#include "parsers.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    namespace ParserWriter{

      class GuessParserError : public ParserWriterException{
       public:
        GuessParserError() {};
        GuessParserError(const std::string & e): ParserWriterException(e) {};
        virtual ~GuessParserError() throw() {};
      };

      shared_ptr<Parser>
      guess_parser_from_extension(const std::string & filename){
        typedef shared_ptr<Parser> ParserSharedPtr;
        ParserSharedPtr result;
        const std::string extension(determine_extension(filename));
        if (extension == "xyz"){
          result.reset(new XYZ());
        } else if (extension == "xml"){
          result.reset(new HoomdBlueXml());
        } else {
          std::stringstream error;
          error << "Failed to determine parser from file extension '";
          error << extension << "'.";
          throw GuessParserError(error.str());
        }
        /*std::cerr << "Detected file format for file '" << filename;
        std::cerr << "' is '" << result->file_format() << "'." << std::endl;*/
        return result;
      }

      shared_ptr<Parser>
      guess_parser(const std::string & filename){
        return guess_parser_from_extension(filename);
      }

    } // namespace ParserWriter

  } // namespace SimulationConfiguration
      
} // namespace mdst

#endif // MDST_GUESS_PARSER_H_
