// =====================================================================================
//
//       Filename:  parser_writer.cpp
//
//    Description:  Meta-library to link to all parsers and writers.
//
//        Version:  1.0
//        Creaated: 03/07/2013 10:14:00 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

// all registered writers

namespace mdst{
  
  namespace SimulationConfiguration{

    namespace ParserWriter{

    } // namespace ParserWriter

  } // namespace SimulationConfiguration
    
} // namespace mdst
