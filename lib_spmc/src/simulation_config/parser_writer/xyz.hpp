// =====================================================================================
//
//       Filename:  xyz.hpp
//
//    Description:  Parse and write xyz files.
//
//        Version:  1.0
//        Created:  07/03/2013 10:00:00 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_XYZ_H_
#define MDST_XYZ_H_

#include "parser_writer.hpp"

namespace mdst{

  namespace SimulationConfiguration{
     
    namespace ParserWriter{

      class XYZ : public Parser,
                  public Writer
      {
       public:

        static const std::string STR_SIGNATURE;

        virtual shared_ptr<SimulationConfiguration::SimulationConfig>
        parse(const std::string & filename) const;

        virtual shared_ptr<SimulationConfiguration::SimulationConfig>
        parse(std::istream & stream) const;

        virtual unsigned int
        determine_step(const std::string & filename) const{
          throw ParseException("Unable to determine step for xyz-format.");
        }

        virtual unsigned int
        determine_step(std::istream & stream) const{
          throw ParseException("Unable to determine step for xyz-format.");
        }

        virtual void
        write(const SimulationConfiguration::SimulationConfig &,
             std::ostream &,
             unsigned int = 0) const;

        virtual void
        write(const SimulationConfiguration::SimulationConfig &,
             const std::string & filename,
             unsigned int = 0) const;

        virtual std::string
        file_format() const { return "xyz";}

        virtual std::string
        file_extension() const { return "xyz";}

        virtual ~XYZ() throw() {};
      };

      template<
        class TagIterator>
      void
      write_molecule_(
        const SimulationConfig & config,
        TagIterator first,
        TagIterator last,
        const size_t n,
        std::ostream & stream,
        const std::string comment = std::string())
      {
        stream << n << std::endl;
        if (! comment.empty())
          stream << comment << std::endl;
        for(; first != last; ++first)
        {
          SimulationConfiguration::ParticleView p = config.particle(* first);
          stream
            << p.type() << " "
            << std::setprecision(DEFAULT_PRECISION) << p.position().x() << " "
            << std::setprecision(DEFAULT_PRECISION) << p.position().y() << " "
            << std::setprecision(DEFAULT_PRECISION) << p.position().z() << " "
            << p.image().x() << " "
            << p.image().y() << " "
            << p.image().z() << " "
            << std::endl
          ;
        }
      }

    } // namespace ParserWriter

  } // namespace SimulationConfiguration
    
} // namespace mdst

#endif // MDST_XYZ_H_
