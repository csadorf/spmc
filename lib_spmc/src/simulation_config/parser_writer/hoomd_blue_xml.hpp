// =====================================================================================
//
//       Filename:  hoomd_blue_xml.h
//
//    Description:  Parse and dump xml files in hoomd blue format.
//
//        Version:  1.0
//        Created:  10/21/2012 06:46:27 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_HOOMD_BLUE_XML_H_
#define MDST_HOOMD_BLUE_XML_H_

#include "pugixml.hpp"

#include "parser_writer.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    namespace ParserWriter{
      
      class HoomdBlueXml : public ParserWriter::Parser,
                           public ParserWriter::Writer{

       public:
        virtual shared_ptr<SimulationConfiguration::SimulationConfig>
        parse(const std::string & filename) const;
        
        virtual shared_ptr<SimulationConfiguration::SimulationConfig>
        parse(std::istream & stream) const;

        virtual unsigned int
        determine_step(const std::string & filename) const;

        virtual unsigned int
        determine_step(std::istream & stream) const;

        virtual void
        write(const SimulationConfiguration::SimulationConfig &,
              std::ostream &,
              unsigned int step = 0) const;

        virtual void
        write(const SimulationConfiguration::SimulationConfig &,
              const std::string & filename,
              unsigned int step = 0) const;

        virtual std::string
        file_format() const { return "HoomdBlue xml";}

        virtual std::string
        file_extension() const { return "xml";}

        virtual
        ~HoomdBlueXml() throw() {};
       private:
        bool
        process_result_(const pugi::xml_parse_result& result) const;

        void
        assert_(bool test) const;
        
        shared_ptr<SimulationConfiguration::SimulationConfig>
        parse_(const pugi::xml_document &) const;

        unsigned int
        determine_step_(const pugi::xml_document &) const;

        void
        reduce_positions(
          SimulationConfig & config) const;

      };

    } // namespace ParserWriter

  } // namespace SimulationConfiguration
      
} // namespace mdst

#endif // MDST_HOOMD_BLUE_XML_H_
