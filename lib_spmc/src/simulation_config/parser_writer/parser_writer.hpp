// =====================================================================================
//
//       Filename:  parser_writer.hpp
//
//    Description:  Virtual base class for parsers and writers.
//
//        Version:  1.0
//        Creaated: 03/07/2013 10:14:00 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_PARSER_WRITER_H_
#define MDST_PARSER_WRITER_H_

#include <memory.hpp>
#include <string>

#include "exception.hpp"
#include "simulation_config.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    namespace ParserWriter{

      const int DEFAULT_PRECISION = std::numeric_limits<Attribute>::digits10 + 2;

      class ParserWriterException : public Exception{
       public:
        ParserWriterException() {};
        ParserWriterException(const std::string & e): Exception(e) {};
        virtual ~ParserWriterException() throw() {};
      };
      
      class ParseException : public ParserWriterException{
       public:
        ParseException() {};
        ParseException(const std::string& e): ParserWriterException(e) {};
        virtual ~ParseException() throw() {};
      };

      class WriteException : public ParserWriterException{
       public:
        WriteException() {};
        WriteException(const std::string& e): ParserWriterException(e) {};
        virtual ~WriteException() throw() {};
      };
      
      class Parser{
       public:
        virtual shared_ptr<SimulationConfiguration::SimulationConfig>
        parse(const std::string & filename) const = 0;

        virtual shared_ptr<SimulationConfiguration::SimulationConfig>
        parse(std::istream & stream) const = 0;

        virtual unsigned int
        determine_step(const std::string & filename) const = 0;

        virtual unsigned int
        determine_step(std::istream & stream) const = 0;

        virtual std::string
        file_format() const = 0;

        virtual std::string
        file_extension() const = 0;

        virtual ~Parser() throw() {};
      };

      class Writer{
       public:
        virtual void
        write(const SimulationConfiguration::SimulationConfig &,
             std::ostream &,
             unsigned int step = 0) const = 0;

        virtual void
        write(const SimulationConfiguration::SimulationConfig &,
             const std::string & filename,
             unsigned int step = 0) const = 0;

        virtual std::string
        file_format() const = 0;


        virtual std::string
        file_extension() const = 0;

        virtual ~Writer() throw() {};
      };

    } // namespace ParserWriter

  } // namespace SimulationConfiguration
    
} // namespace mdst

#endif // MDST_PARSER_WRITER_H_
