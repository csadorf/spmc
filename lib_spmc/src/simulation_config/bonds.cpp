/*
 * =====================================================================================
 *
 *       Filename:  bonds.cpp
 *
 *    Description:  Bond-related calculations.
 *
 *        Version:  1.0
 *        Created:  12/16/2013
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
 *   Organization:  RWTH Aachen University
 *
 * =====================================================================================
 */

#include "simulation_config.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    bool
    SimulationConfig::share_bond(
      const ParticleTag a,
      const ParticleTag b) const
    {
      if (num_bonds() == 0)
        return false;
      Tag bond_a = reversed_bonds_->find(a)->second;
      Tag bond_b = reversed_bonds_->find(b)->second;
      if (bond_a == bond_b)
        return true;
      else
        return false;
    }

    bool
    SimulationConfig::share_angle(
      const ParticleTag a,
      const ParticleTag b) const
    {
      if (num_angles() == 0)
        return false;
      Tag angle_a = reversed_angles_->find(a)->second;
      Tag angle_b = reversed_angles_->find(b)->second;
      if (angle_a == angle_b)
        return true;
      else
        return false;
    }

    bool
    SimulationConfig::share_dihedral(
      const ParticleTag a,
      const ParticleTag b) const
    {
      if (num_dihedrals() == 0)
        return false;
      Tag dihedral_a = reversed_dihedrals_->find(a)->second;
      Tag dihedral_b = reversed_dihedrals_->find(b)->second;
      if (dihedral_a == dihedral_b)
        return true;
      else
        return false;
    }

    bool
    SimulationConfig::bonded(
        const ParticleTag a,
        const ParticleTag b) const
    {
      SharedLock _(mutex_);
      if (a == b)
        return true;
      return molecule_map_.find(a)->second->count(b) > 0;
    }

    bool
    SimulationConfig::directly_bonded(
        const ParticleTag a,
        const ParticleTag b) const
    {
      SharedLock _(mutex_);
      return directly_bonded_(a, b);
    }

    
    bool
    SimulationConfig::directly_bonded_(
        const ParticleTag a,
        const ParticleTag b) const
    {
      if (a == b)
        return true;
      else {
        TagMapConstRange range = particle_map_->equal_range(a);
        for(TagMapConstIterator it = range.first; it != range.second; ++it)
          if (it->second == b)
            return true;
        return false;
      }
    }

    bool
    SimulationConfig::indirectly_bonded(
      const ParticleTag a,
      const ParticleTag b,
      int depth) const
    {
      if (a == b)
        return true;
      if (is_linear_flag_)
      {
        if (std::max(a, b) - std::min(a, b) <= depth)
          if (bonded(a, b))
            return true;
      } else {
        switch (depth)
        {
          case 0:
            return directly_bonded(a, b); break;
          /*
          case 1:
            return indirectly_bonded_depth_1_calc_(a, b); break;
          case 2:
            return indirectly_bonded_depth_2_calc_(a, b); break;
            //return indirectly_bonded_depth_2_(a, b); break;
          case 3:
            return indirectly_bonded_depth_3_calc_(a, b); break;
            //return indirectly_bonded_depth_3_(a, b); break;
           */
          default:
          // The recurisve function turned out to be faster than the nested loops... (csa)
            return indirectly_bonded_recursive_(a, b, depth); break;
        }
      }
      return false;
    }

    bool
    SimulationConfig::indirectly_bonded_depth_1_(
      const ParticleTag a,
      const ParticleTag b) const
    {
      IndirectBondMap & lookup_table_ = indirect_bond_map_1_;
      IndirectBondMapConstIterator entry = lookup_table_.find(TagPair(a, b));
      if (entry != lookup_table_.end())
        return entry->second;
      else {
        bool bonded = indirectly_bonded_depth_1_calc_(a, b);
        lookup_table_[TagPair(a, b)] = bonded;
        return bonded;
      }
    }

    bool
    SimulationConfig::indirectly_bonded_depth_1_calc_(
      const ParticleTag a,
      const ParticleTag b) const
    {
      TagMapConstRange range = particle_map_->equal_range(a);
      for(TagMapConstIterator it = range.first; it != range.second; ++it)
        if (it->second == b)
          return true;
      return false;
    }

    bool
    SimulationConfig::indirectly_bonded_depth_2_(
      const ParticleTag a,
      const ParticleTag b) const
    {
      IndirectBondMap & lookup_table_ = indirect_bond_map_2_;
      IndirectBondMapConstIterator entry = lookup_table_.find(TagPair(a, b));
      if (entry != lookup_table_.end())
        return entry->second;
      else {
        bool bonded = indirectly_bonded_recursive_(a, b, 2);
        //bool bonded = indirectly_bonded_depth_2_calc_(a, b);
        lookup_table_[TagPair(a, b)] = bonded;
        return bonded;
      }
    }

    bool
    SimulationConfig::indirectly_bonded_depth_2_calc_(
      const ParticleTag a,
      const ParticleTag b) const
    {
      TagMapConstRange range = particle_map_->equal_range(a);
      for(TagMapConstIterator it = range.first; it != range.second; ++it)
      {
        if (it->second == b)
          return true;
        TagMapConstRange range_2 = particle_map_->equal_range(it->second);
        for(TagMapConstIterator it2 = range_2.first; it2 != range_2.second; ++it2)
          if (it2->second == b)
            return true;
      }
      return false;
    }

    bool
    SimulationConfig::indirectly_bonded_depth_3_(
      const ParticleTag a,
      const ParticleTag b) const
    {
      IndirectBondMap & lookup_table_ = indirect_bond_map_3_;
      IndirectBondMapConstIterator entry = lookup_table_.find(TagPair(a, b));
      if (entry != lookup_table_.end())
        return entry->second;
      else {
        bool bonded = indirectly_bonded_recursive_(a, b, 3);
        //bool bonded = indirectly_bonded_depth_3_calc_(a, b);
        lookup_table_[TagPair(a, b)] = bonded;
        return bonded;
      }
    }
        
    bool
    SimulationConfig::indirectly_bonded_depth_3_calc_(
      const ParticleTag a,
      const ParticleTag b) const
    {

      TagMapConstRange range = particle_map_->equal_range(a);
      for(TagMapConstIterator it = range.first; it != range.second; ++it)
      {
        if (it->second == b)
          return true;
        TagMapConstRange range_2 = particle_map_->equal_range(it->second);
        for(TagMapConstIterator it2 = range_2.first; it2 != range_2.second; ++it2)
        {
          if (it2->second == b)
            return true;
          TagMapConstRange range_3 = particle_map_->equal_range(it->second);
          for(TagMapConstIterator it3 = range_3.first; it3 != range_3.second; ++it3)
            if (it3->second == b)
              return true;
        }
      }
      return false;
    }
        

    bool
    SimulationConfig::indirectly_bonded_recursive_(
      const ParticleTag a,
      const ParticleTag b,
      int depth) const
    {
      if (a == b)
        return true;

      TagMapConstRange range = particle_map_->equal_range(a);
      if (depth > 3) {
        for(TagMapConstIterator it = range.first; it != range.second; ++it) {
	  if (it->second == b) return true;
          TagMapConstRange range1 = particle_map_->equal_range(it->second);
          for(TagMapConstIterator it1 = range1.first; it1 != range1.second; ++it1) {
            if (it1->second == it->second) continue;
	    if (it1->second == b) return true;
            TagMapConstRange range2 = particle_map_->equal_range(it1->second);
            for(TagMapConstIterator it2 = range2.first; it2 != range2.second; ++it2) {
              if (it2->second == it->second) continue;
              if (it2->second == it1->second) continue;
	      if (it2->second == b) return true;
	      if (indirectly_bonded_recursive_(it2->second, b, depth - 3)) return true;
	    }
	  }
	}
      } else if (depth == 3) {
        for(TagMapConstIterator it = range.first; it != range.second; ++it) {
	  if (it->second == b) return true;
          TagMapConstRange range1 = particle_map_->equal_range(it->second);
          for(TagMapConstIterator it1 = range1.first; it1 != range1.second; ++it1) {
            if (it1->second == it->second) continue;
	    if (it1->second == b) return true;
            TagMapConstRange range2 = particle_map_->equal_range(it1->second);
            for(TagMapConstIterator it2 = range2.first; it2 != range2.second; ++it2) {
              if (it2->second == it->second) continue;
              if (it2->second == it1->second) continue;
	      if (it2->second == b) return true;
	    }
	  }
	}
      } else if (depth == 2) {
        for(TagMapConstIterator it = range.first; it != range.second; ++it) {
	  if (it->second == b) return true;
          TagMapConstRange range1 = particle_map_->equal_range(it->second);
          for(TagMapConstIterator it1 = range1.first; it1 != range1.second; ++it1) {
            if (it1->second == it->second) continue;
	    if (it1->second == b) return true;
	  }
	}
      } else {
        for(TagMapConstIterator it = range.first; it != range.second; ++it)
          if (it->second == b)
            return true;
      }
      return false;
    }

    bool
    SimulationConfig::is_linear_(
      TagSetConstIterator first,
      TagSetConstIterator last) const
    {
      if (last == first)
        return false; // No particle
      for(++first; first != last; ++first)
        if (! directly_bonded_(*first, *last))
          return false;
      return true;
    }

    bool
    SimulationConfig::is_linear_() const
    {
      const TagSetPtrSet molecules_ = molecules();
      for(TagSetPtrSetConstIterator molecule = molecules_.begin();
          molecule != molecules_.end(); ++molecule)
        if (! is_linear_((*molecule)->begin(), (*molecule)->end()))
          return false;
      return true;
    }

    void
    SimulationConfig::neighborhood(
      const ParticleTag p,
      int depth,
      TagSet & result) const
   {
     if (depth > 0)
     {
       TagMapConstRange range = particle_map_->equal_range(p);
       for(TagMapConstIterator & it = range.first; it != range.second; ++it)
         neighborhood(it->second, depth - 1, result);
     }
   }

  } // namespace SimulationConfiguration

} // namespace mdst
