/*
 * =====================================================================================
 *
 *       Filename:  iterators.hpp
 *
 *    Description:  Iterator classes provided to iterate through tag groups.
 *
 *        Version:  1.0
 *        Created:  04/14/2014
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
 *   Organization:  RWTH Aachen University
 *
 * =====================================================================================
 */

#ifndef MDST_ITERATORS_H_
#define MDST_ITERATORS_H_

#include "types.hpp"

namespace mdst{
   
  namespace SimulationConfiguration{

    namespace internal{

      class TagIterator : public std::forward_iterator_tag{};

      class TagCounter : public TagIterator
      {
       public:
        TagCounter():
          i_(TAG_NULL)
        {};

        explicit
        TagCounter(
          const Tag & tag):
          i_(tag)
        {};

        Tag
        operator*() const
        {
          return i_;
        }

        TagCounter &
        operator++()
        {
          ++i_;
          return * this;
        }

        TagCounter
        operator++(int)
        {
          TagCounter tmp(* this);
          ++(*this);
          return tmp;
        }

        bool
        operator==(
          const TagCounter & rhs) const
        {
          return i_ == rhs.i_;
        }

        bool
        operator!=(
          const TagCounter & rhs) const
        {
          return ! operator==(rhs);
        }

       private:
        Tag i_;
      };

    } // namespace internal

  } // namespace SimulationConfiguration

} // namespace mdst

#endif // MDST_ITERATORS_H_
