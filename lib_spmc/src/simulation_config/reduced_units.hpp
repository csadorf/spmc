// =====================================================================================
//
//       Filename:  reduced_units.hpp
//
//    Description:  Provide defined unit sets, to reduce units. 
//
//        Version:  1.0
//        Created:  11/05/2012 08:55:29 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_REDUCED_UNITS_H_
#define MDST_REDUCED_UNITS_H_

#include "units.hpp"
#include "types.hpp"

#include <boost/units/base_units/metric/angstrom.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/io.hpp>
#include <boost/units/systems/si/codata/physico-chemical_constants.hpp>
#include <boost/units/systems/si/codata/electron_constants.hpp>


namespace mdst{

namespace reduced_units{

using namespace mdst::units;
namespace si = boost::units::si;

class ReducedUnitsSet{
 public:

  // constructor
  ReducedUnitsSet(){};
  ReducedUnitsSet(const Length & l,
                  const Mass & m,
                  const Energy & e):
    length(l),
    area(Area(l * l)),
    mass(m),
    energy(e) {};

  const Length length;
  const Area area;
  const Mass mass;
  const Energy energy;

  Length expand_length(Attribute l) const
    {return l * length;}
  Area expand_area(Attribute a) const
    {return a * Area(boost::units::pow<2>(length));}
  Mass expand_mass(Attribute m) const
    { return m * mass;}
  Energy expand_energy(Attribute e) const
    { return e * energy;}
  PlaneAngle expand_plane_angle(Attribute a) const
    { return a * si::radian;}
  BondStretchingForce expand_bond_stretching_force(Attribute q) const
    { return q * BondStretchingForce(energy / boost::units::pow<2>(length));}
  BondBendingForce expand_bond_bending_force(Attribute q) const
    { return q * (energy / boost::units::pow<2>(si::radian));}
  
  Attribute reduce(const Length & l) const
    {return static_cast<Attribute>(l / length);}
  Attribute reduce(const Area & a) const
    {return static_cast<Attribute>(a / Area(length * length));}
  Attribute reduce(const Mass & m) const
    {return static_cast<Attribute>(m / mass);}
  Attribute reduce(const Energy & e) const
    {return static_cast<Attribute>( e / energy);}
  Attribute reduce(const PlaneAngle& q) const
    {return static_cast<Attribute>(q / si::radian);}
  Attribute reduce(const BondStretchingForce & q) const
    {return static_cast<Attribute>((q /
                                    BondStretchingForce(energy /
                                          boost::units::pow<2>(length))));}
  Attribute reduce(const BondBendingForce& q) const
    {return static_cast<Attribute>(q /
                                  (energy / boost::units::pow<2>(si::radian)));}
};

// Argon set example set
using namespace boost::units::si::constants::codata;

const Length argon_length(0.3406 * nanometer);
const Mass argon_mass(39.948 * amu);
const Energy argon_energy(119.8 * si::kelvin * k_B) ;

const ReducedUnitsSet argon(argon_length, argon_mass, argon_energy);

} // namespace reduced_units

// Pull into mdst namespace
using reduced_units::ReducedUnitsSet;
using reduced_units::argon;

} // namespace mdst

#endif // MDST_REDUCED_UNITS_H_
