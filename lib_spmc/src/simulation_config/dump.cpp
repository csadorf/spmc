// =====================================================================================
//
//       Filename:  dump.cpp
//
//    Description:  Dump properties of objects of SimulationConfig.
//
//        Version:  1.0
//        Created:  12/20/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "dump.hpp"
#include "structure_library.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    std::ostream &
    print_positions(
      const SimulationConfig & config,
      std::ostream & stream)
    {
      TagSet all = config.all();
      for (TagSetConstIterator p = all.begin(); p != all.end(); ++p)
        stream << config.particle(*p).position() << std::endl;
      return stream;
    }

  } // namespace SimulationConfiguration

} // namespace mdst
