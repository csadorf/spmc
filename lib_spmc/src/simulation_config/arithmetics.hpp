// =====================================================================================
//
//       Filename:  arithmetics.hpp
//
//    Description:  Fast and optimized access of SimulationConfig properties.
//
//        Version:  1.0
//        Created:  12/12/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATION_CONFIGURATION_ARITHMETICS_H_
#define MDST_SIMULATION_CONFIGURATION_ARITHMETICS_H_

#include "simulation_config.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    inline
    Attribute
    minimum_image_squared_distance(
      const Tuple3 & pos_a,
      const Tuple3 & pos_b,
      const Tuple3 & box)
    {
      const Tuple3 delta = pos_b - pos_a;
      using std::abs;
      const Tuple3 delta_abs = Tuple3(
        abs(delta.x()), abs(delta.y()), abs(delta.z()));
      const Tuple3 d_pbc = Tuple3(
        abs(delta_abs.x() - int(delta_abs.x()/box.x()) * 2 * box.x()),
        abs(delta_abs.y() - int(delta_abs.y()/box.y()) * 2 * box.y()),
        abs(delta_abs.z() - int(delta_abs.z()/box.z()) * 2 * box.z()));
      const Attribute d = abs_squared(Tuple3(
        d_pbc.x() - int(d_pbc.x() / (2 * box.x())) * d_pbc.x(),
        d_pbc.y() - int(d_pbc.y() / (2 * box.y())) * d_pbc.y(),
        d_pbc.z() - int(d_pbc.z() / (2 * box.z())) * d_pbc.z()));
//      std::cerr << "pos_a=" << pos_a << "; pos_b=" << pos_b << std::endl;
//      std::cerr << "distance(" << a << ", " << b << ")=" << d << std::endl;
      return d;
    }

    inline
    Attribute
    minimum_image_squared_distance(
      const SimulationConfiguration::SimulationConfig & config,
      const ParticleTag a,
      const ParticleTag b)
    {
      return minimum_image_squared_distance(
        config.particle_position(a),
        config.particle_position(b),
        config.box());
    }

  } // namespace SimulationConfiguration

} // namespace mdst

#endif // MDST_SIMULATION_CONFIGURATION_ARITHMETICS_H_
