//=====================================================================================
//
//       Filename:  cell_grid.hpp
//       Filename:  neighbors.hpp
//
//    Description:  Calculate neighbors of particles.
//
//        Version:  1.0
//        Created:  06/20/2014 
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATION_CONFIG_NEIGHBORS_
#define MDST_SIMULATION_CONFIG_NEIGHBORS_

#include "simulation_config.hpp"
#include "cell_grid.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    template<class ParticleOutputIterator>
    ParticleOutputIterator
    get_cell_members(
      const CellGrid & cell_grid,
      unsigned int cell_index,
      const ParticleTag & particle,
      ParticleOutputIterator first)
    {
      throw NotImplementedError();
      /*
      const unsigned int size_b = cell_grid.cell_list()->size(cell_index);
      if (size_b == 0)
        return first;
      const unsigned int offset_b = cell_grid.cell_list()->offset(cell_index);
      for(unsigned int b = offset_b; b < offset_b + size_b; ++b)
      {
        const ParticleTag p_b = cell_grid.cell_list()->particles()[b];
        if (particle == p_b)
          continue;
        * first = p_b;
        ++first;
      }
      */
      return first;
    }

    template<class ParticleOutputIterator>
    ParticleOutputIterator
    calculate_neighboring_particles_pbc(
      const SimulationConfig & config,
      const CellGrid & cell_grid,
      const Resolution & resolution,
      const ParticleTag particle,
      ParticleOutputIterator first,
      const unsigned int dimension = 3)
    {
      unsigned int cell_a[dimension];
      calculate_cell(
        cell_a, config.particle(particle).reduced_position(),
        config.box(), cell_grid.cell_dimension);
      unsigned int n_list[num_neighbors_(resolution)];
      unsigned int num_neighbors =
        calculate_neighbor_list_for_cell_(
          cell_a, cell_grid.num_cells, resolution, n_list);

      ParticleOutputIterator iterator(first);
      for(unsigned int n = 0; n < num_neighbors; ++n){
        const unsigned int cell_index_b = n_list[n];
        CellListConstRange range(
          cell_grid.cell_list()->equal_range(cell_index_b));
        for(CellListConstIterator it = range.first; it != range.second; ++it)
        {
          if (it->second == particle)
            continue;
          * iterator = it->second;
          ++iterator;
        }
      }
      return iterator;
    }

  } // namespace SimulationConfiguration

} // namespace mdst

#endif // MDST_SIMULATION_CONFIG_NEIGHBORS_
