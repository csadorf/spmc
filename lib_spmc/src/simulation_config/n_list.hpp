/*
 * =====================================================================================
 *
 *       Filename:  n_list.hpp
 *
 *    Description:  Neighbor list implementation for SimulationConfig.
 *
 *        Version:  1.0
 *        Created:  10/19/2012 04:54:33 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
 *   Organization:  RWTH Aachen University
 *
 * =====================================================================================
 */

#ifndef MDST_SIMULATION_CONFIGURATION_N_LIST_H_
#define MDST_SIMULATION_CONFIGURATION_N_LIST_H_

#include "types.hpp"
#include "cell_grid.hpp"

//#define DEBUG_NEIGHBOR_LIST

namespace mdst{

  namespace SimulationConfiguration{
    
    class SimulationConfig;

    typedef TagVector NeighborList;
    typedef std::map<ParticleTag, NeighborList> NeighborLists;
    typedef Attribute NListRMax;
    typedef Attribute SkinValue;
      
    struct NeighborListKey{
      NeighborListKey(
        ReducedCutOff reduced_r_cut_,
        SkinValue skin_,
        unsigned int exclude_bonded_,
        const SortedTypePair & sorted_type_pair_):
        reduced_r_cut(reduced_r_cut_),
        skin(skin_),
        exclude_bonded(exclude_bonded_),
        sorted_type_pair(sorted_type_pair_) {};
      ReducedCutOff reduced_r_cut;
      SkinValue skin;
      unsigned int exclude_bonded;
      SortedTypePair sorted_type_pair;

      Attribute
      r_cut() const
      { return reduced_r_cut;}

      Attribute
      squared_r_cut() const
      { return reduced_r_cut * reduced_r_cut;}

      Attribute
      r_max() const
      { return reduced_r_cut + skin;}

      Attribute
      squared_r_max() const
      { const Attribute r_max_ = r_max(); return r_max_ * r_max_;}

      bool
      operator==(
        const NeighborListKey & rhs) const
      {
        return 
          reduced_r_cut     == rhs.reduced_r_cut &&
          skin              == rhs.skin &&      
          exclude_bonded    == rhs.exclude_bonded &&
          sorted_type_pair  == rhs.sorted_type_pair;
      }

      bool
      operator<(
        const NeighborListKey & rhs) const
      {
        if (operator==(rhs))
          return false;
        else
          return r_max() < rhs.r_max();
      }

    };

    typedef std::tr1::unordered_map<ParticleTag, Tuple3> NeighborListDirty_;
    typedef NeighborListDirty_::const_iterator NeighborListDirtyConstIterator_;
    typedef NeighborListDirty_::iterator NeighborListDirtyIterator_;

    typedef std::map<NeighborListKey, NeighborLists> NeighborListsMap;
    typedef NeighborListsMap::const_iterator NeighborListsMapConstIterator;
    typedef NeighborListsMap::iterator NeighborListsMapIterator;
  
    typedef std::map<NeighborListKey, NeighborListDirty_> NeighborListDirtyMap_;

    /*! \brief Calculate a list of all neighboring cells for a certain resolution.
     *
     * \param <cell>
     * The central cell, which is neighbor to all cells in the list.
     * \param <num_cells> 
     * The number of total cells in each dimension.
     * \param <resolution> 
     * The resolution of the cell grid.
     * \param <cell_n_list> 
     * The cell neighbor list is written to this array.
     * The maximum size of this array can be predeterminant by the function num_cell_neighbors().
     * \return
     * Returns the number of actual neighboring cells, and thus the valid size of cell_n_list.
     */
    unsigned int
    calculate_cell_neighbor_list(
      const unsigned int * cell,
      const unsigned int * num_cells,
      const Resolution & resolution,
      unsigned int * cell_n_list);

    size_t
    calculate_neighbor_list(
      const SimulationConfig & config,
      const NeighborListKey & n_list_key,
      const ParticleTag particle,
      NeighborList & n_list);

  } // namespace SimulationConfiguration

} // namespace mdst

#endif // MDST_SIMULATION_CONFIGURATION_N_LIST_H_
