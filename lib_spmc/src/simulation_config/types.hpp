// =====================================================================================
//
//       Filename:  types.hpp
//
//    Description:  Definition of types for namespace mdst.
//
//        Version:  1.0
//        Created:  10/21/2012 11:00:57 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_TYPES_H_
#define MDST_TYPES_H_

#include <vector>
#include <map>
#include <set>
#include <string>
#include <memory.hpp>
#include <complex>
//#include "py_set.hpp"

namespace mdst{

// Forward declerations
class Tuple3;

// class definitions
typedef std::string Type;

inline bool operator==(const Type& lhs, const Type& rhs){return (lhs.compare(rhs) == 0);}
inline bool operator!=(const Type& lhs, const Type& rhs){return !operator==(lhs, rhs);}
inline bool operator<(const Type& lhs, const Type& rhs){return lhs.compare(rhs) < 0;}
inline bool operator>(const Type& lhs, const Type& rhs){return operator<(rhs, lhs);}
inline bool operator<=(const Type& lhs, const Type& rhs){return !operator>(lhs, rhs);}
inline bool operator>=(const Type& lhs, const Type& rhs){return !operator<(lhs, rhs);}

template <class T> 
struct SortedPair{
 public:
  SortedPair() : first_(T()), second_(T()) {};
  SortedPair(const T & x, const T & y):
    first_(x <= y ? x : y), second_(x <= y ? y : x) {};
  T first() const {return first_;};
  T second() const {return second_;};
// The operators are implemented as member functions, else it would be necessary
// to declare all of them as template functions.
  bool operator==(const SortedPair &rhs) const
    { return (first_ == rhs.first_ && second_ == rhs.second_);};
  bool operator!=(const SortedPair &rhs) const
    { return ! operator==(rhs);};
//  bool operator!=(const SortedPair &rhs) const
//    { return (first_ != rhs.first_ && second_ != rhs.second_);};
  bool operator<(const SortedPair &rhs) const
    {return first_ == rhs.first_ ? second_ < rhs.second_ : first_ < rhs.first_;};
  bool operator>(const SortedPair &rhs) const
    {return first_ == rhs.first_ ? second_ > rhs.second_ : first_ > rhs.first_;};
  bool operator<=(const SortedPair &rhs) const
    {return first_ == rhs.first_ ? second_ <= rhs.second_ : first_ <= rhs.first_;};
  bool operator>=(const SortedPair &rhs) const
    {return first_ == rhs.first_ ? second_ >= rhs.second_ : first_ >= rhs.first_;};
 private:
  T first_;
  T second_;
};

// WARNING: The implementation is breaking with OOP-encapsulation.
//          A better way would be to introduce generic "any"-iterators. For the
//          sake of effeciency and simplicity, the farm of iterator-types is
//          hard-coded here.
//          In a future release type erasion should be considered!
//          See also: http://www.artima.com/cppsource/type_erasure.html
//


// Attribute is the basis type for scalar values
// It is defined specifically, so that it is configurable.
typedef double Attribute;
typedef std::complex<Attribute> ComplexAttribute;

typedef std::map<Attribute, Attribute> AttributeMap;
// Tag represents a unique id for Particles, Bonds, etc..
typedef int Tag;
const int TAG_NULL = -1;
typedef Tag ParticleTag;
typedef Tag NBondTag;
typedef NBondTag BondTag;
typedef NBondTag AngleTag;
typedef NBondTag DihedralTag;
typedef std::multimap<Tag, Tag> TagMap;
typedef TagMap::iterator TagMapIterator;
typedef TagMap::const_iterator TagMapConstIterator;
typedef std::pair<TagMapIterator, TagMapIterator> TagMapRange;
typedef std::pair<TagMapConstIterator, TagMapConstIterator> TagMapConstRange;
typedef std::pair<Tag, int> TagMapPair;
//typedef PySet<Tag> TagSet;
typedef std::set<Tag> TagSet;
typedef TagSet::const_iterator TagSetConstIterator;
typedef TagSet::iterator TagSetIterator;
typedef std::set<TagSet> TagSetSet;
typedef TagSetSet::const_iterator TagSetSetConstIterator;
typedef TagSetSet::iterator TagSetSetIterator;
typedef std::set<shared_ptr<TagSet> > TagSetPtrSet;
typedef TagSetPtrSet::const_iterator TagSetPtrSetConstIterator;
typedef TagSetPtrSet::iterator TagSetPtrSetIterator;
typedef shared_ptr<TagSet> TagSetPtr;
typedef std::map<Tag, shared_ptr<TagSet> > TagSetMap;
typedef TagSetMap::const_iterator TagSetMapConstIterator;
typedef TagSetMap::iterator TagSetMapIterator;
typedef std::vector<Tag> TagVector;
typedef TagVector::const_iterator TagVectorConstIterator;
typedef TagVector::iterator TagVectorIterator;
typedef std::set<TagVector> TagVectorSet;
typedef TagVectorSet Walks;
typedef TagVectorSet::const_iterator TagVectorSetConstIterator;
typedef TagVectorSet::iterator TagVectorSetIterator;
typedef Walks::const_iterator WalksConstIterator;
typedef std::map<Tag, TagVector> TagVectorMap;
typedef TagVectorMap::const_iterator TagVectorMapConstIterator;
typedef TagVectorMap::iterator TagVectorMapIterator;

typedef std::pair<Tag, Tag> TagPair;
typedef TagPair Bond;
typedef std::vector<Bond> Bonds;
typedef Bonds::const_iterator BondsConstIterator;
typedef Bonds::iterator BondsIterator;
// Define ParticleTypes and NBondTypes
typedef Type ParticleType;
typedef Type NBondType;
typedef NBondType BondType;
typedef NBondType AngleType;
typedef NBondType DihedralType;

typedef std::vector<Tuple3> Tuples;
typedef Tuples::iterator TuplesIterator;
typedef Tuples::const_iterator TuplesConstIterator;
typedef std::vector<Attribute> Attributes;
typedef Attributes::iterator AttributesIterator;
typedef Attributes::const_iterator AttributesConstIterator;
typedef std::vector<Type> Types;
typedef Types::iterator TypesIterator;
typedef Types::const_iterator TypesConstIterator;
typedef std::set<Type> TypeSet;
typedef TypeSet::iterator TypeSetIterator;
typedef TypeSet::const_iterator TypeSetConstIterator;
typedef SortedPair<Type> SortedTypePair;
typedef std::map<Tag, Tuple3> TupleMap;
typedef TupleMap::const_iterator TupleMapConstIterator;
typedef TupleMap::iterator TupleMapIterator;

typedef TagVector TagSequence;
typedef TagSequence::const_iterator TagSequenceConstIterator;
typedef TagSequence::iterator TagSequenceIterator;
typedef std::vector<TagSequence> TagSequences;
typedef TagSequences::const_iterator TagSequencesConstIterator;
typedef TagSequences::iterator TagSequencesIterator;

} // namespace mdst

#endif // MDST_TYPES_H_

