//=====================================================================================
//
//       Filename:  cell_grid.hpp
//
//    Description:  Calculate cell grids.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATION_CONFIG_CELL_GRID_
#define MDST_SIMULATION_CONFIG_CELL_GRID_

#include "cell_list.hpp"
#include <tuple3.hpp>
#include <misc.hpp>
#include <list>

namespace mdst{

  namespace SimulationConfiguration{

    typedef Attribute ReducedCutOff;
    typedef IntVector Resolution;
    typedef Tuple3 BoxSize;
//    typedef std::pair<ReducedCutOff, Resolution> CellGridsKey;
    typedef IntVector CellGridsKey;

    inline
    Resolution &
    default_resolution()
    {
      static Resolution result(3);
      result[0] = 1;
      result[1] = 1;
      result[2] = 1;
      return result;
    }

    class CellGrid{
     public:
//      Attribute cut_off;
//      unsigned int factor;
      unsigned int num_cells[3];
      Tuple3 cell_dimension;
      CellList *
      cell_list() {
        return cell_list_.get();
      }

      CellList const *
      cell_list() const{
        return cell_list_.get();
      }

      CellGrid() {};
      explicit CellGrid(
        const CellGridsKey & key,
        const Tuple3 & box,
        const size_t num_atoms);
//        const ReducedCutOff reduced_cutoff);
      CellGrid(
        const CellGrid & other);
     private:
      shared_ptr<CellList> cell_list_;
    };

    typedef std::map<CellGridsKey, CellGrid> CellGrids;
    typedef CellGrids::const_iterator CellGridsConstIterator;
    typedef CellGrids::iterator CellGridsIterator;

    void
    calculate_cell(
      unsigned int cell[],
      const Tuple3 & position,
      const Tuple3 & box,
      const Tuple3 & cell_dimension,
      unsigned int dimension = 3);

    CellIndex
    get_cell_index(
      const unsigned int * num_cells,
      const unsigned int * cell);

    void
    add_to_cell(
      CellList & cell_list,
      const CellIndex cell_index,
      const ParticleTag & particle);

    void
    remove_from_cell(
      CellList & cell_list,
      const CellIndex cell_index,
      const ParticleTag & particle);

    void
    move(
      CellList & cell_list,
      const ParticleTag & particle,
      const CellIndex old_cell_index,
      const CellIndex new_cell_index);

    void
    dump_cell_list(
      const CellList & cell_list,
      std::ostream & stream = std::cerr);

    unsigned int
    calculate_num_cells(
      const CellGridsKey & key,
      const Tuple3 & box,
      const unsigned int dimension = 3);

    CellGridsKey
    calculate_key(
      const Tuple3 & box,
      const ReducedCutOff reduced_cutoff,
      const Resolution & resolution = default_resolution(),
      const unsigned int max_num_cells = 1000000,
      const unsigned int dimension = 3);

    unsigned int
    calculate_neighbor_list_for_cell_(
      const unsigned int * cell,
      const unsigned int * num_cells,
      const Resolution & resolution,
      unsigned int * neighbor_list);

    unsigned int
    num_neighbors_(
      const Resolution & resolution,
      const unsigned int dimension = 3);

  } // namespace SimulationConfiguration

} // namespace mdst

#endif // MDST_SIMULATION_CONFIG_CELL_GRID_
