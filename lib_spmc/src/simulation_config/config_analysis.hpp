/*
 * =====================================================================================
 *
 *       Filename:  config_analysis.hpp
 *
 *    Description:  Provide functions to analyze a SimulationConfig object.
 *                  This class considers the SimulationConfig object as constant
 *                  and will never manipulate it in any way.
 *
 *        Version:  1.0
 *        Created:  10/19/2012 04:55:22 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
 *   Organization:  RWTH Aachen University
 *
 * =====================================================================================
 */

#ifndef MDST_CONFIG_ANALYSIS_H_
#define MDST_CONFIG_ANALYSIS_H_

#include <memory.hpp>
#include <vector>

#include "histogram.hpp"
#include "types.hpp"
#include "simulation_config.hpp"

namespace mdst{

  // Forward declarations
  //
  class Tuple3;

  namespace analysis{

    template<class V>
    struct ConfigAnalyzer
    {
      typedef V ValueType;
    };

    Attribute
    mean_squared_distance(
      const SimulationConfiguration::SimulationConfig & config_a,
      const SimulationConfiguration::SimulationConfig & config_b);

    Tuple3
    extremal_position(
      const SimulationConfiguration::SimulationConfig & config,
      const TagSet & subset,
      const bool minimal = true);

    inline
    Tuple3
    minimal_position(
      const SimulationConfiguration::SimulationConfig & config,
      const TagSet & subset)
    {return extremal_position(config, subset);}


    inline
    Tuple3
    minimal_position(
      const SimulationConfiguration::SimulationConfig & config)
    {return minimal_position(config, config.all());}

    inline
    Tuple3
    maximal_position(
      const SimulationConfiguration::SimulationConfig & config,
      const TagSet & subset)
    {return extremal_position(config, subset, false);}

    inline
    Tuple3
    maximal_position(
      const SimulationConfiguration::SimulationConfig & config)
    {return maximal_position(config, config.all());}

    Tuple3
    dimension(
      const SimulationConfiguration::SimulationConfig & config,
      const TagSet & subset);

    inline
    Tuple3
    dimension(
      const SimulationConfiguration::SimulationConfig & config)
    {return dimension(config, config.all());}

    Attribute
    volume(
      const SimulationConfiguration::SimulationConfig & config,
      const TagSet & subset);

    inline
    Attribute
    volume(
      const SimulationConfiguration::SimulationConfig & config)
    {return volume(config, config.all());}

    Attribute
    box_volume(
      const SimulationConfiguration::SimulationConfig & config);

    Tuple3
    center_of_mass(
      const SimulationConfiguration::SimulationConfig & config,
      const TagSet & subset);

    inline
    Tuple3
    center_of_mass(
      const SimulationConfiguration::SimulationConfig & config)
    {return center_of_mass(config, config.all());}

    inline
    Attribute
    number_density(
      const SimulationConfiguration::SimulationConfig & config,
      const TagSet & subset)
    {return subset.size() / box_volume(config);}

    inline
    Attribute
    number_density(
      const SimulationConfiguration::SimulationConfig & config)
    {return number_density(config, config.all());}

    Attribute
    density(
      const SimulationConfiguration::SimulationConfig & config,
      const TagSet & subset);

    inline
    Attribute
    density(
      const SimulationConfiguration::SimulationConfig & config)
    {return density(config, config.all());}

    AttributeMap
    radial_distribution_function(
      const SimulationConfiguration::SimulationConfig & config,
      bool exclude_bonded = true,
      const Attribute delta_r = 0.001,
      float epsilon = 0.01);

    class RadialDistributionFunctionAnalyzer : 
      public ConfigAnalyzer<AttributeMap>
    {
     public:
      explicit
      RadialDistributionFunctionAnalyzer(
        bool exclude_bonded = true,
        Attribute delta_r = 0.001,
        float epsilon = 0.01):
        exclude_bonded_(exclude_bonded),
        delta_r_(delta_r),
        epsilon_(epsilon)
        {};

      AttributeMap
      operator()(
        const SimulationConfiguration::SimulationConfig & config) const
      { return radial_distribution_function(
          config, exclude_bonded_, delta_r_, epsilon_);}

     private:
      const bool exclude_bonded_;
      const Attribute delta_r_;
      const float epsilon_;
    };

    class RadiusOfGyrationAnalyzer : 
      public ConfigAnalyzer<Attribute>
    {
     public:
      Attribute
      operator()(
        const SimulationConfiguration::SimulationConfig & config) const;
     private:
      static Attribute
      rg_molecule_(
        const SimulationConfiguration::SimulationConfig & config,
        const TagSet & molecule);
    };

    int
    expected_number_of_particles(
      const SimulationConfiguration::SimulationConfig & config,
      const AttributeMap & rdf_histogram,
      const Attribute delta_r,
      const Attribute radius);

  } // namespace analysis

  namespace deprecated{

    class ConfigAnalyzer{
     public:
      ConfigAnalyzer(
          const shared_ptr<const SimulationConfiguration::SimulationConfig> & config
                    ):
        config_(config)
      {};

      Tuple3
      origin(bool maximum = false) const;
      
      Tuple3
      dimension() const;
      
      Tuple3
      box_origin(bool maximum = false) const;
      
      Attribute
      volume() const;
      
      Attribute
      box_volume() const;
      
      Attribute
      max_box_length() const;

      Tuple3
      center_of_mass() const;
      
      Tuple3
      center_of_mass(const TagSet& group) const;
      
      // Calculates the mean-squared distance between this config and 'other'.
      // The closest neighbor distance-wise is found for each particle in this
      // config compared to the other.
      // Returns the mean-squared average of all distances or -1,
      // if the number of particles does not match.
      Attribute
      mean_squared_distance(const SimulationConfiguration::SimulationConfig& other) const;

      // Calculates the mean-squared distance between this config's group and 'other'.
      // Returns the mean-suqred average of all distances or -1,
      // if the number of particles in config 'other' does not match size of
      // 'group'.
      Attribute
      mean_squared_distance(const SimulationConfiguration::SimulationConfig& other,
                            const TagSet& group) const;
      
      Attribute
      number_density() const
        {return config_->num_atoms() / box_volume();}

      typedef Histogram<std::vector<Attribute> > AttributeHistogram;

      AttributeMap
      rdf(
        bool exclude_bonded = true,
        const Attribute delta_r = 0.001,
        float epsilon = 0.01) const
      {
        return analysis::radial_distribution_function(
          * config_, exclude_bonded, delta_r, epsilon);
      }

      // Returns the end_to_end_distance of the molecule defined by the particle
      // with ParticleTag 'tag'.
      Attribute
      end_to_end_distance(const ParticleTag& tag) const;

     protected:
      shared_ptr<const SimulationConfiguration::SimulationConfig> config_;
    };

  } // namespace deprecated

} // namespace mdst

#endif // MDST_CONFIG_ANALYSIS_H_
