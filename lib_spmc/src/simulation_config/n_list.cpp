/*
 * =====================================================================================
 *
 *       Filename:  n_list.cpp
 *
 *    Description:  Neighbor list implementation for SimulationConfig.
 *
 *        Version:  1.0
 *        Created:  10/19/2012 04:54:33 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
 *   Organization:  RWTH Aachen University
 *
 * =====================================================================================
 */

#include "n_list.hpp"
#include "arithmetics.hpp"
#include "simulation_config.hpp"
#include "config_analysis.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    void
    initialize_neighbor_lists(
      NeighborLists & n_lists,
      const size_t N_particles,
      const size_t N_reserve)
    {
      for(size_t i = 0; i < N_particles; ++i){
        n_lists[i] = TagVector();
        n_lists[i].reserve(N_reserve);
      }
    }

    int
    calculate_expected_n_list_size(
      const SimulationConfig & config,
      const Attribute r_max,
      const Attribute security_factor = 2)
    {
      const Attribute delta_r = r_max / 100000;
      std::cerr << "calculate rdf for n_list... " << std::flush;
      const AttributeMap rdf_histogram = 
        analysis::radial_distribution_function(config, false, delta_r);
      std::cerr << "done." << std::endl;
      const int N_expected_in_r_max =
        std::max(7, analysis::expected_number_of_particles(
          config, rdf_histogram, delta_r, r_max));
      return int(N_expected_in_r_max * security_factor);
    }


    void
    SimulationConfig::initialize_neighbor_lists_(
      const NeighborListKey & n_list_key) const
    {
      NeighborLists & n_lists = neighbor_lists_map_[n_list_key];
      const Attribute r_max = n_list_key.r_max();
      std::cerr << "Initializing neighbor list for r_max=" << r_max << std::endl;
      const int N_reserve = calculate_expected_n_list_size(* this, r_max);
      initialize_neighbor_lists(n_lists, num_atoms(), N_reserve);
      update_neighbor_lists_();
    }

    void
    SimulationConfig::update_neighbor_lists() const
    {
      std::cerr << "update_neighbor_lists" << std::endl;
      UniqueLock _(mutex_);
      update_neighbor_lists_();
    }

    void
    SimulationConfig::update_neighbor_lists_() const
    {
      const size_t N = num_atoms();
      #pragma omp parallel
      {
        #pragma omp for
        for(ParticleTag i = 0; i < int(N); ++i)
          calculate_neighbor_list_(i);
      }
      neighbor_list_max_displacement_ = 0;
      if (n_list_update_counter_)
        ++(*n_list_update_counter_);
    }

    TagVectorConstIterator
    SimulationConfig::neighbor_list(
      const NeighborListKey & n_list_key,
      const ParticleTag particle,
      TagVectorConstIterator & first,
      bool & required_update) const
    {
      LockGuard _(mutex_);
      if (neighbor_lists_map_.count(n_list_key) == 0)
        initialize_neighbor_lists_(n_list_key);

      NeighborList & n_list = neighbor_lists_map_[n_list_key][particle];

      if (neighbor_list_max_displacement_ > 0.25 * n_list_key.skin * n_list_key.skin){
        update_neighbor_lists_();
        required_update = true;
      } else {
        required_update = false;
      }

      first = n_list.begin();
      return n_list.end();
    }

    unsigned int
    calculate_cell_neighbor_list(
      const unsigned int * cell,
      const unsigned int * num_cells,
      const Resolution & resolution,
      unsigned int * cell_n_list)
    {
      int i[3];
      unsigned int n = 0;
      for(i[0] = 0; i[0] < 2 * resolution[0] + 1; ++i[0])
        for(i[1] = 0; i[1] < 2 * resolution[1] + 1; ++i[1])
          for(i[2] = 0; i[2] < 2 * resolution[2] + 1; ++i[2])
          {
            int n_cell[3];
            unsigned int n_cell_c[3];
            for(int d = 0; d < 3; ++d)
            {
              n_cell[d] = cell[d] + i[d] - resolution[d] + num_cells[d];
              int pbc = (int)(n_cell[d] / num_cells[d]);
              n_cell[d] -= pbc * num_cells[d];
              n_cell_c[d] = (unsigned int) n_cell[d];
              assert(n_cell_c[d] <= num_cells[d]);
            }
            cell_n_list[n] = get_cell_index(num_cells, n_cell_c);
            ++n;
          }
      return n;
    }

    unsigned int
    num_cell_neighbors_(
      const Resolution & resolution,
      const unsigned int dimension = 3)
    {
      unsigned int result = 1;
      for(unsigned int d = 0; d < dimension; ++d)
        result *= 2 * resolution[d] + 1;
      return result;
    }

    size_t
    calculate_neighbor_list(
      const SimulationConfig & config,
      const CellList & cell_list,
      unsigned int * cell_n_list,
      const unsigned int num_cell_neighbors,
      const NeighborListKey & n_list_key,
      const ParticleTag particle,
      NeighborList & n_list)
    {
      throw NotImplementedError();
      return 0;
      /*const Attribute r_max_squared = n_list_key.squared_r_max();
      n_list.resize(0);
      size_t i = 0;
      for(unsigned int n = 0; n < num_cell_neighbors; ++n)
      {
        const unsigned int cell_index_b = cell_n_list[n];
        const unsigned int size_b = cell_list.size(cell_index_b);
        if (size_b == 0)
          continue;
        const unsigned int offset_b = cell_list.offset(cell_index_b);
        for(unsigned int b = offset_b; b < offset_b + size_b; ++b)
        {
          const ParticleTag p_b = cell_list.particles()[b];
          if(particle == p_b)
            continue;
          SortedTypePair compare_sorted_type_pair(
            config.particle(particle).type(), config.particle(p_b).type());
          if(n_list_key.sorted_type_pair != compare_sorted_type_pair)
            continue;
          if(n_list_key.exclude_bonded > 0)
            if (config.indirectly_bonded(particle, p_b, n_list_key.exclude_bonded))
              continue;
          const Attribute r_squared = minimum_image_squared_distance(config, particle, p_b);
          if (r_squared <= r_max_squared){
            n_list.push_back(p_b);
            ++i;
          }
        }
      }
      return i;*/
    }

    size_t
    calculate_neighbor_list(
      const SimulationConfig & config,
      const NeighborListKey & n_list_key,
      const ParticleTag particle,
      NeighborList & n_list)
    {
      const static unsigned int dimension = 3;
      const static Resolution resolution = default_resolution();
      Tuple3 const & box = config.box();
      CellGrid const & cell_grid_ = config.cell_grid(
        calculate_key(config.box(), n_list_key.r_max(), resolution));
      Tuple3 const position = config.particle(particle).position();
      unsigned int cell_a[dimension];
      calculate_cell(cell_a, position, box, cell_grid_.cell_dimension);
      unsigned int cell_n_list[num_cell_neighbors_(resolution, dimension)];
      const unsigned int * num_cells = cell_grid_.num_cells;
      const unsigned int num_cell_neighbors = 
        calculate_cell_neighbor_list(cell_a, num_cells, resolution, cell_n_list);
      const CellList & cell_list = * cell_grid_.cell_list();
      return calculate_neighbor_list(
        config, cell_list, cell_n_list, num_cell_neighbors, 
        n_list_key, particle, n_list);
    }

    void
    SimulationConfig::calculate_neighbor_list_(
      const ParticleTag particle) const
    {
      for(NeighborListsMapIterator map = neighbor_lists_map_.begin();
          map != neighbor_lists_map_.end(); ++map){
        NeighborList & n_list = map->second.at(particle);
        calculate_neighbor_list(* this, map->first, particle, n_list);
      }
    }

  } // namespace SimulationConfiguration

} // namespace mdst
