set(LIB_PARSER_WRITERS hoomd_blue_xml xyz pugixml)
add_subdirectory(parser_writer)

set(LIBS 
  simulation_config
  config_analysis
  config_manipulation
  parameters
  view
  property_view
  cell_list
  cell_grid
  n_list
  bonds
  dump
  )

foreach(lib ${LIBS})
  add_library(${lib} ${lib}.cpp)
  target_link_libraries(${lib} ${LIB_UTILITIES})
endforeach(lib)

target_link_libraries(n_list config_analysis)
target_link_libraries(cell_grid cell_list)

add_library(qcprot qcprot.c)
install(TARGETS qcprot DESTINATION ${LIB_SPMC_INSTALL_PREFIX})

target_link_libraries(property_view view)
target_link_libraries(
  simulation_config
  view
  property_view
  cell_grid
  n_list
  bonds
  dump
  )
target_link_libraries(config_manipulation config_analysis view random_number_generation)
target_link_libraries(parameters ${LIB_PARSER_WRITERS})

find_package(Threads REQUIRED)
find_package(Boost ${BOOST_MIN_VER} COMPONENTS thread REQUIRED)
target_link_libraries(simulation_config
                        ${CMAKE_THREAD_LIBS_INIT}
                        ${Boost_LIBRARIES}
                     )

foreach(lib ${LIBS})
  install(TARGETS ${lib} DESTINATION ${LIB_SPMC_INSTALL_PREFIX})
endforeach(lib)
