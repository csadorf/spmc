// =====================================================================================
//
//       Filename:  py_set.hpp
//
//    Description:  Definition of py_set for namespace mdst.
//
//        Version:  1.0
//        Created:  10/21/2012 11:00:57 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_PY_SET_H_
#define MDST_PY_SET_H_

#include <set>
#include <boost/python.hpp>

namespace mdst{

  template<class KeyType> class PySet : 
    public std::set<KeyType>,
    public boost::python::wrapper<std::set<KeyType> >
  {
    typedef std::set<KeyType> ParentType;
    typedef PySet<KeyType> WrapperType;

    PySet<KeyType> () {};

    explicit
    PySet<KeyType> (const ParentType & s)
      { insert(s.begin(), s.end());}

  };

} // namespace mdst

#endif // MDST_PY_SET_H_

