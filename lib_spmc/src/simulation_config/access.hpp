// =====================================================================================
//
//       Filename:  access.hpp
//
//    Description:  Access SimulationConfig objects. 
//
//        Version:  1.0
//        Created:  12/25/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATION_CONFIG_ACCESS_H_
#define MDST_SIMULATION_CONFIG_ACCESS_H_

#include "simulation_config.hpp"
#include "view.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    inline
    ParticleView
    check_bounds(
      const SimulationConfig & config,
      const ParticleTag tag)
    {
      if (tag < 0 or tag >= static_cast<ParticleTag>(config.num_atoms()))
      {
        std::stringstream msg;
        msg
          << "particle_safe(" << tag << "): "
          << "Out of range!"
          ;
        throw std::invalid_argument(msg.str());
      }
      return ParticleView(& config, tag);
    }

    template<class ParticleIterator>
    class PositionsIterator : public ParticleIterator
    {
     public:
      PositionsIterator(
        SimulationConfig & config,
        ParticleIterator iterator):
        ParticleIterator(iterator),
        config_(config)
        {};

      Tuple3
      operator*()
      {
        return config_.particle_position(ParticleIterator::operator*());
      }

     private:
      const SimulationConfig & config_;
    };

    class PositionsConstIterator : public TuplesConstIterator
    {
     public:
      PositionsConstIterator(
        const SimulationConfig & config,
        const ParticleTag i = 0):
        TuplesConstIterator(config.particle_positions_cbegin() + i),
        config_(config)
        {};

      Tuple3
      operator*()
      {
        return element_product(TuplesConstIterator::operator*(), config_.box());
      }
     private:
      const SimulationConfig & config_;
    };

    class RemappedPositionsIterator : public std::forward_iterator_tag
    {
     public:
      RemappedPositionsIterator(
        SimulationConfig & config,
        const ParticleTag i = 0):
        config_(config),
        i_(i)
        {};

      RemappedPositionsIterator(
        const RemappedPositionsIterator & other):
        config_(other.config_),
        i_(other.i_)
        {};

      bool
      operator==(
        const RemappedPositionsIterator & rhs)
      { 
        return i_ == rhs.i_;
      }

      bool
      operator!=(
        const RemappedPositionsIterator & rhs)
      {
        return ! operator==(rhs);
      }

      RemappedPositionsIterator &
      operator++()
      {
        ++i_;
        return *this;
      }

      RemappedPositionsIterator
      operator++(int)
      {
        RemappedPositionsIterator tmp(*this);
        ++(*this);
        return tmp;
      }

      Tuple3
      operator*()
      {
        return config_.particle(i_).remapped_position();
      }

     private:
      const SimulationConfig & config_;
      ParticleTag i_;
    };

    template<
      class TagConstIterator>
    class RemappedPositionsConstIteratorFromTagIterator : public std::forward_iterator_tag
    {
     public:
      explicit
      RemappedPositionsConstIteratorFromTagIterator(
        const SimulationConfig & config,
        TagConstIterator iterator):
      config_(config),
      iterator_(iterator)
      {};

      bool
      operator==(
        const RemappedPositionsConstIteratorFromTagIterator & rhs) const
      {
        return iterator_ == rhs.iterator_;
      }

      bool
      operator!=(
        const RemappedPositionsConstIteratorFromTagIterator & rhs) const
      {
        return ! operator==(rhs);
      }

      Tuple3
      operator*() const
      {
        return config_.particle(* iterator_).remapped_position();
      }

      RemappedPositionsConstIteratorFromTagIterator &
      operator++()
      {
        ++iterator_;
        return *this;
      }

      RemappedPositionsConstIteratorFromTagIterator
      operator++(int)
      {
        RemappedPositionsConstIteratorFromTagIterator tmp(* this);
        ++(*this);
        return tmp;
      }

      private:
       const SimulationConfig & config_;
       TagConstIterator iterator_;
    };

    template<
      class TagIterator>
    class ParticleIterator : public std::forward_iterator_tag
    {
     public:
      ParticleIterator():
        config_(SimulationConfig()),
        iterator_(TagIterator())
      {};
      
      explicit
      ParticleIterator(
        SimulationConfig & config,
        TagIterator iterator):
        config_(config),
        iterator_(iterator)
      {};

      bool
      operator==(
        const ParticleIterator & rhs) const
      {
        return iterator_ == rhs.iterator_;
      }

      bool
      operator!=(
        const ParticleIterator & rhs) const
      {
        return !operator==(rhs);
      }

      ParticleView
      operator*()
      {
        return config_.particle(* iterator_);
      }

      ParticleView
      operator*() const
      {
        return config_.particle(* iterator_);
      }

      shared_ptr<ParticleView>
      operator->()
      {
        return shared_ptr<ParticleView>(new ParticleView(& config_, * iterator_));
      }

      ParticleIterator &
      operator++()
      {
        ++iterator_;
        return * this;
      }

      ParticleIterator
      operator++(int)
      {
        ParticleIterator tmp(* this);
        ++(* this);
        return tmp;
      }

     private:
      SimulationConfig & config_;
      TagIterator iterator_;
    };

    template<
      class TagIterator>
    ParticleIterator<TagIterator>
    particle_iterator(
      SimulationConfig & config,
      TagIterator iterator)
    {
      return ParticleIterator<TagIterator>(config, iterator);
    }

    template<
      class ParticleIterator>
    void
    lock_particles(
      SimulationConfig & config,
      ParticleIterator first,
      ParticleIterator last)
    {
      while(first != last)
      {
        config.lock_particle(* first);
        ++first;
      }
    }

    template<
      class ParticleIterator>
    void
    unlock_particles(
      SimulationConfig & config,
      ParticleIterator first,
      ParticleIterator last)
    {
      while(first != last)
      {
        config.unlock_particle(* first);
        ++first;
      }
    }

  } // namespace SimulationConfiguration

} // namespace mdst

#endif // MDST_SIMULATION_CONFIG_ACCESS_H_
