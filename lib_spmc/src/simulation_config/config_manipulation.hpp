/*
 * =====================================================================================
 *
 *       Filename:  config_manipulation.hpp
 *
 *    Description:  Provide functions to manipulate a SimulationConfiguration::SimulationConfig object.
 *
 *        Version:  1.0
 *        Created:  10/19/2012 04:56:39 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
 *   Organization:  RWTH Aachen University
 *
 * =====================================================================================
 */

#ifndef MDST_CONFIG_MANIPULATION_H_
#define MDST_CONFIG_MANIPULATION_H_

#include "config_analysis.hpp"

#include <memory.hpp>

namespace mdst{

  // Forward declarations
  class Tuple3;

  //
  // Class declarations
  // 

  // 
  class ConfigManipulator{
   public:
    explicit ConfigManipulator(
      const shared_ptr<SimulationConfiguration::SimulationConfig> & config):
      config_(config),
      analyzer_(config_) {};

    void set_config(
      const shared_ptr<SimulationConfiguration::SimulationConfig> & config);

    // Basic manipulation
    //
    //
    void move(const Tuple3& position,
              const Tuple3& image,
              const TagSet& group);
    void move(const Tuple3& position,
              const TagSet& group);
    void move(const Tuple3& position,
              const Tuple3 image = Tuple3(0));

    // Rotation
    //
    // Rotate group by delta_theta and delta_phi around pivot.
    void rotate(const Tuple3& pivot,
                const PlaneAngle& delta_theta,
                const PlaneAngle& delta_phi,
                const TagSet& group);
    // Rotate group by the rotation of vector from to vector to around pivot.
    void rotate(const Tuple3& pivot,
                const Tuple3& from,
                const Tuple3& to,
                const TagSet& group);
    // Rotate group by the rotation of vector center_of_mass of group
    // to vector to around pivot.
    void rotate(const Tuple3& pivot,
                const Tuple3& to,
                const TagSet& group);
    // Rotate all particles by the rotation of vector center_of_mass to
    // vector to around pivot.
    void rotate(const Tuple3& pivot,
                const Tuple3& to);

    // Advanced manipulation
    //
    //
    void
    rename(const ParticleType& new_type,
           const TagSet& group);

    // Move every particle to the position given in 'map'.
//    void
//    remap(const TupleMap& map);

    // Map every particle in 'group' into the simulation box.
    void
    remap(const TagSet & group);

    // Map every particle into the simulation box.
    void
    remap();

    // Set temperature of every particle in 'group' to 'reduced_temperature'.
    void
    set_temperature(Attribute reduced_temperature,
                    unsigned int random_seed,
                    const TagSet & group);

    // Set temperature of every particle to 'reduced_temperature'.
    void
    set_temperature(Attribute reduced_temperature,
                    unsigned int random_seed);

   protected:
    shared_ptr<SimulationConfiguration::SimulationConfig> config_;
    deprecated::ConfigAnalyzer analyzer_;
  };

} // namespace mdst

#endif // MDST_CONFIG_MANIPULATION_H_
