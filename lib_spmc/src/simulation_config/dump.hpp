// =====================================================================================
//
//       Filename:  dump.hpp
//
//    Description:  Dump properties of objects of SimulationConfig.
//
//        Version:  1.0
//        Created:  12/20/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_SIMULATION_CONFIG_DUMP_H_
#define MDST_SIMULATION_CONFIG_DUMP_H_

#include "simulation_config.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    std::ostream &
    print_positions(
      const SimulationConfig & config,
      std::ostream & stream = std::cout);

  } // namespace SimulationConfiguration

} // namespace mdst

#endif // MDST_SIMULATION_CONFIG_DUMP_H_
