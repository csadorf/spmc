// =====================================================================================
//
//       Filename:  property_view.hpp
//
//    Description:  PropertyView for SimulationConfiguration.
//
//        Version:  1.0
//        Created:  11/27/2012 11:49:37 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_PROPERTY_VIEW_H_
#define MDST_PROPERTY_VIEW_H_

#include "view.hpp"

namespace mdst{

  namespace SimulationConfiguration{

    /*! \brief Special views on a simulation configuration.
     *
     * Special ways to view or manipulate a simulation configuration.
     * Methods within this function are encapsulated in this special
     * class in order to keep the actual resource management class
     * for simulation configurations as brief as possible.
     * Note: This class may underly more frequent changes 
     * than SimulationConfig.
     */
    class PropertyView : public View{
     private:
      typedef shared_ptr<std::set<Tag> > TagSetPtr;
      typedef std::set<TagSet> TagSetSet;
      typedef TagSetSet::const_iterator TSSConstIterator;

     public:
      PropertyView( const SimulationConfig * parent): View(parent){};
      // Returns true if the particles 'a' and 'b' are directly bonded.
      bool directly_bonded(const ParticleTag& a, const ParticleTag& b) const;
      // Returns true if the particles 'a', 'b', 'c' and 'd' are directly bonded.
      bool directly_bonded(const ParticleTag& a,
                           const ParticleTag& b,
                           const ParticleTag& c,
                           const ParticleTag& d = ParticleTag(-1)) const;
      // Returns true if the particles 'a' and 'b' are directly or indirectly bonded.
      bool bonded(const ParticleTag& a, const ParticleTag& b) const;
      // Returns true if the particles 'a' and 'b' are indirectly bonded and
      // there are not more than 'steps' - 1 particles between them.
      bool bonded(const ParticleTag& a, const ParticleTag& b, unsigned int steps) const;
      // Generate a set of paths, which have their root in particle 'parent'.
      // Parent ist always the first member of path.
      // Each path has a maximum depth of 'depth'.
      // If 'depth=-1', every path is walked until its end.
      Walks paths(const ParticleTag& parent,
                  int depth = -1,
                  TagVector* path = new TagVector()) const;
      // Generate a set particles in the neighbourhood of particle 'parent'
      // up to depth 'depth'.
      // If 'depth'=-1, all bonded particles are returned.
      TagSet neighbourhood(const ParticleTag& parent,
                           int depth = 1,
                           const TagSetPtr& result = TagSetPtr(new TagSet())) const;
      // Generate a set of particles, which are neighbours of particle 'parent'
      // up to depth 'depth'.
      // The particle 'parent' itself, is not part of the set.
      TagSet neighbours(const ParticleTag& parent, int depth = 1) const;
      // Return a TagSet containing all tags, which are in the same molecule as 'tag'. (Precalculated)
      TagSet molecule(const ParticleTag & tag) const;
      // Return a TagSet containing all tags, which are in the same molecule as 'tag'.
      TagSet calculate_molecule(const ParticleTag& tag) const
        {return neighbourhood(tag, -1);}
      // Returns a TagSetSet of all molecules present in the configuration.
      TagSetSet molecules() const;
      // Generate multiple walks of the molecule 'molecule'.
      Walks generate_defined_walks(const TagSet& molecule) const;
      // Generate multiple walks through the whole configuration.
      Walks generate_defined_walks() const;
      // Generate multiple, but unique walks of the molecule 'molecule'.
      // No particle is seen twice by this walk.
      Walks generate_unique_defined_walks(const TagSet& molecule) const;
      // Generate multiple, but unique walks of the whole configuration.
      // No particle is seen twice by this walk.
      Walks generate_unique_defined_walks() const;
      // Generate a unified walk of the molecule 'molecule'.
      // No particle is seen twice by this walk.
      TagVector generate_unique_unified_walk(const TagSet& molecule) const;
      // Generate a unified walk of the whole configuration.
      // No particle is seen twice by this walk.
      TagVector generate_unique_unified_walk() const;
      // Calculate the distance between particle with tag 'a' and particle with
      // tag 'b'. Distance is returned in reduced units.
      Attribute distance(const ParticleTag& a,
                         const ParticleTag& b) const;
      // Map the molecule defined by the single particle with ParticleTag 'tag' into
      // the simulation box.
      TupleMap remap(const ParticleTag& tag,
                     const ParticleTag& caller = -1,
                     bool init = true) const;
      // If no single end can be identified a DataCorruptedError is thrown.
      // Find all ends of the molecule defined by 'molecule'. The order
      // of ends is determined by the natural order of tags.
      TagSet find_ends(const TagSet& molecule) const;

      // Return a list of expanded positions
      Tuples
      expanded_positions() const;

      shared_ptr<TagSet>
      molecule_ptr(
        const ParticleTag & p) const;

     private:
      bool add_walk_(const TagSet& molecule,
                     const ParticleTag& start,
                     Walks& walks,
                     int path) const;
      bool in_n_bond_(const TagSet& tags, const unsigned int n) const;
      TagSet n_bonds_(const ParticleTag& particle,
                      const unsigned int n) const;
    };

  } // namespace SimulationConfiguration

} // namespace mdst

#endif // MDST_PROPERTY_VIEW_H_
