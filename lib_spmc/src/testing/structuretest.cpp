// =====================================================================================
//
//       Filename:  structuretest.cpp
//
//    Description:  Test the structure lookup library.
//
//        Version:  1.0
//        Created:  04/04/2014
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <simulation_config/structure_library.hpp>
#include <utility/misc.hpp>

using namespace mdst;
using namespace SimulationConfiguration;

int
main ( int argc, char *argv[] )
{
  typedef Structure<Attribute, 3> Structure3;
  Structure3 frag_small(3);
  frag_small.matrix()[0][0] = 1;
  frag_small.matrix()[1][0] = 0;
  frag_small.matrix()[2][0] = 0;
  frag_small.matrix()[0][1] = -1;
  frag_small.matrix()[1][1] = 2;
  frag_small.matrix()[2][1] = 10;
  frag_small.matrix()[0][2] = 0;
  frag_small.matrix()[1][2] = 0;
  frag_small.matrix()[2][2] = 10;
  std::cerr << "Center test: " << std::endl;
  frag_small.dump();
  frag_small.centered().dump();

  Structure3 frag_a(7);
  Structure3 frag_b(7);

  frag_a.matrix()[0][0] =  -2.803;
  frag_a.matrix()[1][0] = -15.373;
  frag_a.matrix()[2][0] =  24.556;
  frag_a.matrix()[0][1] =   0.893;
  frag_a.matrix()[1][1] = -16.062;
  frag_a.matrix()[2][1] =  25.147;
  frag_a.matrix()[0][2] =   1.368;
  frag_a.matrix()[1][2] = -12.371;
  frag_a.matrix()[2][2] =  25.885;
  frag_a.matrix()[0][3] =  -1.651;
  frag_a.matrix()[1][3] = -12.153;
  frag_a.matrix()[2][3] =  28.177;
  frag_a.matrix()[0][4] =  -0.440;
  frag_a.matrix()[1][4] = -15.218;
  frag_a.matrix()[2][4] =  30.068;
  frag_a.matrix()[0][5] =   2.551;
  frag_a.matrix()[1][5] = -13.273;
  frag_a.matrix()[2][5] =  31.372;
  frag_a.matrix()[0][6] =   0.105;
  frag_a.matrix()[1][6] = -11.330;
  frag_a.matrix()[2][6] =  33.567;

  frag_b.matrix()[0][0] = -14.739;
  frag_b.matrix()[1][0] = -18.673;
  frag_b.matrix()[2][0] =  15.040;
  frag_b.matrix()[0][1] = -12.473;
  frag_b.matrix()[1][1] = -15.810;
  frag_b.matrix()[2][1] =  16.074;
  frag_b.matrix()[0][2] = -14.802;
  frag_b.matrix()[1][2] = -13.307;
  frag_b.matrix()[2][2] =  14.408;
  frag_b.matrix()[0][3] = -17.782;
  frag_b.matrix()[1][3] = -14.852;
  frag_b.matrix()[2][3] =  16.171;
  frag_b.matrix()[0][4] = -16.124;
  frag_b.matrix()[1][4] = -14.617;
  frag_b.matrix()[2][4] =  19.584;
  frag_b.matrix()[0][5] = -15.029;
  frag_b.matrix()[1][5] = -11.037;
  frag_b.matrix()[2][5] =  18.902;
  frag_b.matrix()[0][6] = -18.577;
  frag_b.matrix()[1][6] = -10.001;
  frag_b.matrix()[2][6] =  17.996;

  frag_a.dump();
  frag_a.centered().dump();
  frag_b.dump();
  Structure3 * frag_b_check = new Structure3(frag_b);
  frag_b_check->dump();
  delete frag_b_check;
  frag_a.center();
  frag_b.center();
  Structure3::RotationalMatrix rm;
  std::cerr << "rmsd = " << frag_a.calculate_rmsd_rotational_matrix(frag_b, rm) << std::endl;
  print_iterable(rm.begin(), rm.end()) << std::endl;

  Attribute sum = 0;
  //const int iterations = 1000000;
  const int iterations = 1000;
  for(int i = 0; i < iterations; ++i)
    sum +=
      frag_a.calculate_rmsd(frag_b);

  std::cerr << "average rmsd = " << sum / iterations << " (correct value: .719106)" << std::endl;

  return 0; 
}				// ----------  end of function main  ---------- 
