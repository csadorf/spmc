// =====================================================================================
//
//       Filename:  fftest.cpp
//
//    Description:  Testing program.
//
//        Version:  1.0
//        Created:  10/22/2012 11:27:01 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <iostream>
#include <parser_writer/hoomd_blue_xml.hpp>
#include "forcefield_calculation.hpp"
#include "dodecane.hpp"
#include <ctime>

using namespace mdst;
using namespace SimulationConfiguration;
using namespace parameters::forcefields;

int
main ( int argc, char *argv[] ){
  std::string filename;
  if (argc > 1)
    filename = std::string(argv[1]);
  else
    filename = "restart.xml";
  std::cerr << "Reading '" << filename << "' ..." << std::endl;
  int ITERATIONS = 1;
  if (argc > 2){
    std::string par2(argv[2]);
    std::istringstream(par2) >> ITERATIONS;
  }

  try{
    ParserWriter::HoomdBlueXml parser;
    shared_ptr<SimulationConfig> config(parser.parse(filename));
    ForceFieldCalculation forcefield_calculation(dodecane::carbon,
                                                 dodecane::parameters());
    const TagSet subset = config->all();
    Attribute energy = 0;
//    energy += forcefield_calculation.lennard_jones(*config, subset, 3);
//    energy += forcefield_calculation.harmonic_bond(*config, subset);
//    energy += forcefield_calculation.harmonic_angle(*config, subset);
    energy += forcefield_calculation.torsion(*config, subset);
    std::cout << "Potential energy: " << energy << std::endl;
    return (int) energy;
  }
  catch (ParserWriter::ParseException & e){
    std::cerr << "Error while parsing." << std::endl;
    // Intentionally not exiting the program!!
  }
  std::cerr << "Exit." << std::endl;
  return 0;
}				// ----------  end of function main  ---------- 
