// =====================================================================================
//
//       Filename:  dumptest.cpp
//
//    Description:  Testing program.
//
//        Version:  1.0
//        Created:  10/22/2012 11:27:01 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <iostream>
#include <parser_writer/hoomd_blue_xml.hpp>
#include <parser_writer/xyz.hpp>
#include "simulation_config.hpp"
#include "config_analysis.hpp"
#include "config_manipulation.hpp"

using namespace mdst;
using namespace SimulationConfiguration;

void
test_parser_writer(const ParserWriter::Parser& parser,
                   const ParserWriter::Writer & writer,
                   const std::string filename){
  shared_ptr<SimulationConfig> config(parser.parse(filename));
  writer.write(* config, std::cout);
}

void
dump_hoomd(const SimulationConfig & config){
  ParserWriter::HoomdBlueXml().write(config, std::cout);
}

void
dump_property( SimulationConfig * config){
    std::cerr << config->num_atoms() << std::endl;
    std::cerr << config->dimension() << std::endl;
    Tag tag(0);
    ParticleView my_particle = config->particle(tag);
    std::cerr << tag << ": " << my_particle.mass() << std::endl;
    std::cerr << config->num_atoms() << std::endl;
    std::cerr << "box " << config->box() << std::endl;
}

void
dump_distance( SimulationConfig * config){
    std::cerr << "Calculate distance between particle 0 and 1: ";
    std::cerr << config->property.distance(0, 1) << std::endl;
}

void
remap(const shared_ptr<SimulationConfig> & config){
  ConfigManipulator manipulator(config);
  manipulator.remap();
}
  

void
dump_analyzer( const shared_ptr<SimulationConfig> & config){
  ConfigAnalyzer analyzer(config);
  std::cerr << "box_volume: " << analyzer.box_volume() << std::endl;
//  std::cerr << "Calculating rdf (bonded included)..." << std::endl;
//  dump_map<AttributeMap>(analyzer.rdf(false));
  std::cerr << "Calculating rdf (bonded excluded)..." << std::endl;
  dump_map<AttributeMap>(analyzer.rdf(), std::cout);
}

void
dump_copy(const SimulationConfig & config){
  // Create a copy
  SimulationConfig copied_config(config);
  dump_hoomd(copied_config);
}

void
dump_subset(const SimulationConfig & config){
  // subset is first and third molecule
  const TagSetSet molecules = config.property.molecules();
  TagSetSetConstIterator it = molecules.begin();
  TagSet subset;
  subset.insert(it->begin(), it->end());
  ++it;
  subset.insert(it->begin(), it->end());
  SimulationConfig subset_config(config, subset);
  dump_hoomd(subset_config);
}

void
dump_group_subset(SimulationConfig const & config){
  const TagSet group_C = config.group(ParticleType("C"));
  SimulationConfig config_C(config, group_C);
  dump_hoomd(config_C);
}

void
dump_append(const SimulationConfig & config){
  SimulationConfig copied_config(config);
  copied_config.append(config);
  dump_hoomd(copied_config);
}

void
dump_change_size(SimulationConfig * config, int new_size){
  config->set_num_atoms(new_size);
}

void
dump_generate_angles(SimulationConfig* config){
  config->generate_angles();
}

shared_ptr<SimulationConfig>
sparse(const SimulationConfig & config){
  const TagSet selection = config.group(0, 12);
  return shared_ptr<SimulationConfig> (new SimulationConfig(config, selection));
}

void
dump_mean_squared_distance(shared_ptr<SimulationConfig> config,
                           const SimulationConfig& other){
  ConfigAnalyzer analyzer(config);
  std::cout << analyzer.mean_squared_distance(other);
}

void
set_temperature(shared_ptr<SimulationConfig> config,
                Attribute reduced_temperature){
  ConfigManipulator manipulator(config);
  manipulator.set_temperature(
    reduced_temperature,
    static_cast<unsigned int>(std::time(0))
    );
}

int
main ( int argc, char *argv[] ){
  std::string filename("restart.xml");
  if (argc > 1)
    filename = std::string(argv[1]);
  std::string filename2("other.xml");
  if (argc > 2)
    filename2 = std::string(argv[2]);
  try{
    shared_ptr<SimulationConfig> config = 
      ParserWriter::HoomdBlueXml().parse(filename);
    remap(config);
    set_temperature(config, 6);
    ParserWriter::HoomdBlueXml().write(*config, std::cout);
  }
  catch (ParserWriter::ParseException & e){
    std::cerr << "Error while parsing: " << e.error() << std::endl;
    // Intentionally not exiting the program!!
  }
  std::cerr << "Exit." << std::endl;
  return 0;
}				// ----------  end of function main  ---------- 
