// =====================================================================================
//
//       Filename:  simulate.cpp
//
//    Description:  Simulate configurations.
//
//        Version:  1.0
//        Created:  01/19/2013 07:26:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <iostream>
#include <boost/program_options.hpp>
#include <boost/version.hpp>

#if BOOST_VERSION>=104200
#include <boost/exception/all.hpp>
#endif

#include <omp.h>

int
main(int argc, const char * argv[])
{
  #pragma omp parallel
  std::cout << "Hello World!" << std::endl;

  return 0;
}

