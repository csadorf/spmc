// =====================================================================================
//
//       Filename:  tupletest.cpp
//
//    Description:  Test the boost::tuple class.
//
//        Version:  1.0
//        Created:  10/22/2012 09:35:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>
#include <vector>
#include <iostream>

typedef boost::tuple<int, int> Pair;
typedef std::vector<Pair> Pairs;

int
main ( int argc, char *argv[] )
{
  Pair mypair(1, 2);
  Pairs mypairs;
  mypairs.push_back(mypair);
  std::cout << mypair << std::endl;
  std::cout << mypairs.front() << std::endl;
  std::cout << "End of test." << std::endl;
  return 0; 
}				// ----------  end of function main  ---------- 
