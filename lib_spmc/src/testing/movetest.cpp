// =====================================================================================
//
//       Filename:  rmtest.cpp
//
//    Description:  Testing program.
//
//        Version:  1.0
//        Created:  10/22/2012 11:27:01 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <iostream>
#include <parser_writer/hoomd_blue_xml.hpp>
#include "dodecane.hpp"
#include "monte_carlo_mover.hpp"
#include "config_manipulation.hpp"
#include <ctime>
// DEBUG
#include "coordinates.hpp"

using namespace mdst;
using namespace SimulationConfiguration;
using namespace parameters::forcefields;

int
main ( int argc, char *argv[] ){
  std::string filename;
  if (argc > 1)
    filename = std::string(argv[1]);
  else
    filename = "restart.xml";
  std::cerr << "Reading '" << filename << "' ..." << std::endl;
  int iterations = 1;
  if (argc > 2){
    std::string par2(argv[2]);
    std::istringstream(par2) >> iterations;
  }

  try{
    ParserWriter::HoomdBlueXml parser_writer;
    shared_ptr<SimulationConfig> result(new SimulationConfig());
    shared_ptr<SimulationConfig> config = parser_writer.parse(filename);
    RandomNumberGenerator random_number_generator((unsigned int) std::time(0));

    MonteCarloMover mover(config,
                          dodecane::parameters(),
                          dodecane::carbon,
                          random_number_generator);
    ConfigManipulator manipulator(config);
    ParticleTag planet = 0;
    TagSet moons = config->all();
    moons.erase(0);

    result->append(*config);
    std::cerr << "Calculating..." << std::endl;
    int i  = 0;
    while ( i < iterations){
      ++i;
      const Tuple3 pivot = config->particle(planet).position();
      manipulator.rotate(pivot,
                         Tuple3(1, 0, 0),
                         Tuple3(0, 1, 0),
                         config->all());
//      mover.rotate_fixed_moons(planet, moons);
      result->append(*config);
    }
    parser_writer.write(*result, std::cout);
  }
  catch (ParserWriter::ParseException & e){
    std::cerr << "Error while parsing." << std::endl;
    // Intentionally not exiting the program!!
  }
  std::cerr << "Exit." << std::endl;
  return 0;
}				// ----------  end of function main  ---------- 

