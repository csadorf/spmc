// =====================================================================================
//
//       Filename:  structuretest.cpp
//       Filename:  reverse_mapping.cpp
//
//    Description:  Test the reverse mapping library.
//
//        Version:  1.0
//        Created:  04/09/2014
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <simulation_config/structure_library.hpp>
#include <reverse_mapping/wavelet_reverse_mapping_library.hpp>
#include <simulation_config/simulation_config.hpp>
#include <simulation_config/parser_writer/writers.hpp>
#include <simulation_config/access.hpp>
#include <utility/misc.hpp>
#include <simulator/session.hpp>
#include <utility/random_number_generation.hpp>

using namespace mdst;
using namespace SimulationConfiguration;
using namespace reverse_mapping;
using ParserWriter::dump_xyz;
using ParserWriter::get_writer;

Tuple3
random_tuple(
  RandomNumberGenerator & rng)
{
  return Tuple3(
    rng.random_number(),
    rng.random_number(),
    rng.random_number());
}

void
dump_config(
  const SimulationConfig & config)
{
  if (config.num_atoms() > 20 * pow(2, 10))
    return;
  dump_xyz(config, "xyz", std::cout);
}

void
write_config(
  const SimulationConfig & config,
  const std::string & filename)
{
  get_writer("hoomd_blue_xml")->write(config, filename);
}

int
main ( int argc, char *argv[] )
{
  RandomNumberGenerator rng(42);

  size_t n;
  n = 100 * pow(2, 14);
  n = 10 * pow(2, 14);
  n = 20 * pow(2, 9);
  n = 16;
  shared_ptr<SimulationConfig> config_cg(new SimulationConfig());
  config_cg->set_box(Tuple3(100));
  shared_ptr<SimulationConfig> config_at(new SimulationConfig(*config_cg));
  config_cg->set_num_atoms(n);
  config_at->set_num_atoms(2 * n);
  const Tuple3 delta(0.5, 0, 0);
  TagSetMap mapping;
  std::cerr << "setting up cg and at config..." << std::endl;
  Tuple3 last(element_product(random_tuple(rng), config_cg->box()));
  for(size_t i = 0; i < n; ++i)
  {
    config_cg->particle(i).set_position(last);
    const float bond_length = 1.4;
    if (rng.random_number() < 0.7)
      last = last + bond_length * normalize(random_tuple(rng));
    else
      last = last - bond_length * normalize(random_tuple(rng));
  }
  for(size_t i = 0; i < n; ++i)
  {
    Tuple3 const & cg_bead(config_cg->particle(i).remapped_position());
    const Tuple3 delta(normalize(random_tuple(rng)) * 0.5);
    config_at->particle(2 * i).set_position(cg_bead - delta);
    config_at->particle(2 * i + 1).set_position(cg_bead + delta);
    shared_ptr<TagSet> set(new TagSet);
    set->insert(2 * i);
    set->insert(2 * i + 1);
    mapping.insert(std::make_pair(i, set));
  }
  std::cerr << "bonding all particles..." << std::endl;
  config_cg->bond_all_particles();
  config_at->bond_all_particles();
  write_config(* config_cg, "_config_cg.xml");
  write_config(* config_at, "_config_at.xml");
  WaveletReverseMappingLibrary lib;
  std::cerr << "learning from..." << std::endl;
  lib.learn_from(* config_cg, * config_at, mapping);
  std::cerr << "disturbing cg config..." << std::endl;
  typedef ParticleIterator<SimulationConfig::TagIterator> PI;
  for(PI it(* config_cg, config_cg->all_begin());
      it != PI(* config_cg, config_cg->all_end()); ++it)
  {
    it->set_position(it->remapped_position() + random_tuple(rng));
  }
  shared_ptr<SimulationConfig> config_cg_disturbed(config_cg); // emphasize difference
  config_cg.reset();                                           // ...
  write_config(* config_cg_disturbed, "_config_cg2.xml");
  shared_ptr<SimulationConfig> config_at2(new SimulationConfig);
  config_at2->set_num_atoms(2 * n);
  config_at2->set_box(config_cg_disturbed->box());
  const Tuple3 delta2(0, 0.5, 0);
  for(size_t i = 0; i < n; ++i)
  {
    Tuple3 const & cg_bead(config_cg_disturbed->particle(i).remapped_position());
    config_at2->particle(2 * i).set_position(cg_bead - delta2);
    config_at2->particle(2 * i + 1).set_position(cg_bead + delta2);
  }
  config_at2->bond_all_particles();
  write_config(* config_at2, "_config_at2_pre.xml");
  std::cerr << "improving from lib..." << std::endl;
  lib.improve(* config_cg_disturbed, * config_at2, mapping);
  std::cerr
    << "rmsd of a and b: "
    << lib.compare(* config_at, * config_at2)
    << std::endl
    ;
  write_config(* config_at2, "_config_at2_post.xml");
  return 0; 
}				// ----------  end of function main  ---------- 
