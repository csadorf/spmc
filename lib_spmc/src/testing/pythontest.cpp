// =====================================================================================
//
//       Filename:  pythontest.cpp
//
//    Description:  Test program to test python bindings.
//
//        Version:  1.0
//        Created:  11/07/2012 06:51:06 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <Python.h>
#include <boost/python.hpp>
#include <iostream>
#include <string>
#include <sstream>

namespace py = boost::python;
//using boost::python::object;
//using boost::python::exec;
//using boost::python::import;

int
main(int argc, char * argv[]){
  try{
    Py_Initialize();
    py::object main_module = py::import("__main__");
    py::object main_namespace = main_module.attr("__dict__");
    py::exec("print 'Hello World!'", main_namespace);
    py::object n_dodecane_module = py::import("n_dodecane");
    py::dict n_dodecane = py::extract<py::dict>(n_dodecane_module.attr("n_dodecane"));
    std::cout << py::len(n_dodecane.values()) << std::endl;
    py::list iterkeys = static_cast<py::list>(n_dodecane.iterkeys());
    for(int i = 0; i < py::len(iterkeys); ++i){
      std::string key =  py::extract<std::string>(iterkeys[i]);
      std::string value = py::extract<std::string>(n_dodecane[iterkeys[i]]);
      std::cout << "Key:   " << key << std::endl;
      std::cout << "Value: " << value << std::endl;
    }
  }
  catch (...)
  {
    PyErr_Print();
    PyErr_Clear();
    return 1;
  }
  Py_Finalize();
  return 0;
}

