// =====================================================================================
//
//       Filename:  parametertest.cpp
//
//    Description:  Test classes for unit conversion.
//
//        Version:  1.0
//        Created:  11/09/2012 12:47:30 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================


#include "dodecane.hpp"

using namespace mdst;
using namespace mdst::units;
using namespace si;
using namespace parameters::forcefields;

int
main(int argc, char* argv[]){
  const Length my_length(12 * nano * meter);
  const Mass my_mass( 1 * amu);
  const Energy my_energy(1 * zepto * joule);
  const BondStretchingForce my_bsforce(my_energy / boost::units::pow<2>(my_length));
  const BondBendingForce my_bbforce(my_energy / boost::units::pow<2>(radian));
  std::cout << "Quantities:" << std::endl;
  std::cout << my_length << std::endl;
  std::cout << my_mass << std::endl;
  std::cout << my_energy << std::endl;
  std::cout << my_bsforce << std::endl;
  std::cout << my_bbforce << std::endl;
  std::cout << "Quantities (reduced by Argon):" << std::endl;
  std::cout << argon.reduce(my_length) << std::endl;
  std::cout << argon.reduce(my_mass) << std::endl;
  std::cout << argon.reduce(my_energy) << std::endl;
  std::cout << argon.reduce(my_bsforce) << std::endl;
  std::cout << argon.reduce(my_bbforce) << std::endl;
  
  std::cout << "Argon:" << std::endl;
  std::cout << mdst::reduced_units::argon_length << std::endl;
  std::cout << mdst::reduced_units::argon_mass << std::endl;
  std::cout << mdst::reduced_units::argon_energy << std::endl;

  std::cout << boost::units::quantity<boost::units::si::plane_angle>(1 * boost::units::si::radians) << std::endl;

  try{
    mdst::Parameters dodecane_pars(dodecane::parameters());
  }
  catch (mdst::SimulationConfiguration::ParserWriter::ParseException & e){
    std::cerr << "Error while parsing: " << e.error() << std::endl;
  }
  mdst::ReducedUnitsSet dodecane(dodecane::carbon);
}

