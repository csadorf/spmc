// =====================================================================================
//
//       Filename:  rmtest.cpp
//
//    Description:  Testing program.
//
//        Version:  1.0
//        Created:  10/22/2012 11:27:01 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <iostream>
#include <parser_writer/hoomd_blue_xml.hpp>
#include "reverse_mapping.hpp"
#include "dodecane.hpp"
#include "ff_agrawal.hpp"
#include <ctime>
// DEBUG
#include "coordinates.hpp"
#include <boost/filesystem.hpp>

using namespace mdst;
using namespace SimulationConfiguration;
using namespace parameters::forcefields;

int
main ( int argc, char *argv[] ){
  std::string filename("restart.xml");
  if (argc > 1)
    filename = std::string(argv[1]);
  std::cerr << "Reading '" << filename << "' ..." << std::endl;
  int number_of_threads = 0;
  if (argc > 2){
    std::string par(argv[2]);
    std::istringstream(par) >> number_of_threads;
  }
  int iterations = 1;
  if (argc > 3){
    std::string par(argv[3]);
    std::istringstream(par) >> iterations;
  }

  try{
    ParserWriter::HoomdBlueXml parser_writer;
    shared_ptr<SimulationConfig> other_config(parser_writer.parse(filename));
    shared_ptr<SimulationConfig> result(new SimulationConfig());
    result->set_box(other_config->box());
    unsigned int seed = 0;

    std::cerr << "Calculating..." << std::endl;
    int i = 0;
    while( i < iterations){
      unsigned int new_seed = static_cast<unsigned int> (std::time(0));
      if (new_seed == seed)
        continue;
      time_t rawtime;
      time(&rawtime);
      std::cerr << asctime(localtime(&rawtime)) << std::endl;

      ++i;
      std::cerr << std::endl << "ITERATION: " << i << "/" << iterations << std::endl;
      seed = new_seed;
      shared_ptr<SimulationConfig> config(new SimulationConfig());
      reverse_mapping::ReverseMapper rm(config,
                       dodecane::parameters(),
                       dodecane::carbon,
                       seed);
      rm.reverse_map(*other_config,
                     number_of_threads);
      result->append(*config);
      time(&rawtime);
      std::cerr << asctime(localtime(&rawtime)) << std::endl;
    }
    parser_writer.write(*result, "result.xml");
  }
  catch (ParserWriter::ParseException & e){
    std::cerr << "Current directory: '" << boost::filesystem::current_path() << "'" << std::endl;
    std::cerr << "Error while parsing: ";
    std::cerr << e.error() << std::endl;	
    // Intentionally not exiting the program!!
  }
  std::cerr << "Exit." << std::endl;
  return 0;
}				// ----------  end of function main  ---------- 

