// =====================================================================================
//
//       Filename:  forcefield_parameters.hpp
//
//    Description:  Includes all canonical forcefield parameters.
//
//        Version:  1.0
//        Created:  01/20/2013 07:45:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef FORCEFIELD_PARAMETERS_H_
#define FORCEFIELD_PARAMETERS_H_

#include "dodecane.hpp"
#include "ff_agrawal.hpp"
#include "misc.hpp"

namespace mdst{

  namespace parameters{

    namespace forcefields{

       ParameterOptions
       parse_options(const std::string & options){
         ParameterOptions parsed_options;
         // split by ','
//         Strings tokens_by_comma;
         std::vector<std::string> tokens_by_comma;
         split(tokens_by_comma, options, ',', split_t::no_empties);
         // split by '='
         for(StringsConstIterator it = tokens_by_comma.begin();
             it != tokens_by_comma.end();
             ++it){
           std::vector<std::string> key_value;
           split(key_value, *it, '=', split_t::no_empties);
           if (key_value.size() < 2){
             std::stringstream error;
             error << "Failed to parse parameter options: '";
             error << options << "'.";
             error << "Should be of form : 'key1=arg1,key2=arg2,...,keyn=argn'.";
             throw ParameterError(error.str());
           }
           parsed_options[key_value[0]] = key_value[1];
         }
         return parsed_options;
       }

       mdst::Parameters
       by_name(const std::string & name,
               const ParameterOptions & options = ParameterOptions()){
         if (name == "dodecane")
           return dodecane::parameters(options);
         if (name == "agrawal")
           return ff_agrawal::parameters(options);
      // else if (name == "generic") // <-- (csa) example usage
      //  return generic::parameters();
         else {
           std::stringstream error;
           error << "No forcefield parameters found for '" << error << "'.";
           throw mdst::ParameterError(error.str());
         }
       }

    } // namespace forcefields

    namespace reduced_units{

      mdst::ReducedUnitsSet
      by_name(const std::string & name){
        if (name == "carbon")
          return carbon;
        else if (name == "hydrogen")
          return hydrogen;
        else{
          std::stringstream error;
          error << "Failed to find ReducedUnitsSet with name '";
          error << name << "'.";
          throw mdst::ParameterError(error.str());
        }
      }

    } // namespace reduced_units

  } // namespace parameters

} // namespace mdst

#endif // FORCEFIELD_PARAMETERS_H_
