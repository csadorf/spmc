// =====================================================================================
//
//       Filename:  ff_agrawal.hpp
//
//    Description:  Parameters for ff_agrawal.
//
//        Version:  1.0
//        Created:  11/13/2012 06:10:20 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef FF_AGRAWAL_H_
#define FF_AGRAWAL_H_

#include "parameters.hpp"
#include "reduced_units.hpp"
#include <parser_writer/xyz.hpp>

namespace mdst{

  namespace parameters{

    namespace forcefields{

      namespace ff_agrawal{
        
        namespace internal{

          using namespace mdst::units;
          using namespace boost::units;
          using namespace si;
          using namespace constants::codata;
          using namespace degree;

          using boost::units::pow;

          const Length SIGMA_A(3.93 * angstrom);
          const Energy EPSILON_A(114.0 * kelvin * k_B);

          ParticleParameters A = {
            Length(SIGMA_A),
            Energy(EPSILON_A),
            ReverseMappingParameters()
          };

          ReverseMappingParameters
          cg_th_reverse_mapping(unsigned int cg){
            std::cerr << "Generating replacement template for " << cg << std::endl;
            std::stringstream xyz_buffer;
            xyz_buffer << cg << std::endl;
            xyz_buffer << "Auto-generated template config (lvl " << cg << ")." << std::endl;
            Tuple3 position;
            Attribute bond_length = 1;
            for(unsigned int i = 0; i < cg; ++i){
              xyz_buffer << "A " << position << std::endl;
              position.set_z(position.z() + bond_length);
            }
            return ReverseMappingParameters(
              SimulationConfiguration::ParserWriter::XYZ().parse(xyz_buffer));
          }
          
          // 0  : unlimited ff parameters
          // > 0: i-th ff parameters 
          PairforceParametersMaps
          parse_tables(const std::string & filename,
                       const unsigned int I,
                       const ReducedUnitsSet & units){
            PairforceParametersMaps result(I);
            SortedTypePair type_pair("A", "A");
            for (unsigned int i = 0; i < I; ++i){
              std::stringstream filename_buffer;
              filename_buffer << filename << "_" << i << ".txt";
              TabulatedEnergy table =
                parse_tabulated_energy_file(filename_buffer.str(), units);
              PairforceParametersMap parameters_map;
              TabulatedPairforceParameters pp;
              pp.table = table;
              parameters_map.insert(
                std::make_pair(type_pair,
                               new TabulatedPairforceParameters(pp)));
              result[i] = parameters_map;
            }
            return result;
          }

          const unsigned int MIN_NUM_OPTIONS = 2;

          void
          print_usage(){
            std::cerr << "Usage of ff_agrawal: " << std::endl;
            std::cerr << "arguments: " << std::endl;
            std::cerr << "i  - the number of steps in the forcefield" << std::endl;
            std::cerr << "cg - coarse graining level" << std::endl;
          }

          Parameters
          parameters(const ParameterOptions & options){
            if (options.size() < MIN_NUM_OPTIONS){
              print_usage();
              std::stringstream error;
              error << "The number of options provided to the forcefield ";
              error << "parameter retrieval function is too low. Minimum ";
              error << "required number of options is: " << MIN_NUM_OPTIONS;
              error << ".";
              throw ParameterError(error.str());
            }

            unsigned int i;
            std::istringstream(retrieve_option(options, "i")) >> i;
            unsigned int cg;
            std::istringstream(retrieve_option(options, "cg")) >> cg;

            Parameters result;

            result.pairforce_parameters_maps =
              parse_tables("potential_iteration9", i, dodecane::carbon);
            A.reverse_map = cg_th_reverse_mapping(cg);
            result.particles["A"] = A;

            return result;
          }

        } // namespace internal

        using internal::parameters;
        using internal::cg_th_reverse_mapping;

      } // namespace ff_agrawal

    } // namespace forcefields

  } // namespace parameters

} // namespace mdst

#endif // FF_AGRAWAL_H_
