// =====================================================================================
//
//       Filename:  dodecane.hpp
//
//    Description:  Parameters for dodecane.
//
//        Version:  1.0
//        Created:  11/13/2012 06:10:20 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef DODECANE_H_
#define DODECANE_H_

#include "parameters.hpp"
#include "reduced_units.hpp"
#include <parser_writer/hoomd_blue_xml.hpp>
#include <boost/units/systems/angle/degrees.hpp>

namespace mdst{

  namespace parameters{

    namespace forcefields{

      namespace dodecane{
        
        namespace internal{

          using namespace mdst::units;
          using namespace boost::units;
          using namespace si;
          using namespace constants::codata;
          using namespace degree;

          using boost::units::pow;
          
          const Mass MASS_C = 12.001 * amu;
          const Mass MASS_H = 1.008 * amu;

          // Siepmann 1993
          // United-Atom Forcefield-Parameters

          const Length SIGMA_A(3.93 * angstrom);
          const Energy EPSILON_A(114.0 * kelvin * k_B);

          mdst::ParticleParameters A = {
            Length(SIGMA_A),
            Energy(EPSILON_A)
          };

          const Length SIGMA_B(3.93 * angstrom);
          const Energy EPSILON_B(47.0 * kelvin * k_B);

          mdst::ParticleParameters B = {
            Length(SIGMA_B),
            Energy(EPSILON_B)
          };

          mdst::HarmonicBondParameters A_B(
            Length(1.54 * angstrom),
            BondStretchingForce(5000 * (kilo * joules) / (pow<2>(angstrom) / (N_A * mole))));
            //       rigid bond ^^^^

          mdst::HarmonicAngleParameters A_B_B(
            PlaneAngle(114 * degrees),
            BondBendingForce(62500 * kelvin * k_B / pow<2>(radians)));

          mdst::HarmonicDihedralParameters A_B_B_B_0(
            Energy(355.03 * kelvin * k_B),
            +1, 1);

          mdst::HarmonicDihedralParameters A_B_B_B_1(
            Energy(-68.19 * kelvin * k_B),
            -1, 2);

          mdst::HarmonicDihedralParameters A_B_B_B_2(
            Energy(791.32 * kelvin * k_B),
            1, 3);
          
          // Damm, et. al, 1997, J.C.CH., Vol. 18, No. 16, 1955-1970
          // All-Atom-Force-Field (OPLS)

          const Length SIGMA_CT(3.50 * angstrom);
          const Energy EPSILON_CT(0.276144 * kilo*joules / (N_A * mole));

          mdst::ReducedUnitsSet carbon(SIGMA_CT, MASS_C, EPSILON_CT);
          
          mdst::ParticleParameters CT = {
            Length(SIGMA_CT),
            Energy(EPSILON_CT),
            mdst::ReverseMappingParameters()
          };

          const Length SIGMA_HC(2.50 * angstrom);
          const Energy EPSILON_HC(0.12552 * kilo*joules / (N_A * mole));
          
          mdst::ReducedUnitsSet hydrogen(SIGMA_HC, MASS_H, EPSILON_HC);
          
          mdst::ParticleParameters HC = {
            Length(SIGMA_HC),
            Energy(EPSILON_HC)
          };

          // Harmonic Bond Parameters
          mdst::HarmonicBondParameters CT_HC(
            Length(1.090 * angstrom),
            BondStretchingForce(1422.56 * (kilo * joules) / (pow<2>(angstrom)) / (N_A * mole)));

          mdst::HarmonicBondParameters CT_CT(
            Length(1.529 * angstrom),
            BondStretchingForce(1121.312 * (kilo * joules) / (pow<2>(angstrom)) / (N_A * mole)));
          
          mdst::HarmonicAngleParameters CT_CT_CT(
            PlaneAngle(112.70 * degrees),
            BondBendingForce(244.1364 * kilo*joules / pow<2>(radians) / (N_A * mole)));
          
          mdst::HarmonicAngleParameters CT_CT_HC(
            PlaneAngle(110.70 * degrees),
            BondBendingForce(156.9 * kilo*joules / pow<2>(radians) / (N_A * mole)));
          
          mdst::HarmonicAngleParameters HC_CT_HC(
            PlaneAngle(107.80 * degrees),
            BondBendingForce(138.072 * kilo*joules / pow<2>(radians) / (N_A * mole)));

          mdst::HarmonicDihedralParameters H_C_C_H_0(
            Energy(0 * joule),
            +1, 1);

          mdst::HarmonicDihedralParameters H_C_C_H_1(
            Energy(0 * joule),
            -1, 2);

          mdst::HarmonicDihedralParameters H_C_C_H_2(
            Energy(1.330512 * kilo*joules / (N_A * mole)),
            +1, 3);

          mdst::HarmonicDihedralParameters H_C_C_C_0(
            Energy(0 * joule),
            +1, 1);

          mdst::HarmonicDihedralParameters H_C_C_C_1(
            Energy(0 * joule),
            -1, 2);

          mdst::HarmonicDihedralParameters H_C_C_C_2(
            Energy(1.531344  * kilo*joules / (N_A*mole)),
            +1, 3);

          mdst::HarmonicDihedralParameters C_C_C_C_0(
            Energy(7.28016 * kilo*joules / (N_A*mole)),
            +1, 1);

          mdst::HarmonicDihedralParameters C_C_C_C_1(
            Energy(-0.656888 * kilo*joules / (N_A*mole)),
            -1, 2);

          mdst::HarmonicDihedralParameters C_C_C_C_2(
            Energy(1.167336 * kilo*joules / (N_A*mole)),
            +1, 3);

          using namespace mdst;
          // This function returns a parameter set, which can be used in a rm-environment
          Parameters
          parameters(const ParameterOptions & options = ParameterOptions()){
            SimulationConfiguration::ParserWriter::HoomdBlueXml parser;

            Parameters result;
            
            // OPLS parameters
            result.particles["C"] = CT;
            result.particles["H"] = HC;
            result.bonds[Type("C-C")] = CT_CT;
            result.bonds[Type("C-H")] = CT_HC;
            result.angles[Type("C-C-C")] = CT_CT_CT;
            result.angles[Type("C-C-H")] = CT_CT_HC;
            result.angles[Type("H-C-H")] = HC_CT_HC;
            HarmonicDihedralParametersVector H_C_C_H(3);
            H_C_C_H.push_back(H_C_C_H_0);
            H_C_C_H.push_back(H_C_C_H_1);
            H_C_C_H.push_back(H_C_C_H_2);
            result.dihedrals[Type("H-C-C-H")] = H_C_C_H;
            HarmonicDihedralParametersVector H_C_C_C(3);
            H_C_C_C.push_back(H_C_C_C_0);
            H_C_C_C.push_back(H_C_C_C_1);
            H_C_C_C.push_back(H_C_C_C_2);
            result.dihedrals[Type("C-C-C-H")] = H_C_C_C;
            HarmonicDihedralParametersVector C_C_C_C(3);
            C_C_C_C.push_back(C_C_C_C_0);
            C_C_C_C.push_back(C_C_C_C_1);
            C_C_C_C.push_back(C_C_C_C_2);
            result.dihedrals[Type("C-C-C-C")] = C_C_C_C;

            // United-Atoms parameters
            shared_ptr<SimulationConfiguration::SimulationConfig>
                                config_A(parser.parse("rm_dodecane_A.xml"));
            shared_ptr<SimulationConfiguration::SimulationConfig>
                                config_B(parser.parse("rm_dodecane_B.xml"));
            A.reverse_map = ReverseMappingParameters(config_A);
            B.reverse_map = ReverseMappingParameters(config_B);
            result.particles["A"] = A;
            result.particles["B"] = B;
            result.bonds[Type("A-B")] = A_B;
            result.bonds[Type("B-B")] = A_B;
            result.angles[Type("A-B-B")] = A_B_B;
            result.angles[Type("B-B-B")] = A_B_B;
            HarmonicDihedralParametersVector A_B_B_B(3);
            A_B_B_B.push_back(A_B_B_B_0);
            A_B_B_B.push_back(A_B_B_B_1);
            A_B_B_B.push_back(A_B_B_B_2);
            result.dihedrals[Type("A-B-B-B")] = A_B_B_B;
            result.dihedrals[Type("B-B-B-B")] = A_B_B_B;

            result.lj_pairforces = result.generate_lj_pairforces();
            
            return result;
          }

        } // namespace internal

        using internal::carbon;
        using internal::hydrogen;
        using internal::parameters;

      } // namespace docecane

    } // namespace forcefields

    namespace reduced_units{

      using forcefields::dodecane::carbon;
      using forcefields::dodecane::hydrogen;

    } // namespace reduced_units

  } // namespace parameters

} // namespace mdst

#endif // DODECANE_H_
