// =====================================================================================
//
//       Filename:  iterable.hpp
//
//    Description:  Provides wrapper classes for protected access of
//                  containers.
//
//        Version:  1.0
//        Created:  10/24/2012 01:52:20 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_ITERABLE_H
#define MDST_ITERABLE_H

#include <memory.hpp>
namespace mdst{

// This is a wrapper class, to protect access to the iterable containers
template<class C>
class Iterable{
 public:
  explicit Iterable( C &c): c_(c) {};
  Iterable( const Iterable& rhs): c_(rhs.c_){};
  typename C::const_reference at( typename C::size_type n) const
    { return c_.at(n);};
  typename C::reference at( typename C::size_type n){ return c_.at(n);};
  typename C::const_reference back() const { return c_.back();};
  typename C::reference back(){ return c_.back();};
  typename C::const_iterator begin() const { return c_.begin();};
  typename C::iterator begin(){ return c_.begin();};
  bool empty() const { return c_.empty();};
  typename C::const_iterator end() const { return c_.end();};
  typename C::iterator end(){ return c_.end();};
  typename C::const_reference front() const { return c_.front();};
  typename C::reference front(){ return c_.front();};
  typename C::const_reference operator[](typename C::size_type n) const
    { return c_.operator[](n);};
  typename C::reference operator[](typename C::size_type n)
    { return c_.operator[](n);};
  typename C::size_type size() const { return c_.size();};
 private:
  C & c_;
};

} // namespace mdst

#endif // MDST_ITERABLE_H
