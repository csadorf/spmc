/*
 * =====================================================================================
 *
 *       Filename:  tuple3.h
 *
 *    Description:  Provide a consistent interface to manipulate 3-tuple values.
 *
 *        Version:  1.0
 *        Created:  10/19/2012 04:37:38 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
 *   Organization:  RWTH Aachen University
 *
 * =====================================================================================
 */

#ifndef MDST_TUPLE3_H_
#define MDST_TUPLE3_H_

#include "types.hpp"
#include <iostream>
#include <algorithm>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>
#include <boost/tuple/tuple_comparison.hpp>

namespace mdst{

typedef boost::tuple<Attribute, Attribute, Attribute> tuple3_;

inline Attribute 
abs_squared(const Tuple3 & t);

class Tuple3{
 public:
  Tuple3(): data_(0, 0, 0){};
  explicit Tuple3( Attribute d0): data_(d0, d0, d0){};
  Tuple3(Attribute d0, Attribute d1, Attribute d2): data_(d0, d1, d2){};
  Attribute x() const { return data_.get<0>();};
  Attribute y() const { return data_.get<1>();};
  Attribute z() const { return data_.get<2>();};
  Attribute set_x( Attribute value) { return data_.get<0>() = value;};
  Attribute set_y( Attribute value) { return data_.get<1>() = value;};
  Attribute set_z( Attribute value) { return data_.get<2>() = value;};
 
  // operators 
  const Attribute & operator[] ( int index) const;
  Attribute & operator[] ( int index);
  Tuple3& operator+=(const Tuple3& rhs);
  Tuple3& operator-=(const Tuple3& rhs);

  // extra
  Attribute abs_squared_()
  { return abs_squared(* this);}

  // non-member friend functions
  friend std::ostream& operator<<(std::ostream&, const Tuple3&);
  friend Attribute max( const Tuple3 &);
  friend Attribute min( const Tuple3 &);
 private:
  tuple3_ data_;
}; // class Tuple3
//
// operators
inline const Tuple3 
operator+(const Tuple3& lhs, const Tuple3& rhs) {
   return Tuple3( lhs.x() + rhs.x(), lhs.y() + rhs.y(), lhs.z() + rhs.z());
}

inline const Tuple3 
operator-(const Tuple3& lhs, const Tuple3& rhs) {
   return Tuple3( lhs.x() - rhs.x(), lhs.y() - rhs.y(), lhs.z() - rhs.z());
}

bool operator==(const Tuple3& lhs, const Tuple3& rhs);
bool operator!=(const Tuple3& lhs, const Tuple3& rhs);
bool operator<(const Tuple3& lhs, const Tuple3& rhs);
bool operator>(const Tuple3& lhs, const Tuple3& rhs);
bool operator<=(const Tuple3& lhs, const Tuple3& rhs);
bool operator>=(const Tuple3& lhs, const Tuple3& rhs);
std::ostream& operator<<(std::ostream &out, const Tuple3 &tuple3);

// utility functions
Attribute abs(const Tuple3 & t);

inline Attribute 
abs_squared(const Tuple3 & t) {
    return t.x() * t.x() + t.y() * t.y() + t.z() * t.z();
}

std::string tuple3_to_string(const Tuple3 & t);

} // namespace mds_tools


#endif // MDST_TUPLE3_H_
