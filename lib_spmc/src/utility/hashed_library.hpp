// =====================================================================================
//
//       Filename:  hashed_library.hpp
//
//    Description:  Hashed library for fast lookups.
//
//        Version:  1.0
//        Created:  04/04/2014
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_HASHED_LIBRARY_H_
#define MDST_HASHED_LIBRARY_H_

#include <map>
#include <vector>
#include <algorithm>
#include <iostream>

namespace mdst{

    static const float HASH_FRACTION_SETPOINT = 0.10;

    template<
      class Key,
      class HashFunct, 
      class KeyCompare,
      class T>
    class HashedLibrary{
     public:
      typedef typename HashFunct::T Hash;
      typedef std::pair<Key, T > Entry;
      typedef shared_ptr<Entry> EntryPtr;
      //typedef std::multimap<Hash, EntryPtr> EntryMap;
      typedef std::multimap<Hash, EntryPtr, typename HashFunct::Compare > EntryMap;
      typedef typename EntryMap::const_iterator EntryMapConstIterator;
      typedef typename EntryMap::iterator EntryMapIterator;
      typedef EntryMapConstIterator ConstIterator;
      typedef EntryMapIterator Iterator;

      static Attribute
      adjust_rate() 
      {
        return HashFunct::adjust_rate();
      }

      static Attribute
      set_adjust_rate(
        const Attribute & rate)
      {
        return HashFunct::set_adjust_rate(rate);
      }

      ConstIterator
      begin() const
      {
        return entry_map_.begin();
      }
      
      ConstIterator
      end() const
      {
        return entry_map_.end();
      }

      void
      insert(
        const Key & key,
        const T & value)
      {
        EntryMapIterator check(entry_map_.insert(
          std::make_pair(
            hash_funct_(key), 
            //EntryPtr(new Entry(std::make_pair(key, value))))));
            EntryPtr(new Entry(key, value)))));
      }

      ConstIterator
      find(
        const Key & key,
        typename KeyCompare::T & min_delta) const
      {
        return find_(key, min_delta);
      }

      size_t
      size() const
      {
        return entry_map_.size();
      }

     private:
      EntryMapConstIterator
      find_(
        const Key & key,
        typename KeyCompare::T & min_delta) const
      {
        typedef std::pair<EntryMapConstIterator, EntryMapConstIterator> Range;
        const Hash hash = hash_funct_(key);
        Range range = entry_map_.equal_range(hash);
        EntryMapConstIterator it = range.first;
        if (it == range.second)
        {
          if (entry_map_.size() > 0)
          {
            /*std::cerr 
              << "hf = " << 0
              << " (!= " << HASH_FRACTION_SETPOINT << ")"
              << " epsilon = " << HashFunct::epsilon()
              << std::endl;*/
              HashFunct::adjust_accuracy(- 1.0);
          }
          return range.second;
        }
        EntryMapConstIterator min_entry = range.first;
        min_delta = key_compare_(min_entry->second->first, key);
        size_t n_range = 0;
        while(++it != range.second)
        {
          ++n_range;
          static typename KeyCompare::T delta;
          delta = key_compare_(min_entry->second->first, key);
          if (delta < min_delta)
          {
            min_delta = delta;
            min_entry = it;
          }
        }
        const float hash_fraction(float(n_range) / entry_map_.size());
        const float d_hf(hash_fraction - HASH_FRACTION_SETPOINT);
        HashFunct::adjust_accuracy(d_hf);
        /*std::cerr 
          << "hf = " << hash_fraction
          << " (!= " << HASH_FRACTION_SETPOINT << ")"
          << " epsilon = " << HashFunct::epsilon()
          << std::endl;*/

        return min_entry;
      };

      HashFunct hash_funct_;
      KeyCompare key_compare_;
      EntryMap entry_map_;
    };
    
} // namespace mdst

#endif // MDST_HASHED_LIBRARY_H_
