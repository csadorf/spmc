// =====================================================================================
//
//       Filename:  tuple3_arithmetic.cpp
//
//    Description:  Provide basic arithmetic operations.
//
//        Version:  1.0
//        Created:  11/16/2012 12:41:38 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "tuple3_arithmetic.hpp"

namespace mdst{

  Attribute
  operator*(const Tuple3& lhs, const Tuple3& rhs){
    return lhs.x() * rhs.x() + lhs.y() * rhs.y() + lhs.z() * rhs.z();
  }

  Tuple3
  operator*(Attribute lhs, const Tuple3& rhs){
    return Tuple3(lhs * rhs.x(), lhs * rhs.y(), lhs * rhs.z());
  }

  Tuple3
  operator*(const Tuple3& lhs, Attribute rhs){
    return operator*(rhs, lhs);
  }

  Tuple3
  operator/(const Tuple3 & lhs, Attribute rhs){
    return operator*(lhs, 1 / rhs);
  }

  Tuple3
  cross_product(const Tuple3& lhs, const Tuple3& rhs){
    return Tuple3(lhs.y() * rhs.z() - lhs.z() * rhs.y(),
                  lhs.z() * rhs.x() - lhs.x() * rhs.z(),
                  lhs.x() * rhs.y() - lhs.y() * rhs.x());
  }

  Tuple3
  element_division(
    const Tuple3 & lhs,
    const Tuple3 & rhs)
  {
    return Tuple3(lhs[0] / rhs[0],
                  lhs[1] / rhs[1],
                  lhs[2] / rhs[2]);
  }

  Tuple3
  normalize(const Tuple3& t){
    const Attribute a = abs(t);
    if (a != 0)
      return t / a;
    return Tuple3(0);
  }

  unsigned int
  max_index(
    const Tuple3 & t)
  {
    unsigned int max = 0;
    for(unsigned int i = 0; i < 3; ++i)
      if (t[i] > t[max])
        max = i;
    return max;
  }

  unsigned int
  min_index(
    const Tuple3 & t)
  {
    unsigned int min = 0;
    for(unsigned int i = 0; i < 3; ++i)
      if(t[i] < t[min])
        min = i;
    return min;
  }

} // namespace mdst
