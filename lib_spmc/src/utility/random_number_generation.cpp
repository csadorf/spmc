// =====================================================================================
//
//       Filename:  random_number_generation.cpp
//
//    Description:  Generate random numbers.
//
//        Version:  1.0
//        Created:  11/26/2012 10:39:41 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "random_number_generation.hpp"
#define _USE_MATH_DEFINES
#include <math.h>

namespace mdst{
  
  RandomNumberGenerator::RandomNumberGenerator(
    unsigned int seed):
      random_generator_(seed),
      uni_dst_(0, 1),
      normal_dst_(0, 1),
//      half_turn_dst_(0, M_PI),
      half_turn_dst_(-M_PI / 2, M_PI / 2),
      full_turn_dst_(-M_PI, M_PI),
      random_number(random_generator_, uni_dst_),
      normal_number(random_generator_, normal_dst_),
      random_half_turn(random_generator_, half_turn_dst_),
      random_full_turn(random_generator_, full_turn_dst_)
      {};

} // namespace mdst
