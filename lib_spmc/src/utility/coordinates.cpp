// =====================================================================================
//
//       Filename:  coordinates.cpp
//
//    Description:  Coordinate system arithmetics.
//
//        Version:  1.0
//        Created:  11/18/2012 06:38:02 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "coordinates.hpp"
#include "tuple3_arithmetic.hpp"
#include <stdexcept>

namespace mdst{

  using namespace mdst::units;

  Quaternion 
  convert_to_quaternion(const Tuple3& v){
    return Quaternion(0, v.x(), v.y(), v.z());
  }

  Tuple3
  convert_to_tuple3(const Quaternion& q){
    return Tuple3(q.R_component_2(),
                  q.R_component_3(),
                  q.R_component_4());
  }

  // Warning: function is unstable!
  void
  convert(const RotationQuaternion& q,
          PlaneAngle& angle,
          Tuple3& rotation_axis){
    std::cerr << "convert(const RotationQuaternion&, PlaneAngle&, Tuple3&): ";
    std::cerr << "Function is ill-defined. Do not use! Debug only!" << std::endl;
    const Attribute a = 2 * acos(q.R_component_1());
    const Attribute f = sin(a / 2);
    rotation_axis = Tuple3(q.R_component_2() / f,
                           q.R_component_3() / f,
                           q.R_component_4() / f);
    angle = a * si::radians;
  }

  Quaternion
  quaternion(const Attribute& w,
             const Tuple3& t){
    return Quaternion(w, t.x(), t.y(), t.z());
  }


  RotationQuaternion
  rotation_quaternion(const PlaneAngle& angle,
                      const Tuple3& rotation_axis){
    const Attribute a = angle / si::radians;
    return RotationQuaternion(
                      cos(a / 2),
                      rotation_axis.x() * sin(a / 2),
                      rotation_axis.y() * sin(a / 2),
                      rotation_axis.z() * sin(a / 2));
  }
  
  RotationQuaternion
  rotation_quaternion(const Tuple3& x,
                      const Tuple3& y){
/*    const Tuple3 rotation_axis = cross_product(x, y);
    const Attribute w = sqrt(abs_squared(x) + abs_squared(y)) + x * y;
    const Quaternion q = quaternion(w, rotation_axis);
    return q / norm(q);*/

    const Tuple3 v0 = normalize(x);
    const Tuple3 v1 = normalize(y);
    const Attribute d = v0 * v1;
    const Attribute s = sqrt((1+d)*2);
    const Attribute inv = 1 / s;
    const Tuple3 c = cross_product(v0, v1);
    const Tuple3 rot_axis = inv * c;
    const Attribute w = 0.5 * s;
    const Quaternion q = quaternion(w, rot_axis);
//    std::cerr << "noramlized q=" << q / norm(q) << std::endl;
    return q / norm(q);
  }

  RotationQuaternion
  rotation_quaternion(const Tuple3& pivot,
                      const Tuple3& from,
                      const Tuple3& to){
    return rotation_quaternion(from - pivot,
                               to - pivot);
  }
  
  Tuple3
  rotation(const Tuple3& p,
           const RotationQuaternion& q){
    return convert_to_tuple3(q * convert_to_quaternion(p) * conj(q));
  }

  Tuple3
  rotation(const Tuple3& pivot,
           const Tuple3& p,
           const RotationQuaternion& q){
    return rotation(p - pivot, q) + pivot;
  }

  Tuple3
  rotation(const Tuple3& pivot,
           const Tuple3& p,
           const Tuple3& from,
           const Tuple3& to){
    const RotationQuaternion q(rotation_quaternion(pivot, from, to));
//    return convert_to_tuple3(q * convert_to_quaternion((p - pivot)/2) * conj(q)) + pivot;
    return rotation(pivot, p , q);
  }

  Tuple3
  rotation(const Tuple3& p,
           const PlaneAngle& angle,
           const Tuple3& rotation_axis){
    return rotation(p, rotation_quaternion(angle, rotation_axis));
  }

  Tuple3
  rotation(const Tuple3& p,
           const PlaneAngle& theta,
           const PlaneAngle& phi){
    const RotationQuaternion q1 = rotation_quaternion(theta, Tuple3(1, 0, 0));
    const RotationQuaternion q2 = rotation_quaternion(phi,   Tuple3(0, 0, 1));
    return rotation(p, q2 * q1);
  }

  Tuple3
  rotation(const Tuple3& p,
           const PlaneAngle& theta,
           const PlaneAngle& phi,
           const PlaneAngle& psi){
    const RotationQuaternion q1 = rotation_quaternion(theta, Tuple3(1, 0, 0));
    const RotationQuaternion q2 = rotation_quaternion(phi,   Tuple3(0, 0, 1));
    const RotationQuaternion q3 = rotation_quaternion(psi,   Tuple3(0, 1, 0));
    return rotation(p, q3 * q2 * q1);
  }

  Tuple3
  rotation(const Tuple3& pivot,
           const Tuple3& p,
           const PlaneAngle& theta,
           const PlaneAngle& phi){
    return rotation(p - pivot, theta, phi) + pivot;
  }

  Tuple3
  rotation(const Tuple3& pivot,
           const Tuple3& p,
           const PlaneAngle& theta,
           const PlaneAngle& phi,
           const PlaneAngle& psi){
    return rotation(p - pivot, theta, phi, psi) + pivot;
  }

  Quaternion
  uniform_random_rotation(
      const Attribute x0,
      const Attribute x1,
      const Attribute x2){
    const Attribute gamma1 = 2 * M_PI * x1;
    const Attribute gamma2 = 2 * M_PI * x2;
    const Attribute s1 = sin(gamma1);
    const Attribute c1 = cos(gamma1);
    const Attribute s2 = sin(gamma2);
    const Attribute c2 = cos(gamma2);
    const Attribute r1 = sqrt(1 - x0);
    const Attribute r2 = sqrt(x0);
    return Quaternion(c2 * r2, s1 * r1, c1 * r1, s2 * r2);
  }

  Quaternion
  uniform_random_restricted_rotation(
      RandomNumberGenerator & rng,
      const PlaneAngle & cut_off){
    Attribute c = cut_off / si::radians;
    Attribute alpha;
    while(true){
      Attribute u = c * c * c * rng.random_number();
      alpha = std::pow(u, 1 / 3.0);
      if (rng.random_number() < (std::pow(sin(alpha), 2) / pow(alpha, 2))){
        break;
      }
    }
    return rotation_quaternion(
      alpha * si::radians,
      Tuple3( rng.random_number(),
              rng.random_number(), 
              rng.random_number())
      );
  }

  Tuple3
  cartesian(const Attribute r,
            const PlaneAngle& theta,
            const PlaneAngle& phi){
    Attribute theta_ = theta / si::radian;
    Attribute phi_   = phi / si::radian;
    if (theta_ > M_PI)
      theta_ -= M_PI;
    if (phi_ > M_PI)
      phi_ -= 2 * M_PI;
    if (phi_ <  - M_PI)
      phi_ += 2 * M_PI;
    return Tuple3(r * sin(theta_) * cos(phi_),
                  r * sin(theta_) * sin(phi_),
                  r * cos(theta_));
  }

  void
  spherical(
    const Tuple3 & cartesian,
    Attribute & r,
    PlaneAngle & theta,
    PlaneAngle & phi)
  {
    r = abs(cartesian);
    theta = acos(cartesian.z() / r) * si::radians;
    phi = atan2(cartesian.y(), cartesian.x()) * si::radians;
  }

  Tuple3
  oriented_position(
    const Tuple3 & r1,
    const Tuple3 & r2,
    const Attribute rho,
    const PlaneAngle & phi,
    const PlaneAngle & theta,
    const Attribute rand_theta_phi)
  {
    const Tuple3 c(cross_product(r2, r1));
    Attribute val = abs_squared(c);
    if (val == 0 or phi!=phi or theta!=theta)
     {
       const Tuple3 r(
       cos(M_PI - rand_theta_phi),
       sin(M_PI - rand_theta_phi) * cos(rand_theta_phi),
       sin(M_PI - rand_theta_phi) * sin(rand_theta_phi)); 
       return rho * (r);
     } 
    const Tuple3 d(cross_product(c, r1));
    const Attribute r_phi = phi / si::radians;
    const Attribute r_theta = theta / si::radians;
    const Tuple3 r(
      cos(M_PI - r_phi),
      sin(M_PI - r_phi) * cos(r_theta),
      sin(M_PI - r_phi) * sin(r_theta));
    return rho * (
        r.x() * normalize(r1) 
      + r.y() * normalize(d)
      + r.z() * normalize(c));
  }

  PlaneAngle
  calculate_angle(const Tuple3& x, const Tuple3& y){
    return PlaneAngle(acos((x * y) / (abs(x) * abs(y))) * si::radians);
  }
  
  PlaneAngle
  calculate_angle(const Tuple3& a, const Tuple3& b, const Tuple3& c){
    return calculate_angle(a - b, c - b);
  }

  PlaneAngle
  calculate_phi(const Tuple3& center, const Tuple3& x){
    if (center == x){
      std::stringstream error;
      error << "calculate_phi(const Tuple3& center, const Tuple3& x): ";
      error << "Length between x and center must be > 0.";
      error << " (center= " << center << "; x= " << x << ")";
      throw std::invalid_argument(error.str());
    }
    const Tuple3 v = x - center;
    return PlaneAngle(atan2(v.y(), v.x()) * si::radians);
  }

  PlaneAngle
  calculate_theta(const Tuple3& center, const Tuple3& x){
    if (center == x){
      std::stringstream error;
      error << "calculate_theta(const Tuple3& center, const Tuple3& x): ";
      error << "Length between x and center must be > 0.";
      error << " (center=x=" << center << ")";
      throw std::invalid_argument(error.str());
    }
    const Tuple3 v = x - center;
    return PlaneAngle(acos(v.z() / abs(v)) * si::radians);
  }

/*  PlaneAngle
  calculate_torsion(
      const Tuple3 & a,
      const Tuple3 & b,
      const Tuple3 & c,
      const Tuple3 & d)
  {
    const Tuple3 b1(b - a);
    const Tuple3 b2(c - b);
    const Tuple3 b3(d - c);
    const Tuple3 n1 = normalize(cross_product(b1, b2));
    const Tuple3 n2 = normalize(cross_product(b2, b3));
    const Tuple3 m1 = cross_product(n1, normalize(b2));
    const Attribute x = n1 * n2;
    const Attribute y = m1 * n1;
    return PlaneAngle(atan2(y, x) * si::radians);
  }*/

PlaneAngle
  calculate_torsion(
      const Tuple3 & a,
      const Tuple3 & b,
      const Tuple3 & c,
      const Tuple3 & d)
  {
    const Tuple3 b1(b - a);
    const Tuple3 b2(c - b);
    const Tuple3 b3(d - c);
    const Tuple3 n1 = cross_product(b1, b2);
    const Tuple3 n2 = cross_product(b2, b3);
    const Attribute y = (n1 * b3) * sqrt(b2 * b2);
    const Attribute x = (n1 * n2);
    return PlaneAngle(atan2(y, x) * si::radians);
  }


  PlaneAngle
  calculate_torsion2(
      const Tuple3 & a,
      const Tuple3 & b,
      const Tuple3 & c,
      const Tuple3 & d)
  {
    const Tuple3 b1(b - a);
    const Tuple3 b2(c - b);
    const Tuple3 b3(d - c);
    PlaneAngle result(
              atan2(abs(b2) * b1 * cross_product(b2, b3),
                    cross_product(b1, b2) * cross_product(b2, b3))
              * si::radians);
    return result;
  }

} // namespace mdst
