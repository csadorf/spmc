// =====================================================================================
//
//       Filename:  misc.hpp
//
//    Description:  Miscellaneous utility functions.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_MISC_H_
#define MDST_MISC_H_

#include <simulation_config/types.hpp>
#include <iostream>
#include <sstream>

namespace mdst{

  TagSet
  generate_subset(const unsigned int n,
                  const int offset = 0);

  template <typename T>
  inline
  std::vector<T>
  vector_from_set(
    const std::set<T> & set)
  {
    typedef std::set<T> set_type;
    std::vector<T> vector(set.size());
    size_t i = -1;
    for(typename set_type::const_iterator it = set.begin();
        it != set.end(); ++it){
      vector[++i] = *it;
    }
    return vector;
  }

  template <typename T>
  inline
  std::set<T>
  set_from_vector(
    const std::vector<T> & vector)
  {
    typedef std::vector<T> vector_type;
    std::set<T> set;
    for(typename vector_type::const_iterator it = vector.begin();
        it != vector.end(); ++it){
      set.insert(*it);
    }
    return set;
  }

  template <class C>
  void
  inline
  dump_container(const C& c,
                 std::ostream& stream = std::cerr);

  std::ostream &
  dump_tagset(const TagSet& tags,
              std::ostream& stream = std::cerr);

  template <typename T>
  inline
  std::ostream &
  print_array(
      T * array,
      const unsigned int size,
      std::ostream & stream = std::cerr){
    for(unsigned int i = 0; i < size; ++i)
      stream << array[i] << ' ';
    stream << std::endl;
    return stream;
  }

  template <typename T>
  inline
  std::ostream &
  print_pair(
      T pair,
      std::ostream & stream = std::cerr){
   stream << "(" << pair.first() << ", "
          << pair.second() << ")";
   return stream;
  }

  template <typename T>
  inline
  std::ostream &
  print_iterable(
      T first,
      T last,
      std::ostream & stream = std::cerr)
  {
    stream << "[";
    if (first != last){
      stream << * first++;
      while(first != last)
        stream << ", " << * first++;
    }
    stream << "]";
    return stream;
  }

  bool
  intersect(const TagSet& a, const TagSet& b);

  TagSet
  difference(const TagSet & a, const TagSet & b);

// Based on Source: www.cplusplus.com/faq/sequences/strings/split/#getline
  struct split_t
  {
    enum empties_t { empties_ok, no_empties };
  };

  template <typename Container>
  inline
  Container&
  split(
    Container&                                 result,
    const typename Container::value_type&      s,
    typename Container::value_type::value_type delimiter,
    split_t::empties_t                         empties = split_t::empties_ok)
  {
    result.clear();
    std::istringstream ss( s );
    while (!ss.eof()){
      typename Container::value_type field;
      getline( ss, field, delimiter );
      if ((empties == split_t::no_empties) && field.empty()) continue;
      result.push_back( field );
    }
    return result;
  }
// end of based on source

  typedef std::vector<std::string> Strings;
  typedef Strings::const_iterator StringsConstIterator;

  Strings
  split(const std::string & s,
        Strings::value_type::value_type delimiter);

  std::string
  determine_extension(const std::string & filename);

  typedef std::vector<int> IntVector;
  typedef IntVector::iterator IntVectorIterator;
  typedef IntVector::const_iterator IntVectorConstIterator;

  IntVector
  find_prime_numbers(
    int number);

  IntVectorIterator
  limit_prime_numbers(
    IntVector & prime_numbers,
    const unsigned int limit);

  unsigned int
  sum(
    const IntVector & v);

  unsigned int
  product(
    const IntVector & v);

  Attribute
  mean(
    AttributesConstIterator first,
    AttributesConstIterator last);

  Attribute
  variance(
    AttributesConstIterator first,
    AttributesConstIterator last,
    const Attribute expectance_value);

  Attribute
  variance(
    AttributesConstIterator first,
    AttributesConstIterator last);

  template <class InputIterator>
  inline
  typename InputIterator::value_type
  interpolate_linear(
    InputIterator first,
    InputIterator last,
    Attribute x)
  {
    size_t i = size_t(x * ((last - first) - 1));
    return 0.5 * (*(first + i) + *(first + i + 1));
  }

  template <class Map>
  inline
  typename Map::mapped_type
  interpolate_linear(
    Map const & map,
    typename Map::key_type const & key)
  {
    typename Map::const_iterator iter = map.lower_bound(key);
    if (iter == map.end())
      return (--iter)->second;
    if (iter == map.begin())
      return iter->second;
    typename Map::mapped_type const v0 = iter->second;
    typename Map::mapped_type const v1 = (--iter)->second;
    return 0.5 * (v0 + v1);
  }

  template<class T, class InputIterator, class OutputIterator>
  inline
  OutputIterator
  find_type_in_iterable(
    InputIterator first,
    InputIterator last,
    OutputIterator result)
  {
    size_t i = 0;
    while(first != last){
      if(dynamic_cast<T>(* first))
        * (result + i++) = * first;
      ++first;
    }
    return result + i;
  }

  template<class T, class InputIterator, class OutputIterator>
  inline
  OutputIterator
  find_type_in_iterable_of_shared_ptr(
    InputIterator first,
    InputIterator last,
    OutputIterator result)
  {
    size_t i = 0;
    while(first != last){
      if(dynamic_cast<T *>(first->get()))
        * (result + i++) = * first;
      ++first;
    }
    return result + i;
  }

  inline
  bool
  significantly_similar(
    float a, float b, int n)
  {
    using std::abs;
    using std::max;
    return (std::abs(a - b) < pow(0.1, n) * max(std::abs(a), std::abs(b)));
  }

  inline
  bool
  significantly_different(
    float a, float b, int n)
  {
    return ! significantly_similar(a, b, n);
  }

} // namespace mdst

#endif // MDST_MISC_H_
