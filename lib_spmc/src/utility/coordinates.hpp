// =====================================================================================
//
//       Filename:  coordinates.hpp
//
//    Description:  Coordinate system arithmetics.
//
//        Version:  1.0
//        Created:  11/18/2012 06:38:02 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_COORDINATES_H_
#define MDST_COORDINATES_H_

#include "units.hpp"
#include "tuple3.hpp"
#include "random_number_generation.hpp"
#include <boost/math/quaternion.hpp>

namespace mdst{

  using namespace mdst::units;
  typedef ::boost::math::quaternion<Attribute> Quaternion;
  typedef Quaternion RotationQuaternion;

  Tuple3
  rotation(
    const Tuple3 & pivot,
    const Tuple3 & p,
    const RotationQuaternion & q);

  Tuple3
  rotation(
    const Tuple3 & p,
    const RotationQuaternion & q);

  Tuple3
  rotation(const Tuple3& p,
           const PlaneAngle& angle,
           const Tuple3& rotation_axis);
  Tuple3
  rotation(const Tuple3& p,
           const PlaneAngle& theta,
           const PlaneAngle& phi);

  Tuple3
  rotation(const Tuple3& p,
           const PlaneAngle& theta,
           const PlaneAngle& phi,
           const PlaneAngle& psi);

  Tuple3
  rotation(const Tuple3& origin,
           const Tuple3& p,
           const PlaneAngle& theta,
           const PlaneAngle& phi);

  Tuple3
  rotation(const Tuple3& origin,
           const Tuple3& p,
           const PlaneAngle& theta,
           const PlaneAngle& phi,
           const PlaneAngle& psi);

  Tuple3
  rotation(const Tuple3& pivot,
           const Tuple3& p,
           const Tuple3& from,
           const Tuple3& to);

  Tuple3
  cartesian(const Attribute r,
            const PlaneAngle& theta,
            const PlaneAngle& phi);

  void
  spherical(
    const Tuple3 & cartesian,
    Attribute & r,
    PlaneAngle & theta,
    PlaneAngle & phi);
  
  Tuple3
  oriented_position(
    const Tuple3 & r1,
    const Tuple3 & r2,
    const Attribute rho,
    const PlaneAngle & phi,
    const PlaneAngle & theta, 
    const Attribute rand_theta_phi);

  void
  rotation(PlaneAngle& theta, const PlaneAngle& delta_theta,
           PlaneAngle& phi, const PlaneAngle& delta_phi);

  Quaternion
  uniform_random_rotation(
    const Attribute x0,
    const Attribute x1,
    const Attribute x2);

  Quaternion
  uniform_random_restricted_rotation(
    RandomNumberGenerator & rng,
    const PlaneAngle & cut_off);

  PlaneAngle
  calculate_angle(const Tuple3& a, const Tuple3& b, const Tuple3& c);

  PlaneAngle
  calculate_theta(const Tuple3& center, const Tuple3& x);

  PlaneAngle
  calculate_phi(const Tuple3& center, const Tuple3& x);

  PlaneAngle
  calculate_torsion(const Tuple3& a, const Tuple3& b,
                    const Tuple3& c, const Tuple3& d);

} // namespace mdst

#endif // MDST_COORDINATES_H_
