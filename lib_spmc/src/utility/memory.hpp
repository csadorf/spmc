// =====================================================================================
//
//       Filename:  memory.hpp
//
//    Description:  Memory utility functions.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_UTILITY_MEMORY_H_
#define MDST_UTILITY_MEMORY_H_

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

namespace mdst{

  using boost::shared_ptr;
  using boost::static_pointer_cast;

  using boost::make_shared;
  using boost::allocate_shared;

} // namespace mdst

#endif // MDST_UTILITY_MEMORY_H_
