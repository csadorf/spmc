/*
 * =====================================================================================
 *
 *       Filename:  tuple3.cpp
 *
 *    Description:  Provide a consistent interface to manipulate 3-tuple values.
 *
 *        Version:  1.0
 *        Created:  10/19/2012 04:52:15 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
 *   Organization:  RWTH Aachen University
 *
 * =====================================================================================
 */

#include <iostream>
#include <sstream>
#include <vector>
#include <stdexcept>
#include "tuple3.hpp"
#include <math.h>

namespace mdst{

  Attribute &
  Tuple3::operator[]( int index){
    switch (index){
      case 0: return data_.get<0>(); break;
      case 1: return data_.get<1>(); break;
      case 2: return data_.get<2>(); break;
      default: throw std::range_error("Tuple3 can be indexed from 0 to 2.");
    }
  }

  const Attribute &
  Tuple3::operator[]( int index) const{
    switch (index){
      case 0: return data_.get<0>(); break;
      case 1: return data_.get<1>(); break;
      case 2: return data_.get<2>(); break;
      default: throw std::range_error("Tuple3 can be indexed from 0 to 2.");
    }
  }

  Tuple3&
  Tuple3::operator+=(const Tuple3& rhs){
    Tuple3 tmp(*this);
    tmp = tmp + rhs;
    data_ = tmp.data_;
    return *this;
  }

  Tuple3&
  Tuple3::operator-=(const Tuple3& rhs){
    Tuple3 tmp(*this);
    tmp = tmp - rhs;
    data_ = tmp.data_;
    return *this;
  }

  bool operator==(const Tuple3& lhs, const Tuple3& rhs){
    return lhs.x() == rhs.x() && lhs.y() == rhs.y() && lhs.z() == rhs.z();
  }

  bool operator!=(const Tuple3& lhs, const Tuple3& rhs){
    return !operator==(lhs, rhs);
  }

  bool operator<(const Tuple3& lhs, const Tuple3& rhs){
    return (lhs.x() < rhs.x() && lhs.y() < rhs.y() && lhs.z() < rhs.z());
  }
  bool operator>(const Tuple3& lhs, const Tuple3& rhs){
    return operator<(rhs, lhs);
  }
  bool operator<=(const Tuple3& lhs, const Tuple3& rhs){
    return !operator>(lhs, rhs);
  }
  bool operator>=(const Tuple3& lhs, const Tuple3& rhs){
    return !operator<(lhs, rhs);
  }

  std::ostream&
  operator<<(std::ostream &out, const Tuple3 &tuple3){
    out << tuple3.data_;
    return out;
  }

  std::string
  tuple3_to_string(
    const Tuple3 & t)
  {
    std::stringstream ss;
    ss << t;
    return ss.str();
  }

  Attribute
  abs(const Tuple3 & t){
    return sqrt( t.x()*t.x() + t.y()*t.y() + t.z()*t.z());
  }

  double
  max( const Tuple3 & t){
    return std::max(std::max(t.data_.get<0>(),
                             t.data_.get<1>()),
                             t.data_.get<2>());
  }

  double
  min( const Tuple3 & t){
    return std::min(std::min(t.data_.get<0>(),
                             t.data_.get<1>()),
                             t.data_.get<2>());
  }

} // namespace mdst
