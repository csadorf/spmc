// =====================================================================================
//
//       Filename:  histogram.hpp
//
//    Description:  Provide easy and efficient counting functions.
//
//        Version:  1.0
//        Created:  10/25/2012 03:12:12 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_HISTOGRAM_H_
#define MDST_HISTOGRAM_H_

#include <iostream>
#include <map>

namespace mdst{

template<class C>
std::ostream &
dump_map(const C & iterable,
         std::ostream & out){
  for(typename C::const_iterator it = iterable.begin(); it != iterable.end(); ++it)
    out << it->first << "\t" << it->second << std::endl;
  return out;
}

template<class C>
class Histogram{
// TODO (csa) The allocator for int has to be explicitely defined!
//            The class works with gcc, because ints are initialized to zero.
  typedef std::map<typename C::value_type, int> Map;
 public:
  Histogram(): map_(Map()) {};
  Histogram(const C & iterable) { count(iterable);};
  int & operator[]( const typename C::value_type & v) { return map_[v];};
  void count( const C & iterable);
  std::ostream & dump(std::ostream & out = std::cout) const;
 private:
  Map map_;
};

template<class C>
void
Histogram<C>::count( const C & iterable){
  typedef typename C::const_iterator CIterator;
  for(CIterator it = iterable.begin(); it != iterable.end(); ++it)
    map_[*it]++;
}

template<class C>
std::ostream &
Histogram<C>::dump(std::ostream & out) const{
  return dump_map<typename Histogram<C>::Map>(map_, out);
}


} // namespace mdst

#endif // MDST_HISTOGRAM_H_
