// =====================================================================================
//
//       Filename:  math.hpp
//
//    Description:  Math functions.
//
//        Version:  1.0
//        Created:  27/12/2013
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_MATH_H_
#define MDST_MATH_H_

#include <exception>

namespace mdst{
  
  namespace math{

    /// Returns mean of sample from first to last.
    template <class InputIterator>
    Attribute
    mean(
      InputIterator first,
      InputIterator last)
    {
      size_t n = last - first;
      Attribute sum = 0;
      for(; first != last; ++first)
        sum += * first;
      return sum / n;
    }
    
    template <class InputIterator>
    Attribute
    mean_skip(
      InputIterator first,
      InputIterator last,
      size_t p)
    {
      const size_t n = last - first;
      if (n % p != 0)
        throw std::invalid_argument("mean_skip(): invalid p");
      Attribute sum = 0;
      for(; first != last; first += p)
        sum += * first;
      return p * sum / n;
    }

    template <class InputIterator>
    Attribute
    variance_skip(
      InputIterator first,
      InputIterator last,
      size_t p)
    {
      const size_t n = last - first;
      if (n % p != 0)
        throw std::invalid_argument("variance_skip(): invalid p");
      const Attribute mu = mean_skip(first, last, p);
      Attribute sum = 0;
      for(; first != last; first += p)
        sum += (* first - mu) * (* first - mu);
      return p * sum / (n - 1);
    }

    
    /// Returns unbiased variance of sample from first to last.
    template <class InputIterator>
    Attribute
    variance(
      InputIterator first,
      InputIterator last)
    {
      const Attribute mu = mean(first, last);
      Attribute sum = 0;
      const size_t n = last - first;
      for(; first != last; ++first)
        sum += (* first - mu) * (* first - mu);
      return sum / (n - 1);
    }

    /// Returns covariance of samples x_first to x_last and
    /// y_first to y_last.
    /// Warning: Current implementation is numerically unstable.
    template < class InputIterator>
    Attribute
    covariance(
      InputIterator x_first,
      InputIterator x_last,
      InputIterator y_first,
      InputIterator y_last)
    {
      const size_t n_x = x_last - x_first;
      const size_t n_y = y_last - y_first;
      if (n_x != n_y)
      {
        std::stringstream msg;
        msg
          << "Covariance calculation undefined for samples of different size! "
          << "n_x = " << n_x << ", n_y = " << n_y
        ;
        throw std::invalid_argument(msg.str());
      }
      const Attribute mean_x = mean(x_first, x_last);
      const Attribute mean_y = mean(y_first, y_last);
      Attribute sum = 0;
      for(; x_first != x_last;)
      {
        sum += (* x_first) * (* y_first);
        ++x_first;
        ++y_first;
      }
      // TODO The two-pass algorithm is numerically unstable!
      return sum / (n_x) - mean_x * mean_y;
    }

    /// Split sample from first to last into half, by the 'blocking'
    /// method. 
    /// x_i'  = 0.5 * (x_(2i) + x_(2i+1))
    /// n'    = 0.5 * n 
    template <class InputIterator, class OutputIterator>
    OutputIterator
    block(
      InputIterator first,
      InputIterator last,
      OutputIterator result)
    {
      const size_t n = last - first;
      if (n < 2)
      {
        std::stringstream msg;
        msg
          << "block(): Input size must be greater or equal than 2. "
          << "n = " << n
        ;
        throw std::invalid_argument(msg.str());
      }
      for(; first != last; ++first)
      {
        * result = 0.5 * (*first);
        * result += 0.5 * (*(++first));
        ++result;
      }
      return result;
    }

    /// Rearrange sample from first to last by odd and even entries.
    template <class InputIterator, class OutputIterator>
    void
    arrange_even_odd(
      InputIterator first,
      InputIterator last,
      OutputIterator even_first,
      OutputIterator & even_last,
      OutputIterator odd_first,
      OutputIterator & odd_last)
    {
      const size_t n = last - first;
      const size_t n_half = n / 2;
      if (n < 2)
      {
        * even_first++ = * first++;
        even_last = even_first;
        odd_last = odd_first;
      } else {
        InputIterator even_last_ = first + 2 * n_half;
        for(; first != even_last_; ++first)
        {
          * even_first++  = * first++;
          * odd_first++   = * first++;
        }
        if (first != last)
          * even_first++  = * first++;
        even_last = even_first;
        odd_last  = odd_first;
      }
    }

  } // namespace math

} // namespace mdst

#endif // MDST_MATH_H_
