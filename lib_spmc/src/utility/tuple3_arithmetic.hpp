// =====================================================================================
//
//       Filename:  tuple3_arithmetic.hpp
//
//    Description:  Provide basic arithmetic operations.
//
//        Version:  1.0
//        Created:  11/16/2012 12:41:38 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_TUPLE3_ARITHMETIC_H_
#define MDST_TUPLE3_ARITHMETIC_H_

#include "types.hpp"
#include "tuple3.hpp"

namespace mdst{

  Attribute
  operator*(
    const Tuple3& lhs,
    const Tuple3& rhs);

  Tuple3
  operator*(
    Attribute lhs,
    const Tuple3& rhs);

  Tuple3
  operator*(
    const Tuple3& lhs, 
    Attribute rhs);

  Tuple3
  operator/(
    const Tuple3& lhs, 
    Attribute rhs);

  Tuple3
  operator/(
    Attribute lhs,
    const Tuple3& rhs);

  inline Tuple3
  element_product(
    const Tuple3& lhs, 
    const Tuple3& rhs
  ) {
    return Tuple3(lhs.x() * rhs.x(),
                  lhs.y() * rhs.y(),
                  lhs.z() * rhs.z());
  }

  Tuple3
  element_division(
    const Tuple3 & lhs,
    const Tuple3 & rhs);

  Tuple3
  cross_product(
    const Tuple3& lhs, 
    const Tuple3& rhs);

  Tuple3
  normalize(
    const Tuple3& t);

  unsigned int
  max_index(
    const Tuple3 & t);

  unsigned int
  min_index(
    const Tuple3 & t);

} // namespace mdst

#endif // MDST_TUPLE3_ARITHMETIC_H_
