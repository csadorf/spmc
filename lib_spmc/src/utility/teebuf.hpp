#ifndef MDST_LOGGING_TEEBUF_H_
#define MDST_LOGGING_TEEBUF_H_

namespace mdst{

  namespace logging{

    template <typename char_type,
              typename traits = std::char_traits<char_type> >
    class BasicTeebuf :
      public std::basic_streambuf<char_type, traits>
      {
       public:
        typedef typename traits::int_type int_type;

        BasicTeebuf(
          std::basic_streambuf<char_type, traits> * sb1,
          std::basic_streambuf<char_type, traits> * sb2):
          sb1_(sb1),
          sb2_(sb2)
          {};

       private:
        virtual int sync()
        {
          int const r1 = sb1_->pubsync();
          int const r2 = sb2_->pubsync();
          return r1 == 0 && r2 == 0 ? 0 : -1;
        }

        virtual int_type overflow(int_type c)
        {
          const int_type eof = traits::eof();
          if (traits::eq_int_type(c, eof))
          {
            return traits::not_eof(c);
          } else {
            const char_type ch = traits::to_char_type(c);
            const int_type r1 = sb1_->sputc(ch);
            const int_type r2 = sb2_->sputc(ch);

            return
              traits::eq_int_type(r1, eof) ||
              traits::eq_int_type(r2, eof) ? eof : c;
          }
        }

        std::basic_streambuf<char_type, traits> * sb1_;
        std::basic_streambuf<char_type, traits> * sb2_;
      };

      typedef BasicTeebuf<char> TeeBuf;

  } // namespace logging

} // namespace mdst

#endif // MDST_LOGGING_TEEBUF_H_
