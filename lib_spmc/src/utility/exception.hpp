// =====================================================================================
//
//       Filename:  exception.hpp
//
//    Description:  Provides exceptions in mdst context.
//
//        Version:  1.0
//        Created:  11/15/2012 09:55:43 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_EXCEPTION_H_
#define MDST_EXCEPTION_H_

#include <exception>
#include <string>

namespace mdst{

  class Exception : public std::exception{
   public:
    Exception():
      error_(std::string()) {};
    explicit Exception(
      const std::string& e):
      error_(e) {};
    virtual ~Exception() throw() {};
    virtual const char*
    what() const throw()
      {return error_.c_str();}
    virtual std::string 
    error() const throw()
      {return error_;}
   private:
    std::string error_;
  };

  class RuntimeError: public Exception{
   public:
    RuntimeError() {};
    explicit RuntimeError(
      const std::string & e):
      Exception(e) {};
    virtual ~RuntimeError() throw() {};
  };

  class RuntimeWarning: public Exception{
   public:
    RuntimeWarning() {};
    explicit RuntimeWarning(
      const std::string & e):
      Exception(e) {};
    virtual ~RuntimeWarning() throw() {};
  };

  class NotImplementedError : public RuntimeError{
   public:
    NotImplementedError() {};
    explicit NotImplementedError(
      const std::string & e):
      RuntimeError(e) {};
    virtual ~NotImplementedError() throw() {};
  };

  class AccuracyError: public Exception{
   public:
    AccuracyError() {};
    explicit AccuracyError(
      const std::string & e):
      Exception(e) {};
    virtual ~AccuracyError() throw() {};
  };

  class AccuracyWarning: public Exception{
   public:
    AccuracyWarning() {};
    explicit AccuracyWarning(
      const std::string & e):
      Exception(e) {};
    virtual ~AccuracyWarning() throw() {};
  };

} // namespace mdst

#endif // MDST_EXCEPTION_H_
