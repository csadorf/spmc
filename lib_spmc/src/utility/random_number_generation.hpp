// =====================================================================================
//
//       Filename:  random_number_generation.hpp
//
//    Description:  Generate random numbers.
//
//        Version:  1.0
//        Created:  11/26/2012 10:39:41 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_RANDOM_NUMBER_GENERATION_H_
#define MDST_RANDOM_NUMBER_GENERATION_H_

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/generator_iterator.hpp>

namespace mdst{

  class RandomNumberGenerator{
   public:
    explicit RandomNumberGenerator(unsigned int seed);

   private:
    typedef boost::mt19937 BaseGeneratorType;
    typedef boost::uniform_real<> UniformRealDistribution;
    typedef boost::normal_distribution<> NormalDistribution;

    BaseGeneratorType random_generator_;

    UniformRealDistribution uni_dst_;
    NormalDistribution normal_dst_;
    UniformRealDistribution half_turn_dst_;
    UniformRealDistribution full_turn_dst_;
   public:
    boost::variate_generator<BaseGeneratorType &, UniformRealDistribution> random_number;
    boost::variate_generator<BaseGeneratorType &, NormalDistribution> normal_number;
    boost::variate_generator<BaseGeneratorType &, UniformRealDistribution> random_half_turn;
    boost::variate_generator<BaseGeneratorType &, UniformRealDistribution> random_full_turn;
  };

} // namespace mdst
#endif // MDST_RANDOM_NUMBER_GENERATION_H_
// Random number generation
