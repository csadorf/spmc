// =====================================================================================
//
//       Filename:  multi_threading.hpp
//
//    Description:  Types and functions for multithreading.
//
//        Version:  1.0
//        Created:  01/08/2013 06:36:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_MULTITHREADING_H_
#define MDST_MULTITHREADING_H_

#include <boost/thread/locks.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/recursive_mutex.hpp>

namespace mdst{
  
  namespace multithreading{

    typedef boost::mutex Mutex;
    typedef boost::shared_mutex SharedMutex;
    typedef boost::recursive_mutex RecursiveMutex;
    typedef boost::lock_guard<boost::mutex> LockGuard;
    typedef boost::lock_guard<boost::recursive_mutex> RecursiveLockGuard;

  } // namespace multithreading

} // namespace mdst

#endif // MDST_TYPES_H_

