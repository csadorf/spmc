// =====================================================================================
//
//       Filename:  misc.cpp
//
//    Description:  Miscellaneous utility functions.
//
//        Version:  1.0
//        Created:  11/21/2012 01:08:27 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <algorithm>
#include <sstream>

#include "misc.hpp"

namespace mdst{

  TagSet
  generate_subset(const unsigned int n, const int offset){
    TagSet result;
    for(unsigned int i = 0; i < n; ++i)
      result.insert(i + offset);
    return result;
  }

  template <class C>
  void
  dump_container(const C& c,
                 std::ostream& stream){
    for(typename C::const_iterator it = c.begin(); it != c.end(); ++it)
      stream << *it << " ";
    stream << std::endl;
  }

  std::ostream &
  dump_tagset(const TagSet& tags,
              std::ostream& stream){
    for(TagSetConstIterator it = tags.begin(); it != tags.end(); ++it)
      stream << *it << " ";
    stream << std::endl;
    return stream;
  }

  bool
  intersect(const TagSet& a, const TagSet& b){
    typedef std::vector<Tag> TagVector;
    TagVector v(std::min(a.size(), b.size()));
    TagVector::iterator it;

    it = std::set_intersection(a.begin(), a.end(), b.begin(), b.end(), v.begin());

    return int(it - v.begin()) > 0;
  }

  TagSet
  difference(const TagSet & a, const TagSet & b){
    typedef std::vector<Tag> TagVector;
    TagVector v(a.size());
    TagVector::iterator last;
    last = std::set_difference(a.begin(), a.end(), b.begin(), b.end(), v.begin());
    return TagSet(v.begin(), last);
  }

  Strings
  split(const std::string & s,
        Strings::value_type::value_type delimiter){
    Strings result;
    return split(result, s, delimiter, split_t::no_empties);
  }

  std::string
  determine_extension(const std::string & filename){
    std::vector<std::string> tokens;
    split(tokens, filename, '.', split_t::no_empties);
    return tokens.at(tokens.size()-1);
  }

  IntVector
  find_prime_numbers(
    int number)
  {
    assert(number > 3);
    IntVector result;
    size_t n = 1;

    int z = 2;
    while (z * z <= number){
      if (number % z == 0){
        ++n;
        result.push_back(z);
        number /= z;
      } else {
        ++z;
      }
    }
    result.push_back(number);
    result.resize(n);
    std::sort(result.begin(), result.end());
    return result;
  }

  IntVectorIterator
  limit_prime_numbers(
    IntVector & prime_numbers,
    const unsigned int limit)
  {
    assert(limit > 0);
    assert(limit <= prime_numbers.size());
//    assert(std::is_sorted(prime_numbers.begin(), prime_numbers.end()));
    size_t n = 0;
    size_t s = prime_numbers.size();
    while(s - n > limit){
      prime_numbers[n + 1] *= prime_numbers[n];
      ++n;
      std::sort(prime_numbers.begin() + n, prime_numbers.end());
    }
    assert(s - n == limit);
    for(unsigned int i = 0; i < limit; ++i)
      prime_numbers[i] = prime_numbers[i + n];
    prime_numbers.resize(limit);
    return prime_numbers.end();
  }

  unsigned int
  sum(
    const IntVector & v)
  {
    unsigned int s = 0;
    for(IntVectorConstIterator i = v.begin(); i != v.end(); ++i)
      s += *i;
    return s;
  }

  unsigned int
  product(
    const IntVector & v)
  {
    unsigned int p = 1;
    for(IntVectorConstIterator i = v.begin(); i != v.end(); ++i)
      p *= *i;
    return p;
  }

  Attribute
  mean(
    AttributesConstIterator first,
    AttributesConstIterator last)
  {
    Attribute sum = 0;
    const size_t number_of_values = last - first;
    for(; first != last; ++first)
    {
      sum += * first;
    }
    return sum / number_of_values;
  }

  Attribute
  variance(
    AttributesConstIterator first,
    AttributesConstIterator last,
    const Attribute mu)
  {
    Attribute sum = 0;
    const size_t number_of_values = last - first;
    for(; first != last; ++first)
    {
      Attribute d = *first - mu;
      sum += d * d;
    }
    return sum / number_of_values;
  }

} // namespace mdst
