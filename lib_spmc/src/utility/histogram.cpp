// =====================================================================================
//
//       Filename:  histogram.cpp
//
//    Description:  Provide easy and efficient counting functions.
//
//        Version:  1.0
//        Created:  10/25/2012 03:12:12 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================


#include <vector>
#include "histogram.hpp"

namespace mdst{

// Histogram is a template function, therefore all definitions are in
// the header file!


} // namespace mdst


// TESTING FUNCTION!
int
main ( int argc, char *argv[] )
{
  std::vector<int> l;
  l.push_back(0);
  l.push_back(1);
  l.push_back(2);
  l.push_back(2);
  l.push_back(3);
  l.push_back(3);
  l.push_back(3);
  mdst::Histogram<std::vector<int> > h(l);
  h[4] = 100;
  h.dump();
  std::cout << h[3] << std::endl;
  return 0;
}				// ----------  end of function main  ---------- 
