// =====================================================================================
//
//       Filename:  logging.hpp
//
//    Description:  Basic logging framework.
//
//        Version:  1.0
//        Created:  09/09/13
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_LOGGING_H_
#define MDST_LOGGING_H_

#include "memory.hpp"
#include "teebuf.hpp"

#include <sstream>
#include <fstream>

namespace mdst{
  
  namespace logging{

    class Logger{
      
     public:
      Logger(
        size_t num_channels,
        std::ostream & dst = std::cout):
        num_channels_(num_channels),
        standard_dst_(dst),
        ostreams_(num_channels),
        ofstreams_(num_channels)
      {
        clear();
      }

      void
      clear(){
        init_(num_channels_, standard_dst_);
      }

      std::ostream &
      channel(
        size_t id){
        return * ostreams_.at(id);
      }

      void
      flush(
        size_t id)
      {
        * ostreams_.at(id) << std::flush;
      }

      void
      flush_all()
      {
        for(OStreamsConstIterator_ it = ostreams_.begin();
            it != ostreams_.end(); ++it)
          (**it) << std::flush;
      }

      void
      redirect(
          size_t id,
          std::ostream & dst){
        ostreams_.at(id)->rdbuf(dst.rdbuf());
      }

      void
      redirect_to_file(
        size_t id,
        const std::string & filename){
        OfStreamPtr_ file(
          new std::ofstream(
            filename.c_str(), 
            std::ios_base::app));
        ofstreams_[id] = file;
        ostreams_.at(id)->rdbuf(file->rdbuf());
      }

     private:

      void
      init_(
        size_t num_channels,
        const std::ostream & dst)
      {
        for (size_t i = 0; i < num_channels; ++i){
          ostreams_[i] = OStreamPtr_(
            new std::ostream(dst.rdbuf()));
        }
      }

      typedef shared_ptr<std::ostream> OStreamPtr_;
      typedef shared_ptr<std::ofstream> OfStreamPtr_;
      typedef std::vector<OStreamPtr_> OStreams_;
      typedef OStreams_::const_iterator OStreamsConstIterator_;
      typedef std::vector<OfStreamPtr_> OfStreams_;

      const size_t num_channels_;
      const std::ostream & standard_dst_;
      OStreams_ ostreams_;
      OfStreams_ ofstreams_;
    };

  } // namespace logging

} // namespace mdst

#endif // MDST_LOGGING
