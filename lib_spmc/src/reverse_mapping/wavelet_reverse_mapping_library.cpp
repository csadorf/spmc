// =====================================================================================
//
//       Filename:  wavelet_reverse_mapping_library.cpp
//
//    Description:  Improve wavelet configurations from library.
//
//        Version:  1.0
//        Created:  04/14/2014
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "wavelet_reverse_mapping_library.hpp"

namespace mdst{

  namespace reverse_mapping{
    
    using namespace mdst::SimulationConfiguration;
    typedef ReverseMappingStructure3::AssocMap AssocMap;
    typedef AssocMap::const_iterator AssocMapConstIterator;

    //int RoGHash::epsilon_ = RoG_EPSILON_DEFAULT;
    Attribute RoGHash::epsilon_ = RoG_EPSILON_DEFAULT;
    Attribute RoGHash::adjust_rate_ = 0.001;


    static const size_t DEFAULT_ROD_SIZE = pow(2, 8);
    static const size_t MIN_ROD_SIZE = pow(2, 2);

    WaveletReverseMappingLibrary::WaveletReverseMappingLibrary():
      ReverseMappingLibrary(),
      default_rod_size_(DEFAULT_ROD_SIZE)
    {};

    void
    WaveletReverseMappingLibrary::learn_from_subset(
      const SimulationConfig & config_cg,
      const SimulationConfig & config_at,
      const AssocMap & mapping,
      TagVectorIterator first,
      TagVectorIterator last)
    {
      const size_t n_cg = last - first;
      const size_t n_at = 2 * n_cg;
      Structure3 structure_cg(n_cg);
      structure3_from_config(config_cg, structure_cg, first, last);
      AssocMap::const_iterator mapping_first(mapping.find(* first));
      if (mapping_first == mapping.end())
      {
        std::cerr
          << "Failed to find mapping for particle " << * first << "!"
          << std::endl
          ;
      }
      assert(mapping_first != mapping.end());
      const ParticleTag shift(* mapping.find(* first)->second->begin());
      Structure3 structure_at(n_at);
      structure3_from_config(
        config_at, structure_at,
        config_at.particle_iterator(shift),
        config_at.particle_iterator(shift + n_at));
      structure_lib_.save(
        structure_cg,
        ReverseMappingStructure3(structure_at, AssocMap()));
    }

    void
    WaveletReverseMappingLibrary::learn_from(
      const SimulationConfig & config_cg,
      const SimulationConfig & config_at,
      const AssocMap & mapping)
    {
      TagSetPtrSet molecules(config_cg.molecules());
      for(TagSetPtrSetConstIterator molecule = molecules.begin();
          molecule != molecules.end(); ++molecule)
      {
        TagVector mol_vec(vector_from_set(** molecule));
        TagVectorIterator first = mol_vec.begin();
        //learn_from_subset(config_cg, config_at, mapping, first, mol_vec.end());
        const size_t rod_size 
          = std::max(MIN_ROD_SIZE, std::min(default_rod_size_, mol_vec.size() / 10));
        TagVectorIterator last = first + rod_size;
        while(last <= mol_vec.end())
        {
          learn_from_subset(
            config_cg,
            config_at,
            mapping,
            first,
            last);
          ++first;
          ++last;
        }
      }
    }

    Attribute
    WaveletReverseMappingLibrary::improve_subset_(
      const SimulationConfig & config_cg,
      SimulationConfig & config_at,
      const AssocMap & mapping,
      TagVectorIterator first,
      TagVectorIterator last)
    {
      const size_t n_subset = last - first;
      Structure3 structure_cg(n_subset);
      structure3_from_config(config_cg, structure_cg, first, last);
      //structure_cg.center();
      Attribute min_delta;
      /*
      if (structure_lib_.size() == 0)
      {
        std::cerr << "Library is empty!" << std::endl;
        return 0;
      }
      */
      Structure3Library::ConstIterator match = structure_lib_.find(structure_cg, min_delta);
      if (structure_lib_.size() && match == structure_lib_.end())
      {
        throw RuntimeWarning("Failed to improve.");
      }
      if (match == structure_lib_.end())
      {
        return 0;
      }
      const Structure3 & structure_at = match->second->second.structure_at;
      Structure3 const & structure_cg_lib(match->second->first);
      const AssocMap::const_iterator mapping_first(mapping.find(* first));
      assert(mapping_first != mapping.end());
      assert(mapping_first->second->size());
      const ParticleTag mapping_shift = * mapping_first->second->begin();
      size_t i_at = -1;
      size_t i_cg = -1;
      for(; first != last; ++first)
      {
        const Tuple3 pos_cg_bead(config_cg.particle(*first).remapped_position());
        const Tuple3 delta(pos_cg_bead - column(structure_cg_lib, ++i_cg));
        ++i_at;
        config_at.particle(mapping_shift + i_at).set_position(
          delta + column(structure_at, i_at));
        ++i_at;
        config_at.particle(mapping_shift + i_at).set_position(
          delta + column(structure_at, i_at));
        /*const Tuple3 check(0.5 * (
            column(structure_at, i_at - 1) 
          + column(structure_at, i_at)) - column(structure_cg_lib, i_cg));
        if (abs(check) > 1e-10){
          std::cerr 
            << "pos_cg_bead: " << pos_cg_bead << std::endl
            << "struct_cg_bead: " << column(structure_cg_lib, i_cg) << std::endl
            << "check: " << check << std::endl
            << "struct_at_beads: " << column(structure_at, i_at - 1) 
            << column(structure_at, i_at) << std::endl
            << "delta: " << delta << std::endl
            ;
          std::stringstream msg;
          msg
            << "Error during 'learn_from_subset'. "
            << "Inaccuracy of saved subset too high: "
            << abs(check)
            << std::endl;
          throw RuntimeError(msg.str());
        }*/
      }
#ifdef DEBUG
      config_at.check();
#endif
      return 0;
    }

    bool
    WaveletReverseMappingLibrary::check_transformation(
      const SimulationConfiguration::SimulationConfig & config_cg,
      const SimulationConfiguration::SimulationConfig & config_at,
      const TagSetMap & mapping){
      for(TagSetMapConstIterator it = mapping.begin();
          it != mapping.end(); ++it){
        const Tuple3 cg_bead(config_cg.particle(it->first).remapped_position());
        Tuple3 centroid;
        for(TagSetConstIterator p_at = it->second->begin();
            p_at != it->second->end(); ++p_at){
          const Tuple3 at_bead(config_at.particle(* p_at).remapped_position());
          centroid += at_bead;
        }
        size_t n_atoms = it->second->size();
        centroid = centroid / n_atoms;
        const Tuple3 delta(centroid - cg_bead);
        if (abs(delta) > 10e-12){
          std::stringstream msg;
          msg
            << "Invalid wavelet transformation! "
            << "cg bead " << it->first << ": " << cg_bead << " "
            << "centroid " << centroid << " "
            << "delta " << delta
            ;
          std::cerr 
            << msg.str() 
            << std::endl
            << "Mapping: "
            << std::endl
            ;
          print_iterable(it->second->begin(), it->second->end()) << std::endl;
          //throw RuntimeError(msg.str());
          return false;
        }
      }
      return true;
    }

    Attribute
    WaveletReverseMappingLibrary::improve(
      const SimulationConfiguration::SimulationConfig & config_cg,
      SimulationConfiguration::SimulationConfig & config_at,
      const TagSetMap & mapping)
    {
      TagSetPtrSet molecules(config_cg.molecules());
      for(TagSetPtrSetConstIterator molecule = molecules.begin();
          molecule != molecules.end(); ++molecule)
      {
        TagVector mol_vec(vector_from_set(**molecule));
        TagVectorIterator first = mol_vec.begin();
        //improve_subset(config_cg, config_at, mapping, first, mol_vec.end());
        const size_t rod_size =
          std::max(MIN_ROD_SIZE, std::min(default_rod_size_, mol_vec.size() / 10));
        TagVectorIterator last = first + rod_size;
        //size_t i = 0;
        while(last <= mol_vec.end())
        {
          //std::cerr << float(++i) / mol_vec.size() * 100 << "%" << std::endl;
          improve_subset(config_cg, config_at, mapping, first, last);
          first += rod_size;
          last += rod_size;
          //++first;
          //++last;
        }
      }
      return 0;
    }

    Attribute
    WaveletReverseMappingLibrary::compare(
      const SimulationConfiguration::SimulationConfig & config_a,
      const SimulationConfiguration::SimulationConfig & config_b) const
    {
      Structure3 structure_a(config_a.num_atoms());
      structure3_from_config(config_a, structure_a);
      Structure3 structure_b(config_b.num_atoms());
      structure3_from_config(config_b, structure_b);
      return structure_a.calculate_rmsd(structure_b);
    }
      
  } // namespace reverse_mapping

} // namespace mdst
