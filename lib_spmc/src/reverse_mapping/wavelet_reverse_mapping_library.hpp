// =====================================================================================
//
//       Filename:  wavelet_reverse_mapping_library.hpp
//
//    Description:  Improve wavelet configurations from library.
//
//        Version:  1.0
//        Created:  04/14/2014
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_WAVELET_REVERSE_MAPPING_LIBRARY_H_
#define MDST_WAVELET_REVERSE_MAPPING_LIBRARY_H_

#include "reverse_mapping_library.hpp"

namespace mdst{

  namespace reverse_mapping{

    template<
      class TagIterator>
    void
    structure3_from_config(
      const SimulationConfiguration::SimulationConfig & config,
      Structure3 & structure,
      TagIterator first,
      TagIterator last)
    {
      typedef
        SimulationConfiguration::RemappedPositionsConstIteratorFromTagIterator<TagIterator>
        RePosIterator;
      structure.from_tuples(
        RePosIterator(config, first),
        RePosIterator(config, last));
    };

    void
    structure3_from_config(
      const SimulationConfiguration::SimulationConfig & config,
      Structure3 & structure)
    {
      TagVector all(vector_from_set(config.all()));
      structure3_from_config(config, structure, all.begin(), all.end());
    };

    class WaveletReverseMappingLibrary : public ReverseMappingLibrary
    {
     public:

      explicit
      WaveletReverseMappingLibrary();

      virtual void
      learn_from_subset(
        const SimulationConfiguration::SimulationConfig & config_cg,
        const SimulationConfiguration::SimulationConfig & config_at,
        const ReverseMappingStructure3::AssocMap & mapping,
        TagVectorIterator first,
        TagVectorIterator last);

      virtual void
      learn_from(
        const SimulationConfiguration::SimulationConfig & config_cg,
        const SimulationConfiguration::SimulationConfig & config_at,
        const ReverseMappingStructure3::AssocMap & mapping);

      virtual Attribute
      improve(
        const SimulationConfiguration::SimulationConfig & config_cg,
        SimulationConfiguration::SimulationConfig & config_at,
        const TagSetMap & mapping);

      virtual bool
      check_transformation(
        const SimulationConfiguration::SimulationConfig & config_cg,
        const SimulationConfiguration::SimulationConfig & config_at,
        const TagSetMap & mapping);

      Attribute
      compare(
        const SimulationConfiguration::SimulationConfig & config_a,
        const SimulationConfiguration::SimulationConfig & config_b) const;

     size_t
     default_rod_size() const
     { return default_rod_size_;}

     void
     set_default_rod_size(
      const size_t value)
     { default_rod_size_ = value;}

     protected:
      virtual Attribute
      improve_subset_(
        const SimulationConfiguration::SimulationConfig & config_cg,
        SimulationConfiguration::SimulationConfig & config_at,
        const ReverseMappingStructure3::AssocMap & mapping,
        TagVectorIterator first,
        TagVectorIterator last);

     private:
      size_t default_rod_size_;
      
    };


  } // namespace reverse_mapping

} // namespace mdst

#endif // MDST_WAVELET_REVERSE_MAPPING_LIBRARY_H_

