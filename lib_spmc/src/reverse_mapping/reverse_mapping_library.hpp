// =====================================================================================
//
//       Filename:  reverse_mapping_library.hpp
//
//    Description:  Hashed library for fast lookups.
//    Description:  Lookup configurations and improve them from library.
//
//        Version:  1.0
//        Created:  04/07/2014
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#ifndef MDST_REVERSE_MAPPING_LIBRARY_H_
#define MDST_REVERSE_MAPPING_LIBRARY_H_

#include <simulation_config/simulation_config.hpp>
#include <simulation_config/access.hpp>
#include <simulation_config/structure_library.hpp>

namespace mdst{

  namespace reverse_mapping{

    static const Attribute RoG_EPSILON_MIN = pow(2, 0);
    static const Attribute RoG_EPSILON_MAX = pow(2, 8);
    static const Attribute RoG_EPSILON_DEFAULT = pow(2, 5);

    typedef SimulationConfiguration::Structure<Attribute, 3> Structure3;

    Tuple3
    column(
      const Structure3 & structure,
      const int n)
    {
      return Tuple3(structure.matrix(0,n), structure.matrix(1,n), structure.matrix(2,n));
    };

    TuplesIterator
    tuples_from_structure3
    (
      const Structure3 & structure,
      TuplesIterator result)
    {
      const size_t m = structure.m();
      const size_t n = structure.n();
      for(size_t i = 0; i < n; ++i)
      {
        for(size_t j = 0; j < m; ++j)
        {
          //result->operator[](j) = matrix[j][i];
          result->operator[](j) = structure.matrix(j, i);
        }
        ++result;
      }
      return result;
    };


    class ReverseMappingStructure3
    {
     public:
      Structure3 structure_at;

      //typedef std::multimap<ParticleTag, ParticleTag> AssocMap;
      typedef TagSetMap AssocMap;
      AssocMap cg_scheme;

      //typedef std::map<ParticleTag, ParticleTag> ParticleMap;
      //ParticleMap at_mapping;

      explicit
      ReverseMappingStructure3(
        const Structure3 & s_at,
        const AssocMap & cg_s):
        //const ParticleMap & at_m):
        structure_at(s_at),
        cg_scheme(cg_s)
        {};

      void
      center()
      {
        structure_at.center();
      };

      ReverseMappingStructure3
      centered() const
      {
        ReverseMappingStructure3 ret(* this);
        ret.center();
        return ret;
      }
    };

    /*
    template<
      class T>
    struct ComparisonFunctor
    {
     public:
      ComparisonFunctor(
        T & epsilon):
        epsilon_(epsilon)
      {};

      bool
      operator()(
        const T & a,
        const T & b) const
      {
        return a < b;
      }

     private:
      T & epsilon_;
    };*/

    class RoGHash{
     public:
      typedef Attribute T;

      T
      operator() (
        const Structure3 & structure) const
      {
        const size_t m = 3;
        const size_t n = structure.n();

        T r_g = 0;
        for(size_t i = 0; i < m; ++i)
          for(size_t j = 0; j < n; ++j)
          {
            static T d;
            d = structure.matrix(i, j); // Assuming the structure is already centered!
            r_g += d * d;
          }
        /*std::cerr
          << "RoG of following structure is: " << r_g / n
          << std::endl;
        structure.dump();*/

        return r_g / n;
      }

      struct Compare_
      {
        bool
        operator()(
          const T & a,
          const T & b) const
        {
          /*std::cerr
            << "a = " << a << " "
            << "b = " << b << " "
            << "|a - b|=" << std::abs(a - b) 
            << std::endl;*/
          if (std::abs(a - b) < epsilon_)
            return false;
          else
            return a < b;
        }
      };

      typedef Compare_ Compare;

      static Attribute
      set_epsilon(
        const Attribute & epsilon)
      {
        epsilon_ = std::max(RoG_EPSILON_MIN, std::min(RoG_EPSILON_MAX, epsilon));
        return epsilon_;
      }

      static Attribute
      adjust_accuracy(
        const double acc_delta)
      {
        return set_epsilon(epsilon_ + (adjust_rate_ * - acc_delta * RoG_EPSILON_MAX));
      }

      static Attribute
      set_least_accurate()
      {
        epsilon_ = RoG_EPSILON_MAX;
        return epsilon_;
      }

      static Attribute
      set_most_accurate()
      {
        epsilon_ = RoG_EPSILON_MIN;
        return epsilon_;
      }

      static Attribute
      epsilon()
      {
        return epsilon_;
      }

      static Attribute
      adjust_rate()
      {
        return adjust_rate_;
      }

      static Attribute
      set_adjust_rate(
        const T & rate)
      {
        adjust_rate_ = std::max(0.01, std::min(1.0, rate));
        return adjust_rate_;
      }
 
     private:
      static Attribute epsilon_;
      static Attribute adjust_rate_;

    };

    template<
      class T_>
    class Structure3Compare{
     public:

      typedef T_ T;

      T
      operator()(
       const Structure3 & a,
       const Structure3 & b) const
      {
        return a.calculate_rmsd(b);
      }

    };

    typedef HashedLibrary<
      Structure3,
      RoGHash,
      Structure3Compare<Attribute>,
      ReverseMappingStructure3
        > Structure3Library_;

    class Structure3Library: public Structure3Library_
    {
     public:
      ConstIterator
      find(
        const Structure3 & structure,
        Attribute & min_delta) const
      {
        return Structure3Library_::find(structure.centered(), min_delta);
      }

      void
      save(
        const Structure3 & key_structure,
        const ReverseMappingStructure3 & structure_rm)
      {
        Structure3Library_::insert(key_structure.centered(), structure_rm.centered());
      }

    };

    class ReverseMappingLibrary
    {
     public:

      explicit
      ReverseMappingLibrary(
        float adjust_ratio_limit = 0.1):
        adjust_ratio_limit_(adjust_ratio_limit)
      {};

      size_t
      size() const
      {
        return structure_lib_.size();
      }

      virtual void
      learn_from_subset(
        const SimulationConfiguration::SimulationConfig & config_cg,
        const SimulationConfiguration::SimulationConfig & config_at,
        const ReverseMappingStructure3::AssocMap & mapping,
        TagVectorIterator first,
        TagVectorIterator last) = 0;

      virtual void
      learn_from(
        const SimulationConfiguration::SimulationConfig & config_cg,
        const SimulationConfiguration::SimulationConfig & config_at,
        const ReverseMappingStructure3::AssocMap & mapping) = 0;

      Attribute
      improve_subset(
        const SimulationConfiguration::SimulationConfig & config_cg,
        SimulationConfiguration::SimulationConfig & config_at,
        const ReverseMappingStructure3::AssocMap & mapping,
        TagVectorIterator first,
        TagVectorIterator last)
      {
        return improve_subset_(config_cg, config_at, mapping, first, last);
      }

      virtual Attribute
      improve(
        const SimulationConfiguration::SimulationConfig & config_cg,
        SimulationConfiguration::SimulationConfig & config_at,
        const ReverseMappingStructure3::AssocMap & mapping) = 0;

      virtual bool
      check_transformation(
        const SimulationConfiguration::SimulationConfig & config_cg,
        const SimulationConfiguration::SimulationConfig & config_at,
        const ReverseMappingStructure3::AssocMap & mapping) = 0;

     protected:

      virtual Attribute
      improve_subset_(
        const SimulationConfiguration::SimulationConfig & config_cg,
        SimulationConfiguration::SimulationConfig & config_at,
        const ReverseMappingStructure3::AssocMap & mapping,
        TagVectorIterator first,
        TagVectorIterator last) = 0;
      

     protected:
      Structure3Library structure_lib_;

     private:
      float adjust_ratio_limit_;
    };

  } // namespace reverse_mapping

} // namespace mdst

#endif // MDST_REVERSE_MAPPING_LIBRARY_H_

