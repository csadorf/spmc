// =====================================================================================
//
//       Filename:  rmtest.cpp
//
//    Description:  Testing program.
//
//        Version:  1.0
//        Created:  10/22/2012 11:27:01 AM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <iostream>
#include <parser_writer/hoomd_blue_xml.hpp>
#include "config_manipulation.hpp"

using namespace mdst;
using SimulationConfiguration::SimulationConfig;
using SimulationConfiguration::PropertyView;
using SimulationConfiguration::BondView;
using SimulationConfiguration::ParticleView;

int
main ( int argc, char *argv[] ){
  std::string filename;
  if (argc > 1)
    filename = std::string(argv[1]);
  else
    filename = "restart.xml";
  std::cerr << "Reading '" << filename << "' ..." << std::endl;

  try{
    SimulationConfiguration::ParserWriter::HoomdBlueXml parser_writer;
    shared_ptr<SimulationConfig> aa_config(parser_writer.parse(filename));
    shared_ptr<SimulationConfig> coarse_grained_config(new SimulationConfig());
    coarse_grained_config->set_box(aa_config->box());
    
    const TagSetSet molecules(aa_config->property.molecules());
    int iteration = 0;
    for(TagSetSetConstIterator it = molecules.begin();
        it != molecules.end();
        ++it){
      std::cerr << ++iteration << "/" << molecules.size() << std::endl;
//      if (iteration > 9)
//        break;
      shared_ptr<SimulationConfig> molecule_config(new SimulationConfig(*aa_config, *it));
      const TagSet group_C = molecule_config->group(ParticleType("C"));
      shared_ptr<SimulationConfig> cg_molecule_config(
          new SimulationConfig(*molecule_config,
                               group_C));
//      std::stringstream filename;
//      filename << iteration << ".xml";                                    
//      parser_writer.dump_xml(cg_molecule_config.get(), filename.str());
      coarse_grained_config->append(*cg_molecule_config);
    }

    ConfigManipulator manipulator(coarse_grained_config);
    manipulator.rename("B", coarse_grained_config->all());
    TagSet group_chain_ends;
    const TagSetSet cg_molecules = coarse_grained_config->property.molecules();
    for(TagSetSetConstIterator it = cg_molecules.begin();
        it != cg_molecules.end();
        ++it){
      const TagSet ends = coarse_grained_config->property.find_ends(*it);
      group_chain_ends.insert(ends.begin(), ends.end());
    }
    manipulator.rename("A", group_chain_ends);

    parser_writer.write(*coarse_grained_config, std::cout);
  }
  catch (SimulationConfiguration::ParserWriter::ParseException & e){
    std::cerr << "Error while parsing." << std::endl;
    // Intentionally not exiting the program!!
  }
  return 0;
}				// ----------  end of function main  ---------- 

