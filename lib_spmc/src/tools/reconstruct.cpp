// =====================================================================================
//
//       Filename:  reconstruct.cpp
//
//    Description:  Reconstruct coarse-grained simulation configurations. 
//
//        Version:  1.0
//        Created:  01/19/2013 07:26:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include "reverse_mapping.hpp"
#include <parser_writer/guess_parser.hpp>
#include "forcefield_parameters.hpp"
#include <memory.hpp>

#include <iostream>
#include <boost/program_options.hpp>
#include <boost/version.hpp>

#if BOOST_VERSION>=104200
#include <boost/exception/all.hpp>
#endif

using namespace mdst;
using namespace SimulationConfiguration;
namespace po = boost::program_options;

typedef std::vector< shared_ptr<SimulationConfig> > SimulationConfigs;
typedef SimulationConfigs::const_iterator SimulationConfigsConstIterator;
typedef std::vector<std::string> Filenames;
typedef Filenames::const_iterator FilenamesConstIterator;

unsigned int
seed(){
  return static_cast<unsigned int> (std::time(0));
}

shared_ptr<SimulationConfig>
reconstruct(const SimulationConfig & coarse_grained_config,
            const Parameters& parameters,
            const ReducedUnitsSet& reduced_units_set,
            unsigned int seed,
            int number_of_threads,
            bool no_learning,
            int convergence){
  shared_ptr<SimulationConfig> result(new SimulationConfig());
  reverse_mapping::ReverseMapper rm(
    result,
    parameters,
    reduced_units_set,
    seed);
  rm.reverse_map(coarse_grained_config,
                 number_of_threads,
                 no_learning,
                 convergence);
  return result;
}

po::variables_map
parse_command_line(int argc,
                   char* argv[],
                   po::options_description & general_description){
  general_description.add_options()
    ("help,h", "produce help message")
    ("output-file", "set output file")
    ("output-prefix", po::value<std::string>()->default_value("reconstructed_"),
     "Reconstructed configurations will be written to output files. "
     "Filenames are the inputfile's filenames prefixed by this value.")
    ("input-format", po::value<std::string>(),
     "Force a specific input format. If not set, the parser will try to guess the\
      format by extension.")
    ("output-format", po::value<std::string>(),
     "Choose the output format. If not set, the writer will use the input format.")
    ("include-path,I", po::value< std::vector<std::string> >(), "include path")
    ("number-of-threads,n", po::value<int>()->default_value(1),
     "Hint at how many threads are to be used.")
    ("no-learning", po::value<bool>()->default_value(false)->implicit_value(true)->zero_tokens(),
     "Reconstruct without learning algorithm.")
    ("convergence", po::value<int>()->default_value(5),
     "Defines the convergence factor.")
    ("parameter,p", po::value<std::string>(),
     "The parameter set, which is supposed to be used for reconstruciton.")
    ("parameter-options,o", po::value<std::string>()->default_value(std::string()))
    ("reduce-by,r", po::value<std::string>()->default_value("carbon"),
     "The base dimension, which is to be used for reduction.")
#if BOOST_VERSION>=104200
    ("input-files", po::value<Filenames>()->required(), "input files")
#else
    ("input-files", po::value<Filenames>(), "input files")
#endif
  ;

  po::positional_options_description positional_options;
  positional_options.add("input-files", -1);

  po::variables_map variables_map;
  po::store(po::command_line_parser(argc, argv).
            options(general_description).positional(positional_options).run(), variables_map);
  po::notify(variables_map);
  return variables_map;
}

void
print_help(const po::options_description& descr){
  std::cerr << descr << std::endl;
}

int
main ( int argc, char *argv[] ){
  po::options_description general_description("Allowed options");
  po::variables_map variables_map(
      parse_command_line(argc, argv, general_description));

  try{
    if ( variables_map.count("help")
#if BOOST_VERSION<104200
      ||
        !variables_map.count("input-files")
#endif
       ){
      print_help(general_description);
      return 1;
    }

    Filenames filenames;
    if (variables_map.count("input-files")){
      filenames = variables_map["input-files"].as<Filenames>();
    }

    const std::string OUTPUT_PREFIX(variables_map["output-prefix"].as<std::string>());
    const bool output_to_files = filenames.size() > 1;
    if (output_to_files){
      std::cerr << "More than one input file, reconstructed configurations ";
      std::cerr << "will be written to '" << OUTPUT_PREFIX;
      std::cerr << "$input_filename'." << std::endl;
    }

    for(FilenamesConstIterator it = filenames.begin(); it != filenames.end(); ++it){
      shared_ptr<ParserWriter::Parser> parser(
        ParserWriter::guess_parser(*it));
      shared_ptr<ParserWriter::Writer> writer(dynamic_cast<ParserWriter::Writer *>(parser.get()));
      
      shared_ptr<SimulationConfig> config(parser->parse(*it));

      shared_ptr<SimulationConfig> reconstructed_config(
        reconstruct(
          * config,
          parameters::forcefields::by_name(
              variables_map["parameter"].as<std::string>(),
              parameters::forcefields::parse_options(
                variables_map["parameter-options"].as<std::string>())),
          parameters::reduced_units::by_name(
              variables_map["reduce-by"].as<std::string>()),
          seed(),
          variables_map["number-of-threads"].as<int>(),
          variables_map["no-learning"].as<bool>(),
          variables_map["convergence"].as<int>()
        )
      );
      if (output_to_files){
        std::stringstream reconstructed_filename;
        reconstructed_filename << OUTPUT_PREFIX << *it;
        writer->write(*reconstructed_config, reconstructed_filename.str());
      } else {
        writer->write(*reconstructed_config, std::cout);
      }
    }
  }
#if BOOST_VERSION>=104200
  catch (boost::exception & e){
    std::cerr << boost::diagnostic_information(e);
    return 1;
  }
#endif
  catch (po::unknown_option & e){ // TODO (csa) Does not catch.
    print_help(general_description);
    std::cerr << "Error: " << e.what() << std::endl;
    return 1;
  }
  catch (ParserWriter::ParseException & e){
    std::cerr << "Error while parsing: " << e.error() << std::endl;
    return 2;
  }
  return 0;
}				// ----------  end of function main  ---------- 
