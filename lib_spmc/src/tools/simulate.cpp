// =====================================================================================
//
//       Filename:  simulate.cpp
//
//    Description:  Simulate configurations.
//
//        Version:  1.0
//        Created:  01/19/2013 07:26:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <simulator/mc_nvt_sampler.hpp>
#include <simulator/forces.hpp>
#include <simulator/analyzer.hpp>
//#include <parameters/forcefield_parameters.hpp>
#include <utility/tuple3_arithmetic.hpp>

#include <iostream>
#include <boost/program_options.hpp>
#include <boost/version.hpp>

#if BOOST_VERSION>=104200
#include <boost/exception/all.hpp>
#endif

using namespace mdst;
using namespace SimulationConfiguration;
using namespace ParserWriter;
using namespace simulator;
namespace po = boost::program_options;

unsigned int
seed(){
  return static_cast<unsigned int> (std::time(0));
}

Length
length_(Attribute l){
  return argon.expand_length(l * argon.reduce(argon.length));
}

Energy
energy_(Attribute e){
  return argon.expand_energy(e * argon.reduce(argon.energy));
}

int
main(int argc, const char * argv[]){
  assert(argc > 1);
  Session session;
  session.init_read(std::string(argv[1]));
  int number_of_steps = 100;
  if (argc > 2){
    std::istringstream(std::string(argv[2])) >> number_of_steps;
  }
  if (argc > 3){
    int box_size;
    std::istringstream(std::string(argv[3])) >> box_size;
    session.config()->set_box(Tuple3(box_size));
  }
//  Tuple3 box = session.config()->box();
//  session.config()->set_box(box * 10);
//  Parameters dodecane(parameters::forcefields::by_name("dodecane"));
//  HarmonicBondParameters p = dodecane.bonds.at(Type("C-C"));
//  shared_ptr<Force> hbf(new HarmonicBondForce("C-C", p));
//  session.forcefield_add(hbf);
  shared_ptr<LennardJonesPairforce> lj_test(new LennardJonesPairforce(
    SortedTypePair("A","A"), energy_(1), length_(2), 1.0, length_(5)));
  session.forcefield_add(lj_test);
  shared_ptr<MonteCarloMove> move(new TranslateJiggleMove(1));
  session.moves_add(move);
  shared_ptr<Simulator> mcnvt(new MonteCarloNVTSampler(7));
  session.set_simulator(mcnvt);
  session.random_seed(42);
//  testing.potential_energy(session);
  session.analyzer_add("pe");
  session.analyzer_set_frequency(1);
  session.run(number_of_steps);
  session.dump_write("sampled.xml", "hoomd_blue_xml");
  return 0;
}

