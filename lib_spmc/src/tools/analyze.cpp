// =====================================================================================
//
//       Filename:  analyze.cpp
//
//    Description:  Analyze simulation configurations.
//
//        Version:  1.0
//        Created:  01/18/2013 02:40:00 PM
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  C. Simon Adorf (csa), carl.simon.adorf@rwth-aachen.de
//   Organization:  RWTH Aachen University
//
// =====================================================================================

#include <parser_writer/hoomd_blue_xml.hpp>
#include "simulation_config.hpp"
#include "config_analysis.hpp"
#include "forcefield_parameters.hpp"

#include <iostream>
#include <boost/program_options.hpp>
#include <boost/version.hpp>

using namespace mdst;
using namespace SimulationConfiguration;
namespace po = boost::program_options;

typedef std::vector< shared_ptr<SimulationConfig> > SimulationConfigs;
typedef SimulationConfigs::const_iterator SimulationConfigsConstIterator;
typedef std::vector<std::string> Filenames;
typedef Filenames::const_iterator FilenamesConstIterator;

void
analysis_rdf(const SimulationConfigs& configs){
  for(unsigned int i = 0; i < configs.size(); ++i){
    std::cerr << "Calculating radial distribution function for ";
    std::cerr << "simulation configuration " << i+1 << "/";
    std::cerr << configs.size() << " ..." << std::endl;
    ConfigAnalyzer analyzer(configs.at(i));
    dump_map<AttributeMap>(analyzer.rdf(), std::cout);
  }
}

void
analysis_eted(const SimulationConfigs& configs){
  std::cerr << "'eted': Not supported." << std::endl;
  return;
  for(unsigned int i = 0; i < configs.size(); ++i){
    std::cerr << "Calculating end-to-end-distance for ";
    std::cerr << "simulation configuration " << i+1 << "/";
    std::cerr << configs.size() << " ..." << std::endl;
    ConfigAnalyzer analyzer(configs.at(i));
    // todo (csa) Function is not implemented yet.
  }
}

void
analysis_msd(const SimulationConfigs& configs){
  if (configs.size() < 2){
    std::stringstream error;
    error << "At least 2 configurations required, in order to ";
    error << "calculate mean squared distance." << std::endl;
    throw std::invalid_argument(error.str());
  }
  std::cerr << "Calculating mean squared distance of ";
  std::cerr << "simulation configuration 0 to " << std::endl;
  ConfigAnalyzer analyzer(configs.at(0));
  for(unsigned int i = 1; i < configs.size(); ++i){
    std::cerr << i << "/" << configs.size()-1 << "..." << std::endl;
    std::cout << analyzer.mean_squared_distance(*configs.at(i));
    std::cout << std::endl;
  }
}

void
analysis_potential_energy(const SimulationConfigs& configs,
                          const po::variables_map & variables_map){
  if (!variables_map.count("parameter") || !variables_map.count("reduce-by")){
    std::stringstream error;
    error << "Insufficient parameters for calculation of potential energy.";
    std::cerr << error;
  }
  ParameterOptions po;
  ForceFieldCalculation forcefield_calculation(
    parameters::reduced_units::by_name(
      variables_map["reduce-by"].as<std::string>()),
    parameters::forcefields::by_name(
      variables_map["parameter"].as<std::string>(),
      parameters::forcefields::parse_options(
        variables_map["parameter-options"].as<std::string>()))
  );

  const bool no_lj = variables_map["no-lennard-jones"].as<bool>();

  for (unsigned int i = 0; i < configs.size(); ++i){
    std::cerr << "Calculating potential energy ";
    if (no_lj){
      std::cerr << "(without lennard-jones potential) ";
    }
    std::cerr << "of simulation configuration ";
    std::cerr << i+1 << "/" << configs.size() << "..." << std::endl;
    ConfigAnalyzer analyzer(configs.at(i));
    std::cout << analyzer.potential_energy(forcefield_calculation, no_lj) << std::endl;
  }
}

po::variables_map
parse_command_line(int argc,
                   char* argv[],
                   po::options_description & general_description){
  general_description.add_options()
  // basic options
    ("help,h", "produce help message")
    ("output-file", "set output file")
//    ("include-path,I", po::value< std::vector<std::string> >(), "include path")
  // specific options
    ("parameter,p", po::value<std::string>(),
     "The parameter set, which is supposed to be used for calculations.")
    ("parameter-options,o", po::value<std::string>()->default_value(std::string()))
    ("reduce-by,r", po::value<std::string>()->default_value("carbon"),
     "the base dimension, which is to be used for calculations.")
  // special options
    ("no-lennard-jones",
     po::value<bool>()->default_value(false)->implicit_value(true)->zero_tokens(),
     "Do not calculate lennard jones energy for potential energy.")
#if BOOST_VERSION>=104200
    ("input-files", po::value<Filenames>()->required(), "input files")
    ("analyze,a", po::value<std::string>()->required(), "Specify, what to analyze.")
#else
    ("input-files", po::value<Filenames>(), "input files")
    ("analyze,a", po::value<std::string>(), "Specify, what to analyze.")
#endif
  ;

  po::positional_options_description positional_options;
  positional_options.add("input-files", -1);

  po::variables_map variables_map;
  po::store(po::command_line_parser(argc, argv).
            options(general_description).positional(positional_options).run(), variables_map);
  po::notify(variables_map);
  return variables_map;
}

SimulationConfigs
parse_config_files(const Filenames & filenames){
  SimulationConfigs result(filenames.size());
  for(unsigned int i = 0; i < filenames.size(); ++i){
    result[i] = shared_ptr<SimulationConfig>(
      ParserWriter::HoomdBlueXml().parse(filenames.at(i)));
  }
  return result;
}

void
run_analysis(const po::variables_map & variables_map,
             const SimulationConfigs& configs){
    if (variables_map.count("analyze")){
      const std::string function(
        variables_map["analyze"].as<std::string>() );

      if (function == "rdf" || function == "radial-distribution-function")
        analysis_rdf(configs);
      else
        if (function == "eted" || function == "end-to-end-distance")
          analysis_eted(configs);
      else
        if (function == "msd" || function == "mean-squared-distance")
          analysis_msd(configs);
      else
        if (function == "pe" || function == "potential-energy")
          analysis_potential_energy(configs, variables_map);
      else
        std::cerr << "Unknown option: '" << function << "'" << std::endl;
    }
}

int
main ( int argc, char *argv[] ){
  po::options_description general_description("Allowed options");
  po::variables_map variables_map(
      parse_command_line(argc, argv, general_description));

  try{
    if ( variables_map.count("help")
#if BOOST_VERSION<104200
      ||
        !variables_map.count("input-files") ||
        !variables_map.count("analyze")
#endif
       ){
      std::cout << general_description << std::endl;
      return 1;
    }


    SimulationConfigs configs;
    if (variables_map.count("input-files")){
      configs = 
        parse_config_files(variables_map["input-files"].as<Filenames>() );
    }

    run_analysis(variables_map, configs);
    
  }
  catch (ParserWriter::ParseException & e){
    std::cerr << "Error while parsing: " << e.error() << std::endl;
  }
  return 0;
}				// ----------  end of function main  ---------- 
