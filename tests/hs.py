#!/usr/bin/env python

# generate random numbers for initialization
import random
#random.seed(42) # Comment this line for true randomness!

# Define parameters
from math import pi
N = 100 
packing_fraction = 0.5
sphere_radius = 1.0
sphere_volume = 4.0 / 3 * pi * pow(sphere_radius, 3)
cubic_box_length = pow(N * sphere_volume / packing_fraction, 1.0 / 3)

# import the SPMC package
import spmc

# create a simulation session
session = spmc.Session()

# Set box size to 100x100x100
session.config.box = spmc.Tuple3(cubic_box_length)

# create 100 random particles of name A
import spmc.init
spmc.init.create_random(config = session.config, N = N, p_type = 'A')

# Set up Lennard-Jones potential and add to session
import spmc.potentials
potentials = spmc.potentials.Potentials(session.reduced_units)
hs = potentials.hard_sphere_potential('A', 'A', r_cut = sphere_radius)
session.add_potential(hs)

# Add MC moves
session.add_move(spmc.RandomDisplacementMove(1))

# run 10,000 MC cycles in NVT ensemble
session.simulator = spmc.MonteCarloNVTSampler(1.0) # beta = 1.0
session.random_seed(int(random.random() * 100))

# Unit test
analyzers = ('pe', 'osmotic_pressure', 'acceptance_ratio')
from spmc.unit_test import UnitTest
ut = UnitTest('hs', session, analyzers, 2000)
#ut.calibrate()
ut.run_test()
