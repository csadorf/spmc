#!/usr/bin/env python

# generate random numbers for initialization
import random
#random.seed(42) # Comment this line for true randomness!

# Define parameters
from math import pi
N = 100 
packing_fraction = 0.5
sphere_radius = 1.0
sphere_volume = 4.0 / 3 * pi * pow(sphere_radius, 3)
cubic_box_length = pow(N * sphere_volume / packing_fraction, 1.0 / 3)

# import packages
import spmc
import wavelet.session

# create wavelet session
wavelet_session = wavelet.session.WaveletSession(
    beta = 1.0,
    box = spmc.Tuple3(100),
    id_ = 'wavelet_test',
    )

# Add random molecules to box
wavelet_session.add_molecules(pow(2, 5), 3)
wavelet_session.dumping = False

session = wavelet_session.session()
session.random_seed(int(random.random() * 100))

# Unit test -- atomistic
from spmc.unit_test import UnitTest
analyzers = ('pe', 'radius_of_gyration', 'acceptance_ratio')
ut = UnitTest('wv_at', session, analyzers, 4000)
#ut.calibrate()
ut.run_test()
