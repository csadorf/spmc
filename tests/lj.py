#!/usr/bin/env python

# generate random numbers for initialization
import random
#random.seed(42) # Comment this line for true randomness!

# import the SPMC package
import spmc

# create a simulation session
session = spmc.Session()

# Set box size to 100x100x100
session.config.box = spmc.Tuple3(10)

# create 864 random particles of name A
import spmc.init
spmc.init.create_random(config = session.config, N = 100, p_type = 'A')

# Set up Lennard-Jones potential and add to session
import spmc.potentials
potentials = spmc.potentials.Potentials(session.reduced_units)
lj = potentials.lennard_jones_potential(
    'A', 'A',
    epsilon = 1.0,
    sigma = 1.0,
    r_cut = 3)
session.add_potential(lj)

# Add MC moves
session.add_move(spmc.RandomDisplacementMove(1))

# Set up simulator
session.simulator = spmc.MonteCarloNVTSampler(1.0)
session.random_seed(int(random.random() * 100))

# Unit test
analyzers = ('pe', 'osmotic_pressure', 'acceptance_ratio')
from spmc.unit_test import UnitTest
ut = UnitTest('lj', session, analyzers, 2000)
#ut.calibrate()
ut.run_test()
